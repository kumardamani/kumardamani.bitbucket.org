
var hasBirthday = true;
var requiredArr = [
    'txtFirstName',
    'txtLastName',
    'txtEmail',
    'txtCompanyName',
    'idType',
    'txtIATA',
    'txtCompanyAddress',
    'countryCompany',
    'txtCity'
];
var player;

function onYouTubeIframeAPIReady() {
  player = new YT.Player('youtube-video', {
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

function onPlayerReady(event) {
    event.target.playVideo();
}

var done = false;
function onPlayerStateChange(event) {
    if (event.data == 0) {
        var html = '<div id="travel-signup-video-done">'
                    html += '<h3>Thank you for watching</h3>';
                    html += '<span class="let-me-in" id="youtube-done">Now let me in</span>';
                    html += '</div>';
        $('.youtube-video-container').append(html);
    }
}
function stopVideo() {
    player.stopVideo();
}

$('form#frmSettings')
    .submit(function() {
        isValid = checkRequiredFields(false);
        
        return isValid;
    });


function fancifySelectBoxes() {

    var dropdownArr = [
        'accountGender',
        'currency',
        'countryCompany',
        'idType'
    ];
    var i ;
    for (i = 0; i < dropdownArr.length; i++) {
    
        $('select#' + dropdownArr[i]).tzSelect({
            selectboxClass: dropdownArr[i] + '_selectbox',
            dropdownClass: dropdownArr[i] + '_dropdown',
            onChange: function() {
                
                var text = $(this).text();
                var id = $(this).parent().attr('class').replace(/_dropdown*/i, '').replace(/dropdown*/i, '').replace(/\s+/i, '');				
                $('select#' + id + ' option').each(function() {
                
                    if ($(this).text() == text) {
                        
                        $('input#hdn' + id).val($(this).val());
                        $('#hdn' + id).trigger('change');
                        return false;
                    }
                });
                
            }
        });	
        
        $('select#' + dropdownArr[i] + ' option').each(function() {
            
            var text = $('div.' + dropdownArr[i] + '_selectbox > span.text').text();
            if ($(this).text() == text) {
                
                $('input#hdn' + dropdownArr[i]).val($(this).val());
                return false;
            }					
        });
    }		
}

$('html')
    .on('click', function(e) {
        checkForCompleteness();
        //checkRequiredFields(true);
    });
    
$(".imageWrapper, #videoWrap, #travel-start-video-mobile").on("click", function (e) {
    e.preventDefault();
    if($(".youtube-wrapper").css("display") == "none"||$(".youtube-wrapper").css("display") == "hidden"){
        $(".black_overlay").show();
        $(".youtube-wrapper").show();
    }
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    
});
$(".youtube-wrapper a.close").on("click", function (e) {
    player.stopVideo();
    $(".youtube-wrapper").hide();
    $(".black_overlay").hide();
});


$('.youtube-wrapper').on('click', '.let-me-in', function() {
    $('.youtube-wrapper').hide();
    $(".black_overlay").hide();
    $('#travel-sign-up-form').show();
});   
function checkForCompleteness() {	

    $('span#chkName').css(
        'display', 
        $('#txtFirstName').val().replace(/\s+/i, '').length > 0 && $('#' + requiredArr[1]).val().replace(/\s+/i, '').length > 0 ? 'inline-block' : 'none'
    );
    
    $('span#chkIata').css(
        'display', 
        $('#idType').val().length > 0 && $('#txtIATA').val().replace(/\s+/i, '').length > 0 ? 'inline-block' : 'none'
    );
    
    $('span#chkCountryCompany').css(
        'display', 
        $('#countryCompany').val().length > 0 ? 'inline-block' : 'none'
    );
    
    var fields = ['Email', 'CompanyName', 'CompanyAddress', 'City'];
    
    for (var i=0; i<fields.length; i++) {
        var field = fields[i];
        
        $('span#chk' + field).css(
            'display',
            $('#txt' + field).val().replace(/\s+/i, '').length > 0 ? 'inline-block' : 'none'
        );
    }
}

function checkRequiredFields(isOnFocus) {
    
    var success = true;	
    var scrolled = false;
    for (var i = 0; i < requiredArr.length; i++) {
    
        var currReqObj = $('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden'
            ? $('div.' + requiredArr[i].replace('hdn', '') + '_selectbox')
            : $('#' + requiredArr[i]);
        var immediateParentCntr = $(currReqObj).parent();
        var mainElementsCntr = $(currReqObj).parents('div.elements-cntr').first();
        var extraValidation = true;
        //For email validation
        if(requiredArr[i] === "txtEmail") {
            var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            extraValidation = emailPattern.test($('#' + requiredArr[i]).val());
        }
        
        if ($('#' + requiredArr[i]).val() == null || $('#' + requiredArr[i]).val().replace(/\s+/i, '').length == 0 || !extraValidation) {
            
            $(currReqObj).css('border', '6px solid #BF883B').css('height', '34px');
            $(mainElementsCntr).css('padding-bottom', 0);
            if ($(immediateParentCntr).find('p.error').length == 0) {
            
                $(immediateParentCntr).append('<p class="error">Please complete this field</p>');
            }
            
            if ($('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden') {
                
                $(currReqObj).css('line-height', '34px');
                $(currReqObj).find('span.icon').first().css('top', '14px');
            }
            
            if (scrolled == false && isOnFocus == false) {
                
                scrolled = true;
                scrollToElement($(currReqObj).parents('.elements-cntr').first().find('.element-label').first());
            }
            
            success = success ? false : success;
            
        } else {
            $(currReqObj).css('border', '0').css('height', '46px');
            $(immediateParentCntr).find('p.error').first().remove();
            $(mainElementsCntr).css('padding-bottom', '10px');
            if ($('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden') {
                
                $(currReqObj).css('line-height', '46px');
                $(currReqObj).find('span.icon').first().css('top', '22px');
            }
        }
        
    }

    $('.key-markets-group .error').remove();
    if (!keyMarketsChecked()) {
        success = false;
        $('.key-markets-group').append('<p class="error">Please select at least one Key Market</p>');
    }
    
    $('.target-group-group .error').remove();
    if (!targetGroupsChecked()) {
        success = false;
        $('.target-group-group').append('<p class="error">Please select at least one Target Group</p>');
    }
    
    $('#agree-terms-cb-holder .error').remove();
    if (!$('#agree-terms-cb').is(':checked')) {
        success = false;
        $('#agree-terms-cb-holder').append('<p class="error">Please agree to the terms and conditions</p>');
    }
    
    return success;
}

function isEmpty(obj) {
    var blEmpty = true;
    
    if (typeof obj !== 'undefined' && obj != null) {
        if (typeof obj === 'string') {
            blEmpty = obj.replace(/\s+/ig, '').length == 0;
        } else if (Object.prototype.toString.call(obj) === '[object Array]') {
            blEmpty = obj.length == 0;
        } else {
            blEmpty = false;
        }
    }
    
    return blEmpty;
}

function scrollToElement(element) {
    try {
        $('html, body').animate({
            scrollTop: $(element).offset().top - 100
        }, 'fast');
    } catch (e) { }
}

function keyMarketsChecked() {
    var inputs = $('.key-markets-group input[type="checkbox"]');
    
    for (var i=0; i<inputs.length; i++) {
        if ($(inputs[i]).is(':checked')) {
            return true;
        }
    }
    
    return false;
}

function targetGroupsChecked() {
    var inputs = $('.target-group-group input[type="checkbox"]');
    
    for (var i=0; i<inputs.length; i++) {
        if ($(inputs[i]).is(':checked')) {
            return true;
        }
    }
    
    return false;
}
