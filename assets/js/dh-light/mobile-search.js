$(function() {
    var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false);
    
    var elements = {
        'searchBox': $('#mobile-search-bar .search-box'),
        'searchButton': $('#mobile-search-bar .search-button'),
        'searchContainer': $('#mobile-search-bar > .container'),
        'autoSuggest': $('#mobile-search-bar .search .auto-suggest'),
        'body': $(document.body),
        'mobileMainHeader': $('#mobile-main-header')
    };
    
    var windowResized = function() {
        var w = $(window);
        var windowWidth = w.width();
        var windowHeight = w.height();
        var mobileMainHeaderWidth = elements.mobileMainHeader.width();
        
        
        var searchBoxWidth = mobileMainHeaderWidth - 127;
        var autoSuggestWidth = mobileMainHeaderWidth - 71;
        
        elements.searchBox.width(searchBoxWidth);
        elements.autoSuggest.width(autoSuggestWidth);
    };
    
    $(window).resize(windowResized);
    windowResized();
});

$(function() {
    var ajaxState = false;
    var input = $('#mobile-search-bar .search-box');
    var keyTimer = false;
    var autoSuggest = $('#mobile-search-bar .auto-suggest');
    var searchResults = $('#mobile-search-bar .auto-suggest li:first-child');
    var hotelCount = $('#mobile-search-bar .hotels-and-resorts .count');
    var hotelDiv = $('#mobile-search-bar .hotels-and-resorts');
    var searchButton = $('#mobile-search-bar .search .button');
    var searchBox = $('#mobile-search-bar .search-box');
    var selectAdults = $('#mobile-search-bar .select-adults');
    var selectChildren = $('#mobile-search-bar .select-children');
    var inputCheckin = $('#mobile-search-bar .input-checkin');
    var inputCheckout = $('#mobile-search-bar .input-checkout');

    input.keyup(function() {
        if (keyTimer) {
            clearTimeout(keyTimer);
            keyTimer = false;
        }

        keyTimer = setTimeout(function() {
            doAjax();
        }, 200);
    });

    var doAjax = function() {
        if (ajaxState) {
            ajaxState.abort();
            ajaxState = false;
        }

        if (typeof(DH_CURRENT_VERSION) == 'undefined') {
            DH_CURRENT_VERSION = 'master';
        }
        
        ajaxState = $.ajax({
            type: "GET",
            url: '/auto-complete/',
            cache: true,
            data: {"q": input.val(), "max": 10, "lang": "en", "version": DH_CURRENT_VERSION},
            success: function(response) {
                ajaxState = false;
                autoSuggest.show();

                searchResults.empty();
                searchResults.hide();

                if (response.suggestions.length) {
                    searchResults.show();
                }

                for (var i = 0; i < response.suggestions.length; i++) {
                    if (i == 5) {
                        break;
                    }
                    
                    var suggestion = response.suggestions_type[i];

                    var div = $(
                        '<div class="suggestion suggestion-' + suggestion.type 
                        + '"></div>'
                    );
                    
                    var icon = $(
                        '<span class="icon '
                        + (
                            suggestion.type == 'property'
                            ? SearchBarAutoCompleteIconProperty
                            : SearchBarAutoCompleteIconLocation
                        )
                        + '"></span>'
                    );
                    
                    var label = $('<span class="label"></span>');
                    label.text(suggestion.suggestion);
                    
                    div.append(icon);
                    div.append(label);

                    searchResults.append(div);

                    div.click(function() {
                        var suggestion = $(this).text();

                        input.val(suggestion);
                        autoSuggest.hide();
                    });
                }

                hotelCount.text('(' + response.properties.length + ')');
            }
        });
    }

    $(document).click(function() {
        setTimeout(function() {
            autoSuggest.hide();
        }, 200);
    });

    var doSearch = function() {
        var hasDates = false;
        var numAdults = selectAdults.val();
        var numChildren = selectChildren.val();
        var child_age_url = '';
        var start = inputCheckin.val();
        var end = inputCheckout.val();
        var keyword = searchBox.val();
        var thisIsHotelsNearMePage = false;
        var lat = false;
        var lng = false;

        if (
            typeof(currentLocationLat) != 'undefined'
            && typeof(currentLocationLng) != 'undefined'
        ) {
            thisIsHotelsNearMePage = true;
            lat = currentLocationLat;
            lng = currentLocationLng;
        }

        if (keyword == '' && !thisIsHotelsNearMePage) {
            searchBox.addClass('attention');
            return;
        }

        if (!numAdults) {
            numAdults = '2';
        }

        if (!numChildren) {
            numChildren = '0';
        } else {    // there are children
            var children_obj = {};
            for (var count = 0; count < numChildren; count++) {
                // get the age of this child
                // where the ages are store in the DOM depends on the screen width
                if ($(window).width() < 769) {    // mobile
                    // search in the lightbox for avail widget
                    var age_value = $('#lightboxChildAgeMenuSearch span#ageForChildMobile' + count);
                } else {  // desktop
                    // search in the avail widget
                    var age_value = $('#search-bar li#ageForChild' + count)
                            .find('span.spinnerValue');
                }
                var age = parseInt(age_value.text());
                children_obj[count] = age;
            }
            child_age_url = '&ages=' + encodeURIComponent(JSON.stringify(children_obj));
        
        }

        if (keyword == '') {
            var location = '/search?lat=' + lat + '&lng=' + lng
                + '&adults=' + numAdults + '&children=' + numChildren + child_age_url;
        } else {
            var location = '/search?q=' + encodeURIComponent(keyword)
                + '&adults=' + numAdults + '&children=' + numChildren + child_age_url;
        }

        if (start && end) {
            location += '&start=' + start + '&end=' + end;
        }

        window.location = location;
    }

    searchButton.click(function() {
        doSearch();
    });

    hotelDiv.click(function() {
        doSearch();
    });
});

$(function() {
    var searchOpen = false;
    var extendedSearchOpen = false;
    var calendarOpen = false;
    var isAnimating = false;
    var searchAfterLastDate = false;
    
    var searchButton = $('#mobile-search-bar .search-button');
    var searchDiv = $('#mobile-search-bar .search');
    var searchBox = $('#mobile-search-bar .search-box');
    var searchMore = $('#mobile-search-bar .search-more');
    var selectAdults = $('#mobile-search-bar .select-adults');
    var labelAdults = $('#mobile-search-bar .label-adults');
    var selectChildren = $('#mobile-search-bar .select-children');
    var labelChildren = $('#mobile-search-bar .label-children');
    var calendarHolder = $('#mobile-search-bar .calendar-holder');
    var calendarHolderInner = $('#mobile-search-bar .calendar-holder .inner');
    var labelCheckin = $('#mobile-search-bar .label-checkin');
    var labelCheckout = $('#mobile-search-bar .label-checkout');
    var inputCheckin = $('#mobile-search-bar .input-checkin');
    var inputCheckout = $('#mobile-search-bar .input-checkout');
    var clearAll = $('#mobile-search-bar .calendar-holder .clear-all');
    var search = $('#mobile-header');
    var wrapper = $('#container');
    var geoButton = $('#mobile-search-bar .geo-location-button');
    var updateChildAge = function(select, child_age) {
        var ageLabel = $(select).parent().siblings('span.age');
        var arrow_icon = $(select).parent().siblings('span.icon');
        ageLabel.text(child_age);
        // show the age
        ageLabel.css('display', 'block');
        // need to hide the arrow
        arrow_icon.css('display', 'none');
    };

    var buildChildAgeMenuForAvailSearch = function(children) {
        // first empty out the lighbox
        $('#lightboxChildAgeMenuSearch div.childWrapper').remove();
        // add the children number of child age selectors
        for (var count = children-1; count >= 0; count--) {
            
            var age_options = '';
            for (var i = 1; i < 18; i++) {
                age_options += '<option>' + i + '</option>';
            }
            var child_html = '<div class="childWrapper">' + 
                              '<div class="control control-children">' + 
                              '<div class="label"><span>Child\'s age</span><select>' + 
                                      '<option selected>0</option>' + age_options + 
                              '</select></div>' + 
                              '<span class="icon icon-arrow-down-black"></span>' + 
                              '<span class="age" id="ageForChildMobile' + count + '"></span>' + 
                              '</div>' + 
                            '</div>';
            // add this to the lightbox
            $('#lightboxChildAgeMenuSearch div.content').after(child_html);
        }
        // update the children ages from selects
        $('#lightboxChildAgeMenuSearch select').change(function(e) {
            child_age = $(this).val();
            el = e.target;
            updateChildAge(el, child_age);
        });

        $('#lightboxChildAgeMenuSearch div.lightboxAgeMenuClose').click(function(e) {
            $("#lightboxChildAgeMenuSearch").dialog('close');
        });
    };
    
    if (typeof(pleaseOpenTheSearchBar) == 'undefined') {
        pleaseOpenTheSearchBar = false;
    }
    
    searchButton.click(function() {
        if (isAnimating) {
            return;
        }

        isAnimating = true;

        if (searchOpen) {
            searchDiv.slideUp({
                complete: function() {
                    isAnimating = false;
                    searchOpen = false;

                    searchMore.hide();
                    extendedSearchOpen = false;
                    searchAfterLastDate = false;
                    searchButton.css('opacity', 1);

                    wrapper.css('padding-top', '');
                }
            });
        } else {
            searchDiv.slideDown({
                complete: function() {
                    isAnimating = false;
                    searchOpen = true;
                    searchButton.css('opacity', 0.5);

                    if (
                        pleaseOpenTheSearchBar
                        && ScreenSizeIsMobile
                    ) {
                        // Because search is open by default on homepage
                        // we need to move the content down a bit so it
                        // is not partly obscured.
                        wrapper.css('padding-top', '54px');
                    }
                }
            });
        }
    });

    $(window).on('to-desktop', function() {
        wrapper.css('padding-top', '');
    });

    $(window).on('to-mobile', function() {
        if (
            pleaseOpenTheSearchBar
            && ScreenSizeIsMobile
            && searchOpen
        ) {
            wrapper.css('padding-top', '54px');
        }
    });

    searchBox.click(function() {
        searchBox.removeClass('attention');

        if (isAnimating || extendedSearchOpen) {
            return;
        }

        searchMore.slideDown({
            complete: function() {
                search.css('position','absolute');
                window.scrollTo(0,0);
                isAnimating = false;
                extendedSearchOpen = true;
            }
        });
    });

    $('#hotelsNav .search-details span').click(function() {
        if (extendedSearchOpen && searchOpen) {
            return;
        }

        if (!searchOpen) {
            searchMore.show();

            searchDiv.slideDown({
                complete: function() {
                    isAnimating = false;
                    searchOpen = true;
                    searchButton.css('opacity', 0.5);
                    extendedSearchOpen = true;
                }
            });
        } else {
            searchMore.slideDown({
                complete: function() {
                    isAnimating = false;
                    extendedSearchOpen = true;
                }
            });
        }
    });

    selectAdults.change(function() {
        var number = $(this).val();

        var text = (number == 1 ? ' Adult' : ' Adults');
        labelAdults.text(number + text);
    });

    selectChildren.change(function() {
        var number = $(this).val();

        var text = (number == 1 ? ' Child' : ' Children');
        labelChildren.text(number + text);
        // now open the child age menu....
        $("#lightboxChildAgeMenuSearch").dialog('open');
        buildChildAgeMenuForAvailSearch(number);
    });

    if (inputCheckin.length && inputCheckin.length){
        var selectedStart = inputCheckin.val().split('-');
        var selectedEnd = inputCheckout.val().split('-');
        var selectedDate = new Date();

        if (selectedStart.length == 3 && selectedEnd.length == 3) {
            selectedStart = new Date(selectedStart[0], selectedStart[1] - 1, selectedStart[2]);
            selectedEnd = new Date(selectedEnd[0], selectedEnd[1] - 1, selectedEnd[2]);

            selectedDate = [selectedStart, selectedEnd];
        }

        calendarHolderInner.DatePicker({
            flat: true,
            prev: '',
            next: '',
            date: selectedDate,
            format: 'b d, Y',
            starts: 1,
            calendars: 1,
            view: 'days',
            mode: 'range',
            onChange: function(formated, dates) {
                labelCheckin.text(formated[0]);
                inputCheckin.val(
                    dates[0].getFullYear()
                    + '-' + twoDigits(dates[0].getMonth() + 1)
                    + '-' + twoDigits(dates[0].getDate())
                );

                labelCheckout.text('Check Out');
                inputCheckout.val('');

                if (formated[0] != formated[1]) {
                    labelCheckout.text(formated[1]);
                    inputCheckout.val(
                        dates[1].getFullYear()
                        + '-' + twoDigits(dates[1].getMonth() + 1)
                        + '-' + twoDigits(dates[1].getDate())
                    );

                    toggleCalendar();

                    if (searchAfterLastDate) {
                        $('#mobile-search-bar .search .button').trigger('click');
                    }
                }
            }
        });
    }
    
    clearAll.click(function() {
        labelCheckin.text('Check In');
        labelCheckout.text('Check Out');
        inputCheckin.val('');
        inputCheckout.val('');
        calendarHolderInner.DatePickerClear();

        toggleCalendar();
    });

    calendarHolderInner.DatePickerShow();

    var toggleCalendar = function() {
        if (isAnimating) {
            return;
        }

        isAnimating = true;

        if (calendarOpen) {
            calendarHolder.slideUp({
                complete: function() {
                    isAnimating = false;
                    calendarOpen = false;
                }
            });
        } else {
            calendarHolder.slideDown({
                complete: function() {
                    isAnimating = false;
                    calendarOpen = true;
                }
            });
        }
    }
    
    $(document).mousedown(function(e) {
        if (!extendedSearchOpen) {
            return;
        }

        var target = $(e.target);

        if (
            target.hasClass('search-more')
            || target.hasClass('search-box')
            || target.hasClass('search-button')
            || target.closest('.search-more').length
            || target.closest('.search-box').length
            || target.closest('.search-button').length
            || target.closest('.auto-suggest').length
        ) {
            return;
        }

        extendedSearchOpen = false;
        isAnimating = true;

        searchMore.slideUp({
            complete: function() {
                search.css('position','fixed');
                isAnimating = false;
            }
        });
    });

    labelCheckin.click(toggleCalendar);
    labelCheckout.click(toggleCalendar);

    $('#mobileFooterCheckAvailability').click(function() {
        if (extendedSearchOpen && searchOpen && calendarOpen) {
            return;
        }

        isAnimating = true;
        searchAfterLastDate = true;

        if (!searchOpen) {
            searchMore.show();
            calendarHolder.show();

            searchDiv.slideDown({
                complete: function() {
                    isAnimating = false;
                    searchOpen = true;
                    searchButton.css('opacity', 0.5);
                    extendedSearchOpen = true;
                    calendarOpen = true;
                }
            });
        } else if (!extendedSearchOpen) {
            calendarHolder.show();

            searchMore.slideDown({
                complete: function() {
                    isAnimating = false;
                    extendedSearchOpen = true;
                    calendarOpen = true;
                }
            });
        } else {
            calendarHolder.slideDown({
                complete: function() {
                    isAnimating = false;
                    calendarOpen = true;
                }
            });
        }
    });

    $('#mobileFooterBookNow').click(function() {
        $('#mobile-search-bar .search .button').trigger('click');
    });

    if (pleaseOpenTheSearchBar) {
        searchButton.trigger('click');
    }

    $('.mobile-footer-location, #mobile-search-bar .geo-location-button').click(function() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(pos) {
                    var lat = pos.coords.latitude;
                    var lng = pos.coords.longitude;

                    window.location.href = '/search?q=geoloc&lat=' + lat + '&lng=' + lng;
                },
                function() {
                    alert('Unable to determine the current location. Please make sure your location service is turned on.');
                }
                , {
                    //enableHighAccuracy: false,
                    timeout: 10000,
                    maximumAge: 10000
                }
            );
        }
    });
});
