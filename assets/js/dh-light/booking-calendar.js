var AvailabilityService = new function() {
    var callbacks = {};
    var unavailableDays = {};
    var unavailableDaysLoading = {};
    
    var dateToKey = function(date) {
        var newDate = new Date(date.getTime());
        newDate.setDate(1);
        newDate.setHours(0, 0, 0, 0);
        return newDate.getTime();
    }
    
    var loadUnavailability = function(date) {
        var key = dateToKey(date);
        unavailableDaysLoading[key] = true;
        
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        
        var startDate = year + '/' + ('0' + month).slice(-2) + '/01';
        var endDate = year + '/' + ('0' + month).slice(-2) + '/' + daysInMonth(month, year);
        
        if(typeof propid !='undefined'){
            $.ajax({
                type: "POST",
                url: "/get-availability",
                data: {propid: propid, start: startDate, end: endDate}
            }).done(function(data) {
                var daysUnavailable = {};
                var dataJson = jQuery.parseJSON(data);
                
                if (typeof(dataJson.length != 'undefined')) {
                    for (var i=0; i<dataJson.length; i++) {
                        var start = parseInt(dataJson[i].start.split('/')[1], 10);
                        var end = parseInt(dataJson[i].end.split('/')[1], 10);
                        
                        for (var j=start; j<=end; j++) {
                            daysUnavailable[j] = true;
                        }
                    }
                }
                
                unavailableDays[key] = {
                    processed: daysUnavailable, 
                    raw: data
                };
                
                if (typeof(callbacks[key]) != 'undefined') {
                    for (var i=0; i<callbacks[key].length; i++) {
                        var callback = callbacks[key][i];
                        
                        if (callback.process) {
                            callback.callback(unavailableDays[key].processed);
                        } else {
                            callback.callback(unavailableDays[key].raw);
                        }
                    }
                    
                    callbacks[key] = [];
                }
            });
        }
    }
    
    this.get = function(date, callback, process) {
        if (typeof(process) == 'undefined') {
            process = false;
        }
        
        var key = dateToKey(date);
        
        if (typeof(unavailableDays[key]) != 'undefined') {
            if (process) {
                callback(unavailableDays[key]['processed']);
                return;
            }
            
            callback(unavailableDays[key]['raw']);
            return;
        }
        
        if (typeof(callbacks[key]) == 'undefined') {
            callbacks[key] = [];
        }
        
        callbacks[key].push({
            callback: callback,
            process: process
        });
        
        if (typeof(unavailableDaysLoading[key]) == 'undefined') {
            loadUnavailability(date);
        }
    }
}();

var DhBookingCalendar = function(
    version,
    parent,
    triggerCheckIn,
    triggerCheckOut,
    checkInLabel,
    checkInInput,
    checkOutLabel,
    checkOutInput
) {
    this.version = version;
    this.parent = parent;
    this.triggerCheckIn = triggerCheckIn;
    this.triggerCheckOut = triggerCheckOut;
    this.isOpen = false;
    this.checkStatus = 'in';
    this.checkInLabel = checkInLabel;
    this.checkInInput = checkInInput;
    this.checkOutLabel = checkOutLabel;
    this.checkOutInput = checkOutInput;
    
    this.checkInDate = false;
    this.checkOutDate = false;
    this.firstDateMonth = new Date();
    this.dataUnavailable = {};
    this.dataUnavailableLoading = {};
    
    this.debug = true;
};

DhBookingCalendar.prototype.init = function() {
    var self = this;
    var date = new Date();
    
    if (this.checkInInput.val() && this.checkOutInput.val()) {
        this.checkInDate = this.valueToDate(this.checkInInput.val());
        this.checkOutDate = this.valueToDate(this.checkOutInput.val());
    }
    
    var hidePickerCode = function() {
        self.isOpen = false;
        self.parent.find('.calendar-tail').hide();
        self.parent.find('.dividerCal').hide();
        self.parent.find('div.datePickerLegend').hide();
        self.parent.find('div.datePickerClose').hide();
        self.parent.find('div.datePickerArrow').hide();
        self.parent.find('div.datePickerClearAll').hide();
        self.parent.find('a.datePickerPrevMonth').hide();
        self.parent.find('a.datePickerNextMonth').hide();
        
        //self.checkInLabel.css('font-family', 'DINWebPro');
        //self.checkOutLabel.css('font-family', 'DINWebPro');
    }
    
    var showPickerCode = function() {
        self.isOpen = true;
        self.parent.find('.calendar-tail').show();
        self.parent.find('.dividerCal').show();
        self.parent.find('div.datePickerLegend').show();
        self.parent.find('div.datePickerClose').show();
        self.parent.find('div.datePickerArrow').show();
        self.parent.find('div.datePickerClearAll').show();
        self.parent.find('a.datePickerPrevMonth').show();
        self.parent.find('a.datePickerNextMonth').show();
    }
    
    var widgetOptions = {
        flat: true,
        prev: '',
        next: '',
        date: date,
        format: 'b d, Y',
        starts: 1,
        calendars: 2,
        mode: 'single',
        view: 'days',
        autoHide: true,
        
        // This happens before onFill
        onMonthChange: function(firstDate) {
            self.firstDateMonth = new Date(firstDate.getTime());
        },
        
        // This happens when the calendar is done updating HTML after month
        // changes (not after initially showing)
        onFill: function() {
            if (self.isOpen) {
                self.showAvailability();
            }
        },
        
        onShow: function() {
            showPickerCode();
            self.showAvailability();
        },
        
        onHide: function() {
            hidePickerCode();
        },
        
        // A date has been picked
        onChange: function(formated, date) {
            self.parent.DatePickerHide();
            hidePickerCode();
            self.datePicked(formated, date);
        }
    };
    
    this.parent.DatePicker(widgetOptions);
    
    this.triggerCheckIn.mousedown(function(ev) {
        ev.stopPropagation();
        ev.preventDefault();
        if (!ScreenSizeIsMobile/* && !self.isOpen*/) {
            self.openCheckIn();
            
            if (self.isOpen) {
                self.showAvailability();
            }
        }
    });
    
    this.triggerCheckOut.mousedown(function(ev) {
        ev.stopPropagation();
        ev.preventDefault();
        if (!ScreenSizeIsMobile/* && !self.isOpen*/) {
            if (self.checkInDate) {
                self.openCheckOut();
            } else {
                self.openCheckIn();
            }
            
            if (self.isOpen) {
                self.showAvailability();
            }
        }
    });
    
    this.parent.find('a.datePickerPrevMonth').mousedown(function(ev) {
        ev.stopPropagation();
        self.parent.find('th.datepickerGoPrev').eq(0).trigger('click');
    });

    this.parent.find('a.datePickerNextMonth').mousedown(function(ev) {
        ev.stopPropagation();
        self.parent.find('th.datepickerGoNext').eq(0).trigger('click');
    });
    
    this.parent.find('div.datePickerClearAll').mousedown(function(ev) {
        ev.stopPropagation();
        
        self.checkInLabel.text('Check In');
        self.checkOutLabel.text('Check Out');
        
        self.checkInInput.val('');
        self.checkOutInput.val('');
        
        self.parent.DatePickerClear();
        self.checkInDate = false;
        self.checkOutDate = false;
        
        self.parent.DatePickerHide();
        hidePickerCode();
    });
    
    if (this.version != 'searchbar' && this.version != 'searchbar2') {
        this.availability();
    }
};

DhBookingCalendar.prototype.availability = function() {
    var now = new Date();
    this.getAvailabilityForMonth(now);
    now.setMonth(now.getMonth( ) + 1);
    this.getAvailabilityForMonth(now);
};

DhBookingCalendar.prototype.getAvailabilityForMonth = function(date) {
    var self = this;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var yearMonth = year + '_' + ('0' + month).slice(-2);
    
    if (
        typeof(this.dataUnavailable[yearMonth]) != 'undefined'
        || typeof(this.dataUnavailableLoading[yearMonth]) != 'undefined'
    ) {
        return;
    }
    
    this.dataUnavailableLoading[yearMonth] = true;
    
    var startDate = year + '/' + ('0' + month).slice(-2) + '/01';
    var endDate = year + '/' + ('0' + month).slice(-2) + '/' + daysInMonth(month, year);
    
    AvailabilityService.get(date, function(data) {
        self.dataUnavailable[yearMonth] = data;
        var currentYearMonth = self.getCurrentYearMonth();
        
        if (currentYearMonth[0] == yearMonth) {
            self.redraw(0);
        }
        
        if (currentYearMonth[1] == yearMonth) {
            self.redraw(1);
        }
    }, true);
};

DhBookingCalendar.prototype.openCheckIn = function() {
    this.checkStatus = 'in';
    this.parent.DatePickerShow();
    
    if (this.version != 'searchbar2') {
        //this.checkInLabel.css('font-family', 'DINWebProMedium');
        //this.checkOutLabel.css('font-family', 'DINWebPro');
    }
    
    if (this.version == 'widget') {
        this.parent.css('top', '0px');
    } else if (this.version == 'search') {
        this.parent.css('right', '0px');
    } else if (this.version == 'searchbar') {
        if ($(window).width() > 1264) {
            this.parent.css('right', '230px');
        } else if ($(window).width() > 1150) { 
            this.parent.css('right', '28%');
        } else {
            this.parent.css('right', '31%');
        }
    } else if (this.version == 'searchbar2') {
        this.parent.css('right', '232px');
    }
};

DhBookingCalendar.prototype.openCheckOut = function() {
    this.checkStatus = 'out';
    this.parent.DatePickerShow();
    
    if (this.version != 'searchbar2') {
        //this.checkInLabel.css('font-family', 'DINWebPro');
        //this.checkOutLabel.css('font-family', 'DINWebProMedium');
    }
    
    if (this.version == 'widget') {
        this.parent.css('top', '50px');
    } else if (this.version == 'search') {
        this.parent.css('right', '-70px');
    } else if (this.version == 'searchbar') {
        if ($(window).width() > 1264) {
            this.parent.css('right', '100px');
        } else if ($(window).width() > 1150) { 
            this.parent.css('right', '16%');
        } else {
            this.parent.css('right', '20%');
        }
    } else if (this.version == 'searchbar2') {
        this.parent.css('right', '92px');
    }
};

DhBookingCalendar.prototype.datePicked = function(formated, date) {
    date.setHours(0, 0, 0, 0);
    
    var label = formated;
    var value = this.dateToValue(date);
    
    if (this.version == 'searchbar' || this.version == 'searchbar2') {
        value = date;
    }
    
    if (this.checkStatus == 'in') {
        this.checkInDate = date;
        
        if (!this.checkOutDate || this._checkoutBeforeCheckin() || this._unavailabilityCheckInOut()) {
            this.checkOutDate = new Date(date.getTime());
            this.checkOutDate.setDate(this.checkOutDate.getDate() + 1);
        }
        
        this.checkInLabel.text(label);
        this.checkInInput.val(value);
        
        var outLabel = this.parent.DatePickerFormat(this.checkOutDate, 'b d, Y');
        var outValue = this.dateToValue(this.checkOutDate);
        
        if (this.version == 'searchbar' || this.version == 'searchbar2') {
            outValue = this.checkOutDate;
        }
        
        this.checkOutLabel.text(outLabel);
        this.checkOutInput.val(outValue);
    } else {
        this.checkOutDate = date;
        this.checkOutLabel.text(label);
        this.checkOutInput.val(value);
    }
};

DhBookingCalendar.prototype._checkoutBeforeCheckin = function() {
    if (!this.checkOutDate) {
        return false;
    }
    
    var checkIn = this.checkInDate.getFullYear() * 10000
        + (this.checkInDate.getMonth() + 1) * 100
        +  this.checkInDate.getDate();
    
    var checkOut = this.checkOutDate.getFullYear() * 10000
        + (this.checkOutDate.getMonth() + 1) * 100
        +  this.checkOutDate.getDate();
    
    return checkOut <= checkIn;
};

DhBookingCalendar.prototype._unavailabilityCheckInOut = function() {
    if (this.version == 'searchbar' || this.version == 'searchbar2') {
        return false;
    }
    
    var startDate = new Date(this.checkInDate.getTime());
    var endDate = new Date(this.checkOutDate.getTime());
    
    var currentDate = new Date(startDate.getTime());
    while (currentDate < endDate) {
        var year = currentDate.getFullYear();
        var month = currentDate.getMonth();
        var date = currentDate.getDate();
        
        var yearMonth = year + '_' + ('0' + (month + 1)).slice(-2);
        var unavailableDays;
        if (typeof(this.dataUnavailable[yearMonth]) != 'undefined' && this.version != 'searchbar') {
            unavailableDays = this.dataUnavailable[yearMonth];
        } else {
            unavailableDays = {};
        }
        
        if (typeof(unavailableDays[date]) != 'undefined') {
            return true;
        }
        
        currentDate = new Date(year, month, date + 1, 0, 0, 0, 0);
    }
    
    return false;
};

DhBookingCalendar.prototype.dateToValue = function(date) {
    return date.getFullYear()
        + '-' + twoDigits(date.getMonth() + 1)
        + '-' + twoDigits(date.getDate());
};

DhBookingCalendar.prototype.showAvailability = function() {
    if (this.version == 'searchbar' || this.version == 'searchbar2') {
        this.redraw(0);
        this.redraw(1);
        return;
    }
    
    var tbodies = this.parent.find('tbody.datepickerDays');
    tbodies.find('td').not('.datepickerNotInMonth').addClass('date-not-loaded');
    
    var month2 = new Date(this.firstDateMonth.getTime());
    month2.setMonth(month2.getMonth() + 1);
    
    var yearMonth = this.getCurrentYearMonth();
    
    if (typeof(this.dataUnavailableLoading[yearMonth[0]]) == 'undefined') {
        // Didn't attempt to load, load
        this.getAvailabilityForMonth(this.firstDateMonth);
    } else if (typeof(this.dataUnavailable[yearMonth[0]]) != 'undefined') {
        // Already loaded, draw
        this.redraw(0);
    }
    
    if (typeof(this.dataUnavailableLoading[yearMonth[1]]) == 'undefined') {
        // Didn't attempt to load, load
        this.getAvailabilityForMonth(month2);
    } else if (typeof(this.dataUnavailable[yearMonth[1]]) != 'undefined') {
        // Already loaded, draw
        this.redraw(1);
    }
    
    // If it's already loading do nothing
};

DhBookingCalendar.prototype.getCurrentYearMonth = function() {
    var month2 = new Date(this.firstDateMonth.getTime());
    month2.setMonth(month2.getMonth() + 1);
    
    var yearMonth1 = this.firstDateMonth.getFullYear() + '_'
        + ('0' + (this.firstDateMonth.getMonth() + 1)).slice(-2);
    
    var yearMonth2 = month2.getFullYear() + '_'
        + ('0' + (month2.getMonth() + 1)).slice(-2);
    
    return [yearMonth1, yearMonth2];
};

DhBookingCalendar.prototype.redraw = function(whichCalendar) {
    if (!this.isOpen) {
        return;
    }
    
    var yearMonth = this.getCurrentYearMonth();
    yearMonth = yearMonth[whichCalendar];
    
    var unavailableDays;
    if (typeof(this.dataUnavailable[yearMonth]) != 'undefined' && this.version != 'searchbar' && this.version != 'searchbar2') {
        unavailableDays = this.dataUnavailable[yearMonth];
    } else {
        // This shouldn't happen, but if it does don't break the application
        unavailableDays = {};
    }
    
    var tbodies = this.parent.find('tbody.datepickerDays');
    var tbody = $(tbodies[whichCalendar]);
    var cells = tbody.find('td').not('.datepickerNotInMonth');
    cells.removeClass('date-not-loaded');
    
    if (this.checkStatus != 'in') {
        var nextUnavailable = this.findNextUnavailableDate();
    }
    
    for(var i=0; i<cells.length; i++) {
        var currentDate = new Date(2000, 1, 1, 0, 0, 0, 0);
        currentDate.setFullYear(this.firstDateMonth.getFullYear());
        currentDate.setMonth(this.firstDateMonth.getMonth() + whichCalendar);
        currentDate.setDate(i + 1);
        currentDate.setHours(0, 0, 0, 0);
        
        var cell = $(cells[i]);
        
        if (this.checkStatus == 'in') {
            this._redrawCellIn(cell, currentDate, unavailableDays);
        } else {
            this._redrawCellOut(cell, currentDate, nextUnavailable);
        }
    }
};

DhBookingCalendar.prototype.findNextUnavailableDate = function() {
    if (!this.checkInDate) {
        return false;
    }
    
    var startDate = new Date(this.checkInDate.getTime());
    var endDate = new Date(this.firstDateMonth.getTime());
    endDate.setMonth(endDate.getMonth() + 1);
    endDate.setDate(32);
    
    var currentDate = new Date(startDate.getTime());
    while (currentDate < endDate) {
        var year = currentDate.getFullYear();
        var month = currentDate.getMonth();
        var date = currentDate.getDate();
        
        var yearMonth = year + '_' + ('0' + (month + 1)).slice(-2);
        var unavailableDays;
        if (typeof(this.dataUnavailable[yearMonth]) != 'undefined' && this.version != 'searchbar'  && this.version != 'searchbar2') {
            unavailableDays = this.dataUnavailable[yearMonth];
        } else {
            unavailableDays = {};
        }
        
        if (typeof(unavailableDays[date]) != 'undefined') {
            return currentDate;
        }
        
        currentDate = new Date(year, month, date + 1, 0, 0, 0, 0);
    }
    
    return false;
}

DhBookingCalendar.prototype._redrawCellIn = function(cell, date, unavailableDays) {
    var day = date.getDate();
    
    cell.removeClass('datepickerDisabled');
    cell.removeClass('datepickerSelected');
    cell.removeClass('date-booked');
    cell.removeClass('date-available');
    cell.removeClass('datepickerSelectedDisabled');
    cell.removeClass('date-checkin');
    cell.removeClass('date-checkout');
    
    var now = new Date();
    now.setHours(0, 0, 0, 0);
    
    if (date.getTime() < now.getTime()) {
        cell.addClass('datepickerDisabled');
    } else {
        if (typeof(unavailableDays[day]) != 'undefined') {
            cell.addClass('date-booked');
        } else if (this.version != 'searchbar' && this.version != 'searchbar2') {
            cell.addClass('date-available');
        }
    }
    
    if (this.dateEquals(date, this.checkInDate)) {
        cell.addClass('date-checkin');
    }
    
    if (this.dateEquals(date, this.checkOutDate)) {
        cell.addClass('date-checkout');
    }
    
    if (this.checkInDate && this.checkOutDate) {
        if (date.getTime() >= this.checkInDate.getTime() && date.getTime() <= this.checkOutDate.getTime()) {
            cell.addClass('datepickerSelected');
        }
    } else if (this.checkInDate) {
        if (date.getTime() == this.checkInDate.getTime()) {
            cell.addClass('datepickerSelected');
        }
    }
};

DhBookingCalendar.prototype._redrawCellOut = function(cell, date, nextUnavailable) {
    cell.removeClass('datepickerDisabled');
    cell.removeClass('datepickerSelected');
    cell.removeClass('date-booked');
    cell.removeClass('date-available');
    cell.removeClass('datepickerSelectedDisabled');
    cell.removeClass('date-checkin');
    cell.removeClass('date-checkout');
    
    if (!this.checkInDate) {
        return;
    }
    
    var checkInPlusOne = new Date(this.checkInDate.getTime());
    checkInPlusOne.setDate(checkInPlusOne.getDate() + 1);
    checkInPlusOne.setHours(0, 0, 0, 0);
    
    if (date < checkInPlusOne) {
        cell.addClass('datepickerDisabled');
    } else {
        if (nextUnavailable && date > nextUnavailable) {
            cell.addClass('date-booked');
        } else if (this.version != 'searchbar' && this.version != 'searchbar2') {
            cell.addClass('date-available');
        }
    }
    
    if (this.dateEquals(date, this.checkInDate)) {
        cell.addClass('date-checkin');
    }
    
    if (this.dateEquals(date, this.checkOutDate)) {
        cell.addClass('date-checkout');
    }
    
    if (date.getTime() > this.checkInDate.getTime() && date.getTime() <= this.checkOutDate.getTime()) {
        cell.addClass('datepickerSelected');
    } else if (date.getTime() == this.checkInDate.getTime()) {
        cell.addClass('datepickerSelectedDisabled');
    }
};

DhBookingCalendar.prototype._debug = function(thing) {
    if (!this.debug) {
        return;
    }
    
    console.debug(thing);
};

DhBookingCalendar.prototype.valueToDate = function(value) {
    if (value.length > 12) {
        return new Date(value);
    }
    
    value = value.split('-');
    var year = parseInt(value[0], 10);
    var month = parseInt(value[1], 10) - 1;
    var day = parseInt(value[2], 10);
    
    return new Date(year, month, day, 0, 0, 0, 0);
};

DhBookingCalendar.prototype.dateEquals = function(date1, date2) {
    if (!date1 || !date2) {
        return false;
    }
    
    return date1.getDate() == date2.getDate()
        && date1.getMonth() == date2.getMonth()
        && date1.getFullYear() == date2.getFullYear();
};
