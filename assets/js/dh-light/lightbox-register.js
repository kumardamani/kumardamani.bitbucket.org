$(function() {
    $('#lightboxRegisterForm form').bind("keyup keypress", function(e) {
        var code = e.keyCode || e.which;
        if (code  == 13) {
            e.preventDefault();
            $('#lightboxRegisterForm form a.submit').click();
        }
    });
    
    $('#lightboxRegisterForm form').bind("keyup keypress", function(e) {
        var code = e.keyCode || e.which;
        if (code  == 13) {
            e.preventDefault();
            $('#lightboxRegisterForm form a.submit').click();
        }
    });
    
    $('div#lightboxRegister').on('click', 'a.submit', function() {
        // var min_price_range = 0;
        var valid = true;
        var $emailAddress = $('input#registerEmailAddress');
        //var $emailAddressConfirm = $('input#registerEmailAddressConfirm');
        var emailAddress = $emailAddress.val();
        var signupSource = $('input#signup_source').val();
        var travelAgent = $('input#travelAgent').val();
        var agreeWithTerms = $('input#agreeWithTerms').val();
        
        if (agreeWithTerms == 0) {
            valid = false;
            
            $('input#agreeWithTerms').parent().addClass('error');
            $('input#agreeWithTerms').parent().find('.errorMessage').text('Error: agree with legal terms');
            $('input#agreeWithTerms').parent().find('.checkbox').focus();
            //$('input#agreeWithTerms').parent().find('div.valid').hide();
        } else {
            $('input#agreeWithTerms').parent().removeClass('error');
            //$('input#agreeWithTerms').parent().find('div.valid').show();
            $('input#agreeWithTerms').parent().find('.errorMessage').text('');
        }
        
        var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (emailPattern.test(emailAddress) == false) {
            valid = false;

            $emailAddress.parent().addClass('error');
            $emailAddress.parent().find('.errorMessage').text('Error: enter a valid e-mail address');
            $emailAddress.parent().find('div.valid').hide();
            $emailAddress.focus();
        } else {
            $emailAddress.parent().removeClass('error');
            //$emailAddress.parent().find('div.valid').show();
            $emailAddress.parent().find('.errorMessage').text('');
        }

        if (valid) {
            $.ajax({
                type: "POST",
                url: "/user/ajax/register",
                data: {emailAddress: emailAddress, signupSource: signupSource, travelAgent: travelAgent}
            }).done(function(msg) {
                s.events = 'even1, event4';
                s.prop4 = 'signup';
                
                if (typeof msg.success != 'undefined') {
                    $emailAddress.parent().find('div.valid').show().delay(1000).queue(function(){
                        if (ScreenSizeIsMobile) {
                            window.location.href = '/mycommunity/thankyou';
                        } else {
                            $('div#lightboxRegisterForm').hide();
                            $('div#lightboxRegisterSuccess').show();
                        }
                    });
                } else {
                    $('input#registerEmailAddress').parent().addClass('error');
                    $('input#registerEmailAddress').parent().find('.errorMessage').text("Error: The email address is in use");
                    $('input#registerEmailAddress').parent().find('div.valid').hide();
                }
            });
        }
    });
    
    $('#lightboxLoginForm form').bind("keyup keypress", function(e) {
        var code = e.keyCode || e.which;
        if (code  == 13) {
            e.preventDefault();
            $('#lightboxLoginForm form a.submit').click();
        }
    });
});