
// Javascript Open in New Window (validation workaround)
//
function externalLinks() {
 if (!document.getElementsByTagName) return;
 var anchors = document.getElementsByTagName("a");
 for (var i=0; i<anchors.length; i++) {
   var anchor = anchors[i];
   if (anchor.getAttribute("href") &&
       anchor.getAttribute("rel") == "external")
     anchor.target = "_blank";
 }
}

// Currency switcher on desktop nav
//
function toggleCurrency() {
    $("#switch-currency").on('click', function(e) {
        e.stopPropagation();
        if ( $('div.currencySwitchList').is(':visible') ) $('div.currencySwitchList').slideUp();
        else $('div.currencySwitchList').slideDown();
    });
    
    // Hide elements when body is clicked
    $('body').click(function(e) {
        var target = $(e.target);
        if(!target.is('div.currencySwitchList') && !target.is('#switch-currency')) {
           if ( $('div.currencySwitchList').is(':visible') ) $('div.currencySwitchList').slideUp();
        }
    });
    
    var setCurrency = function(currency) {
        $.ajax({
            type: 'POST',
            url: '/user/ajax/setcurrency',
            data: {currency: currency},
            complete: function() {
                window.location.reload();
            }
        });
    };
    
    $('.currencySwitchList ul li a').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        setCurrency($(this).data('currency'));
    });
    
    $('.mobile-currency select').change(function() {
        setCurrency($(this).val());
    });
}

function toggleMobileSearchBar() {
    $("#open-mobile-search").on('click', function(){
        $("#mobile-search-bar").slideToggle('fast');
    });
}

function toggleMobileSearchAutosuggest() {
    /*$("#mobile-search-bar input").on('keydown', function(){
        if( $('#mobile-search-autosuggest').hasClass("as-closed") ) {
            $('#mobile-search-autosuggest').removeClass("as-closed");
            $('#mobile-search-autosuggest').addClass("as-opened");
        } else {
            $('#mobile-search-autosuggest').removeClass("as-opened");
            $('#mobile-search-autosuggest').addClass("as-closed");            
        }
    });*/
}

function highlightMobileSearch() {
    /*$("#mobile-search-bar input").focusin(function() {
        $("#mobile-search-bar, #mobile-search-autosuggest").addClass("mobile-search-shadow");        
    });
    $("#mobile-search-bar input").focusout(function() {
        $("#mobile-search-bar, #mobile-search-autosuggest").removeClass("mobile-search-shadow");        
    });*/
}

function stickyNavbar() {

    var mn = $("#search-bar");
        mns = "search-bar-fixed";
        cnt = $("#main-container");
        hdr = $('header').height();
        cntnm = "main-container-no-margin";
    
    $(window).scroll(function() {
        if( $(this).scrollTop() > hdr ) {
            mn.addClass(mns);
//            cnt.addClass(cntnm);
        } else {
            mn.removeClass(mns);
//            cnt.removeClass(cntnm);
        }
    });
}

function setGoldenstageHeight() {
    var hdr = $('header').height();
        mhdr = $('#mobile-header').outerHeight();
        vph = $(window).height();
        mn = $("#search-bar").height();
        mf = $("#mainMobileFooter").height();
        newheight = vph - hdr;
        //newMheight = vph - mhdr;
        isMobile = $('#mobile-header').is(':visible');

    // Check for Mobile breakpoint        
    if(isMobile) {
        $('.goldenstage').height(vph - mf + 1);
        $('#goldenstage-wrap .gs-arrow').height(vph - mf - mhdr + 1);
        $('#goldenstage-wrap .gs-arrow').css({"top":mhdr});
        $('#goldenstage-wrap article section').css({"padding-top":mhdr});
    } else {
        if ( newheight > 960 ) {
            newheight = 960
        } else if ( newheight < 300 ) {
            newheight = 300
        }
        $('.goldenstage').height(newheight);
        $('#goldenstage-wrap article section').css({"padding-top":mn});
        $('#goldenstage-wrap .gs-arrow').height(newheight - mn + 3).css('margin-top',mn - 3);
    }
    
    // Sideload check for "if video" and hide slider arrows
    if ( $("#goldenstage-wrap").hasClass("gs-video-type")) {
        $("#goldenstage-wrap .gs-arrow").hide();
    }
}

var globalImageSwipes;
var stickNavHeight;
var mobileStickNavHeight;
var isMobile;

function homepageSlider() {
    var isVideo = $("#goldenstage-wrap").hasClass("gs-video-type");

    if (!isVideo) {
        // var gsImageSwipes = []
        // $('.gsslider').each(function(i, obj) {
            // gsImageSwipes[i] = new Swipe(obj, {
                // speed: 400,
                // auto: 3000,
                // continuous: true
            // });
        // });
        // $(".golden-arrow-left").on('click', function() {
            // gsImageSwipes[0].prev();
        // });
        // $(".golden-arrow-right").on('click', function() {
            // gsImageSwipes[0].next();
        // });

        // use flexslider instead for image slideshow for transition purposes
        $('#slider').css('visibility', 'visible');
        $('#slider .flexslider').flexslider({
            animation: 'fade',
            controlNav: false,
            directionNav: false,
            keyboard: false,
            slideshowSpeed: 4000,
            touch: true,
            slideshow: true
        });

        // Add an interval since Flexslider doesn't support autoplay after prev/next buttons are clicked
        // setInterval(function(){ $('#slider .flexslider').flexslider('next'); }, 4000);

        $('#goldenstage-wrap .flex-prev').on('click', function() {
            $('#slider .flexslider').flexslider('prev');
            return false;
        });
        $('#goldenstage-wrap .flex-next').on('click', function() {
            $('#slider .flexslider').flexslider('next');
            return false;
        });
    }
    
    var swipes = [];
    globalImageSwipes = swipes;
    
    $('.slider').each(function(i, obj) {
        var sw = new Swipe(obj);
        swipes[i] = sw;
        
        (function(index, swipe, element) {
            element.parent().find('.gs-arrow-left').on('click', function() {
                swipe.prev();
            });
            
            element.parent().find('.gs-arrow-right').on('click', function() {
                swipe.next();
            });
        })(i, sw, $(this));
    });
    
    stickNavHeight = $("#search-bar").height();
    mobileStickNavHeight = $("#mobile-header").outerHeight();
    isMobile = $('#mobile-header').is(':visible');

    $(".gs-arrow-down").on('click', function() {
        
        if (isMobile) {
            $('html,body').animate({
                scrollTop: $("#main").offset().top - mobileStickNavHeight
            });
            
        } else {
            $('html,body').animate({
                scrollTop: $("#main").offset().top - stickNavHeight
            });
        }
        
    });
}



var mobileFooter = function() {
    $(window).on('to-desktop', function() {
        $('li.footerURL').slideDown();
    });

    $(window).on('to-mobile', function() {
        $('li.footerURL').slideUp();
    });
    
    $("li.header").click(function(){
        if (!ScreenSizeIsMobile) {
            return;
        }
        
        $("li.header i").removeClass("icon-arrow-small-up").addClass("icon-arrow-small-down");
        
        if ($(this).nextAll("li.footerURL").css("display") === "none") {
            $("li.footerURL").slideUp();
            $(this).nextAll("li.footerURL").slideDown();
            $(this).children("i").removeClass("icon-arrow-small-down").addClass("icon-arrow-small-up");
        } else {
            $(this).nextAll("li.footerURL").slideUp();
        }

        $('.footerURL a').each(function(i,v){
            if($(this).text()=="Register"){
                $(this).parent().hide();
            }
        });
    });
    
    var selectCurrency = $('div.mobile-currency select');
    
    selectCurrency.change(function() {
        var labelCurrency = $('.mobile-currency .selected-currency');
        var activeCurrency = $(this).val();

        labelCurrency.text(activeCurrency);

        $.ajax({
            type: "POST",
            url: "/user/ajax/setcurrency",
            data: {currency: activeCurrency}
        }).done(function(msg) {
            if (typeof msg.currency != 'undefined') {
                location.reload();
            }
        });
    });
};

var mobileGeoLocation = function() {
    $('.mobile-footer-location, #searchEngineMobile .geo-location-button').click(function() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(pos) {
                    var lat = pos.coords.latitude;
                    var lng = pos.coords.longitude;

                    window.location.href = '/search?q=geoloc&lat=' + lat + '&lng=' + lng;
                },
                function() {
                    alert('Unable to determine the current location. Please make sure your location service is turned on.');
                }
                , {
                    //enableHighAccuracy: false,
                    timeout: 10000,
                    maximumAge: 10000
                }
            );
        }
    });
};

function dealsSlider() {
    // Initialize Deals slider (this may need to be moved to homepage footer only)
    window.dealsSliderSwipe = new Swipe(document.getElementById('#deals-slider'), {
        speed: 720,
        auto: 5000,
        continuous: true,
        disableScroll: false,
        stopPropagation: false,
        transitionEnd: function(index, elem) {}
    });
    
    $(".deals-arrow-left").on('click', function() {
        window.dealsSliderSwipe.prev();
    });
    
    $(".deals-arrow-right").on('click', function() {
        window.dealsSliderSwipe.next();
    });
}

//function scale450() {
    //var sliderHeight = $(".slider-450").outerHeight();
    
    //// Cache a reference to $(window), for performance, and get the initial dimensions of the window
    //var $window = $(window),
        //previousDimensions = {
            //width: $window.width(),
            //height: $window.height()
        //};

    //$window.resize(function(e) {
        //var newDimensions = {
            //width: $window.width(),
            //height: $window.height()
        //};

        //if (newDimensions.width > previousDimensions.width) {
            //// scaling up
            //sliderHeight = $(".slider-450, .slider-arrows, .slider-450 .swipe-wrap .single-deal, .slider-450 .swipe-wrap .single-hotel").css({"height": sliderHeight + 1});
        //} else {
            //// scaling down
            //sliderHeight = $(".slider-450, .slider-arrows, .slider-450 .swipe-wrap .single-deal, .slider-450 .swipe-wrap .single-hotel").css({"height": sliderHeight - 1});
        //}

        //// Store the new dimensions
        //previousDimensions = newDimensions;
    //});
    
//}

function searchInputHelper() {
    sb = $('#search-bar .wrapper').width();
    sp = $('#search-params').width() + 73;
    newWidth = sb - sp;

    $('#search-input').css({"display":"inline"});
    
    $('#search-input').outerWidth(newWidth);

    $('#search-input').focusin( function() {
        $(this).prev('i.icon-magnify-glass').css('color','#000000');
    });
    $('#search-input').focusout( function() {
        $(this).prev('i.icon-magnify-glass').css('color','#888888');
    });
    
    var autocompleteWidth = newWidth + 39;
    var hotelListLeft = autocompleteWidth;
    
    $('#searchEngineAutocomplete').width(autocompleteWidth);
    $('#searchEngineAutocomplete .hotels-list').css('left', hotelListLeft + 'px');
    
    var suggestMaxHeight = $(window).height() - 250;
    $('#searchEngineAutocomplete .auto-suggest-list').css('max-height', suggestMaxHeight + 'px');
}

function socialPopout() {
    if (!$('#goldenstage-wrap').length) {
        $('#social-sidebar').css('right', '0px');
        return;
    }
    
    var target = $('#goldenstage-wrap').offset().top + $('#goldenstage-wrap').height();
    var timeout = null;

    $(window).scroll(function () {
        if ($(this).width() >= 768) {
            if (!timeout) {
                timeout = setTimeout(function () {            
                    clearTimeout(timeout);
                    timeout = null;
                    if ($(window).scrollTop() >= target - 130) {
                        $('#social-sidebar').animate({
                            right: "0px"
                        }, 250);
                    } else if ($(window).scrollTop() < target) {
                        $('#social-sidebar').animate({
                            right: "-48px"
                        }, 250);
                    }
                }, 250);
            }
        }
    });
}

function windowPopup(url, width, height) {
  // Calculate the position of the popup so
  // it’s centered on the screen.
  var left = (screen.width / 2) - (width / 2),
      top = (screen.height / 2) - (height / 2);

  window.open(
    url,
    "",
    "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left
  );
}

// Execute when the DOM is ready
//
$(document).ready(function() {
    externalLinks();
    toggleCurrency();
    toggleMobileSearchBar();
    toggleMobileSearchAutosuggest();
    highlightMobileSearch();
    stickyNavbar();
    mobileFooter();
    mobileGeoLocation();
    searchInputHelper();
    socialPopout();

    $("a.social-share-intent").on("click", function(e) {
      e.preventDefault();
      windowPopup($(this).attr("href"), 500, 300);
    });

    // Run on window resize
    $(window).resize(function() {
        searchInputHelper();
    });

    // Only run if on homepage
    if ( $('body').is('#home') ) {

        homepageSlider();
        setGoldenstageHeight();

        $(window).resize(function() {
            setGoldenstageHeight();
            //scale450();
        });
    }
    
    var widgetCalendarSearchBar = new DhBookingCalendar(
        'searchbar2',
        $('#search-bar-calendar'),
        $('#choose-check-in'),
        $('#choose-check-out'),
        $('#choose-check-in > .label'),
        $('#choose-check-in > input'),
        $('#choose-check-out > .label'),
        $('#choose-check-out > input')
    );
    widgetCalendarSearchBar.init();
    
    $($('.block-collapse-mobile')[0]).addClass('block-collapse-mobile-first');
    
    $('.block-collapse-mobile h2 > a').on('click', function(e) {
        e.preventDefault();
    });
    
    $('.block-collapse-mobile h2').on('click', function(e) {
        $(this).closest('.block-collapse-mobile').toggleClass('block-collapse-mobile-open');
        for (var i=0; i<globalImageSwipes.length; i++) {
            globalImageSwipes[i].setup();
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top - mobileStickNavHeight
        });
    });
});
