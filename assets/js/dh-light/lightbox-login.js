$(function() {
    $('div#lightboxLogin').on('click', 'a.submit', function() {
        valid = true;
        var $emailInputLogin = $('input#loginEmailAddress');
        var $passwordInputLogin = $('input#loginPassword');
        var emailLogin = $emailInputLogin.val();
        var passwordLogin = $passwordInputLogin.val();
        var keepMeLoggedIn = $('input#keepMeLoggedIn').val();
        //validation
        var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (emailPattern.test(emailLogin) === false) {
            valid = false;

            $emailInputLogin.parent().addClass('error');
            $emailInputLogin.parent().find('.errorMessage').text('Error: enter valid e-mail address');
            $emailInputLogin.parent().find('div.valid').hide();
        } else {
            $emailInputLogin.parent().removeClass('error');
           // $emailInputLogin.parent().find('div.valid').show();
            $emailInputLogin.parent().find('.errorMessage').text('');
        }

        if (passwordLogin == '') {
            valid = false;

            $passwordInputLogin.parent().addClass('error');
            $passwordInputLogin.parent().find('.errorMessage').text('Error: enter password');
            $passwordInputLogin.parent().find('div.valid').hide();
        } else {
            $passwordInputLogin.parent().removeClass('error');
            //$passwordInputLogin.parent().find('div.valid').show();
            $passwordInputLogin.parent().find('.errorMessage').text('');
        }

        if (valid) {
            $.ajax({
                type: "POST",
                url: "/mycommunity/login",
                data: {'email': emailLogin, 'password': passwordLogin,'fromBookingFlow': $('input#fromBookingFlow').val(), 'keepMeLoggedIn': keepMeLoggedIn}
            }).done(function(msg) {
                s.event = 'event12';
                data = JSON.parse(msg);

                if (data.success == 'true') {
                    $emailInputLogin.parent().find('div.valid').show();
                    $passwordInputLogin.parent().find('div.valid').show().delay(1000).queue(function(){
                    
                    if ($('input#fromBookingFlow').val() == 1) {
                        $('#bookingFirstName').val(data.first);
                        $('#bookingLastName').val(data.last);
                        $('#bookingCountry').val(data.country);
                        countryname= $('#bookingCountry option[value="'+data.country+'"]').text();

                        $('.bookingCountry_selectbox .text').text(countryname);
                        $('#bookingAddress1').val(data.address_line1);
                        $('#bookingAddress2').val(data.address_line2);
                        $('#bookingCity').val(data.city);
                        $('#bookingState').val(data.state);

                        statename = $('#bookingState option[value="'+data.state+'"]').text();
                        $('.bookingState_selectbox .text').text(statename);

                        $('#bookingPostcode').val(data.postalcode);

                        $('div#lightboxLogin').dialog('close');
                    } else {
                        $('div#lightboxLogin').dialog('close');

                        if ((typeof(thisIsStep3) != 'undefined' && thisIsStep3)
                            || (typeof(isStory) != 'undefined' && isStory)
                            || (typeof(isCampaign) != 'undefined' && isCampaign)) {
                            window.location.reload();
                        } else {
                            window.location = "/mycommunity/overview";
                        }
                    }
                    });
                } else {
                    //wrong parametes pass
                    $emailInputLogin.val('');
                    $passwordInputLogin.val('');
                    $emailInputLogin.parent().addClass('error');
                    $passwordInputLogin.parent().addClass('error');
                    $emailInputLogin.parent().find('.errorMessage').html('Email address and password do not match our records.<br /><br />If you had an account with us before, please<br /> reset it by clicking "Forgot Password"');
                    $passwordInputLogin.parent().find('div.valid').hide();
                    $emailInputLogin.parent().find('div.valid').hide();
                }
            });
        }
    });
});