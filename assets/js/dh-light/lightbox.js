$(function() {
    var lightboxOldScrollValue = 0;
    
    var lightboxOptionsDesktopMobile = {
        autoOpen: false,
        dialogClass: 'lightboxDialog mobileLightboxDialog',
        height: 'auto',
        modal: true,
        resizable: false,
        width: '100%',
        position: {
            my: "left top",
            at: "left top",
            of: $(document.body)
        },
        open: function(event, ui) {
            lightboxOldScrollValue = $(window).scrollTop();
            $(window).scrollTop(0);
            $(this).parent().css('left', '0px');
            $(this).parent().css('top', '0px');
            $("body").css({overflow: 'hidden'});
            $("body").addClass('lightbox-currently-open');
        },
        beforeClose: function(event, ui) {
            $("body").css({overflow: 'inherit'});
            $("body").removeClass('lightbox-currently-open');
            $(window).scrollTop(lightboxOldScrollValue);
        }
    };
    
    var lightboxOptionsDesktop = {
        autoOpen: false,
        dialogClass: 'lightboxDialog',
        height: 'auto',
        modal: true,
        resizable: false,
        width: '430',
        position: {
            my: "center center",
            at: "center center",
            of: $(document.body)
        },
        open: function(event, ui) {
            $("body").css({overflow: 'hidden'});
            $("body").addClass('lightbox-currently-open');
        },
        beforeClose: function(event, ui) {
            $("body").css({overflow: 'inherit'});
            $("body").removeClass('lightbox-currently-open');
        }
    };
    
    var updatePreferencesOptions = {
        autoOpen: false,
        dialogClass: 'lightboxDialog',
        height: 'auto',
        modal: true,
        width: '610',
        resizable: false,
        open: function(event, ui) {
            $("body").css({overflow: 'hidden'});
            $("body").addClass('lightbox-currently-open');
        },
        beforeClose: function(event, ui) {
            $("body").css({overflow: 'inherit'});
            $("body").removeClass('lightbox-currently-open');
        }
    };
    
    var lightboxOptionsDesktopWhyBook = jQuery.extend({}, lightboxOptionsDesktop);
    lightboxOptionsDesktopWhyBook.width = '640';
    
    $(window).on('to-mobile', function() {
        $('div#lightboxRegister').dialog(lightboxOptionsDesktopMobile);
        $('div#lightboxLogin').dialog(lightboxOptionsDesktopMobile);
        $('div#lightboxLoginOrFb').dialog(lightboxOptionsDesktopMobile);
        $('div#lightboxRegisterOrFb').dialog(lightboxOptionsDesktopMobile);
        $('div#lightboxForgotPassword').dialog(lightboxOptionsDesktopMobile);
        $('div#lightboxAccountDelete').dialog(lightboxOptionsDesktopMobile);
        $('div#lightboxAccountDeleteConfirmation').dialog(lightboxOptionsDesktopMobile);
        $('div#lightboxAccountSaved').dialog(lightboxOptionsDesktopMobile);
        $('div#lightboxReset').dialog(lightboxOptionsDesktopMobile);
        $('div#lightboxUpdatePreferences').dialog(lightboxOptionsDesktopMobile);
        $('#lightboxWhyBookWithUs').dialog(lightboxOptionsDesktopMobile);
        $('#lightboxChildAgeMenuSearch').dialog(lightboxOptionsDesktopMobile);
    });
    
    $(window).on('to-desktop', function() {
        $('div#lightboxRegister').dialog(lightboxOptionsDesktop);
        $('div#lightboxLogin').dialog(lightboxOptionsDesktop);
        $('div#lightboxLoginOrFb').dialog(lightboxOptionsDesktop);
        $('div#lightboxRegisterOrFb').dialog(lightboxOptionsDesktop);
        $('div#lightboxForgotPassword').dialog(lightboxOptionsDesktop);
        $('div#lightboxAccountDelete').dialog(lightboxOptionsDesktop);
        $('div#lightboxAccountDeleteConfirmation').dialog(lightboxOptionsDesktop);
        $('div#lightboxAccountSaved').dialog(lightboxOptionsDesktop);
        $('div#lightboxReset').dialog(lightboxOptionsDesktop);
        $('div#lightboxUpdatePreferences').dialog(updatePreferencesOptions);
        $('#lightboxWhyBookWithUs').dialog(lightboxOptionsDesktopWhyBook);
        $('#lightboxChildAgeMenuSearch').dialog(lightboxOptionsDesktop);
    });
    
    $('div.lightbox').on('click', 'a.close, p.cancel a, a.gobtn', function() {
        $(this).closest('div.lightbox').dialog('close');
    });
    
    $('.callUpdatePreferences').on('click', function() {
        var openTheLightbox = function() {
            if(typeof passwordSetUp != 'undefined' && passwordSetUp != false){
                $('div#lightboxLogin').dialog('open');
                $("div#lightboxLogin #loginEmailAddress").focus();
            } else {
                //$('div#lightboxUpdatePreferences').dialog('open');
                if(typeof customerId != 'undefined' && customerId != false) {
                    $("div#lightboxResetForm #id_customer").val(customerId);
                    $('div#lightboxReset').dialog('open');
                } else {
                    $('div#lightboxUpdatePreferences').dialog('open');
                }
            }
        }
        
        openTheLightbox();
    });
    
    $('#trigger-register, .callRegister').on('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
        
        var signup;
        if($(this).data('signup') != undefined && $(this).data('signup') != '') {
            signup = $(this).data('signup');
        }
        var openTheLightbox = function() {
            $('div#lightboxLoginOrFb').dialog('close');
            $('div#lightboxLogin').dialog('close');
            $('div#lightboxRegisterOrFb').dialog('open');
            //$('div#lightboxRegister').dialog('open');
            // $('div#lightboxRegister #registerEmailAddress').focus();
            document.cookie = 'dhSignupLightboxOpened=1; path=/';
            $('#signup_source').val(signup);
            $('#registerEmailAddress').val($('#joinBox').val());
            if ($('#joinBox').val() == undefined && $('.emailInput #email').val() != '') {
                $('#registerEmailAddress').val($('.emailInput #email').val());
            }
        }
        
        openTheLightbox();
    });
    
    $('#registerEmailButton').click(function() {
        $('div#lightboxRegister').dialog('open');
        $('div#lightboxRegisterOrFb').dialog('close');
    });
    
    $('.callForgotPassword').on('click', function() {
        var openTheLightbox = function() {
            $('div#lightboxLogin').dialog('close');
            $('div#lightboxRegister').dialog('close');
            $('div#lightboxLoginOrFb').dialog('close');
            $('div#lightboxRegisterOrFb').dialog('close');
            $('div#lightboxForgotPassword').dialog('open');
            $('div#lightboxForgotPassword #forgottenpasswordEmailAddress').focus();
        }

        openTheLightbox();
    });

    $('#trigger-login, .callLogin').on('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
        
        var openTheLightbox = function() {
            $('div#lightboxRegister').dialog('close');
            $('div#lightboxForgotPassword').dialog('close');
            $('div#lightboxRegisterOrFb').dialog('close');
            $('div#lightboxLoginOrFb').dialog('open');
            //$("div#lightboxLogin #loginEmailAddress").focus();
            
            try {
                $("#lightboxRegisterAuto").unbind("dialogclose");
                $('div#lightboxRegisterAuto').dialog('close');
            } catch(e) { }
        }

        openTheLightbox();
    });
    
    $('#loginEmailButton').click(function() {
        $('div#lightboxLoginOrFb').dialog('close');
        $('div#lightboxLogin').dialog('open');
        if($('#lightboxLoginOrFb .checkbox-check').hasClass('checkbox-check-visible')) {
            $('div#lightboxLoginForm #keepMeLoggedIn').val("1");
        }
        $("div#lightboxLogin #loginEmailAddress").focus();
    });
    
    lightboxOptionsRegAuto = {
        autoOpen: false,
        dialogClass: 'lightboxDialog',
        modal: true,
        width: '800',
        height: 'auto',
        resizable: false,
        open: function(event, ui) {
            $("body").css({overflow: 'hidden'});
            $("body").addClass('lightbox-currently-open');
        },
        beforeClose: function(event, ui) {
            
            var date = new Date();
            date.setTime(date.getTime()+(7*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
            document.cookie = 'auto_signup_showndhSignupLightboxOpened=1; path=/'+ expires;
            $("body").css({overflow: 'inherit'});
            $("body").removeClass('lightbox-currently-open');
        }
    };
    
    $('#lightboxRegisterAuto').dialog(lightboxOptionsRegAuto);

    lightboxOptionsSignupWelcome = {
        autoOpen: false,
        dialogClass: 'lightboxDialog',
        modal: true,
        width: '800',
        height: 'auto',
        resizable: false,
        open: function(event, ui) {
            $("body").css({overflow: 'hidden'});
            $("body").addClass('lightbox-currently-open');
        },
        beforeClose: function(event, ui) {
            $("body").css({overflow: 'inherit'});
            $("body").removeClass('lightbox-currently-open');
        }
    };
    
    $('#lightboxSignupWelcome').dialog(lightboxOptionsSignupWelcome);
    
    
});
