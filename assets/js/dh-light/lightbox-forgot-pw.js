$(function() {
    //Forgot Password
    $('#lightboxForgotPasswordForm form').bind("keypress", function(e) {
      var code = e.keyCode || e.which;
      if (code  == 13) {
        e.preventDefault();
        $('#lightboxForgotPasswordForm form a.submit').click();
      }
    });
    $('div#lightboxForgotPassword').on('click', 'a.submit', function() {
        valid = true;
        var $emailInputForgot = $('input#forgottenpasswordEmailAddress');
        var emailForgot = $emailInputForgot.val();
        var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (emailPattern.test(emailForgot) === false) {
            valid = false;

            $emailInputForgot.parent().addClass('error');
            $emailInputForgot.parent().find('.errorMessage').text('Error: enter valid e-mail address');
            $emailInputForgot.parent().find('div.valid').hide();
        } else {
            $emailInputForgot.parent().removeClass('error');
            //$emailInputForgot.parent().find('div.valid').show();
            $emailInputForgot.parent().find('.errorMessage').text('');
        }

        if (valid) {
            $.ajax({
                type: "POST",
                url: "/mycommunity/forgottenpassword",
                data: {email: emailForgot}
            }).done(function(msg) {
                if (typeof msg.success != 'undefined') {
                    $emailInputForgot.parent().find('div.valid').show().delay(1000).queue(function(){
                        $('div#lightboxForgotPasswordForm').hide();
                        $('div#lightboxRegisterSuccess').show();
                    });
                        
                } else {
                    $emailInputForgot.val('');
                    $emailInputForgot.parent().addClass('error');
                    $emailInputForgot.parent().find('.errorMessage').text('No account found under the email address you entered.');
                    $emailInputForgot.parent().find('div.valid').hide();
                }
            });
        }
    });

    $('#resendAgain').on('click',  function() {
        var $emailInputForgot = $('input#forgottenpasswordEmailAddress');
        var emailForgot = $emailInputForgot.val();
        $.ajax({
            type: "POST",
            url: "/mycommunity/forgottenpassword",
            data: {email: emailForgot}
        }).done(function(msg) {
            if (typeof msg.success != 'undefined') {
                $('div#lightboxForgotPasswordForm').hide();
                $('div#lightboxRegisterSuccess').show();
            } else {
                $emailInputForgot.val('');
                $emailInputForgot.parent().addClass('error');
                $emailInputForgot.parent().find('.errorMessage').text('No account found under the email address you entered.');
                $emailInputForgot.parent().find('div.valid').hide();
            }
        });
    });

    //Reset email from preference lightbox
    $("#lightboxUpdatePreferencesForm").on('click', "a#lnkReset", function(){
        var emailForgot = $(this).data("email");
        $.ajax({
            type: "POST",
            url: "/mycommunity/forgottenpassword",
            data: {email: emailForgot}
        }).done(function(msg) {
            if (typeof msg.success != 'undefined') {
                $('div#lightboxUpdatePreferencesForm').hide();
                $('.txtUpdatePreference').hide();
                $('div#lightboxResetMessage').show();
            }
        });
    });


    //RESET FIRST TIME
    $('div#lightboxReset').on('click', 'a.submit', function() {
        valid = true;

        var $passwordResetInput = $('input#passwordReset');
        var $retypePasswordInput = $('input#retypePassword');
        var passwordReset = $passwordResetInput.val();
        var retypePassword = $retypePasswordInput.val();

        if (passwordReset == '' ) {
            valid = false;
            $passwordResetInput.parent().addClass('error');
            $passwordResetInput.parent().find('.errorMessage').text('Error: enter a password');
            $passwordResetInput.parent().find('div.valid').hide();
        } else {
            $passwordResetInput.parent().removeClass('error');
            $passwordResetInput.parent().find('.errorMessage').text('');
            //$passwordResetInput.parent().find('div.valid').show();
        }

        if (passwordReset != retypePassword || retypePassword == '') {
            valid = false;
            $retypePasswordInput.parent().addClass('error');
            $retypePasswordInput.parent().find('.errorMessage').text("Error: passwords don't match");
            $retypePasswordInput.parent().find('div.valid').hide();
        } else {
            $retypePasswordInput.parent().removeClass('error');
            $retypePasswordInput.parent().find('.errorMessage').text('');
            //$retypePasswordInput.parent().find('div.valid').show();
        }

        if (valid) {
            $.ajax({
                type: "POST",
                url: "/mycommunity/firstreset",
                data: {'password': passwordReset,'retypePassword': retypePassword ,'id_instance':$('#id_customer').val()}
            }).done(function(msg) {
                if (typeof msg.success != 'undefined') {
                    $passwordResetInput.parent().find('div.valid').show();
                    $retypePasswordInput.parent().find('div.valid').show().delay(1000).queue(function(){
                        $('div#lightboxResetForm').hide();
                        $('div#lightboxResetSuccess').show();
                        s.prop4 = 'confirmed';
                    });
                } else {
                    $retypePasswordInput.parent().addClass('error');
                    $retypePasswordInput.parent().find('.errorMessage').text("Password can't be reset. Probably the link has expired");
                    $retypePasswordInput.parent().find('div.valid').hide();
                }
            });
        }
    });
});