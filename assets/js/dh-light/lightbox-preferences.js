$(function() {
    //UPDATE PREFERENCES (AUTOLOGIN)
    $('div#lightboxUpdatePreferences').on('click', 'a.submit', function() {
        var data = {
            'hdnchkNewsletter': $('#hdnchkNewsletter').val(),
            'hdnchkResetPassword': $('#hdnchkResetPassword').val(),
            'hdnEmail': $('#hdnEmail').val(),
            'newsletters': []
        };

        var inputs = $('.newsletter-input');
        for (var i=0; i<inputs.length; i++) {
            var input = $(inputs[i]);
            if (input.val() == '0') {
                continue;
            }

            data.newsletters.push(input.data('newsletter'));
        }

        $.ajax({
            type: "POST",
            url: "/mycommunity/update-preferences",
            data: data
        }).done(function(msg) {
            if ( msg ) {
                $('div#lightboxUpdatePreferencesForm').hide();
                $('div#lightboxUpdatePreferencesSuccess').show();
                s.prop4 = 'confirmed';
            } else {
                $('div#lightboxUpdatePreferencesSuccess').html("<p id='lightboxRegisterThanks'>Your email preferences were successfully saved � it may take up to 24hours to take effect. Check back to your preferences any time!</p>");
                $('div#lightboxUpdatePreferencesForm').hide();
                $('div#lightboxUpdatePreferencesSuccess').show();
            }
        });


    });

    $('div.lightbox').on('click', 'a.close, p.cancel a, a.gobtn', function() {
        $(this).closest('div.lightbox').dialog('close');
    });

    $('div.lightbox .lightboxCheckboxWrapper').on('click', 'label', function(ev) {
        ev.stopPropagation();
        var $checkbox = $(this).find('.checkbox');
        var $input = $(this).parent().find('input');

        if ($checkbox.hasClass('checked')) {
            $input.val(0);
        } else {
            $input.val(1);
        }

        $checkbox.toggleClass('checked');
    });
});