var MobileScreenSize = 768;
var ScreenSizeIsMobile = false;

$(function() {
    // to-desktop/to-mobile events and update ScreenSizeIsMobile
    (function() {
        var updateIsMobile = function() {
            if ($(window).width() > MobileScreenSize) {
                ScreenSizeIsMobile = false;
            } else {
                ScreenSizeIsMobile = true;
            }
        };
        
        updateIsMobile();
        
        $(window).resize(function() {
            var wasMobile = ScreenSizeIsMobile;
            updateIsMobile();
            
            if (wasMobile && !ScreenSizeIsMobile) {
                $(window).trigger('to-desktop');
            } else if (!wasMobile && ScreenSizeIsMobile) {
                $(window).trigger('to-mobile');
            }
        });
        
        $(window).on('update-size-specific', function() {
            if ($(window).width() > MobileScreenSize) {
                $(window).trigger('to-desktop');
            } else {
                $(window).trigger('to-mobile');
            }
        });
    })();
});

function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}