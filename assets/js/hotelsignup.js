// these are all the ids that are required
var requiredArr = [
    'txtHotelName',
    'txtHotelOpeningDate',
    'txtHotelStreet',
    'hotelStatus',
    'hotelType',
    'hotelBuilding',
    'txtCity',
    'txtHotelCountry',
    'txtHotelPostalCode',
    'txtHotelWebsite',
    'txtHotelDaysOpen',
    'txtHotelNumRooms',
    'txtHotelNumSuites',
    'txtHotelConcept',
    'txtContactOwnEntity',
    'txtContactOwnEntityAdd',
    'txtContactOwnEntityCity',
    'txtContactOwnEntityCountry',
    'txtContactOwnEntityPostal',
    'txtMgmtCompName',
    'txtMgmtCompAdd',
    'txtMgmtCompCity',
    'txtMgmtCompCountry',
    'txtMgmtCompPostal',
    'txtContactFirstName',
    'txtContactLastName',
    'txtContactCompName',
    'txtContactJobTitle',
    'txtContactEmail',
    'txtContactPhone',
    'txtContactMobile'
];

$('form#frmSettings')
    .submit(function() {
        isValid = checkRequiredFields(false);
        // console.log(isValid) ;
        return isValid;
    });

//==============================================
//            KEYBOARD EVENTS
//==============================================

// date auto formatting
//Put our input DOM element into a jQuery Object
var $jqDate = jQuery('input[name="txtHotelOpeningDate"]');

//Bind keyup/keydown to the input
$jqDate.bind('keyup','keydown', function(e){

  //To accomdate for backspacing, we detect which key was pressed - if backspace, do nothing:
  if(e.which !== 8) {	
    var numChars = $jqDate.val().length;
    if(numChars === 2 || numChars === 5){
      var thisVal = $jqDate.val();
      thisVal += '/';
      $jqDate.val(thisVal);
    }
  }
});

// Avg Daily Rate auto formatting EUR constant at the end
$('#txtHotelDailyRate').val('EUR ' + $('#txtHotelDailyRate').val());

// Avg Occupancy auto formatting % constant at the end
// $('#txtHotelOccupancy').blur(function() {
    $('#txtHotelOccupancy').val($('#txtHotelOccupancy').val() + ' %');
// });

// meeting area auto formatting sqm constant at the end
$('#txtHotelMeetingArea').val($('#txtHotelMeetingArea').val() + ' sqm');

//==============================================
//            CLICK EVENTS
//==============================================

// when there is popup, if the user clicks outside a popup, 
// close the popup. UNLESS its anoter popup
$('body').on('click touchstart', function(event) {
  var $target = $(event.target);
  if (!($target.is(".info") || $target.is("p") || $target.is("div.closeInfoButton"))) {
    
    $('.info-box').hide();
    $('.box-tail').hide();
    $('.closeInfoButton').hide();
  }
});

var isIE11orLower = detectIE11orLower();

// this is to chose multiple options WIHTOUT holding cntrl
// IE doesnt support this user will have to hold cntrl
// android chome also doesnt supoprt this 
// ios safari/chrome will overide this so its safe to only
// apply it for desktop screen widths
if (!isIE11orLower && $(window).width() > 768) {
    $("select#hotelOutlets, select#hotelGuestFacilities").mousedown(function(e){
        e.preventDefault();
        
        var select = this;
        var scroll = select.scrollTop;
        
        e.target.selected = !e.target.selected;
        
        setTimeout(function(){select.scrollTop = scroll;}, 0);
        
        $(select).focus();
    }).mousemove(function(e){e.preventDefault()});
}

// to show the popup info for some large text fields
$('.info').on('click', function(event) {
    event.preventDefault();
    // alert($(this));
    // get the parent div 
    var $containerDiv = $(this).parent();
    // get the info-box next to it...
    var $info_box = $containerDiv.next();
    // get the tail bc we to toggle its display
    var $info_tail = $info_box.children().eq(1);
    // get the close X button
    var $close_info_button = $info_box.children().eq(2);

    if($info_box.css('display') == "none") {
        // get the position of the clicked text...
        var $top = $(this).position().top;
        var $left = $(this).position().left;

        // get the size of the info box
        var $height = $info_box.height();
        var $width = $info_box.width();
        // get the dims of the tail
        var $tail_width = $info_tail.width();
        var $tail_height = $info_tail.height();

        // make the info-box in these positions
        // offset by dimensions of info_box
        $info_box.css({top: $top-$height-$tail_height, left: $left-$tail_width});
        $info_tail.css({top: $height, left: $tail_width});
    }
  
    // show/hide the info box
    $info_box.toggle();
    // show/hide the tail
    $info_tail.toggle();
    // show/hide the X button
    $close_info_button.toggle();
});

// make the X button on the info popups 
// close the info popup on click
$('.closeInfoButton').on('click', function(event) {
    event.preventDefault();
    // get the 'info' text link
    var $info_link = $(this).parent().siblings('.element-label').children().eq(0);

    // do the same thing as clicking the info link.
    $info_link.trigger("click");

});

//==============================================
//            CLICK EVENTS - END
//==============================================

// copied over from the pro signup page js
// adds prestyled classes on the fly
// just add the select box in the drowdownArr var
// NOTE: doesnt support multiselect
function fancifySelectBoxes() {
    var dropdownArr = [
        'hotelStatus',
        'hotelType',
        'hotelBuilding',
        'hotelMeetingFacilities'
    ];
    var i ;
    for (i = 0; i < dropdownArr.length; i++) {
    
        $('select#' + dropdownArr[i]).tzSelect({
            selectboxClass: dropdownArr[i] + '_selectbox',
            dropdownClass: dropdownArr[i] + '_dropdown',
            onChange: function() {
                
                var text = $(this).text();
                var id = $(this).parent().attr('class').replace(/_dropdown*/i, '').replace(/dropdown*/i, '').replace(/\s+/i, '');				
                $('select#' + id + ' option').each(function() {
                
                    if ($(this).text() == text) {
                        
                        $('input#hdn' + id).val($(this).val());
                        $('#hdn' + id).trigger('change');
                        return false;
                    }
                });
                
            }
        });	
        
        $('select#' + dropdownArr[i] + ' option').each(function() {
            
            var text = $('div.' + dropdownArr[i] + '_selectbox > span.text').text();
            if ($(this).text() == text) {
                
                $('input#hdn' + dropdownArr[i]).val($(this).val());
                return false;
            }					
        });
    }		
}

//==============================================
//            VALIDATATION 
//==============================================

// again, copied from pro signup form
// looks for empty fields 
// does validation in case of email and date
// warns user if field is empty
function checkRequiredFields(isOnFocus) {
    var success = true;	
    var scrolled = false;
    for (var i = 0; i < requiredArr.length; i++) {
    
        var currReqObj = $('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden'
            ? $('div.' + requiredArr[i].replace('hdn', '') + '_selectbox')
            : $('#' + requiredArr[i]);
        var immediateParentCntr = $(currReqObj).parent();
        var mainElementsCntr = $(currReqObj).parents('div.elements-cntr').first();
        var extraValidation = true;
      console.log(currReqObj);
        //For email validation
        if(requiredArr[i] === "txtContactEmail") {
            var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            extraValidation = emailPattern.test($('#' + requiredArr[i]).val());
        }
        // of opening date validation matches only DD/MM/YYYY
        if (requiredArr[i] == "txtHotelOpeningDate") {
            var datePattern = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
            extraValidation = datePattern.test($('#' + requiredArr[i]).val())
        }

        if ($('#' + requiredArr[i]).val() == null || $('#' + requiredArr[i]).val().replace(/\s+/i, '').length == 0 || !extraValidation) {
            
            $(currReqObj).css('border', '6px solid #BF883B').css('height', '34px');
            $(mainElementsCntr).css('padding-bottom', 0);
            if ($(immediateParentCntr).find('p.error').length == 0) {
            
                $(immediateParentCntr).append('<p class="error">Please complete this field</p>');
            }
            
            if ($('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden') {
                
                $(currReqObj).css('line-height', '34px');
                $(currReqObj).find('span.icon').first().css('top', '14px');
            }
            
            if (scrolled == false && isOnFocus == false) {
                
                scrolled = true;
                scrollToElement($(currReqObj).parents('.elements-cntr').first().find('.element-label').first());
            }
            
            success = success ? false : success;
            
        } else {
            $(currReqObj).css('border', '0').css('height', '46px');
            $(immediateParentCntr).find('p.error').first().remove();
            $(mainElementsCntr).css('padding-bottom', '10px');
            if ($('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden') {
                
                $(currReqObj).css('line-height', '46px');
                $(currReqObj).find('span.icon').first().css('top', '22px');
            }
        }
        
    }

    
    $('#agree-terms-cb-holder .error').remove();
    if (!$('#agree-terms-cb').is(':checked')) {
        success = false;
        $('#agree-terms-cb-holder').append('<p class="error">Please agree to the terms and conditions</p>');
    }
    
    return success;
}

//==============================================
//            HELPERS 
//==============================================

function isEmpty(obj) {
    var blEmpty = true;
    
    if (typeof obj !== 'undefined' && obj != null) {
        if (typeof obj === 'string') {
            blEmpty = obj.replace(/\s+/ig, '').length == 0;
        } else if (Object.prototype.toString.call(obj) === '[object Array]') {
            blEmpty = obj.length == 0;
        } else {
            blEmpty = false;
        }
    }
    
    return blEmpty;
}

function scrollToElement(element) {
    try {
        $('html, body').animate({
            scrollTop: $(element).offset().top - 100
        }, 'fast');
    } catch (e) { }
}

/**
 * detect IE helper 
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE11orLower() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // Edge (IE 12+) => return version number
       // return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}
