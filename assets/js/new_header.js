var SearchBarAutoCompleteIconProperty = 'icon-ux2-single-hotel';
var SearchBarAutoCompleteIconLocation = 'icon-geolocator';

$(function() {
    var searchBar = $("#search-bar");
    var searchBarFixedClass = "search-bar-fixed";
    var headerHeight = $('#header-container > header').height();
    
    $(window).scroll(function() {
        if ($(this).scrollTop() > headerHeight) {
            searchBar.addClass(searchBarFixedClass);
        } else {
            searchBar.removeClass(searchBarFixedClass);
        }
    });
});

$(function() {
    var resize = function() {
        sb = $('#search-bar .wrapper').width();
        sp = $('#search-params').width() + 44;
        newWidth = sb - sp;

        $('#search-input').outerWidth(newWidth);

        $('#search-input').focusin( function() {
            $(this).prev('i.icon-magnify-glass').css('color','#000000');
        });
        $('#search-input').focusout( function() {
            $(this).prev('i.icon-magnify-glass').css('color','#888888');
        });
        
        var autocompleteWidth = newWidth + 12;
        var hotelListLeft = autocompleteWidth;
        
        $('#searchEngineAutocomplete').width(autocompleteWidth);
        $('#searchEngineAutocomplete .hotels-list').css('left', hotelListLeft + 'px');
        
        var suggestMaxHeight = $(window).height() - 250;
        $('#searchEngineAutocomplete .auto-suggest-list').css('max-height', suggestMaxHeight + 'px');
    };
    
    resize();
    $(window).resize(resize);
    
    setTimeout(resize, 2000);
});

// Hier kende zoeken, witte nie?
$(function() {
    var ajaxSearchBar;
    var body = $('body');
    
    var autocomplete = function() {
        searchInput = $('#search-input');
        searchResults = $('ul#searchEngineAutocomplete').find('> li');
        
        if ($(window).width() > 768) {
            searchInput.focus();
        }
        
        var currentItem = -1;
        searchInput.keyup(function(e) {
            var code = e.keyCode || e.which;
            if (searchInput.val() == '') {
                $('ul#searchEngineAutocomplete').hide();
                return;
            }
            
            if (typeof ajaxSearchBar !== 'undefined') {
                ajaxSearchBar.abort();
            }
            
            if (typeof(DH_CURRENT_VERSION) == 'undefined') {
                DH_CURRENT_VERSION = 'master';
            }
            
            ajaxSearchBar = $.ajax({
                type: "GET",
                url: '/auto-complete/',
                cache: true,
                data: {"q": searchInput.val(), "max": 10, "lang": "en", "version": DH_CURRENT_VERSION},
                success: function(response){
                    $(searchResults).first().html('');

                    for (var i = 0; i < response.suggestions_type.length; i++) {
                        var suggestion = response.suggestions_type[i];
                        var div = $(
                            '<div class="suggestion suggestion-' + suggestion.type 
                            + '"></div>'
                        );
                        
                        var icon = $(
                            '<span class="icon '
                            + (
                                suggestion.type == 'property'
                                ? SearchBarAutoCompleteIconProperty
                                : SearchBarAutoCompleteIconLocation
                            )
                            + '"></span>'
                        );
                        
                        var label = $('<span class="label" data-val = "'+ suggestion.suggestion +'"></span>');
                        label.text(suggestion.suggestion);
                        
                        div.append(icon);
                        div.append(label);
                        
                        $(searchResults).first().append(div);

                        $(div).click(function() {
                            var suggestion = $(this).text();
                            $('#searchButtonWrap a').focus();
                            $(searchInput).val(suggestion);
                            $('ul#searchEngineAutocomplete').hide();
                        });
                    }

                    $(searchResults[1]).find('> div > span.count').first().html('(' + response.properties.length + ')');
                    $(searchResults[1]).find('> ul').html('');

                    for (var i = 0; i < response.properties.length; i++) {
                        var regionVU = response.properties[i]['region_vanity_url'];
                        var countryVU = response.properties[i]['country_vanity_url'];
                        var cityVU = response.properties[i]['city_vanity_url'];
                        var hotelVU = response.properties[i]['vanity_url'];
                        var fullVU = countryVU + '/' + cityVU + '/' + hotelVU;
                        $(searchResults[1]).find('> ul').first().append('<li><a href="/hotels/' + fullVU + '">' + response.properties[i]['property_info_name'] + '</a></li>');
                    }

                    $(searchResults[2]).find('> div > span.count').first().html('(' + response.deals.length + ')');
                    $(searchResults[2]).find('> ul').html('');

                    for (var i = 0; i < response.deals.length; i++) {
                        $(searchResults[2]).find('> ul').first().append('<li><a href="/deals/' + response.deals[i]['campaign_vanity_url'] + '">' + response.deals[i]['campaign_name'] + '</a></li>');
                    }

                    $('ul#searchEngineAutocomplete').show();
                    var items = $("ul#searchEngineAutocomplete > li.auto-suggest-list div");
                    
                    if(code === 40) {
                        currentItem++;
                        if(currentItem >= items.length) currentItem = 0;
                    } else if(code === 38) {
                        currentItem--;
                        if(currentItem < 0) currentItem = items.length - 1;
                    }
                    
                    if(currentItem !== -1) {
                        items.removeClass("selected");
                        items.eq(currentItem).addClass("selected");
                    }
                    
                    var windowHeight = $(window).height();
                    
                    $('#searchEngineAutocomplete li ul').each(function() {
                        try {
                            var element = $(this);
                            var parent = element.parent();
                            element.css('top', '1px');
                            
                            var roundabout = parent[0].getBoundingClientRect();
                            
                            var height = element.height();
                            var yPos = parseInt(roundabout.top, 10)
                            if (yPos + height > windowHeight) {
                                var top = windowHeight - (yPos + height);
                                element.css('top', top + 'px');
                            }
                            
                        } catch(e) {}
                    });
                }
            });
        });
        
        $('ul#searchEngineAutocomplete').click(function(ev) {
            ev.stopPropagation();
        });
        
        body.click(function(ev) {
            $('ul#searchEngineAutocomplete').hide();
        });
    };
    
    var searchCode = function() {
        var searchInput = $('#search-input');
        //var searchIcon = $('li#searchIcon');
        var searchButton = $('#search-start');
        
        searchInput.focus(function() {
            searchInput.parent().removeClass('error');
            searchInput.attr("placeholder", "");
            //searchIcon.removeClass('error');
        });
        
        var searchFunction = function(ev) {
            if (typeof(ev) != 'undefined') {
                ev.preventDefault();
            }

            if (searchInput.val() == '' || searchInput.val() == 'Search for a hotel or destination') {
                searchInput.parent().addClass('error');
                searchInput.attr("placeholder", "Please enter a valid destination, hotel");
                searchInput.val('');
                //searchIcon.remove('span');
                //searchIcon.addClass('error');
            } else {
                var hasDates = false;
                var dateRange = '';
                var strDateStart = $('#choose-check-in > input').val();
                var strDateEnd = $('#choose-check-out > input').val();
                var adult = $('input#guestsPickerAdultsValue');
                var children = $('input#guestsPickerChildrenValue');
                var child_age_url = '';
                // if there are more than 0 children then get the age for
                // each of them
                if ($(children).val() > 0) {
                    var num_children = $(children).val();
                    var children_obj = {};
                    for (var count = 0; count < num_children; count++) {
                        // get the age of this child
                        // where the ages are store in the DOM depends on the screen width
                        if ($(window).width() < 769) {    // mobile
                            // search in the lightbox for avail widget
                            var age_value = $('#lightboxChildAgeMenuSearch span#ageForChildMobile' + count);
                        } else {  // desktop
                            // search in the avail widget
                            var age_value = $('#search-bar li#ageForChild' + count)
                                    .find('span.spinnerValue');
                        }
                        var age = parseInt(age_value.text());
                        children_obj[count] = age;
                    }
                    child_age_url = '&ages=' + encodeURIComponent(JSON.stringify(children_obj));
                }
                var suggestionText = $("ul#searchEngineAutocomplete > li.auto-suggest-list div.selected span.label").data("val");

                if (typeof suggestionText !== 'undefined' && suggestionText !== '' && suggestionText !== null) {
                    searchInput.val(suggestionText);
                }
                
                if (strDateStart != null && strDateStart.replace(/\s+/i, '').length > 0 && strDateEnd != null && strDateEnd.replace(/\s+/i, '').length > 0) {
                    var dateStart = new Date(strDateStart);
                    var dateEnd = new Date(strDateEnd);
                    dateStart = dateStart.getFullYear() + '-' + (dateStart.getMonth() + 1) + '-' + dateStart.getDate();
                    dateEnd = dateEnd.getFullYear() + '-' + (dateEnd.getMonth() + 1) + '-' + dateEnd.getDate();
                    dateRange = '&start=' + dateStart + '&end=' + dateEnd;
                    hasDates = true;
                }

                var location = '/search?q=' + encodeURIComponent(searchInput.val()) + '&adults=' + $(adult).val() + '&children=' + $(children).val() + child_age_url ;
                window.location = hasDates ? location + dateRange : location;
            }

            if (typeof ajaxSearchBar !== 'undefined') {
                ajaxSearchBar.abort();
            }
        };
        
        searchButton.click(searchFunction);
        $('#SearchForm').submit(searchFunction);
        
        searchInput.on('keydown', function(e) {
            if (e.keyCode == 13) {
                searchFunction();
            }
        });
    };
    
    var adult = $("input#guestsPickerAdultsValue").val();
    $('input#guestsPickerAdultsValue').spinsterPro({
        'max'     : 8,
        'min'     : 1,
        'start'   : adult,
        'target'  : $('#guestsPickerAdults div.spinner'),
        'onChange': function(value) {
            var label = (parseInt(value) == 1) ? ' Adult' : ' Adults';
            $('span#searchGuestsTotalAdults').text(value + label);
            $(' #label_adult').text(label);
        }
    });

    var child = $("input#guestsPickerChildrenValue").val();
    $('input#guestsPickerChildrenValue').spinsterPro({
        'max'     : 8,
        'min'     : 0,
        'start'   : child,
        'target'  : $('#guestsPickerChildren div.spinner'),
        'onChange': function(value) {
            var label = (parseInt(value) == 1) ? ' Child' : ' Children';
            $('span#searchGuestsTotalChildren').text(value + label);
            $('#label_children').text(label);
        }
    });
    
    $('#choose-guests').click(function(ev) {
        ev.stopPropagation();
        $('ul#guestsPicker').toggle();
        $(this).addClass('selected');
    });
    
    $('ul#guestsPicker, ul#guestsPickerWidget').click(function(ev) {
        ev.stopPropagation();
    });

    $(body).click(function(ev) {
        $('ul#guestsPicker').hide();
        $('ul#guestsPickerWidget').hide();
    });
    
    autocomplete();
    searchCode();
});


$(function() {
    var widgetCalendarSearchBar = new DhBookingCalendar(
        'searchbar2',
        $('#search-bar-calendar'),
        $('#choose-check-in'),
        $('#choose-check-out'),
        $('#choose-check-in > .label'),
        $('#choose-check-in > input'),
        $('#choose-check-out > .label'),
        $('#choose-check-out > input')
    );
    
    widgetCalendarSearchBar.init();
});


$(document).ready(function() {
      // initialize session loaded children ages...
      function setChildValue($el, direction) {
          var $spinnerValue = $el.parent().find('span.spinnerValue');
          var value = parseInt($spinnerValue.text());

          if (direction == 'increase' && value < 17) {
              value++;
          } else if (direction != 'increase' && value > 0) {
              value--;
          }

          $spinnerValue.text(value);
      }
      var $children = $("#search-bar li[id^='ageForChild']");
      $children.find("a.spinnerIncrease").click(function() {
          setChildValue($(this), 'increase');
      });
      $children.find("a.spinnerDecrease").click(function() {
          setChildValue($(this));
      });
});
