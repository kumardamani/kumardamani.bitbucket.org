function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}


var MobileScreenSize = 768;
var ScreenSizeIsMobile;


$(function() {
    var dhM = {
        updateIsMobile: function() {
            if ($(window).width() > MobileScreenSize) {
                ScreenSizeIsMobile = false;
            } else {
                ScreenSizeIsMobile = true;
            }
        },

        init: function() {
            dhM.updateIsMobile();

            $(window).resize(function() {
                var wasMobile = ScreenSizeIsMobile;
                dhM.updateIsMobile();

                if (wasMobile && !ScreenSizeIsMobile) {
                    $(window).trigger('to-desktop');
                } else if (!wasMobile && ScreenSizeIsMobile) {
                    $(window).trigger('to-mobile');
                }
            });

            $('.mobile-fade').click(function(){
                if (ScreenSizeIsMobile && $(this).hasClass("mobile-fade")){
                    $(this).removeClass('mobile-fade');
                    $(this).css('max-height','none');
                    return false;
                }
            });

            var layout = dh.el.body.attr('id');

            dhM.initCommon();

            switch (layout) {
                case 'home':
		    dhM.mobileHomePage();
                    break;
                case 'pdp':
		    dhM.mobilePdpOverview();
			dhM.mobilePdpTabs();
			dhM.mobilePdpMap();
			dhM.mobileWidgetCalendar();
		    break;
                case 'rooms':
		    dhM.mobilePdpOverview();
			dhM.mobilePdpTabs();
			dhM.mobilePdpMap();
            dhM.mobileWidgetCalendar();
		    break;
		case 'deal':
		    dhM.mobilePdpOverview();
		    dhM.mobilePdpTabs();
			//dhM.mobileDealCalendar();
			dhM.mobilePdpMap();
            dhM.mobileWidgetCalendar();
		    break;
                case 'city':
                case 'campaign':
                    dhM.mobilePdpSearch();
                    dhM.hotelsAndResortsMap($('#mapView'));
		    dhM.campaign();

                    if ($('section#noResult').length) {
                        dhM.mobileNoResultCalendar();
                    }
                    break;
                case 'hotelsAndResorts':
                    dhM.hotelsAndResorts();
                    dhM.hotelsAndResortsMap($('.button-view-map'));
                    break;
		case 'bookingFlow':
		    dhM.mobileStep1();
			dhM.mobileStep2();
			dhM.mobileStep3();

		    break;
                case 'overview':
                    dhM.mobileOverview();
                    break;
		case 'contact':
		    dhM.contactPage();
		    dhM.mobilePdpTabs();
		    break;
                case 'myAccountSettings':
                    dhM.myAccountSettings();
                    break;

                case 'benefits':
                    dhM.toggleBenefitInfo();
                    break;

		case 'cancellation':
		    dhM.mobilePdpTabs();
		    break;

            }

            if ($(document.body).hasClass('community-page')) {
                dhM.mobilePdpTabs();
            }

            if (ScreenSizeIsMobile) {
                $(window).trigger('to-mobile');
            } else {
                $(window).trigger('to-desktop');
            }
        },

        initCommon: function() {
            dhM.fixMobileApple();
            dhM.windowResize();
            dhM.mobileSidebar();
            dhM.mobileSearchAutocomplete();
            dhM.mobileSearchBar();
            dhM.mobileAvailabilityWidget();
            dhM.mobileLightboxes();
            dhM.mobileImageSwipe();
            dhM.mobileFooter();
            dhM.mobileMeterMiles();
        },

        mobileMeterMiles: function() {
            var units = readCookie('dhCurrentUnits');

            if (!units || units == 'metric') {
                units = 'metric';
            } else {
                units = 'mile';
            }

            $('.' + units + '-value').css('display', 'inline');

            $('.unit-switcher').click(function() {
                $('.' + units + '-value').css('display', 'none');

                units = (units == 'metric' ? 'mile' : 'metric');
                $('.' + units + '-value').css('display', 'inline');

                createCookie('dhCurrentUnits', units, 31);
            });
        },

        fixMobileApple: function() {
            var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false);

            if (!iOS) {
                return;
            }

            var body = $(document.body);

            $('select,input').focus(function() {
                body.addClass('body-input-open');
            });

            $('select,input').blur(function() {
                body.removeClass('body-input-open');
            });

            $('select').change(function() {
                body.removeClass('body-input-open');
            });

            /*$("#loginEmailAddress").focus();
            $("#loginEmailAddress").blur();

            $('.ui-dialog').css('display', 'block');
            $('.ui-dialog').css('opacity', '0');


            $('.ui-dialog').on('dialogopen', function(event, ui) {
                //$('.ui-dialog').css('display', 'none');

                $('.ui-dialog').css('opacity', '1');
                //$(this).css('display', 'block');
            });*/
        },

        windowResize: function() {
            var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false);

            var elements = {
                'searchBox': $('#searchEngineMobile .search-box'),
                'searchButton': $('#searchEngineMobile .search-button'),
                'searchContainer': $('#searchEngineMobile > .container'),
                'autoSuggest': $('#searchEngineMobile .search .auto-suggest'),
                'sidebarTop': $('#sidebarTop'),
                'navUserMenu': $('nav#userMenu'),
                'body': $(document.body),
                'searchEngineMobile': $('header#searchEngineMobile')
            };

            var windowResized = function() {
                var w = $(window);
                var windowWidth = w.width();
                var windowHeight = w.height();

                var searchBoxWidth = windowWidth - 146;
                var autoSuggestWidth = windowWidth - 90;

                elements.searchBox.width(searchBoxWidth);
                elements.autoSuggest.width(autoSuggestWidth);

                if (!iOS) {
                    elements.autoSuggest.css('max-height', (windowHeight - 120) + 'px');
                }

                if (windowHeight < 440) {
                    elements.body.addClass('small-height');
                    //elements.searchEngineMobile.css('position', 'absolute');
                } else {
                    elements.body.removeClass('small-height');
                    //elements.searchEngineMobile.css('position', '');
                }

                if (ScreenSizeIsMobile) {
                    $('.footerURL a').each(function(i,v){
                        if($(this).text()=="Register"){
                            $(this).parent().hide();
                        }
                    });
                    if ( windowHeight < ( elements.sidebarTop.height() + elements.navUserMenu.height()) ) {
                    /*
                        elements.navUserMenu.css({
                            'position': 'relative',
                            'padding-top': '26px'
                        });
                        */
                    } else {
                    /*
                        elements.navUserMenu.css({
                            'position': 'absolute',
                            'padding-top': '0px'
                        });
                        */
                    }
                } else {
                    $('.footerURL a').each(function(i,v){
                        if($(this).text()=="Register"){
                            $(this).parent().show();
                        }
                    });
                    elements.navUserMenu.css({
                        'position': '',
                        'padding-top': ''
                    });
                }

            };

            $(window).resize(windowResized);
            windowResized();
        },

        mobileFooter: function() {

	    $(window).on('to-desktop', function() {
                $('li.footerURL').slideDown();
		$("li.header").click(function(){
		    return false;
		});
            });

	    $(window).on('to-mobile', function() {
                $('li.footerURL').slideUp();
            });

		
	    var linkAccordion = function() {
		
		$("li.header").click(function(){
		    if (ScreenSizeIsMobile) {
			$("li.header i").removeClass("icon-arrow-small-up").addClass("icon-arrow-small-down");
			if ($(this).nextAll("li.footerURL").css("display") === "none") {
			    $("li.footerURL").slideUp();
			    $(this).nextAll("li.footerURL").slideDown();
			    $(this).children("i").removeClass("icon-arrow-small-down").addClass("icon-arrow-small-up");
			} else {
			    $(this).nextAll("li.footerURL").slideUp();
			}
			
			$('.footerURL a').each(function(i,v){
			    if($(this).text()=="Register"){
				$(this).parent().hide();
			    }
			});
		    }
		});
	    }
	    
	    linkAccordion();

        },

        mobileLightboxes: function() {

            var handleLightboxes = function() {

                var mobileAdjust = function() {
                    var w = $(window);
                    var windowWidth = w.width();
                    var windowHeight = w.height();

                    $(".lightboxDialog").css("width", windowWidth);
                    $(".lightbox").css("width", windowWidth - 20);
                    $(".lightbox").dialog("option", "position", {my: "center top"});
                };

                if (ScreenSizeIsMobile) {

                    // Re-jig lightboxes to resolve display issues on rotation.

                    $(".lightbox").on( "dialogopen", function() {
                        mobileAdjust();
                    } );

                    $(window).resize(mobileAdjust);
                }

            };

            handleLightboxes();

        },
		mobileCalendar: function( $inputCheckin,  $inputCheckout){
		    var calendarHolderInner = $('#Calendar .calendar-holder .inner');
			var clearAll = $('#Calendar .calendar-holder .clear-all');

			if($inputCheckin.val() && $inputCheckout.val()) {
				var selectedStart = $inputCheckin.val().split('-');
				var selectedEnd = $inputCheckout.val().split('-');
				var selectedDate = new Date();

				if (selectedStart.length == 3 && selectedEnd.length == 3) {
					selectedStart = new Date(selectedStart[0], selectedStart[1] - 1, selectedStart[2]);
					selectedEnd = new Date(selectedEnd[0], selectedEnd[1] - 1, selectedEnd[2]);

					selectedDate = [selectedStart, selectedEnd];
				}
			}
		    calendarHolderInner.DatePicker({
                flat: true,
                prev: '',
                next: '',
                date: selectedDate,
                format: 'b d, Y',
                starts: 1,
                calendars: 1,
                view: 'days',
                mode: 'range',
                onChange: function(formated, dates) {
					inputCheckinValue =  dates[0].getFullYear()
                                          + '-' + twoDigits(dates[0].getMonth() + 1)
                                          + '-' + twoDigits(dates[0].getDate());

                    $inputCheckin.val(inputCheckinValue);

                    $inputCheckout.val('');

                    if (formated[0] != formated[1]) {
						inputCheckoutValue = dates[1].getFullYear()
                                          + '-' + twoDigits(dates[1].getMonth() + 1)
                                          + '-' + twoDigits(dates[1].getDate());

                        $inputCheckout.val(inputCheckoutValue );
						$('#checkIn span').text(formated[0]);
                        $('#checkOut span').text(formated[1]);
                    }
                }
            });



            calendarHolderInner.DatePickerShow();

		    clearAll.click(function() {

			    $('#checkIn span').text('Check In');
                $('#checkOut span').text('Check Out');
                $inputCheckin.val('');
                $inputCheckout.val('');
                calendarHolderInner.DatePickerClear();
           });


		},
		mobileBookingCalendar: function(){
			var $inputCheckin = $('#bookingStart');
            var $inputCheckout = $('#bookingEnd');


		    dhM.mobileCalendar($inputCheckin, $inputCheckout);

			$('#checkIn, #checkOut').click(function(){
				$('#Calendar .calendar-holder').slideToggle();
			});
		},
        mobileWidgetCalendar: function(){
            /*var $inputCheckin = $('.control-mobile-in');
            var $inputCheckout = $('.control-mobile-out');
            dhM.mobileCalendar($inputCheckin, $inputCheckout);*/

        },
        /*
        mobileDealCalendar: function(){

            var $inputCheckin = $('#selectedStartDate');
            var $inputCheckout = $('#selectedEndDate');

		    dhM.mobileCalendar($inputCheckin, $inputCheckout);

			$('#bookNowDealMobile').click(function(){
			    bookDealhref = $('#bookNowDealMobile').attr('href');
				if ( bookDealhref.indexOf('&aD=') != -1) {
				    bookDealhref = bookDealhref.substr(0, bookDealhref.indexOf('&aD='));
				}
				if(bookDealhref !== undefined){
				    var startDate = $('#selectedStartDate').val().split('-');
					var endDate = $('#selectedEndDate').val().split('-');

					bookDealhref = bookDealhref+'&aD='+startDate[2]+'&aM='+startDate[1]+'&aY='+startDate[0]+'&dD='+endDate[2]+'&dM='+startDate[1]+'&dY='+startDate[0];
					$('#bookNowDealMobile').attr('href', bookDealhref);
				}





			});
			$(window).on('to-desktop', function() {
				dh.pdpBook();
			});

		},
        */
        mobileNoResultCalendar: function() {
            dhM.mobileCalendar($('#selectedStartDate'), $('#selectedEndDate'), false);

            $('#bookNowDealMobile').click(function(e){
			    // e.preventDefault();

                var link = '' + window.location.href;
                var startDate = $('#selectedStartDate').val();
                var endDate = $('#selectedEndDate').val();

                if (link.match(/([\?&]start=)[^&]+/)) {
                    link = link.replace(/([\?&]start=)[^&]+/, '$1' + startDate);
                } else {
                    link += '&start=' + startDate;
                }

                if (link.match(/([\?&]end=)[^&]+/)) {
                    link = link.replace(/([\?&]end=)[^&]+/, '$1' + endDate);
                } else {
                    link += '&end=' + endDate;
                }

                $(this).attr('href', link);
			});

            $('#Calendar .calendar-holder').show();
        },

        mobileSidebar: function() {
            var sidemenuSlider = function() {
                var sideBarPadding = 32;
                var menuVisible = false;
                var deviceWidth = $(window).width();
                var sidebarLeft = 0;

                $('div#sidebar').css({
                    'z-index': '99999',
                    'left': (deviceWidth * -1) + 'px',
                    'width': (deviceWidth - sideBarPadding) + 'px'
                });
                $('div.fixed-wrapper header#searchEngineMobile').parent().addClass('noslide');
                var nonSidebar = 'div#main, header#searchEngineMobile, #mainFooter, .mobile-footer, #bookingHeader, #bookingContentWrapper, #bookingFooter, .full-wrapper, .fixed-wrapper:not(.noslide)';

                // Sidebar has issues when device is rotated. The function below will hide the sidebar on rotate to quell these issues.
                //
                // var hideSidebarOnRotate = function() {
                    // deviceWidth = $(window).width();
                    // sidebarLeft = deviceWidth * -1;

                    // $(nonSidebar).css({
                        // left: "0"
                    // });

                    // if (ScreenSizeIsMobile) {
                        // $('div#sidebar').css({
                            // left: sidebarLeft + 'px'
                        // });
                    // }

                    // menuVisible = false;
                // };

                var clickHandler = function() {
                    var sidebarWidth = $('div#sidebar').outerWidth();

                    if (menuVisible) {
                        $(nonSidebar).animate({
                            left: '0'
                        }, 750);
                        $('div#sidebar').animate({
                            left: -sidebarWidth + 'px'
                        }, 750);
                    } else {
                        $(nonSidebar).animate({
                            left: sidebarWidth + 'px'
                        }, 750);
                        $('div#sidebar').animate({
                            left: '0'
                        }, 750);
                    }

                    menuVisible = !menuVisible;
                };

                $(window).on('to-desktop', function() {
                    // $('div#sidebar').css('left', '');

                    $(nonSidebar).css({
                        left: '0'
                    });
                });

                // $(window).on('to-mobile', function() {
                    // deviceWidth = $(window).width();
                    // sidebarLeft = deviceWidth * -1;
                    // $('div#sidebar').css('left', sidebarLeft + 'px');
                // });

                var adjustSidebar = function() {
                    if (ScreenSizeIsMobile) {
                        $('div#sidebar').css({
                            'width': ($(window).width() - sideBarPadding) + 'px'
                        });

                        if (!menuVisible) {
                            $('div#sidebar').css({
                                'left': ($(window).width() * -1) + 'px'
                            });
                        }

                        if (menuVisible) {
                            $(nonSidebar).css({
                                left: $(window).width() + 'px'
                            });
                        }
                    }
                };

                $('div#mobile-close, div.menu-button').click(clickHandler);

                $(window).resize(adjustSidebar);
            };

            var updateCurrency = function() {
                var selectCurrency = $('div.mobile-currency select');

                selectCurrency.change(function() {
                    var labelCurrency = $('.mobile-currency .selected-currency');
                    var activeCurrency = $(this).val();

                    labelCurrency.text(activeCurrency);

                    $.ajax({
                        type: "POST",
                        url: "/user/ajax/setcurrency",
                        data: {currency: activeCurrency}
                    }).done(function(msg) {
                        if (typeof msg.currency != 'undefined') {
                            location.reload();
                        }
                    });
                });
            };

            sidemenuSlider();
            updateCurrency();
        },

        mobileSearchAutocomplete: function() {
            var ajaxState = false;
            var input = $('#searchEngineMobile .search-box');
            var keyTimer = false;
            var autoSuggest = $('#searchEngineMobile .auto-suggest');
            var searchResults = $('#searchEngineMobile .auto-suggest li:first-child');
            var hotelCount = $('#searchEngineMobile .hotels-and-resorts .count');
            var hotelDiv = $('#searchEngineMobile .hotels-and-resorts');
            var searchButton = $('#searchEngineMobile .search .button');
            var searchBox = $('#searchEngineMobile .search-box');
            var selectAdults = $('#searchEngineMobile .select-adults');
            var selectChildren = $('#searchEngineMobile .select-children');
            var inputCheckin = $('#searchEngineMobile .input-checkin');
            var inputCheckout = $('#searchEngineMobile .input-checkout');

            input.keyup(function() {
                if (keyTimer) {
                    clearTimeout(keyTimer);
                    keyTimer = false;
                }

                keyTimer = setTimeout(function() {
                    doAjax();
                }, 200);
            });

            var doAjax = function() {
                if (ajaxState) {
                    ajaxState.abort();
                    ajaxState = false;
                }
                
                if (typeof(DH_CURRENT_VERSION) == 'undefined') {
                    DH_CURRENT_VERSION = 'master';
                }
                
                ajaxState = $.ajax({
                    type: "GET",
                    url: '/auto-complete/',
                    cache: true,
                    data: {"q": input.val(), "max": 99, "lang": "en", "version": DH_CURRENT_VERSION},
                    success: function(response) {
                        ajaxState = false;
                        autoSuggest.show();

                        searchResults.empty();
                        searchResults.hide();

                        if (response.suggestions.length) {
                            searchResults.show();
                        }

                        for (var i = 0; i < response.suggestions.length; i++) {
                            if (i == 5) {
                                break;
                            }
                            
                            var suggestion = response.suggestions_type[i];

                            var div = $(
                                '<div class="suggestion suggestion-' + suggestion.type 
                                + '"></div>'
                            );
                            
                            var icon = $(
                                '<span class="icon '
                                + (
                                    suggestion.type == 'property'
                                    ? SearchBarAutoCompleteIconProperty
                                    : SearchBarAutoCompleteIconLocation
                                )
                                + '"></span>'
                            );
                            
                            var label = $('<span class="label"></span>');
                            label.text(suggestion.suggestion);
                            
                            div.append(icon);
                            div.append(label);

                            searchResults.append(div);

                            div.click(function() {
                                var suggestion = $(this).text();

                                input.val(suggestion);
                                autoSuggest.hide();
                            });
                        }

                        hotelCount.text('(' + response.properties.length + ')');
                    }
                });
            }

            $(document).click(function() {
                setTimeout(function() {
                    autoSuggest.hide();
                }, 200);
            });

            var doSearch = function() {
                var hasDates = false;
                var numAdults = selectAdults.val();
                var numChildren = selectChildren.val();
                var child_age_url = '';
                var start = inputCheckin.val();
                var end = inputCheckout.val();
                var keyword = searchBox.val();
                var thisIsHotelsNearMePage = false;
                var lat = false;
                var lng = false;

                if (
                    typeof(currentLocationLat) != 'undefined'
                    && typeof(currentLocationLng) != 'undefined'
                ) {
                    thisIsHotelsNearMePage = true;
                    lat = currentLocationLat;
                    lng = currentLocationLng;
                }

                if (keyword == '' && !thisIsHotelsNearMePage) {
                    searchBox.addClass('attention');
                    return;
                }

                if (!numAdults) {
                    numAdults = '2';
                }

                if (!numChildren) {
                    numChildren = '0';
                } else {    // there are children
                    var children_obj = {};
                    for (var count = 0; count < numChildren; count++) {
                        // get the age of this child
                        // where the ages are store in the DOM depends on the screen width
                        if ($(window).width() < 769) {    // mobile
                            // search in the lightbox for avail widget
                            var age_value = $('#lightboxChildAgeMenuSearch span#ageForChildMobile' + count);
                        } else {  // desktop
                            // search in the avail widget
                            var age_value = $('#search-bar li#ageForChild' + count)
                                    .find('span.spinnerValue');
                        }
                        var age = parseInt(age_value.text());
                        children_obj[count] = age;
                    }
                    child_age_url = '&ages=' + encodeURIComponent(JSON.stringify(children_obj));
                }

                if (keyword == '') {
                    var location = '/search?lat=' + lat + '&lng=' + lng
                        + '&adults=' + numAdults + '&children=' + numChildren + child_age_url;
                } else {
                    var location = '/search?q=' + encodeURIComponent(keyword)
                        + '&adults=' + numAdults + '&children=' + numChildren + child_age_url;
                }

                if (start && end) {
                    location += '&start=' + start + '&end=' + end;
                }

                window.location = location;
            }

            searchButton.click(function() {
                doSearch();
            });

            hotelDiv.click(function() {
                doSearch();
            });
        },

        mobileSearchBar: function() {
            var searchOpen = false;
            var extendedSearchOpen = false;
            var calendarOpen = false;
            var isAnimating = false;
            var searchAfterLastDate = false;

            var searchButton = $('#searchEngineMobile .search-button');
            var searchDiv = $('#searchEngineMobile .search');
            var searchBox = $('#searchEngineMobile .search-box');
            var searchMore = $('#searchEngineMobile .search-more');
            var selectAdults = $('#searchEngineMobile .select-adults');
            var labelAdults = $('#searchEngineMobile .label-adults');
            var selectChildren = $('#searchEngineMobile .select-children');
            var labelChildren = $('#searchEngineMobile .label-children');
            var calendarHolder = $('#searchEngineMobile .calendar-holder');
            var calendarHolderInner = $('#searchEngineMobile .calendar-holder .inner');
            var labelCheckin = $('#searchEngineMobile .label-checkin');
            var labelCheckout = $('#searchEngineMobile .label-checkout');
            var inputCheckin = $('#searchEngineMobile .input-checkin');
            var inputCheckout = $('#searchEngineMobile .input-checkout');
            var clearAll = $('#searchEngineMobile .calendar-holder .clear-all');
            var search = $('#searchEngineMobile');
            var wrapper = $('#wrapper');
            var geoButton = $('#searchEngineMobile .geo-location-button');

        var updateChildAge = function(select, child_age) {
            var ageLabel = $(select).parent().siblings('span.age');
            var arrow_icon = $(select).parent().siblings('span.icon');
            ageLabel.text(child_age);
            // show the age
            ageLabel.css('display', 'block');
            // need to hide the arrow
            arrow_icon.css('display', 'none');
        };

        var buildChildAgeMenuForAvailSearch = function(children) {
            // first empty out the lighbox
            $('#lightboxChildAgeMenuSearch div.childWrapper').remove();
            // add the children number of child age selectors
            for (var count = children-1; count >= 0; count--) {
                
                var age_options = '';
                for (var i = 1; i < 18; i++) {
                    age_options += '<option>' + i + '</option>';
                }
                var child_html = '<div class="childWrapper">' + 
                                  '<div class="control control-children">' + 
                                  '<div class="label"><span>Child\'s age</span><select>' + 
                                          '<option selected>0</option>' + age_options + 
                                  '</select></div>' + 
                                  '<span class="icon icon-arrow-down-black"></span>' + 
                                  '<span class="age" id="ageForChildMobile' + count + '"></span>' + 
                                  '</div>' + 
                                '</div>';
                // add this to the lightbox
                $('#lightboxChildAgeMenuSearch div.content').after(child_html);
            }
            // update the children ages from selects
            $('#lightboxChildAgeMenuSearch select').change(function(e) {
                child_age = $(this).val();
                el = e.target;
                updateChildAge(el, child_age);
            });

            $('#lightboxChildAgeMenuSearch div.lightboxAgeMenuClose').click(function(e) {
                $("#lightboxChildAgeMenuSearch").dialog('close');
            });
        };
            if (typeof(pleaseOpenTheSearchBar) == 'undefined') {
                pleaseOpenTheSearchBar = false;
            }

            searchButton.click(function() {
                if (isAnimating) {
                    return;
                }

                isAnimating = true;

                if (searchOpen) {
                    searchDiv.slideUp({
                        complete: function() {
                            isAnimating = false;
                            searchOpen = false;

                            searchMore.hide();
                            extendedSearchOpen = false;
                            searchAfterLastDate = false;
                            searchButton.css('opacity', 1);

                            wrapper.css('padding-top', '');
                        }
                    });
                } else {
                    searchDiv.slideDown({
                        complete: function() {
                            isAnimating = false;
                            searchOpen = true;
                            searchButton.css('opacity', 0.5);

                            if (
                                pleaseOpenTheSearchBar
                                && ScreenSizeIsMobile
                            ) {
                                // Because search is open by default on homepage
                                // we need to move the content down a bit so it
                                // is not partly obscured.
                                wrapper.css('padding-top', '54px');
                            }
                        }
                    });
                }
            });

            $(window).on('to-desktop', function() {
                wrapper.css('padding-top', '');
            });

            $(window).on('to-mobile', function() {
                if (
                    pleaseOpenTheSearchBar
                    && ScreenSizeIsMobile
                    && searchOpen
                ) {
                    wrapper.css('padding-top', '54px');
                }
            });

            searchBox.click(function() {
                searchBox.removeClass('attention');

                if (isAnimating || extendedSearchOpen) {
                    return;
                }

                searchMore.slideDown({
                    complete: function() {
                        search.css('position','absolute');
                        window.scrollTo(0,0);
                        isAnimating = false;
                        extendedSearchOpen = true;
                    }
                });
            });

            $('#hotelsNav .search-details span').click(function() {
                if (extendedSearchOpen && searchOpen) {
                    return;
                }

                if (!searchOpen) {
                    searchMore.show();

                    searchDiv.slideDown({
                        complete: function() {
                            isAnimating = false;
                            searchOpen = true;
                            searchButton.css('opacity', 0.5);
                            extendedSearchOpen = true;
                        }
                    });
                } else {
                    searchMore.slideDown({
                        complete: function() {
                            isAnimating = false;
                            extendedSearchOpen = true;
                        }
                    });
                }
            });

            selectAdults.change(function() {
                var number = $(this).val();

                var text = (number == 1 ? ' Adult' : ' Adults');
                labelAdults.text(number + text);
            });

            selectChildren.change(function() {
                var number = $(this).val();

                var text = (number == 1 ? ' Child' : ' Children');
                labelChildren.text(number + text);
                
                // now open the child age menu....
                $("#lightboxChildAgeMenuSearch").dialog('open');
                buildChildAgeMenuForAvailSearch(number);
            });

            if (inputCheckin.length && inputCheckin.length){
                var selectedStart = inputCheckin.val().split('-');
                var selectedEnd = inputCheckout.val().split('-');
                var selectedDate = new Date();

                if (selectedStart.length == 3 && selectedEnd.length == 3) {
                    selectedStart = new Date(selectedStart[0], selectedStart[1] - 1, selectedStart[2]);
                    selectedEnd = new Date(selectedEnd[0], selectedEnd[1] - 1, selectedEnd[2]);

                    selectedDate = [selectedStart, selectedEnd];
                }

                calendarHolderInner.DatePicker({
                    flat: true,
                    prev: '',
                    next: '',
                    date: selectedDate,
                    format: 'b d, Y',
                    starts: 1,
                    calendars: 1,
                    view: 'days',
                    mode: 'range',
                    onChange: function(formated, dates) {
                        labelCheckin.text(formated[0]);
                        inputCheckin.val(
                            dates[0].getFullYear()
                            + '-' + twoDigits(dates[0].getMonth() + 1)
                            + '-' + twoDigits(dates[0].getDate())
                        );

                        labelCheckout.text('Check Out');
                        inputCheckout.val('');

                        if (formated[0] != formated[1]) {
                            labelCheckout.text(formated[1]);
                            inputCheckout.val(
                                dates[1].getFullYear()
                                + '-' + twoDigits(dates[1].getMonth() + 1)
                                + '-' + twoDigits(dates[1].getDate())
                            );

                            toggleCalendar();

                            if (searchAfterLastDate) {
                                $('#searchEngineMobile .search .button').trigger('click');
                            }
                        }
                    }
                });
            }
            
            clearAll.click(function() {
                labelCheckin.text('Check In');
                labelCheckout.text('Check Out');
                inputCheckin.val('');
                inputCheckout.val('');
                calendarHolderInner.DatePickerClear();

                toggleCalendar();
            });

            calendarHolderInner.DatePickerShow();

            var toggleCalendar = function() {
                if (isAnimating) {
                    return;
                }

                isAnimating = true;

                if (calendarOpen) {
                    calendarHolder.slideUp({
                        complete: function() {
                            isAnimating = false;
                            calendarOpen = false;
                        }
                    });
                } else {
                    calendarHolder.slideDown({
                        complete: function() {
                            isAnimating = false;
                            calendarOpen = true;
                        }
                    });
                }
            }
            
            $(document).mousedown(function(e) {
                if (!extendedSearchOpen) {
                    return;
                }

                var target = $(e.target);

                if (
                    target.hasClass('search-more')
                    || target.hasClass('search-box')
                    || target.hasClass('search-button')
                    || target.closest('.search-more').length
                    || target.closest('.search-box').length
                    || target.closest('.search-button').length
                    || target.closest('.auto-suggest').length
                ) {
                    return;
                }

                extendedSearchOpen = false;
                isAnimating = true;

                searchMore.slideUp({
                    complete: function() {
                        search.css('position','fixed');
                        isAnimating = false;
                    }
                });
            });

            labelCheckin.click(toggleCalendar);
            labelCheckout.click(toggleCalendar);

            $('#mobileFooterCheckAvailability').click(function() {
                if (extendedSearchOpen && searchOpen && calendarOpen) {
                    return;
                }

                isAnimating = true;
                searchAfterLastDate = true;

                if (!searchOpen) {
                    searchMore.show();
                    calendarHolder.show();

                    searchDiv.slideDown({
                        complete: function() {
                            isAnimating = false;
                            searchOpen = true;
                            searchButton.css('opacity', 0.5);
                            extendedSearchOpen = true;
                            calendarOpen = true;
                        }
                    });
                } else if (!extendedSearchOpen) {
                    calendarHolder.show();

                    searchMore.slideDown({
                        complete: function() {
                            isAnimating = false;
                            extendedSearchOpen = true;
                            calendarOpen = true;
                        }
                    });
                } else {
                    calendarHolder.slideDown({
                        complete: function() {
                            isAnimating = false;
                            calendarOpen = true;
                        }
                    });
                }
            });

            $('#mobileFooterBookNow').click(function() {
                $('#searchEngineMobile .search .button').trigger('click');
            });

            if (pleaseOpenTheSearchBar) {
                searchButton.trigger('click');
            }

            $('.mobile-footer-location, #searchEngineMobile .geo-location-button').click(function() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        function(pos) {
                            var lat = pos.coords.latitude;
                            var lng = pos.coords.longitude;
			    // controller expects a 'q' param
                            window.location.href = '/search?q=geoloc&lat=' + lat + '&lng=' + lng;
                        },
                        function() {
                            alert('Unable to determine the current location. Please make sure your location service is turned on.');
                        }
                        , {
                            //enableHighAccuracy: false,
                            timeout: 10000,
                            maximumAge: 10000
                        }
                    );
                }
            });
        },
        
        mobileAvailabilityWidget: function() {
            if ($('#availability-widget .control-mobile-in').length) {
                var calendar = new DhBookingCalendarMobile(
                    'mobile',
                    $('#availability-widget-holder .calendar-holder'),
                    $('#availability-widget-holder .calendar-holder .inner'),
                    $('#availability-widget .control-mobile-in'),
                    $('#availability-widget .control-mobile-out'),
                    $('#availability-widget .control-mobile-in .label'),
                    $('#availability-widget .control-mobile-in .date-value'),
                    $('#availability-widget .control-mobile-out .label'),
                    $('#availability-widget .control-mobile-out .date-value')
                );
                
                calendar.init();
            } else if ($('#availability-widget-search .control-mobile-in').length) {
                var calendar = new DhBookingCalendarMobile(
                    'mobile',
                    $('#availability-widget-search-holder .calendar-holder'),
                    $('#availability-widget-search-holder .calendar-holder .inner'),
                    $('#availability-widget-search .control-mobile-in'),
                    $('#availability-widget-search .control-mobile-out'),
                    $('#availability-widget-search .control-mobile-in .label'),
                    $('#availability-widget-search .control-mobile-in .date-value'),
                    $('#availability-widget-search .control-mobile-out .label'),
                    $('#availability-widget-search .control-mobile-out .date-value')
                );
                
                calendar.init();
            }                        
            return;

            
            var widgetCalendarOpen = false;
            var isAnimating = false;
            var searchAfterLastDate = false;
            
            var now = new Date();
            var dataUnavailable = {};
            var dataNoArrival = {};
            var dataUnavailableLoading = {};
            var currentDateMonth = new Date(now.getTime());
            currentDateMonth.setMonth(currentDateMonth.getMonth() + 1);
            
            var calendarWidgetHolder = $('#availability-widget-holder .calendar-holder');
            var calendarHolderInner = $('#availability-widget-holder .calendar-holder .inner');
            var labelCheckin = $('#availability-widget .control-mobile-in');
            var labelCheckout = $('#availability-widget .control-mobile-out');
            var inputCheckin = $('#availability-widget .control-mobile-in .date-value');
            var inputCheckout = $('#availability-widget .control-mobile-out .date-value');
            var clearAll = $('#availability-widget .calendar-holder .clear-all');
            var parentName = '#availability-widget';
            
            if (!labelCheckin.length) {
                calendarWidgetHolder = $('#availability-widget-search-holder .calendar-holder');
                calendarHolderInner = $('#availability-widget-search-holder .calendar-holder .inner');
                labelCheckin = $('#availability-widget-search .control-mobile-in');
                labelCheckout = $('#availability-widget-search .control-mobile-out');
                inputCheckin = $('#availability-widget-search .control-mobile-in .date-value');
                inputCheckout = $('#availability-widget-search .control-mobile-out .date-value');
                clearAll = $('#availability-widget-search .calendar-holder .clear-all');
                parentName = '#availability-widget-search';
                
                if (!labelCheckin.length) {
                    return;
                }
            }
            
            var renderCalendarAvailability = function() {
                var yearMonth1 = currentDateMonth.getFullYear() + '_' + ('0' + (currentDateMonth.getMonth() + 1)).slice(-2);
                
                $(parentName + ' .calendar-holder .datepickerDays td').addClass('date-not-loaded');
                
                if (typeof(dataUnavailable[yearMonth1]) != 'undefined') {
                    var parentTd = $($(parentName + ' .calendar-holder .datepickerContainer > table > tbody > tr > td')[0]);
                    var data = jQuery.parseJSON(dataUnavailable[yearMonth1]);
                    renderCalendarAvailabilityRenderDays(parentTd, data);
                }
            }
            
            var renderCalendarAvailabilityRenderDays = function(parent, data) {
                var tds = parent.find('.datepickerDays td');
                tds.removeClass('date-not-loaded');
                
                for (var i=0; i<tds.length; i++) {
                    var td = $(tds[i]);
                    
                    if (
                        td.hasClass('datepickerNotInMonth')
                        || td.hasClass('datepickerDisabled')
                    ) {
                        continue;
                    }
                    
                    td.addClass('date-available');
                }
                
                for (var i=0; i<data.length; i++) {
                    var start = data[i].start.split('/');
                    var end = data[i].end.split('/');
                    
                    for (var j = parseInt(start[1], 10); j <= parseInt(end[1], 10); j++) {
                        var dayTd = parent.find('.day' + j)
                        dayTd.addClass('date-booked');
                    }
                }
            }
            
            var getAvailabilityForMonth = function(date) {
                var year = date.getFullYear();
                var month = date.getMonth() + 1;
                var yearMonth = year + '_' + ('0' + month).slice(-2);
                
                if (
                    typeof(dataUnavailable[yearMonth]) != 'undefined'
                    || typeof(dataUnavailableLoading[yearMonth]) != 'undefined'
                ) {
                    return;
                }
                
                dataUnavailableLoading[yearMonth] = true;
                
                var startDate = year + '/' + ('0' + month).slice(-2) + '/01';
                var endDate = year + '/' + ('0' + month).slice(-2) + '/' + daysInMonth(month, year);
               
                
                AvailabilityService.get(date, function(data) {                    
                    dataUnavailable[yearMonth] = data;
                    renderCalendarAvailability();
                }, false);
            }
            
            getAvailabilityForMonth(now);
            now.setMonth(now.getMonth( ) + 1);
            getAvailabilityForMonth(now);
            
            var selectedStart = inputCheckin.val().split('-');
            var selectedEnd = inputCheckout.val().split('-');
            var selectedDate = new Date();
            
            if (selectedStart.length == 3 && selectedEnd.length == 3) {
                selectedStart = new Date(selectedStart[0], selectedStart[1] - 1, selectedStart[2]);
                selectedEnd = new Date(selectedEnd[0], selectedEnd[1] - 1, selectedEnd[2]);

                selectedDate = [selectedStart, selectedEnd];
            }
            
            calendarHolderInner.DatePicker({
                flat: true,
                prev: '',
                next: '',
                date: selectedDate,
                format: 'b d, Y',
                starts: 1,
                calendars: 1,
                view: 'days',
                mode: 'range',
                onFill: function() {
                    renderCalendarAvailability();
                },
                onMonthChange: function(firstDate) {
                    var date = new Date(firstDate), y_start = date.getFullYear(), m_start = date.getMonth()+1, d_start = date.getDate();
                    currentDateMonth = new Date(date.getTime());
                    currentDateMonth.setMonth(currentDateMonth.getMonth() + 1);
                    
                    var yearMonth1 = currentDateMonth.getFullYear() + '_' + ('0' + (currentDateMonth.getMonth() + 1)).slice(-2);
                    
                    getAvailabilityForMonth(currentDateMonth);
                },
                onChange: function(formated, dates) {
                    labelCheckin.find('.label').text(formated[0]);
                    inputCheckin.val(
                        dates[0].getFullYear()
                        + '-' + twoDigits(dates[0].getMonth() + 1)
                        + '-' + twoDigits(dates[0].getDate())
                    );

                    labelCheckout.find('.label').text('Check Out');
                    inputCheckout.val('');

                    if (formated[0] != formated[1]) {
                        labelCheckout.find('.label').text(formated[1]);
                        inputCheckout.val(
                            dates[1].getFullYear()
                            + '-' + twoDigits(dates[1].getMonth() + 1)
                            + '-' + twoDigits(dates[1].getDate())
                        );

                        toggleCalendar();

                        if (searchAfterLastDate) {
                            $('#searchEngineMobile .search .button').trigger('click');
                        }
                    }
                },
                onShow: function() {
                    renderCalendarAvailability();
                }
            });
            
            calendarWidgetHolder.find('.datePickerPrevMonth').click(function() {
                calendarWidgetHolder.find('.datepickerGoPrev').trigger('click');
            });
            
            calendarWidgetHolder.find('.datePickerNextMonth').click(function() {
                calendarWidgetHolder.find('.datepickerGoNext').trigger('click');
            });
            
            calendarHolderInner.DatePickerShow();
            
            var toggleCalendar = function() {
                if (isAnimating) {
                    return;
                }

                isAnimating = true;

                if (widgetCalendarOpen) {
                    calendarWidgetHolder.slideUp({
                        complete: function() {
                            isAnimating = false;
                            widgetCalendarOpen = false;
                        }
                    });
                } else {
                    calendarWidgetHolder.slideDown({
                        complete: function() {
                            isAnimating = false;
                            widgetCalendarOpen = true;
                        }
                    });
                }
            }
            
            clearAll.click(function() {
                labelCheckin.find('.label').text('Check In');
                labelCheckout.find('.label').text('Check Out');
                inputCheckin.val('');
                inputCheckout.val('');
                calendarHolderInner.DatePickerClear();
                
                toggleCalendar();
            });
            
            labelCheckin.click(toggleCalendar);
            labelCheckout.click(toggleCalendar);
        },

        mobileImageSwipe: function() {
            var imageSwipes = $('.mobile-image-swipe');

            if (!imageSwipes.length) {
                return;
            }

            var swipes = [];
            imageSwipes.each(function() {
                var swipe = new MobileSwipeImage($(this));
                swipe.init();
                swipes.push(swipe);
            });
        },

        mobilePdpSearch: function() {
            var sortSelect = $('#sort #sortings_dd');
            var mobileLabel = $('#sort .navValue');

            var updateSorting = function() {
                var selectedOption = sortSelect.find('option:selected');

                if (!selectedOption.length) {
                    return;
                }

                mobileLabel.text(selectedOption.text());
                dh.sortListings();
            }

            sortSelect.change(updateSorting);

            $('.hotelImage > a').click(function(e) {
                if (!ScreenSizeIsMobile) {
                    // return;
                }

                e.stopPropagation();
                e.preventDefault();
            });

            // This happens inside MobileSwipeImage now
            /*$('.hotelImage .overlay').click(function() {
                var a = $(this);

                var images = $.parseJSON(a.attr('data-images'));
                var hotel = a.data('hotel');
                var city = a.data('city');

                var gallery = new MobileImageGallery(images, hotel, city);
                gallery.init();
            });*/
        },

        // Home Layout Functions
		mobileHomePage: function() {
		    var fixHomeBannerMargin = function(){
				//fix margin in main banner

				$(window).resize(function() {
	                if($(window).width() > MobileScreenSize){
					    $('#homepageBanner').css('margin-bottom', '70px' );
						return false;
					}
					var maxHeight = 0;
					$( "#homepageBanner .slides .homepageBannerCopy " ).each(function( ) {
						currentHeight = $( this ).height();
						if (currentHeight >= maxHeight){
							maxHeight = currentHeight;
						}
					});
					$('#homepageBanner').css('margin-bottom', maxHeight+35 );

				});


			}
			var desktopHomePage = function(){
				var allPanels = $('.accordionMobile').show();
				$('#original_mobile').hide();
				$('#topDestinations header').css('border-bottom','0');
				$('#topDestinations .sectionHeadings').css('border-bottom','1px solid #fff');
				$('#topDestinationFull').css('display','block');
				$('#madeByOriginals header').css('border-bottom','0');
				$('#newValueProposition header').css('border-bottom','0');
				$('.sectionSubHeading').show();
				$('#homepageBanner').css('margin-bottom','70px');
			}

			var allPanels = $('.accordionMobile');
			fixHomeBannerMargin();

			$(window).on('to-mobile', function() {
				allPanels.hide();

				$('#topDestinations .sectionSubHeading').hide();
				$('.mobile_arrows').css('background-position','-283px -132px');
				$('#main header').css('border-bottom','1px solid #c7c7c7');
				$('#main .sectionSubHeading').hide();
			});
			$(window).on('to-desktop', function() {
				desktopHomePage();
			});

			// Accordion panels
			$('#main header').click(function(ev) {

				ev.stopPropagation();
				if (ScreenSizeIsMobile) {
					$('.mobile_arrows').css('background-position','-283px -132px');
					$('#main header').css('border-bottom','1px solid #c7c7c7');
					$('#main .sectionSubHeading').hide();

					if(false == $(this).next().is(':visible')) {
						allPanels.slideUp();
						if( $(this).parent().attr('id') != 'topDestinations'){
							$(this).find('.sectionSubHeading').show();
						}
						$(this).find('.mobile_arrows').css('background-position','-268px -132px');
						$(this).css('border-bottom','0');
						$(this).find('.sectionHeadings').css('border-bottom','0');
					}

					$(this).next().slideToggle();

					return false;
				}
			});

			//top destinations accordiion
			$('.mobiletab').click(function(ev) {
				ev.stopPropagation();
				if (ScreenSizeIsMobile) {
					$('.mobile_arrows_cities').css('background-position','-304px -133px');

					if(false == $(this).next().is(':visible')) {
						$(this).find('.mobile_arrows_cities').css('background-position','-317px -133px');
						$('.topDestinationFull').slideUp();
					}

					$(this).next().slideToggle();
				}
			});


		},

		//PDP Layout page
		mobilePdpOverview: function(){

		    $('#mobile-facts-btn').click(function(){
				$('#mobile-facts-info').slideToggle();
			});


			$('#pdpImage .fancybox').click(function(e) {

                if (ScreenSizeIsMobile) {

				    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }


            });

            //architecture tab
			$('.architecture-mobile .contentbox:first .blockDivider').hide();
		},

        mobilePdpTabs: function(){

            // Conditional for determining if scrolling tabs should show

            var accum_width;
            var viewport_width;
            
            var tabArrowsDrawn = false;
            var drawTabArrows = function() {
                if (tabArrowsDrawn) {
                    return;
                }
                
                tabArrowsDrawn = true;
                
                $('.mobileTabs').scrollbox({
                    direction: 'h',
                    linear: false,
                    delay: 0,
                    step: 1,
                    speed: 30,
                    autoPlay: false,
                    onMouseOverPause: true
                });

                $('.prev-tab').click(function () {
                    $('.mobileTabs').trigger('backward');
                });
                $('.next-tab').click(function () {
                    $('.mobileTabs').trigger('forward');
                });
            };

            var recalculate = function() {
                viewport_width = $(window).width();
                
                accum_width = 0;
                $('#pdpTabs').find('.tab-nav, li').each(function() {
                    accum_width += $(this).outerWidth();
                });
                
                if (accum_width > viewport_width) {
                    $('.tab-nav').css("display","block");
                    drawTabArrows();
                } else {
                    if(ScreenSizeIsMobile){
                        $('.tab-nav').css("display","none");
                        $('#pdpTabs').css("padding","0");
                    }else{
                        $('#pdpTabs').css("padding","0 35px");
                    }
                }
            }

            $(window).resize( function() {
                var viewport_width = $(window).width();

                if (accum_width > viewport_width && ScreenSizeIsMobile) {
                    $('.tab-nav').css("display","block");
                    $('#pdpTabs').css("padding","0 30px");
                } else if(accum_width <= viewport_width && ScreenSizeIsMobile) {
                    $('.tab-nav').css("display","none");
                    $('#pdpTabs').css("padding","0");
                }else{
                    $('.tab-nav').css("display","none");
                    $('#pdpTabs').css("padding","0 35px");
                }
            });
            
            $(window).on('to-mobile', function() {
                $('.mobileTabs ul.slides .tabDivider').remove();
                recalculate();
            });
            
            $(window).on('to-desktop', function() {
                $('.mobileTabs ul.slides .tabDivider').remove();
                var first = true;
                
                $('.mobileTabs ul.slides li').each(function() {
                    if (first) {
                        first = false;
                    } else {
                        $(this).prepend('<li class="tabDivider desktop-only"></li>');
                    }
                });
                
                recalculate();
            });
        },

		mobilePdpMap: function(){
			if(ScreenSizeIsMobile){

				var mapCenter = new google.maps.LatLng(propertyInfo.latitude, propertyInfo.longitude);

                var myOptions = {
                    backgroundColor    : '#ececec',
                    center             : mapCenter,
                    disableDefaultUI   : true,
                    disableDoubleClickZoom: false,
                    draggable          : true,
                    keyboardShortcuts  : false,
                    mapTypeControl     : false,
                    mapTypeId          : google.maps.MapTypeId.ROADMAP,
                    scrollwheel        : false,
                    zoomControl        : true,
                    streetViewControl  : false,
                    zoom               : 13,
                    panControl         : false
                };

                var mobileMap = new google.maps.Map(document.getElementById('mobileMapCanvas'), myOptions);
                dh.mapStyle(mobileMap);
                var lnglat = new google.maps.LatLng(propertyInfo.latitude, propertyInfo.longitude);
                var marker = new google.maps.Marker({
                    position: lnglat,
                    map: mobileMap,
                    icon: new google.maps.MarkerImage("assets/images/sprites.png", new google.maps.Size(60, 36), new google.maps.Point(0, 86)),
                });

                var boxText = document.createElement("div");
                boxText.style.cssText = " margin-top: 8px; background: #fff; padding: 5px;";
                content = '<span class="arrow"></span>'+
				          '<p class="name">' + propertyInfo.languages[lang].name + '</p>' +
						  '<p class="address">' + propertyInfo.languages[lang].address1 + '<br/>' + propertyInfo.languages[lang].address2 + '</p>';
			    if ($(window).width() <= MobileScreenSize) {
					content += '<p class="directions"><a href="http://maps.google.com/maps?q='+ propertyInfo.languages[lang].name+'@'+propertyInfo.latitude+','+propertyInfo.longitude+'&saddr=Current%20Location&daddr='+propertyInfo.languages[lang].address1+','+propertyInfo.languages[lang].address2+'">View Directions</a></p>';
				} else {
					content += '<p class="directions"><a href="http://maps.google.com/maps?q='+propertyInfo.languages[lang].name+'@'+propertyInfo.latitude+','+propertyInfo.longitude+'">View Directions</a></p>';

				}

                infobox = new InfoBox({
                    latlng: marker.getPosition(),
                    map: mobileMap,
                    boxClass: 'infobox',
					content:content,
					closeBoxURL: 'assets/images/mapClose.png',
					closeBoxMargin: "-15px",
                    alignBottom: true,
                    disableAutoPan: true
                });

                infobox.open(mobileMap, marker);



				google.maps.event.addListener(marker, 'click', function() {
					infobox.open(mobileMap, this);
				});



				var mapVisible = false;
				var wrapper = $('#wrapper');
				var wrapperOverflow = wrapper.css('overflow');

				$('#mapView').click(function() {
					mapVisible = true;
					wrapper.css('overflow', 'visible');

					$(document.body).addClass('mobile-map-visible');
					resize();

					google.maps.event.trigger(mobileMap, "resize");
					mobileMap.setCenter(mapCenter);

				});

				$('#close-map').click(function() {
					mapVisible = false;
					wrapper.css('overflow', '');
					$(document.body).removeClass('mobile-map-visible');
					$(window).trigger('resize');
				});



				$(window).resize(function() {
					resize();
				});

			}
					$(window).on('to-desktop', function() {
					$('#mobileMapCanvas').css('height', '');

					setMapOption({
						zoomControlOptions: {
							style: google.maps.ZoomControlStyle.LARGE,
							position: google.maps.ControlPosition.RIGHT_TOP
						}
					});
				});

				$(window).on('to-mobile', function() {
					resize();

					setMapOption({
						zoomControlOptions: {
							style: google.maps.ZoomControlStyle.SMALL,
							position: google.maps.ControlPosition.RIGHT_BOTTOM
						}
					});
				});

				var setMapOption = function(option) {
					if (typeof(mobileMap) == 'undefined') {
						setTimeout(function() {
							setMapOption(option);
						}, 500);

						return;
					}

					mobileMap.setOptions(option);
				}
				var resize = function() {
					currentHeight = $(window).height();

					if (!ScreenSizeIsMobile || !mapVisible) {
						return;
					}

					updateMapForMobile();
				}
				var updateMapForMobile = function() {
					$('#mobileMapCanvas').height(currentHeight);
				}
		},
		//Booking Flow
        mobileStep1: function(){
		    if($('#bookingContent').hasClass('step1')){
				var $selectAdults = $('#bookingAdults');
				var $selectChildren = $('#bookingChildren');
				var $labelAdults = $('.display-adults');
				var $labelChildren =  $('.display-children');

				$selectAdults.change(function() {
					var number = $(this).val();
					$labelAdults.text(number);
				});

				$selectChildren.change(function() {
					var number = $(this).val();
					$labelChildren.text(number );
				});

				dhM.mobileBookingCalendar();
		    }
		},
		mobileStep2: function(){
		    if($('#bookingContent').hasClass('step2')){
			    $('.bookingRooms .callfancybox').click(function(e) {

					if (ScreenSizeIsMobile) {
						e.stopPropagation();
						e.preventDefault();
						return false;
					}
                });
				$('.bookingRoomImage .overlay').click(function() {

					var images = $.parseJSON($(this).attr('data-images'));
					var hotel = $(this).data('hotel');
					var city = $(this).data('city');

					var gallery = new MobileImageGallery(images, hotel, city);
					gallery.init();
				});

                $('select#bookingRoomsNumber').change(function() {
					var number = $(this).val();
					$('.display-rooms').text(number );
					$('.bookingRoomValue').val(number );
				});

				if(ScreenSizeIsMobile){
                    $('li.bookingRoomRate').each(function(i,v){
                        $(this).addClass('open');
                    });
				}
                //$('select').prop('disabled', 'disabled');
			}
		},
		mobileStep3: function() {
		    $('#bookingLogin p a').click( function(){
                $(window).scrollTop(0);

                //setTimeout(function() {
                    $('.callLogin').trigger('click');
                //}, 200);
		    });
            
            $('.messageBar span.right a').click( function(){
                $(window).scrollTop(0);

                //setTimeout(function() {
                    $('.callLogin').trigger('click');
                //}, 200);
		    });
		},

        hotelsAndResorts: function() {
            $('.regions-and-countries-mobile .region-heading').click(function() {
                var region = $(this).closest('.region');
                var countries = region.find('.countries');

                var needToOpen = !region.hasClass('region-open');

                $('.regions-and-countries-mobile .region-open').each(function() {
                    $(this).find('.countries').hide();
                    $(this).removeClass('region-open');
                });

                if (needToOpen) {
                    region.addClass('region-open');
                    countries.slideDown();

                    var scrollTarget = region.offset().top - 76;
                    $(window).scrollTop(scrollTarget);
                }
            });

            $('.button-near-you').click(function() {
                $('.geo-location-button').trigger('click');
            });
        },
        campaign:function(){
			var $readMore = $('#campaign .readmore');
			var $info = $('#pdpDealinfo .info');
			var $readLess = $('#campaign .readless');

			$readMore.click(function(){
				$info.slideDown();
				$readMore.css('opacity','0');
			});

			$readLess.click(function(){
				$info.slideUp();
				$readMore.css('opacity','1');
			});

		},
        hotelsAndResortsMap: function(trigger) {
            var mapWrapper = $('#mapWrapper');
            var body = $(document.body);
            var canvas = $('#mapWrapper #mapCanvas');
            var currentHeight = 0;
            var mapVisible = false;
            var wrapper = $('#wrapper');

            var wrapperOverflow = wrapper.css('overflow');

			var updateMapForMobile = function() {
                canvas.height(currentHeight);
            }

            var resize = function() {
                currentHeight = $(window).height();

                if (!ScreenSizeIsMobile || !mapVisible) {
                    return;
                }

                updateMapForMobile();
            }

            if (typeof (trigger) !== 'undefined'){
				trigger.click(function() {

					if ($('.readmore ').length != 0){
					    $('section#pdpDealinfo, section#pdpDealimage' ).hide();
					    $('.readmore ')[0].style.setProperty('display','none','important');
					}
					mapVisible = true;
					wrapper.css('overflow', 'visible');
					body.addClass('mobile-map-visible');
					resize();
					setMapSettings();
				});
			} else {
			    mapVisible = true;
				wrapper.css('overflow', 'visible');
				body.addClass('mobile-map-visible');
				resize();
				/*google.maps.event.trigger(map, "resize");

				if (typeof(mapInitInfo) != 'undefined') {
					map.setCenter(mapInitInfo.center);
				}*/

                setMapSettings();
			}

            $('#close-map').click(function() {
                mapVisible = false;
                wrapper.css('overflow', '');

				if ($('.readmore ').length != 0){
				    $('section#pdpDealinfo, section#pdpDealimage' ).show();
				    $('.readmore ')[0].style.setProperty('display','block','important');
				}
                body.removeClass('mobile-map-visible');
                $(window).trigger('resize');
            });



            $(window).resize(function() {
                resize();
            });

            $(window).on('to-desktop', function() {
                canvas.css('height', '');

                setMapOption({
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.LARGE,
                        position: google.maps.ControlPosition.RIGHT_TOP
                    }
                });
            });

            $(window).on('to-mobile', function() {
                resize();

                setMapOption({
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL,
                        position: google.maps.ControlPosition.RIGHT_BOTTOM
                    }
                });
            });

            var setMapOption = function(option) {
                if (typeof(map) == 'undefined') {
                    setTimeout(function() {
                        setMapOption(option);
                    }, 500);

                    return;
                }

                map.setOptions(option);
            }

            var setMapSettings = function() {
                if (typeof(map) == 'undefined') {
                    setTimeout(function() {
                        setMapSettings();
                    }, 250);

                    return;
                }

                if (typeof(markerCluster) != 'undefined') {
                    try {
                        markerCluster.fitMapToMarkers();
                    } catch(e) { }
                }

                if (map.getZoom() > 15) {
                    map.setZoom(15);
                }

                google.maps.event.trigger(map, "resize");

                if (typeof(mapInitInfo) != 'undefined') {
                    map.setCenter(mapInitInfo.center);
                }
            }
        },

        mobileOverview: function() {
            $('#mobile-community-header .already-member span').click(function() {
                $('.callLogin').trigger('click');
            });

            var emailMobile = $('#emailMobile');

            if (emailMobile.length) {
                var updateEmailMobile = function() {
                    var width = emailMobile.closest('.emailInput').width();
                    emailMobile.width(width - 125);
                };

                $(window).resize(updateEmailMobile);
                updateEmailMobile();
            }
        },
		contactPage: function(){

		    $('div.areaName').click(function() {
                var $item = $(this).parent();
                var open = $item.hasClass('open');

                $('#areas li').removeClass('open');

                if (open == false) {
                    $item.addClass('open');
                }
            });
        },
	    toggleBenefitInfo: function(){
		$('.benefit-trigger').click(function() {
		    $(this).nextAll('.benefit-toggle').slideDown();
		    $(this).find('a.readmore').css('display','none');
		});
	},
        myAccountSettings: function() {
            $('#frmSettings select').change(function() {
                if (!ScreenSizeIsMobile) {
                    return;
                }

                var id = $(this).attr('id');

                if (typeof(id) == 'undefined' || !id) {
                    return;
                }

                var hidden = $('#hdn' + id);

                if (!hidden.length) {
                    return;
                }

                hidden.val($(this).val());
            });
        }
    };

    dhM.init();
});


// Mobile image gallery

var MobileImageGallery = function(images, hotel, city, startAt) {
    if (typeof(startAt) == 'undefined') {
        startAt = 0;
    }

    this.images = images;
    this.hotel = hotel;
    this.city = city;

    this.oldScroll = 0;
    this.currentImage = startAt;

    this.isAnimating = false;
    this.scrollToCurrentTimer = false;
}

MobileImageGallery.prototype.init = function() {
    var self = this;
    this.oldScroll = $(window).scrollTop();

    $('#main').hide();
    $('#mainFooter').hide();

    var main = $('<div id="mobile-image-gallery"></div>');
    $('body').append(main);

    var images = $('<div class="images"></div>');
    main.append(images);

    var imageHolder = $('<div class="image-holder"></div>');
    images.append(imageHolder);

    var left = $('<div class="button-left"></div>');
    var right = $('<div class="button-right"></div>');

    images.append(left);
    images.append(right);

    var imageElements = [];
    for (var i=0; i<this.images.length; i++) {
        var imageElement = $('<div class="image"></div>');
        imageElement.css('background-image', 'url("' + this.images[i] + '")');

        imageHolder.append(imageElement);
        imageElements.push(imageElement);
    }

    var close = $('<div class="close"></div>');
    main.append(close);

    var bottom = $('<div class="bottom"></div>');
    main.append(bottom);

    var count = $('<div class="count"></div>');
    bottom.append(count);

    var current = $('<span class="current">1</span>');
    var total = $('<span class="total">/' + this.images.length + '</span>');

    count.append(current);
    count.append(total);

    var location = $('<div class="location"></div>');
    bottom.append(location);

    location.text(this.hotel + ', ' + this.city);

    this.elementMain = main;
    this.elementCurrent = current;
    this.elementClose = close;
    this.elementImages = images;
    this.elementHolder = imageHolder;
    this.imageElements = imageElements;
    this.elementLeft = left;
    this.elementRight = right;

    close.on('click.imagegallery', function() {
        self.close();
    });

    $(window).on('resize.imagegallery', function() {
        self.resize();
    });

    this.elementLeft.on('click.imagegallery', function() {
        self.left();
    });

    this.elementRight.on('click.imagegallery', function() {
        self.right();
    });

    imageHolder.swipe({
        swipeLeft: function(event, distance, duration, fingerCount, fingerData) {
            self.right();
        },

        swipeRight: function(event, distance, duration, fingerCount, fingerData) {
            self.left();
        }
    });

    imageHolder.swipe('option', 'threshold', 10);

    this.resize();

    //var width = $(window).width() - 20;
    //this.elementHolder.css('left', (width * this.currentImage * -1) + 'px');
}

MobileImageGallery.prototype.close = function() {
    this.elementClose.off('click.imagegallery');
    $(window).off('resize.imagegallery');

    this.elementLeft.off('click.imagegallery');
    this.elementRight.off('click.imagegallery');

    this.elementHolder.swipe('disable');

    this.elementMain.remove();
    $('#main').show();
    $('#mainFooter').show();
    $(window).scrollTop(this.oldScroll);

    $(window).trigger('resize');
}

MobileImageGallery.prototype.resize = function() {
    var self = this;

    var width = $(window).width() - 20;
    var height = $(window).height() - 20;
    var buttonTop = Math.round(height / 2) - 17;

    this.elementImages.width(width);
    this.elementImages.height(height);

    this.elementHolder.width(width * this.images.length);
    this.elementHolder.height(height);

    for (var i=0; i<this.imageElements.length; i++) {
        this.imageElements[i].width(width);
        this.imageElements[i].height(height);
    }

    this.elementLeft.css('top', buttonTop + 'px');
    this.elementRight.css('top', buttonTop + 'px');

    if (this.scrollToCurrentTimer) {
        clearTimeout(this.scrollToCurrentTimer);
    }

    this.scrollToCurrentTimer = setTimeout(function() {
        self.scrollToCurrentTimer = false;
        self.scrollToCurrent();
    }, 500);
}

MobileImageGallery.prototype.left = function() {
    if (!this.currentImage || this.isAnimating) {
        return;
    }

    this.currentImage--;
    this.scrollToCurrent();
}

MobileImageGallery.prototype.right = function() {
    if (this.isAnimating) {
        return;
    }

    // Cycle
    if (this.currentImage >= (this.images.length - 1)) {
        var width = $(window).width() - 20;
        return;
    }

    this.currentImage++;
    this.scrollToCurrent();
}

MobileImageGallery.prototype.scrollToCurrent = function() {
    var self = this;
    this.isAnimating = true;

    this.elementHolder.stop();

    var width = $(window).width() - 20;

    this.elementHolder.animate(
        {
            left: (width * this.currentImage * -1) + 'px'
        },
        500,
        function() {
            self.isAnimating = false;
            self.elementCurrent.text(self.currentImage + 1);
        }
    );
}


var MobileSwipeImage = function(element) {
    this.element = element;
    this.isAnimating = false;
    this.currentImage = 0;
    this.controlsVisible = false;
    this.propertyId = element.data('property');
    
    this.ratioX = 5760;
    this.ratioY = 3382;
    
    if (typeof(element.data('ratiox')) != 'undefined') {
        this.ratioX = parseInt(element.data('ratiox'), 10);
        this.ratioY = parseInt(element.data('ratioy'), 10);
    }
    
    this.useLink = true;
    if (typeof(element.data('dontlink')) != 'undefined') {
        this.useLink = false;
    }
    
    this.tapCallback = false;
    if (typeof(element.data('tapcallback')) != 'undefined') {
        this.tapCallback = element.data('tapcallback');
    }
    
    this.useTitles = false;
    if (typeof(element.data('titles')) != 'undefined') {
        this.useTitles = true;
        this.titles = element.data('titles');
    }
}

MobileSwipeImage.prototype.init = function() {
    var self = this;

    this.images = this.element.find('.image');
    this.holder = this.element.find('.image-holder');
    this.overlay = this.element.find('.overlay');
    this.iconLeft = this.element.find('.icon-left');
    this.iconRight = this.element.find('.icon-right');
    this.shadow = this.overlay.find('.shadow');
    this.arrows = this.overlay.find('.icon-arrows');
    this.indicatorHolder = this.overlay.find('.indicator-holder');
    this.indicators = [];
    
    this.numberOfImages = this.images.length;
    
    if (this.indicatorHolder.length) {
        for (var i=0; i<this.numberOfImages; i++) {
            var indicator = $('<div class="indicator"></div>');
            this.indicatorHolder.append(indicator);
            this.indicators.push(indicator);
        }
        
        this.allIndicators = this.indicatorHolder.find('.indicator');
        
        this.indicatorHolder.on('click', '.indicator', function() {
            self.currentImage = $(this).index();
            self.scrollToCurrent();
        });
    }
    
    $(window).resize(function() {
        self.resize();
    });
    
    if (this.numberOfImages == 1) {
        this.iconLeft.hide();
        this.iconRight.hide();
    }
    
    if (this.useTitles && this.titles.length) {
        this.overlay.attr('title', this.titles[0]);
    }
    
    this.overlay.swipe({
        swipeLeft: function(event, distance, duration, fingerCount, fingerData) {
            self.right();

        },

        swipeRight: function(event, distance, duration, fingerCount, fingerData) {
            self.left();

        },

        tap: function(event, target) {
            target = $(target);
            
            if (target.hasClass('icon-left') || target.hasClass('feature-request-new-left')) {
                self.left();
                return;
            }
            
            if (target.hasClass('icon-right') || target.hasClass('feature-request-new-right')) {
                self.right();
                return;
            }
            
            if (target.hasClass('overlay')) {
                if (self.tapCallback) {
                    window[self.tapCallback](self.currentImage);
                    return;
                }
                
                var imgLink = target.find('.img-link');
                
                if (imgLink.length) {
                    var url = imgLink.data('pdp');
                    
                    if (typeof(url) != 'undefined') {
                        window.location.href = url;
                        return;
                    }
                }
            }

            if (!ScreenSizeIsMobile) {
                if (target.hasClass('shadow')
                    || target.hasClass('icon-arrows')) {
                    if (self.tapCallback) {
                        window[self.tapCallback](self.currentImage);
                    }
                }
                return;
            }
            
            if (
                target.hasClass('icon-left')
                || target.hasClass('icon-right')
                || !(
                    target.hasClass('shadow')
                    || target.hasClass('icon-arrows')
                )
            ) {
                return;
            }

            var images = $.parseJSON(self.overlay.attr('data-images'));
            var hotel = self.overlay.data('hotel');
            var city = self.overlay.data('city');

            if (ScreenSizeIsMobile) {
                var gallery = new MobileImageGallery(images, hotel, city, self.currentImage);
                gallery.init();
            } else {
                $('.property-fancy-box_' + self.propertyId).eq(self.currentImage).trigger('click');
            }
        },

        excludedElements: ""
    });

    this.overlay.swipe('option', 'threshold', 10);

    /*this.iconLeft.mousedown(function(e) {
        e.preventDefault();
        e.stopPropagation();

        self.left();
    });

    this.iconRight.mousedown(function(e) {
        e.preventDefault();
        e.stopPropagation();

        self.right();
    });*/

    this.resize();

    // Call again to fix stupid scrollbar issue
    this.resize();
    this.showCurrentIndicator();
}

MobileSwipeImage.prototype.resize = function() {
    if (!ScreenSizeIsMobile) {
        // return;
    }

    var width = this.element.width();
    var height = Math.round(width * (this.ratioY / this.ratioX));

    this.images.width(width);
    this.images.height(height);

    this.element.height(height);

    this.holder.width(width * this.numberOfImages);
    this.holder.height();

    this.holder.stop();
    this.holder.css('left', (width * this.currentImage * -1) + 'px');

    this.iconLeft.css('top', Math.round((height / 2) - 17) + 'px');
    this.iconRight.css('top', Math.round((height / 2) - 17) + 'px');

    if (!this.controlsVisible) {
        this.controlsVisible = true;

        if (this.numberOfImages > 1) {
            this.iconLeft.show();
            this.iconRight.show();
        }
        
        this.element.find('.shadow').show();
        this.element.find('.icon-arrows').show();
        this.element.find('.distance').show();
    }
}

MobileSwipeImage.prototype.left = function() {
    var self = this;

    if (this.isAnimating) {
        return;
    }

    // Cycle
    if (!this.currentImage) {
        var temporaryImage = this.images.last().clone();
        var width = this.element.width();
        this.holder.width(width * (this.numberOfImages + 1));
        this.holder.prepend(temporaryImage);
        this.holder.css('left', (width * -1) + 'px');

        this.holder.animate(
            {
                left: '0px'
            },
            500,
            function() {
                var width = self.element.width();

                self.currentImage = self.numberOfImages - 1;
                self.isAnimating = false;
                self.holder.css('left', (width * self.currentImage * -1) + 'px');

                temporaryImage.remove();
                self.holder.width(width * (self.numberOfImages));
                
                if (self.useTitles) {
                    self.overlay.attr('title', self.titles[self.currentImage]);
                }
                
                self.showCurrentIndicator();
            }
        );

        return;
    }

    this.currentImage--;
    this.scrollToCurrent();
}

MobileSwipeImage.prototype.right = function() {
    var self = this;

    if (this.isAnimating) {
        return;
    }

    // Cycle
    if (this.currentImage >= (this.numberOfImages - 1)) {
        var temporaryImage = this.images.first().clone();
        var width = this.element.width();
        this.holder.width(width * (this.numberOfImages + 1));
        this.holder.append(temporaryImage);

        this.holder.animate(
            {
                left: (width * (this.currentImage + 1) * -1) + 'px'
            },
            500,
            function() {
                var width = self.element.width();

                self.currentImage = 0;
                self.isAnimating = false;
                self.holder.css('left', (width * self.currentImage * -1) + 'px');

                temporaryImage.remove();
                self.holder.width(width * (self.numberOfImages));
                
                if (self.useTitles) {
                    self.overlay.attr('title', self.titles[self.currentImage]);
                }
                
                self.showCurrentIndicator();
            }
        );

        return;
    }

    this.currentImage++;
    this.scrollToCurrent();
}

MobileSwipeImage.prototype.scrollToCurrent = function() {
    var self = this;
    this.isAnimating = true;

    this.holder.stop();

    var width = this.element.width();

    this.holder.animate(
        {
            left: (width * this.currentImage * -1) + 'px'
        },
        500,
        function() {
            self.isAnimating = false;
            self.holder.css('left', (width * self.currentImage * -1) + 'px');
            
            if (self.useTitles) {
                self.overlay.attr('title', self.titles[self.currentImage]);
            }

            self.showCurrentIndicator();
        }
    );
}


MobileSwipeImage.prototype.showCurrentIndicator = function() {
    if (!this.indicatorHolder.length) {
        return;
    }
    
    this.allIndicators.removeClass('indicator-active');
    this.indicators[this.currentImage].addClass('indicator-active');
}


var DhBookingCalendarMobile = function(
    version,
    parent,
    inner,
    triggerCheckIn,
    triggerCheckOut,
    checkInLabel,
    checkInInput,
    checkOutLabel,
    checkOutInput
) {
    this.version = version;
    this.parent = parent;
    this.inner = inner;
    this.triggerCheckIn = triggerCheckIn;
    this.triggerCheckOut = triggerCheckOut;
    
    this.checkInLabel = checkInLabel;
    this.checkInInput = checkInInput;
    this.checkOutLabel = checkOutLabel;
    this.checkOutInput = checkOutInput;
    
    this.checkInDate = false;
    this.checkOutDate = false;
    this.firstDateMonth = new Date();
    this.firstDateMonth.setMonth(this.firstDateMonth.getMonth() + 1);
    this.firstDateMonth.setHours(0, 0, 0, 0);
    this.isOpen = true;
    this.checkStatus = 'in';
    this.isAnimating = false;
    
    this.dataUnavailable = {};
    this.dataNoArrival = {};
    this.dataUnavailableLoading = {};
};

DhBookingCalendarMobile.prototype = clonePrototype(DhBookingCalendar.prototype);

DhBookingCalendarMobile.prototype.init = function() {
    
    var self = this;
    var date = new Date();
    
    if (this.checkInInput.val() && this.checkOutInput.val()) {
        this.checkInDate = this.valueToDate(this.checkInInput.val());
        this.checkOutDate = this.valueToDate(this.checkOutInput.val());
    }
    
    var closeCalendar = function() {
        self.checkInLabel.css('font-family', 'DINWebPro');
        self.checkOutLabel.css('font-family', 'DINWebPro');
        
        self.parent.slideUp({
            complete: function() {
                isAnimating = false;
                widgetCalendarOpen = false;
            }
        });
    };
    
    var widgetOptions = {
        flat: true,
        prev: '',
        next: '',
        date: date,
        format: 'b d, Y',
        starts: 1,
        calendars: 1,
        mode: 'single',
        view: 'days',
        //autoHide: true,
        
        // This happens before onFill
        onMonthChange: function(firstDate) {
            self.firstDateMonth = new Date(firstDate.getTime());
            self.firstDateMonth.setMonth(self.firstDateMonth.getMonth() + 1);
            self.firstDateMonth.setHours(0, 0, 0, 0);
        },
        
        // This happens when the calendar is done updating HTML after month
        // changes (not after initially showing)
        onFill: function() {
            self.showAvailability();
        },
        
        onShow: function() {
            //showPickerCode();
            //self.showAvailability();
        },
        
        onHide: function() {
            //hidePickerCode();
        },
        
        // A date has been picked
        onChange: function(formated, date) {
            self.datePicked(formated, date);
            closeCalendar();
            /*if (self.checkStatus == 'in') {
                self.checkInLabel.css('font-family', 'DINWebPro');
                self.checkOutLabel.css('font-family', 'DINWebProMedium');
                self.checkStatus = 'out';
                self.showAvailability();
            } else {
                closeCalendar();
            }*/
        }
    };
    
    this.inner.DatePicker(widgetOptions);
    this.inner.DatePickerShow();
    
    this.parent.find('.datePickerPrevMonth').click(function() {
        self.parent.find('.datepickerGoPrev').trigger('click');
    });
    
    this.parent.find('.datePickerNextMonth').click(function() {
        self.parent.find('.datepickerGoNext').trigger('click');
    });
    
    var isAnimating = false;
    var widgetCalendarOpen = false;
    var toggleCalendar = function() {
        if (isAnimating) {
            return;
        }
        
        isAnimating = true;
        
        if (widgetCalendarOpen) {
            closeCalendar();
        } else {
            self.showAvailability();
            
            self.parent.slideDown({
                complete: function() {
                    isAnimating = false;
                    widgetCalendarOpen = true;
                }
            });
        }
    }
    
    this.parent.find('.clear-all').click(function() {
        self.checkInLabel.text('Check In');
        self.checkOutLabel.text('Check Out');
        self.checkInInput.val('');
        self.checkInInput.val('');
        self.inner.DatePickerClear();
        
        self.checkInDate = false;
        self.checkOutDate = false;
        
        toggleCalendar();
    });
    
    self.triggerCheckIn.click(function() {
        self.checkStatus = 'in';
        self.checkInLabel.css('font-family', 'DINWebProMedium');
        self.checkOutLabel.css('font-family', 'DINWebPro');
        toggleCalendar();
    });
    
    self.triggerCheckOut.click(function() {
        if (self.checkInDate) {
            self.checkStatus = 'out';
            self.checkInLabel.css('font-family', 'DINWebPro');
            self.checkOutLabel.css('font-family', 'DINWebProMedium');
        } else {
            self.checkStatus = 'in';
            self.checkInLabel.css('font-family', 'DINWebProMedium');
            self.checkOutLabel.css('font-family', 'DINWebPro');
        }
        
        toggleCalendar();
    });
    
};

DhBookingCalendarMobile.prototype.showAvailability = function() {
    var self = this;
    
    if (
        $('body').hasClass("dealsPage")
    ) {
        self.redraw(0);
        self.redraw(1);
        return;
    }

    var tbodies = this.parent.find('tbody.datepickerDays');
    tbodies.find('td').not('.datepickerNotInMonth').addClass('date-not-loaded');
    
    var date = new Date(this.firstDateMonth.getTime());
    date.setHours(0, 0, 0, 0);
    var yearMonth = this.getCurrentYearMonth();
    yearMonth = yearMonth[0];
    
    AvailabilityService.get(date, function(data) {        
        self.dataUnavailable[yearMonth] = data.unavailable;
        self.dataNoArrival[yearMonth] = data.dataNoArrival;

        if (date.getTime() == self.firstDateMonth.getTime()) {
            self.redraw(0);
        }
    }, true);
};
