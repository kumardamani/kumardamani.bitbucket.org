var GalleryPage = function() {

    this.$container = $('.gallery-wrapper');

    this.totalMediaCount = this.$container.data('total-media-count');
    this.visibleMediaCount = 0;
    this.mediaLoadingIsInProgress = false;
    this.isSharingEnabled = this.$container.data('is-sharing-enabled');
    this.reservedVu = "";

    this.imageLoadingIntervalIdPool = [];

    this.imageLoadingIntervalIdPool.abortAll = function() {
        $(this).each(function(idx, intervalId) {
            clearInterval(intervalId);
        });
        this.splice(0, this.length);
    };
};


GalleryPage.prototype.init = function() {
    this.initGrid();    
    this.autoLoadAlbum();
	this.initLoadMoreButton();    
    this.initLightbox();
    this.initVimeoSettings();
};


GalleryPage.prototype.autoLoadAlbum = function() {
    this.showQueuedGridItems();
}

GalleryPage.prototype.resetParams = function() {
    this.currentParams = this.defaultParams;
};


GalleryPage.prototype.getGrid = function() {
    return this.$container.find('.gallery-matrix').first();
};


GalleryPage.prototype.initGrid = function() {
    var self = this;

    // Setup Isotope 
    var $galleryPlugin =  self.getGrid().isotope({
        itemSelector: ".grid-item-display", 
        percentPosition: true,

        masonry: {
            gutter: ".gutter-sizer", 
            columnWidth: '.grid-sizer', 
        },

		stagger: "0",
        transitionDuration: "0" 
    });

    // Call the Redraw on window resize
    $(window).on('resize', function() {
        self.redrawGrid(); 
    });

    self.visibleMediaCount = self.getGrid().find('.grid-item-display').length;
};


GalleryPage.prototype.redrawGrid = function() {
    var self = this;

    if (typeof window.picturefill !== "undefined") {
        window.picturefill();
    }

    self.getGrid().isotope('layout'); 

    self.loadGridImages();

};

GalleryPage.prototype.loadGridImages = function() {
    var self = this;

    self.getGrid().find('.grid-item .media-item.is-loading > img').each(function() {
        var imgObj = this;
        var $img = $(imgObj);
        var imgSrc = ($img.hasClass('video-thumbnail')? $img.attr('data-thumbnail') : $img.attr('data-file-path')); 
        $img.load(function() {
            self.getGrid().isotope('layout');
        }).attr('src', imgSrc);
        
        var $mediaItem = $img.closest('.media-item');

        if (self.isImageLoaded(imgObj)) {
            $mediaItem.removeClass('is-loading');
        } else {
            // Try to give the image more chance to load
            var intervalCount = 0;

            var intervalId = setInterval(function(){
                if (self.isImageLoaded(imgObj)) {

                    // Image is already loaded
                    $mediaItem.removeClass('is-loading');
                    clearInterval(intervalId);
                    self.getGrid().isotope('layout');

                } else if (intervalCount > 3) {

                    // Tried more that 3 time and image is still not loaded
                    clearInterval(intervalId);
                    self.getGrid().isotope('layout');

                } else {
                    // Try one more time
                    intervalCount++;
                }
            },1000);

            // Keep the interval Id for clean up later
            self.imageLoadingIntervalIdPool.push(intervalId);
        }

    });
};

GalleryPage.prototype.isImageLoaded = function(img) {

    if (!img.complete) {
        return false;
    }

    if (img.naturalWidth === 0) {
        return false;
    }

    return true;
};

GalleryPage.prototype.showQueuedGridItems = function() {
    var self = this;

    self.mediaLoadingIsInProgress = true;
    self.getLoadMoreButton().addClass("disabled");
    
    var noOfAssetsToLoad = 21;
    if (this.isMobile()) {
        noOfAssetsToLoad = 5;
    }
    var noOfAssetsLoaded = 0;
    self.getGrid().find('.grid-item-queued').each(function() {
        $(this).find('.media-item').addClass('is-loading');
        $(this).removeClass("grid-item-queued");
        $(this).addClass("grid-item-display");
        self.addGridItems($(this)); 
        noOfAssetsLoaded ++;
        if (noOfAssetsLoaded == noOfAssetsToLoad) {
            return false;
        }
    });

    window.setTimeout(
        function() { 
            
            self.visibleMediaCount = self.getGrid().find('.grid-item-display').length;
            // Update the load more button
            if (self.visibleMediaCount < self.totalMediaCount) {
                self.getLoadMoreButton().removeClass("disabled");
                self.getLoadMoreButtonWrapper().show();
            } else {
                self.getLoadMoreButtonWrapper().hide();
            }
            
            self.mediaLoadingIsInProgress = false;
            self.redrawGrid();
       
        },
        100
    );
};

GalleryPage.prototype.isMobile = function() {
    if (getWindowWidth() > 768) {
        return false;
    } else {
        return true;
    }
};

GalleryPage.prototype.addGridItems = function($gridItems) {
	var self = this;

    this.getGrid().append($gridItems); 
    this.getGrid().isotope('appended', $gridItems);


};

GalleryPage.prototype.fixGridItems = function($gridItems) {
   var self = this;

    // Total media count is the first div in result
    var $totalMediaCount = $gridItems.get(0);
    if ($totalMediaCount !== undefined) {

        // Set the total media count
        self.totalMediaCount = $totalMediaCount.getAttribute('data-total-media-count');

        // Remove the element because no need to keep the element
        $gridItems.splice(0,1);
    }

    // Get the correct grid width class
    var $sizer = this.$container.find('.grid-sizer').first();    
    return $gridItems;
};



GalleryPage.prototype.clearGrid = function() {
    var self = this;

    // Remove items from gallery
    var $items = self.getGrid().find('.grid-item');
    self.getGrid().isotope('remove', $items);
    self.getGrid().find('.grid-item').remove();

    self.imageLoadingIntervalIdPool.abortAll();

    // Collapse to a min height 
    self.getGrid().height(100); 

    // Hide the load more button
    self.getLoadMoreButton().addClass("disabled");
    self.getLoadMoreButtonWrapper().show();

};

GalleryPage.prototype.getLoadMoreButton = function() {
    return this.$container.find('.btn-load-gallery').first();    
};

GalleryPage.prototype.getLoadMoreButtonWrapper = function() {
    return this.$container.find('.btn-load-wrapper').first();
};


GalleryPage.prototype.initLoadMoreButton = function() {
    var self = this;

    self.$container.on('click', '.btn-load-gallery', function() {
        if (!self.getLoadMoreButton().hasClass("disabled")) {         
            self.getLoadMoreButton().addClass("disabled");
            self.showQueuedGridItems();
            // Allow only 1 click to load image and then load image using lazy loading
            $(this).hide();
            if (!self.getLoadMoreButton().hasClass("lazyload-active")) { 
                self.getLoadMoreButton().addClass("lazyload-active");
                self.initLazyLoading();
            }            
        }
    });
};

GalleryPage.prototype.initLazyLoading = function() {
    var self = this;
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() > parseInt($('#mainFooter').offset().top + 100)) {
            self.getLoadMoreButton().trigger('click');
        }
    });

};

GalleryPage.prototype.initLightbox = function() {
    var self = this;

    var $lightbox = self.$container.find('.lightbox-gallery');
    var $lightboxContent = $lightbox.find('.lightbox-content');
    var $lightboxCaptionDescription = $lightbox.find('.lightbox-description');
    var $currentMedia;
    var currentMediaIndex;
    var $lightboxMediaWrapper = $lightbox.find('.lightbox-media-wrapper');
    var $lightboxCaptionOuterWrapper = $lightbox.find('.lightbox-caption-outer-wrapper');

    self.$container.on('click', '.media-item', function() {
        $('body').css('overflow-y', 'hidden');
        $currentMedia = $(this).closest('.media-item');
        currentMediaIndex = $currentMedia.attr('data-count');
        self.setLightboxMediaCount($currentMedia);
        self.setLightboxData($currentMedia);
        $lightbox.addClass('lightbox-gallery-open');
    });

    $lightbox.find('.lightbox-close').on('click', function() {
        $lightbox.removeClass('lightbox-gallery-open');
        $lightboxMediaWrapper.empty();
        $('body').css('overflow-y', 'scroll');
        self.routing('');
    });
    
    $lightbox.find('.lightbox-left-nav, .lightbox-right-nav').on('click', function() {
        var $incomingMedia;
        var mediaCountType;

        if ($(this).hasClass('lightbox-left-nav')) {
            mediaCountType = 'previous';            
        } else {
            mediaCountType = 'next';
        }
        
        var incomingMediaIndex = self.getIncomingMediaIndex(currentMediaIndex, mediaCountType);
        $incomingMedia = self.$container.find(".media-item[data-count='" + incomingMediaIndex + "']");
        
        if ($incomingMedia.length != 0) {
            $currentMedia = $incomingMedia;
            currentMediaIndex = $currentMedia.attr('data-count');
            self.setLightboxData($incomingMedia);
            self.setLightboxMediaCount($currentMedia);
        }

    });
    
    $lightbox.find('.lightbox-mobile-share').on('click', function() {
        $lightbox.find('.gallery-lightbox-mobile-social-share').addClass('is-open');        
    });
    
    $lightbox.find('.mobile-social-share-close').on('click', function() {
        $lightbox.find('.gallery-lightbox-mobile-social-share').removeClass('is-open');        
    });

    $(window).on('resize', function() {
        if ($lightbox.hasClass('lightbox-gallery-open')) {
            self.setLightboxSize();
            self.setCaptionHeight();
        }
    });
    
    Hammer($lightboxContent[0]).on('swipeleft', function() {
        var incomingMediaIndex = self.getIncomingMediaIndex(currentMediaIndex, 'next');
        var $incomingMedia = self.$container.find(".media-item[data-count='" + incomingMediaIndex + "']");
        if ($incomingMedia.length != 0) {
            $currentMedia = $incomingMedia;
            currentMediaIndex = $currentMedia.attr('data-count');
            self.setLightboxData($incomingMedia);
            self.setLightboxMediaCount($currentMedia);
        }
    });
        
    Hammer($lightboxContent[0]).on('swiperight', function() {
        var incomingMediaIndex = self.getIncomingMediaIndex(currentMediaIndex, 'previous');
        var $incomingMedia = self.$container.find(".media-item[data-count='" + incomingMediaIndex + "']");

        if ($incomingMedia.length != 0) {
            $currentMedia = $incomingMedia;
            currentMediaIndex = $currentMedia.attr('data-count');
            self.setLightboxData($incomingMedia);
            self.setLightboxMediaCount($currentMedia);
        }
    });

    $lightbox.find('.lightbox-caption-outer-wrapper').addClass('hidden-mobile-media-info');
        
    Hammer($lightboxMediaWrapper[0]).on('tap', function() {
        if ($lightboxCaptionOuterWrapper.hasClass('hidden-mobile-media-info')) {
            $lightboxCaptionOuterWrapper.removeClass('hidden-mobile-media-info')
        } else {
            $lightboxCaptionOuterWrapper.addClass('hidden-mobile-media-info');
        }
    });
};

GalleryPage.prototype.getIncomingMediaIndex = function(currentIndex, nav) {
    var incomingMediaIndex;
    if (nav == 'previous') {
        incomingMediaIndex = parseInt(currentIndex) - 1;
    } else {
        incomingMediaIndex = parseInt(currentIndex) + 1;
    }    

    if (incomingMediaIndex <= 0) {
        incomingMediaIndex = this.totalMediaCount;        
    } else if (incomingMediaIndex > this.totalMediaCount) {
        incomingMediaIndex = 1;
    }
    
    return incomingMediaIndex;
};

GalleryPage.prototype.setLightboxSize = function() {
    var self = this;

    var $lightbox = self.$container.find('.lightbox-gallery');
    var $lightboxMedia = $lightbox.find('.lightbox-media-wrapper');
    var $lightboxImage = $lightbox.find('img');
    var $lightboxCaptionWrapper = $lightbox.find('.lightbox-caption-outer-wrapper');

    var naturalImageWidth = $lightboxImage.prop('naturalWidth');
    var naturalImageHeight = $lightboxImage.prop('naturalHeight');
    var naturalImageRatio = naturalImageWidth / naturalImageHeight;

    var contentWidth = $lightboxMedia.width();
    var contentHeight = $lightboxMedia.height();

    if (contentWidth > naturalImageWidth) {
        contentWidth = naturalImageWidth;
    }

    var expectedHeight = contentWidth / naturalImageRatio;

    if ( expectedHeight > contentHeight ) {
        contentWidth = contentHeight * naturalImageRatio;
    }

    var imgHeight = contentWidth / naturalImageRatio;
    $lightboxImage.css('width',contentWidth);
    $lightboxImage.css('height',imgHeight);
    
    var captionBottom = contentHeight - imgHeight;
    if (captionBottom > 2 && getWindowWidth() > 768) {
        $lightboxCaptionWrapper.css('bottom', (captionBottom/2)+'px');
    } else {
        $lightboxCaptionWrapper.css('bottom', 0);
    }

};

GalleryPage.prototype.setCaptionHeight = function() {
    var $lightbox = this.$container.find('.lightbox-gallery');
    var $lightboxCaptionWrapper = $lightbox.find('.lightbox-caption-inner-wrapper');
    if (getWindowWidth() > 768) {
        $lightboxCaptionWrapper.css('height', '100px');
    } else {
        $lightboxCaptionWrapper.css('height', 'auto');
    }
};

GalleryPage.prototype.setLightboxMediaCount = function($element) {
    $('.lightbox-gallery').find('.lightbox-media-total .current').text($element.attr('data-count'));
};

GalleryPage.prototype.setLightboxData = function($element) {
    var self = this;

    var $lightbox = self.$container.find('.lightbox-gallery');
    var $lightboxMediaWrapper = $lightbox.find('.lightbox-media-wrapper');
    var $lightboxCaptionWrapper = $lightbox.find('.lightbox-caption-inner-wrapper');
    var $lightboxCaptionOuterWrapper = $lightbox.find('.lightbox-caption-outer-wrapper');
    var $lightboxCaptionDescription = $lightbox.find('.lightbox-description');
    $lightbox.find('.lightbox-caption-outer-wrapper').addClass('hidden-mobile-media-info');
    
    // Reset lightbox
    $lightboxMediaWrapper.empty();
    $lightboxCaptionDescription.html('');
    $lightboxCaptionDescription.hide();
    $lightboxCaptionOuterWrapper.css('bottom', 0);
    $lightboxCaptionWrapper.css('visibility', 'hidden');
    if (getWindowWidth() > 768) {
        $lightboxCaptionWrapper.css('height', '100px');
    } else {
        $lightboxCaptionWrapper.css('height', 'auto');
    }

    var mediaSource = ($element.attr('data-file-path') !== undefined ? $element.attr('data-file-path') : $element.attr('data-key'));

    var mediaType = $element.attr('data-type');
    var caption = $element.attr('data-caption');
    var captionDescription = $element.attr('data-description');
    var altText = $element.attr('data-alt-text');
    var vanityUrl = $element.attr('data-vanity-url');
    var publisherId = $element.attr('data-publisher-id');
    var playerId = $element.attr('data-player-id');

    if (mediaType == 'photo') {

        $lightboxMediaWrapper.removeClass('video');

        var loadingImage = new Image();
        loadingImage.src = 'assets/images/loading.gif';
        var $loadingImage = $(loadingImage);
        $loadingImage.css({
            'width': '48px',
            'height': '48px'
        });
        $lightboxMediaWrapper.append($loadingImage);

        var tmpImg = new Image();
        tmpImg.src = mediaSource;
        tmpImg.alt = altText;
        tmpImg.title = altText;

        var $image = $(tmpImg);
        $image.hide();
        tmpImg.onload = function() {
            $loadingImage.remove();
            self.setLightboxSize();
            $image.show();
        };

        $lightboxMediaWrapper.append($image);

    } else {

        $lightboxMediaWrapper.addClass('video');

        var $iframe = $('<iframe src="" frameborder="0" id="lightbox-video-iframe" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');

        if (mediaType == 'youtube') {
            $iframe.attr('src', 'https://www.youtube.com/embed/' + mediaSource+'?rel=0&showinfo=0');
            $lightboxMediaWrapper.append($iframe);
        } else if (mediaType == 'vimeo') {
            $iframe.attr('src', 'https://player.vimeo.com/video/' + mediaSource);
            $lightboxMediaWrapper.append($iframe);
        }
        // fix for swiping over iframe.
        $lightboxMediaWrapper.append($('<div style="height:80%;width:40%;left:0;top:0;position:absolute;z-index:20;"></div>'));
        $lightboxMediaWrapper.append($('<div style="height:80%;width:40%;right:0;top:0;position:absolute;z-index:20;"></div>'));
        
        $lightboxMediaWrapper.show();
    }

    if (caption != '' || captionDescription != '') {
        $lightboxCaptionWrapper.css('visibility', 'visible');
    }

    if (captionDescription != '') {
        var $wrappedCaptionDescription = $('<p>').html(captionDescription);
        $lightboxCaptionDescription.append($wrappedCaptionDescription);
    }

    if (captionDescription != '' ) {
        $lightboxCaptionDescription.show();
    }

    self.routing(vanityUrl);
    self.updateLightboxShareWidget(vanityUrl, mediaSource, mediaType);
};

GalleryPage.prototype.routing = function(vanityUrl) {
    if ($('html').hasClass('ie9') == false) {
        var pageUrl = '/gallery';
        if (vanityUrl && vanityUrl != '' && vanityUrl != 'undefined') {
            pageUrl += '/' + vanityUrl;
        }
        var data = (history.state == null) ? '': [vanityUrl, history.state.data];
        history.pushState({'data': data}, vanityUrl, pageUrl);
    }
};

GalleryPage.prototype.updateLightboxShareWidget = function(vanityUrl, mediaSource, mediaType) {
    var self = this;
    var $socialShare = self.$container.find('.gallery-lightbox-social-share');
    var $socialSidebarWrapper = self.$container.find('.gallery-lightbox-social-sidebar');
    var $mobileShareWrapper = self.$container.find('.lightbox-mobile-share-wrapper');
    $socialSidebarWrapper.addClass('is-hidden');
    $mobileShareWrapper.addClass('is-hidden');
    
    var pageUrl = $socialSidebarWrapper.attr('data-baseurl');
    if (!self.isSharingEnabled || !vanityUrl || !pageUrl) {
        return;
    }

    var lightboxURL;
    if(vanityUrl) {
        $socialSidebarWrapper.removeClass('is-hidden');
        $mobileShareWrapper.removeClass('is-hidden');
        lightboxURL = pageUrl+'/' + vanityUrl;
        $socialShare.find('a.social-share-intent').each(function(){
            var shareUrl = $(this).attr('data-href')+lightboxURL;
            $(this).attr('href', shareUrl);
        });
    }
    
    if (mediaType == 'photo') {
        var pinUrl = self.buildPinUrl(lightboxURL, mediaSource)
        $socialShare.find('a.pinterest').attr('href', pinUrl);
        $socialShare.find('a.pinterest').removeClass('not-image');
    } else {
        $socialShare.find('a.pinterest').addClass('not-image');
        $socialShare.find('a.pinterest').attr('href', '');
    }

};

GalleryPage.prototype.buildPinUrl = function(urlpin, urlmedia, description) {
    var url = 'https://www.pinterest.com/pin/create/button/?';
    
    if (urlmedia && urlmedia != 'undefined') {
        url +='media='+encodeURIComponent(urlmedia);
    }
    if (urlpin && urlpin != 'undefined') {
        url +='&url='+encodeURIComponent(urlpin);
    }
    if (description && description != 'undefined') {
        url +='&description='+encodeURIComponent(description);
    }
                
    return url;    
};

GalleryPage.prototype.initVimeoSettings = function() {
    $(".vimeo-video").each(function() {
        var player = new Vimeo.Player($(this)[0]);
        player.setVolume(0);
        player.ready().then(function() {
            player.setVolume(0);
        });
    });
};

var players = {};

function getWindowWidth() {
    var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
    widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
    $outer.remove();

    var scrollBarWidth =  100 - widthWithScroll;
    return $(window).width() + scrollBarWidth;
};

function onYouTubeIframeAPIReady() {    
    if (getWindowWidth() > 768 && $('html').hasClass('ie9') == false) {
        $(".yt-video").each(function() {
            var divId = $(this).attr('id');
            var key = $(this).attr('data-key');
            var count = $(this).attr('data-count');
            players[count] = new YT.Player(divId, {
                videoId: key,
                playerVars: {
                    autoplay: 1,
                    loop: 1,
                    controls: 0,
                    cc_load_policy: 0,
                    disablekb: 1,
                    showinfo: 0,
                    playlist: key,
                    origin: domain,
                    rel: 0
                },
                events: {
                    onReady: ytVideoReady
                }
            });
        });
    }
}

function ytVideoReady() {
    $.each(players, function(index, player) {
        player.mute();
        $(".gallery-matrix .media-item[data-count='" + index + "']").find('.video-thumbnail.youtube').addClass('video-ready');
        $(".gallery-matrix .media-item[data-count='" + index + "']").find('iframe').addClass('video-ready');
        $('.gallery-matrix').isotope('layout');
    });
}