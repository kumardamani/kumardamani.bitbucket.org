function clonePrototype(obj) {
    function CloneFactory () {}
    CloneFactory.prototype = obj;
    return new CloneFactory();
}

var dh;
var omnevent='';
var blink;
var MobileScreenSize = 768;
$(function() {
    dh = {
        lightboxOldScrollValue: 0,
        standardMobileLightboxOptions: {
            autoOpen: false,
            dialogClass: 'lightboxDialog mobileLightboxDialog',
            height: 'auto',
            modal: true,
            resizable: false,
            width: '100%',
            position: {
                my: "left top",
                at: "left top",
                of: $(document.body)
            },
            open: function(event, ui) {
                dh.lightboxOldScrollValue = $(window).scrollTop();
                $(window).scrollTop(0);
                $(this).parent().css('left', '0px');
                $(this).parent().css('top', '0px');
                $("body").css({overflow: 'hidden'});
                $("body").addClass('lightbox-currently-open');
            },
            beforeClose: function(event, ui) {
                $("body").css({overflow: 'inherit'});
                $("body").removeClass('lightbox-currently-open');
                $(window).scrollTop(dh.lightboxOldScrollValue);
            }
        },
        map: {
            bounds: new google.maps.LatLngBounds()
        },
        state: {
            ajaxPdpRooms: undefined,
            ajaxSearchBar: undefined
        },
        el: {
            body: $("body"),
            homepageBanner: $("div#homepageBanner"),
            topDestinations: $("section#topDestinations")
        },
        init: function() {
            var layout = dh.el.body.attr('id');
            var imager = new Imager();

            //dh.resizeSearchBar();
            // run layout specific JS
            switch (layout) {
                case 'home':
                    dh.topDestinationsTabs();
                    dh.mainBanner();
                    break;
                case 'deal':
                    // dh.dealBookNowBtn();
                    dh.pdpSearchWidget();
                    dh.roomsWidget();
                    dh.pdpRooms();
                    dh.secureCampaign();
                    break;
                case 'pdp':
                    dh.clickableAreas();
                    dh.propertyFancyBox();
                    dh.toggleFeaturedHotelsList();
                    dh.openImageCarousel();
                    dh.pdpOverview();
                    dh.pdpBook();
                    dh.dealsOverview();
                    dh.videoLightbox();
                    dh.pdpImageSocialButtons();
                    dh.pdpSearchWidget();
                    if (dh.el.body.hasClass('tabAtaglance')) {
                        dh.pdpNeighbourhood('neighbourhoodMap');
                    }
                    break;
                case 'contact':
                    if (dh.el.body.hasClass('tabTollfree')) {
                        dh.contactForm();
                    }
                    break;
                case 'discoverus':
                    if (dh.el.body.hasClass('tabFindingInfinity')) {
                        dh.discoverUsSlider();
                    }
                    dh.videoLightbox();
                    break;
                case 'rooms':
                    dh.pdpRooms();
                    dh.openImageCarousel();
                    dh.pdpSearchWidget();
                    dh.roomsWidget();
                    break ;
                case 'countryPage':
                    dh.clickableAreas();
                    dh.gMapInit();
                    dh.sortListings();
                    break;
                case 'hotelsAndResorts':
                    dh.gMapInit();
                    dh.sortListings();
                    break;
                case 'city':
                    dh.clickableAreas();
                    dh.gMapInit();
                    dh.sortListings();
                    break;
                case 'campaign':
                    dh.searching();
                    dh.propertyFancyBox();
                    if ($(dh.el.body).find('section#calendar')) {
                        dh.pdpBook();
                    }
                    //dh.sortListings();
                    dh.clickableAreas();
                    dh.secureCampaign();
                    //for omniture
                    $('.book').on('click', function() {
                        s.eVar51 = $(this).find('a').attr('href');
                    });
                    dh.deepLinking();
                    break;
                case 'campaignOverview':
                    if ($(window).width() > MobileScreenSize) {
                        dh.dealsOverview();
                    }
                    $(window).on('to-desktop', function() {
                        dh.dealsOverview();
                    });

                    break;
                case 'bookingFlow':

                    dh.openImageCarousel();
                    dh.bookingCalendar();
                    dh.bookingStep1();
                    dh.bookingStep2();
                    dh.bookingStep3();
                    //dh.bookingStep4();
                    dh.bookingStep5();
                    $("div#bookingSteps ul").on('click', 'li.completed', function() {
                        window.location = $(this).data("link");
                    });
                    break;
                case 'reset':
                    $('#submitforgotPassword').on('click', function() {
                        valid = true;
                        var $emailInputForgot = $('input#resetEmail');
                        var emailForgot = $emailInputForgot.val();
                        var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                        if (emailPattern.test(emailForgot) === false) {
                            valid = false;

                            $emailInputForgot.parent().addClass('error');
                            $emailInputForgot.parent().find('.errorMessage').text('Error: enter valid e-mail address');
                            $emailInputForgot.parent().find('div.valid').hide();
                        } else {
                            $emailInputForgot.parent().removeClass('error');
                            //$emailInputForgot.parent().find('div.valid').show();
                            $emailInputForgot.parent().find('.errorMessage').text('');
                        }

                        if (valid) {
                            $.ajax({
                                type: "POST",
                                url: "/mycommunity/forgottenpassword",
                                data: {email: emailForgot}
                            }).done(function(msg) {
                                if (typeof msg.success != 'undefined') {
                                    //$('div#lightboxForgotPasswordForm').hide();
                                    $emailInputForgot.parent().find('div.valid').show().delay(1000).queue(function(){
                                        $('div#resetSuccess').show();
                                        $('div#resetErrorForm').hide();
                                    });
                                } else {
                                    $emailInputForgot.val('');
                                    $emailInputForgot.parent().addClass('error');
                                    $emailInputForgot.parent().find('.errorMessage').text('No account found under the email address you entered.');
                                    $emailInputForgot.parent().find('div.valid').hide();
                                }
                            });
                        }
                    });
                    break;
                case 'accountJoin':
                    $('span.checkbox').on('click', function() {
                        if ($(this).hasClass('checked')) {
                            $(this).removeClass('checked');
                        } else {
                            $(this).addClass('checked');
                        }                        
                    }); 
                     
                    $('.joinSubmitButton').on('click', function() {
                        valid = true;
                        var $emailInput = $('input#joinEmail');
                        var emailVal = $emailInput.val();
                        var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        if (emailPattern.test(emailVal) === false) {
                            valid = false;

                            $emailInput.parent().addClass('error');
                            $emailInput.parent().find('.errorMessage').text('Error: enter a valid e-mail address');
                            $emailInput.parent().find('div.valid').hide();
                        } else {
                            $emailInput.parent().removeClass('error');
                            $emailInput.parent().find('.errorMessage').text('');
                        }

                        var $termsCheckbox = $('span.checkbox');
                        var termsVal = $termsCheckbox.hasClass('checked') ? true : false;
                        if (termsVal === false) {
                            valid = false;

                            $termsCheckbox.parent().parent().addClass('error');
                            $termsCheckbox.parent().parent().find('.errorMessage').text('Error: agree with legal terms');
                            $termsCheckbox.parent().parent().find('div.valid').hide();
                        } else {
                            $termsCheckbox.parent().parent().removeClass('error');
                            $termsCheckbox.parent().parent().find('.errorMessage').text('');
                        }

                        if (valid) {
                            window.dataLayer = window.dataLayer || [];
                            dataLayer.push({
                                'event':'registration',
                                'regtype':'mail'
                            });

                            var communitySignupSource = 66; // 66 is for Community page registration
                            var travelAgent = '';
                            $.ajax({
                                type: "POST",
                                url: "/user/ajax/register",
                                data: {emailAddress: $emailInput.val(), signupSource: communitySignupSource, travelAgent: travelAgent}
                            }).done(function(msg) {
                                s.events = 'even1, event4';
                                s.prop4 = 'signup';
                                if (typeof msg.success != 'undefined') {
                                    $('div#lightboxExplore').dialog('open');  
                                } else {
                                    $emailInput.parent().addClass('error');
                                    $emailInput.parent().find('.errorMessage').text("Error: The email address is in use");
                                    $emailInput.parent().find('div.valid').hide();
                                }
                            });
                        }
                    });
                    break;
                case 'overview':
                    dh.setOverlayHeight();
                    $(window).resize(function() {
                        dh.setOverlayHeight();
                    }); 
                    break;
                case 'travelpro-signup':
                    dh.videoLightbox();
                    break;
                case 'cancellation':
                    dh.confirmCancelLightbox();
                    break;
                case 'stories':
                    dh.videoLightbox();
                    break;
                case 'gallery':
                    var galleryPage = new GalleryPage();
                    galleryPage.init();
                    break;
            }

            // general JS
            dh.bookingWidget();
            dh.responsiveEmbeds();
            dh.sidebar();
            dh.lightboxes();
            //those functions for lightbox update preferences account, which can be called in any page from sidebar
            dh.togglerCntr();
            dh.deleteAccount();
            dh.pdpSeparatorLogic();
            dh.fixFooterBottom();
        },

        clickableAreas: function() {

            $('ul.deals')
                .css('cursor', 'pointer')
                .find('> li')
                .each(function() {
                    $(this).on('click', function() {
                        var url = $(this).find('a').first().attr('href');
                        if (typeof url !== 'undefined' && url != null) {

                            window.location = url;
                        }
                    })
                });

            $('div#pbpBookRooms > ul > li')
                .each(function() {
                    $(this)
                        .css('cursor', 'pointer')
                        .on('click', function() {
                            var url = $(this).find('a.bookNow').first().attr('href');
                            if (typeof url !== 'undefined' && url != null) {

                                window.location = url;
                            }
                        });
                });

            $('div.memberHotels')
                .each(function() {
                    var element = $(this);
                    
                    if (
                        typeof(element.data('nobrokenclickable')) == 'undefined'
                        || !element.data('nobrokenclickable')
                    ) {
                        element.css('cursor', 'pointer');
                        element.on('click', function() {
                            var url = $(this).find('div.order > div.book  > a').first().attr('href');
                            
                            if (typeof url !== 'undefined' && url != null) {
                                window.location = url;
                            }
                        });
                    }
                });

            $('div#countryCityListCol2 > ul > li')
                .each(function() {
                    $(this)
                        .css('cursor', 'pointer')
                        .on('click', function() {
                            var url = $(this).find('a').first().attr('href');
                            if (typeof url !== 'undefined' && url != null) {

                                window.location = url;
                            }
                        });
                });
        },
        secureCampaign: function() {
            if ($('div#secureCampaign').length ) {
                var lightboxOptions = {
                    autoOpen: false,
                    dialogClass: 'lightboxDialog',
                    height: 'auto',
                    modal: true,
                    width: '800',
                    resizable: false,
                    open: function(event, ui) {
                        $("body").css({overflow: 'hidden'});
                        $("body").addClass('lightbox-currently-open');
                    },
                    beforeClose: function(event, ui) {
                        $("body").css({overflow: 'inherit'});
                        $("body").removeClass('lightbox-currently-open');
                    }
                };
                mobileLightboxOptions = dh.standardMobileLightboxOptions;

                var updatePreferencesOptions = {
                    autoOpen: false,
                    dialogClass: 'lightboxDialog',
                    height: 'auto',
                    modal: true,
                    width: '800',
                    resizable: false,
                    open: function(event, ui) {
                        $("body").css({overflow: 'hidden'});
                        $("body").addClass('lightbox-currently-open');
                    },
                    beforeClose: function(event, ui) {
                        $("body").css({overflow: 'inherit'});
                        $("body").removeClass('lightbox-currently-open');
                    }
                }

                if($(window).width() <= MobileScreenSize){
                    $('div#secureCampaign').dialog(mobileLightboxOptions);
                    $('div#secureCampaign').dialog('open');
                }else{
                    $('div#secureCampaign').dialog(updatePreferencesOptions);
                    $('div#secureCampaign').dialog('open');
                }

                $(window).resize(function() {
                    if($(window).width() <= MobileScreenSize){
                        $('div#secureCampaign').dialog(mobileLightboxOptions);
                    }else{
                        $('div#secureCampaign').dialog(updatePreferencesOptions);
                    }
                });

                $('div#secureCampaign').dialog('open');

                $('div#lightboxPromoForm').on('click', 'a.submit', function() {
                    $.ajax({
                        type: "POST",
                        url: "/deals/promo",
                        data: {'promoCode': $('input#promoCode').val()}
                    }).done(function(valid) {
                        if(valid){
                            var title = 'Congrats,<br>';
                            title += '<span>You now have access to special rates.</span>';

                            var html = '<div class="promoWrapper">';
                            html += '<a href="javascript:void(0)" class="refresh">GO EXPLORE</a>';
                            html += '</div>';
                            $('#secureCampaign h5').empty();
                            $('#lightboxPromoForm form').empty();
                            $('#secureCampaign h5').append(title);
                            $('#lightboxPromoForm form').append(html);
                        }else{
                            $('#promoCode').css('box-shadow', '0 1px 2px -1px rgba(0,0,0,0.5),inset 0 0 0 5px #eb0000');
                            $('.error').show();
                        }
                    });
                });
                $('div#secureCampaign').on('click', 'a.close', function() {
                    var url = window.location.href;
                    var shortened_url = url.indexOf('/deals/');
                    url = url.substring(0, shortened_url != -1 ? shortened_url : url.length);
                    window.location.href = url+'/deals';
                    return false;
                });
                $('div#lightboxPromoForm').on('click', 'a.refresh', function() {
                    location.reload();
                });

                $('#secureCampaign form').bind("keyup keypress", function(e) {
                  var code = e.keyCode || e.which;
                  if (code  == 13) {
                    e.preventDefault();
                    $('#secureCampaign form a.submit').click();
                  }
                });
            }
        },

        deepLinking: function() {
            setTimeout(function() {
                if(window.location.hash) {
                    var hashUrl = window.location.hash;
                    var allFilters = hashUrl.split("#");
                    allFilters.shift();

                    $(allFilters).each(function(index, selectedFilter){
                        selectedFilter = selectedFilter.split("-").join(" ");
                        
                        $("div.sidebarHelper div.sidebarDiv ul li").each(function(){
                            var filterVal = $(this).text();
                            filterVal = filterVal.replace(/ \((\d+)\)/, '').trim();
                            if(filterVal.toLowerCase() == selectedFilter.toLowerCase()) {
                                $(this).trigger("click");
                            }
                        });
                    });
                }
            }, 500);
        },

        toggleFeaturedHotelsList: function() {

            $('div#fiFeaturedHotels > p.viewAll')
                .on('click', function() {
                    jQuery('div#fiFeaturedHotels > ul > li.hide-me')
                        .each(function() {
                            $(this).slideToggle();
                        });
                });
        },

        openRoomCarousel: function() {

            $('div.room-carousel').flexslider({
                animation: "fade",
                keyboard: true,
                slideshow: false,
                touch: true,
                controlNav: false,
                directionNav: true,
                prevText: ' ',
                nextText: ' ',
                itemMargin: 0,
                animationLoop: true,
                useCss: true,
                easing: "swing"
            });

            $("div.room-carousel-cntr").dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: true,
                closeText: 'hide',
                draggable: false,
                height: '100%',
                clickOutside: true,
                clickOutsideTrigger: '.img-count, #imgCount'

            });

            $('html')
                .on('click', '.img-count, #imgCount', function() {
                    var winHeight = $(window).height() - 40;
                    var baseClass = $(this).attr('id');//$(this).parents('li').attr('class').replace(/^hide\s+/i, '');

                    if (typeof(baseClass) == 'undefined'){
                        baseClass='imgCount';
                    }

                    $('#rcc' + baseClass).dialog('open');
                    $('#rcc' + baseClass).parent().find('div.ui-resizable-handle').css('display', 'none');

                    $("div.room-carousel-cntr").dialog( "option", "height", winHeight );
                    width = winHeight * (4/3);

                    // if($('.flex-active-slide img').attr('data-portrait') == 'true'){
                    //     console.log($('.flex-active-slide img').attr('alt'));
                    //     width = winHeight;
                    // }
                    $("div.room-carousel-cntr").dialog( "option", "width", width );
                    // $("div.room-carousel-cntr").dialog( "option", "position", { my: 'center center', at: 'center top', of: window } );
                    $('div.room-carousel-cntr.ui-dialog-content.ui-widget-content').css('height', 'auto');

                    $('#closeSlide' + baseClass).on('click', function() { $('#rcc' + baseClass).dialog('close'); });
                });
        },
        openImageCarousel: function(){
            (function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };
            }(jQuery, jQuery.fancybox));
            var roomId;
            $('.callfancybox').click(function(){
                roomId = $(this).attr('data-roomId');
                if($("li."+roomId).find('.carousel_container img').length == 0) {
                    arrowsDisplay = false;
                }  else {
                    arrowsDisplay = true;
                }

                $(".fancybox"+roomId)
                    .attr('rel', 'gallery')
                    .fancybox({

                        padding : 0,

                        autoResize  : true,
                        nextClick   :true ,
                        nextMethod  : 'resizeIn',
                        openEffect  : 'elastic',
                        closeEffect : 'elastic',
                        nextEffect  : 'fade',
                        width       : '95%',
                        height      : '95%',
                        arrows      : arrowsDisplay,
                        helpers : {
                            title : {
                                type : 'inside'
                            }
                        }
                        //afterLoad:function(){
                           // $.fancybox.hideLoading();
                        //}
                });

            });
            $(".fancybox")
                .attr('rel', 'gallery')
                .fancybox({

                    padding : 0,

                    autoResize  : true,
                    nextClick   :true ,
                    nextMethod  : 'resizeIn',
                    openEffect  : 'elastic',
                    closeEffect : 'elastic',
                    nextEffect  : 'fade',
                    width       : '95%',
                    height      : '95%',

                    helpers : {
                        title : {
                            type : 'inside'
                        }
                    }//,
                    //beforeLoad:function(){
                       // $.fancybox.showLoading();
                   // },
                    //afterLoad:function(){
                       // $.fancybox.hideLoading();
                    //}
                });


        },

        setOverlayHeight: function() {

            var parentPadding = 70;
            var parentMargin = 70;
            var parentHeight = $('div#main').height() + parentPadding + parentMargin;

            $('header#searchEngine').css('background-color', $('section.communityNoLogin').length > 0 ? '#F6F6F6' : '#ECECEC');
            //$('section.communityNoLogin').css('height', parentHeight + 'px');
        },

        resizeSearchBar: function() {

            var mainDivWidth = $('div#main').width();
            $('header#searchEngine').css('width', mainDivWidth + 'px');
            $(window).resize(function() {

                var mainDivWidth = $('div#main').width();
                $('header#searchEngine').css('width', mainDivWidth + 'px');
            });
        },

        // General Functions
        bookingWidget: function() {
            //blink = setInterval(dh.blink, 1000);
           // dh.blink();
            dh.bookingWidgetAutocomplete();
            dh.bookingWidgetGuests();
            dh.bookingWidgetCalendar();

            $searchInput = $('li#searchInputWrap input');
            $searchIcon = $('li#searchIcon');
            $searchButton = $('li#searchButtonWrap a');

            $searchInput.focus(function() {
                $searchInput.parent().removeClass('error');
                //blink = setInterval(dh.blink, 1000);
                $searchIcon.removeClass('error');
                $('li.blink').css('background-color','#c3c3c3');

            });

            var searchFunction = function(ev) {
                ev.preventDefault();

                if ($searchInput.val() == '') {
                    $searchInput.parent().addClass('error');
                    $searchInput.attr("placeholder", "Please enter a valid destination, hotel");
                    $searchIcon.remove('span');
                    //clearInterval(blink);
                    $('li.blink').css('background-color','#ffffff');
                    $searchIcon.addClass('error');
                } else {
                    var hasDates = false;
                    var dateRange = '';
                    var strDateStart = $('li#searchDateCheckIn > input').val();
                    var strDateEnd = $('li#searchDateCheckOut > input').val();
                    var adult = $('input#guestsPickerAdultsValue');
                    var children = $('input#guestsPickerChildrenValue');
                    var suggestionText = $("ul#searchEngineAutocomplete > li.auto-suggest-list div.selected").html();
                    
                    if (typeof suggestionText !== 'undefined' && suggestionText !== '') {
                        $searchInput.val(suggestionText);
                    }
                    if (strDateStart != null && strDateStart.replace(/\s+/i, '').length > 0 && strDateEnd != null && strDateEnd.replace(/\s+/i, '').length > 0) {
                        var dateStart = new Date(strDateStart);
                        var dateEnd = new Date(strDateEnd);
                        dateStart = dateStart.getFullYear() + '-' + (dateStart.getMonth() + 1) + '-' + dateStart.getDate();
                        dateEnd = dateEnd.getFullYear() + '-' + (dateEnd.getMonth() + 1) + '-' + dateEnd.getDate();
                        dateRange = '&start=' + dateStart + '&end=' + dateEnd;
                        hasDates = true;
                    }

                    var location = '/search?q=' + encodeURIComponent($searchInput.val()) + '&adults=' + $(adult).val() + '&children=' + $(children).val()  ;

                    window.location = hasDates ? location + dateRange : location;
                }

                if (typeof dh.state.ajaxSearchBar !== 'undefined') {

                    dh.state.ajaxSearchBar.abort();
                }
            };

            $searchButton.click(searchFunction);
            $('#SearchForm').submit(searchFunction);
        },
        blink: function(){
            $('.blink').attr('style', '');
            $('.blink').animate(
                {
                    opacity: 0.01
                },
                500,
                function() {
                    $('.blink').animate(
                        {
                            opacity: 1
                        },
                        500, function() {
                            dh.blink();
                        }
                    );
                }
            );
        },

        bookingWidgetAutocomplete: function() {

            searchInput = $('li#searchInputWrap input');
       
            if ($(window).width() > 768) {
                searchInput.focus();
            }

            searchResults = $('ul#searchEngineAutocomplete').find('> li');

            searchInput.mouseout(function() {
                //blink = setInterval(dh.blink, 1000);
            });
            searchInput.mouseenter(function() {
                //clearInterval(blink);
            });
            var currentItem = -1;
            searchInput.keyup(function(e) {
                var code = e.keyCode || e.which;
                if ($searchInput.val() != '') {
                    if (typeof dh.state.ajaxSearchBar !== 'undefined') {
                        dh.state.ajaxSearchBar.abort();
                    }
                    
                    if (typeof(DH_CURRENT_VERSION) == 'undefined') {
                        DH_CURRENT_VERSION = 'master';
                    }
                    
                    dh.state.ajaxSearchBar = $.ajax({
                        type: "GET",
                        url: '/auto-complete/',
                        cache: true,
                        data: {"q": searchInput.val(), "max": 10, "lang": "en", "version": DH_CURRENT_VERSION},
                        success: function(response){
                            $('ul#searchEngineAutocomplete > li').width($('li#searchInputWrap').width() - 31);

                            $('ul#searchEngineAutocomplete > li.auto-suggest-list').width($('ul#searchEngineAutocomplete > li.auto-suggest-list').width() + 32);

                            $('ul#searchEngineAutocomplete ul').css('left', $('li#searchInputWrap').width() - 9);
                            $('ul#searchEngineAutocomplete ul li').width();

                            $(searchResults).first().html('');

                           for (var i = 0; i < response.suggestions.length; i++) {
                                var divObj = $('<div>' + response.suggestions[i] + '</div>');

                                $(searchResults).first().append(divObj);

                                $(divObj).click(function() {

                                    var suggestion = $(this).text();
                                    $('#searchButtonWrap a').focus();
                                    $(searchInput).val(suggestion);
                                    $('ul#searchEngineAutocomplete').hide();
                                });
                            }

                            $(searchResults[1]).find('> div > span.count').first().html('(' + response.properties.length + ')');
                            $(searchResults[1]).find('> ul').html('');

                            for (var i = 0; i < response.properties.length; i++) {
                                var regionVU = response.properties[i]['region_vanity_url'];
                                var countryVU = response.properties[i]['country_vanity_url'];
                                var cityVU = response.properties[i]['city_vanity_url'];
                                var hotelVU = response.properties[i]['vanity_url'];
                                var fullVU = countryVU + '/' + cityVU + '/' + hotelVU;
                                $(searchResults[1]).find('> ul').first().append('<li><a href="/hotels/' + fullVU + '">' + response.properties[i]['property_info_name'] + '</a></li>');
                            }


                            $(searchResults[2]).find('> div > span.count').first().html('(' + response.deals.length + ')');
                            $(searchResults[2]).find('> ul').html('');

                            for (var i = 0; i < response.deals.length; i++) {
                                $(searchResults[2]).find('> ul').first().append('<li><a href="/deals/' + response.deals[i]['campaign_vanity_url'] + '">' + response.deals[i]['campaign_name'] + '</a></li>');
                            }

                            $('ul#searchEngineAutocomplete').show();
                            var items = $("ul#searchEngineAutocomplete > li.auto-suggest-list div");
                            //For key down
                            if(code === 40) {
                                currentItem++;
                                if(currentItem >= items.length) currentItem = 0;
                            } else if(code === 38) { /*for key up*/
                                currentItem--;
                                if(currentItem < 0) currentItem = items.length - 1;
                            }
                            if(currentItem !== -1) {
                                items.removeClass("selected");
                                items.eq(currentItem).addClass("selected");
                               
                            }

                            /*$("ul.hotels-list").mCustomScrollbar({
                                horizontalScroll: false,
                                scrollButtons: {
                                    enable: true
                                },
                                theme: "light-thick"
                            });
                            if ($("ul.hotels-list").css("display") != "none") {
                                $("ul.hotels-list").mCustomScrollbar("update");
                            }*/

                            /*$('ul#searchEngineAutocomplete li', this).click(function(){
                                $(this).find("ul.hotels-list").show();
                                $(this).find("ul.hotels-list").mCustomScrollbar('update');
                                //$(this).find("ul.mCustomScrollbar").css("margin-right", "0px");
                            });*/

                            //$("ul.hotels-list").mCustomScrollbar("update");
                        }
                    });
                } else {
                    $('ul#searchEngineAutocomplete').hide();
                }
            });

            $('ul#searchEngineAutocomplete').click(function(ev) {
                ev.stopPropagation();
            });

            dh.el.body.click(function(ev) {
                $('ul#searchEngineAutocomplete').hide();
            });
        },

        bookingWidgetCalendar: function() {
            // In case they do end up wanting the same logic for the search bar
            var widgetCalendarSearchBar = new DhBookingCalendar(
                'searchbar',
                $('#datePicker'),
                $('#searchDateCheckIn'),
                $('#searchDateCheckOut'),
                $('#searchDateCheckIn > a'),
                $('#searchDateCheckIn > input'),
                $('#searchDateCheckOut > a'),
                $('#searchDateCheckOut > input')
            );
            widgetCalendarSearchBar.init();
            
            if ($('#availability-widget-search-holder').length) {
                var widgetCalendar = new DhBookingCalendar(
                    'search',
                    $('#availability-widget-datePicker'),
                    $('#availability-widget-search .control-check-in .label'),
                    $('#availability-widget-search .control-check-out .label'),
                    $('#availability-widget-search .control-check-in .label'),
                    $('#availability-widget-search .control-check-in > input'),
                    $('#availability-widget-search .control-check-out .label'),
                    $('#availability-widget-search .control-check-out > input')
                );
                widgetCalendar.init();
            } else if ($('#availability-widget-datePicker').length) {
                var widgetCalendar = new DhBookingCalendar(
                    'widget',
                    $('#availability-widget-datePicker'),
                    $('#availability-widget .control-check-in .label'),
                    $('#availability-widget .control-check-out .label'),
                    $('#availability-widget .control-check-in .label'),
                    $('#availability-widget .control-check-in > input'),
                    $('#availability-widget .control-check-out .label'),
                    $('#availability-widget .control-check-out > input')
                );
                widgetCalendar.init();
            }
            
            //Calendar Icon click functionality
            $(".icon-calendar").on("click", function(){
                $(this).prev().mousedown();
            });
        },
        populateAvailability: function() {
        
        },
        bookingWidgetGuests: function() {
            $('li#searchGuestsWrapper').click(function(ev) {
                ev.stopPropagation();
                $('ul#guestsPickerWidget').width($('li#searchGuestsWrapper').width()).hide();
                $('ul#guestsPicker').width($('li#searchGuestsWrapper').width()).toggle();

                $(this).addClass('selected');
            });
            
            $('div.control-occupants').click(function(ev) {
                ev.stopPropagation();
                $('ul#guestsPicker').width($('li#searchGuestsWrapper').width()).hide();
                $('ul#guestsPickerWidget').width($('li#searchGuestsWrapper').width()).toggle();

                $(this).addClass('selected');
            });

            $('ul#guestsPicker, ul#guestsPickerWidget').click(function(ev) {
                ev.stopPropagation();
            });

            dh.el.body.click(function(ev) {
                $('ul#guestsPicker').hide();
                $('ul#guestsPickerWidget').hide();
            });

            var adult = $("input#guestsPickerAdultsValue").val();
            $('input#guestsPickerAdultsValue').spinsterPro({
                'max'     : 8,
                'min'     : 1,
                'start'   : adult,
                'target'  : $('#guestsPickerAdults div.spinner'),
                'onChange': function(value) {
                    var label = (parseInt(value) == 1) ? ' Adult' : ' Adults';

                    $('span#searchGuestsTotalAdults').text(value + label);
                    $(' #label_adult').text(label);


                }
            });

            var child = $("input#guestsPickerChildrenValue").val();
            $('input#guestsPickerChildrenValue').spinsterPro({
                'max'     : 8,
                'min'     : 0,
                'start'   : child,
                'target'  : $('#guestsPickerChildren div.spinner'),
                'onChange': function(value) {
                    var label = (parseInt(value) == 1) ? ' Child' : ' Children';
                    $('span#searchGuestsTotalChildren').text(value + label);
                    $('#label_children').text(label);
                }
            });
            
            $('input#guestsPickerAdultsValueWidget').spinsterPro({
                'max'     : 8,
                'min'     : 1,
                'start'   : adult,
                'target'  : $('ul#guestsPickerWidget #guestsPickerAdultsWidget div.spinner'),
                'onChange': function(value) {
                    var labelAdult = (parseInt(value) == 1) ? ' Adult' : ' Adults';
                    
                    var labelChild = (parseInt($('input#guestsPickerChildrenValueWidget').val()) == 1) ? ' Child' : ' Children';

                    var widget = $('#availability-widget .control-occupants .label span, #availability-widget-search-holder .control-occupants .label span');
                    widget.text(value+' '+labelAdult+', '+$('input#guestsPickerChildrenValueWidget').val()+' '+labelChild);
                }
            });

            $('input#guestsPickerChildrenValueWidget').spinsterPro({
                'max'     : 8,
                'min'     : 0,
                'start'   : child,
                'target'  : $('ul#guestsPickerWidget #guestsPickerChildrenWidget div.spinner'),
                'onChange': function(value) {
                    var labelAdult = (parseInt($('input#guestsPickerAdultsValueWidget').val()) == 1) ? ' Adult' : ' Adults';
                    
                    var labelChild = (parseInt(value) == 1) ? ' Child' : ' Children';

                    var widget = $('#availability-widget .control-occupants .label span, #availability-widget-search-holder .control-occupants .label span');
                    widget.text($('input#guestsPickerAdultsValueWidget').val()+' '+labelAdult+', '+value+' '+labelChild);
                }
            });
        },

        responsiveEmbeds: function() {
            $("div#main div.imageWrapper").fitVids();
        },

        sidebar: function() {
            /*$("div.currencySwitchList ul").mCustomScrollbar({
                horizontalScroll: false,
                scrollButtons: {
                    enable: true
                },

                theme: "light-thick"
            });*/

            $('a#logout').click(function(ev){
                s.event = 'event13';
            });
            $('div.currencySwitchButton').click(function(ev) {
                ev.stopPropagation();

                $(this).parent().find('div.currencySwitchList').show();
                $(this).parent().find('div.currencySwitchList ul').mCustomScrollbar('update');
            });

            $('div.currencySwitchList a').click(function() {
                var currency = $(this).data('currency');

                $.ajax({
                    type: "POST",
                    url: "/user/ajax/setcurrency",
                    data: {currency: currency}
                }).done(function(msg) {
                    if (typeof msg.currency != 'undefined') {
                        location.reload();
                    }
                });
            });

            dh.el.body.click(function(ev) {
                $('div.currencySwitchList').hide();
            });
        },
        deleteAccount: function(){
            $('a#lnkDelete').on('click', function() {
                $('div#lightboxAccountDelete').dialog('open');
            });
            $('a#lnkDeleteAccount').on('click', function() {
                s.event = 'event5';
                s.prop4 = 'cancelled';
                $('form#frmAccountDelete').submit();
            });
            $('a#cancel').on('click', function() {
                $(this).closest('div.lightbox').dialog('close');
            });
                        $('a#cancelConfirmation').on('click', function() {
                $(this).closest('div.lightbox').dialog('close');
            });
        },
        togglerCntr: function(){
            var updateToggling = function(element) {
                var xPos = element.css('left');

                if (parseInt(xPos) >= 22.5) {
                    if (element.parent().attr('data-text') != undefined){
                        text = 'No';
                    } else {
                        text = 'Off';
                    }
                    element.parent().attr('data-value', 0).data('value', 0).removeClass('on').addClass('off').find('.toggler-label').first().text(text);
                    $('input#hdn' + element.parent().attr('id')).val(0);
                    element.closest('div#userType').find("span.element-label").removeClass('userTypeOn').addClass('userTypeOff');
                    element.closest('div#userType').find("span.travel-professional").removeClass('userTypeOff').addClass('userTypeOn');
                }

                else {
                    if (element.parent().attr('data-text') != undefined){
                        text = 'Yes';
                    } else {
                        text = 'On';
                    }
                    element.parent().attr('data-value', 1).data('value', 1).removeClass('off').addClass('on').find('.toggler-label').first().text(text);
                    $('input#hdn' + element.parent().attr('id')).val(1);
                    element.closest('div#userType').find("span.element-label").removeClass('userTypeOff').addClass('userTypeOn');
                    element.closest('div#userType').find("span.travel-professional").removeClass('userTypeOn').addClass('userTypeOff');
                }

                element.css('left', parseInt(xPos) >= 22.5 ? '45px' : '0');
            }

            $('span.toggler').draggable({
                containment: "parent",
                stop: function() {
                    updateToggling($(this));
                }
            });

            $('span.toggler-cntr').click(function() {
                if (!ScreenSizeIsMobile) {
                    // return;
                }

                var element = $(this).find('.toggler');

                var xPos = element.css('left');

                if (parseInt(xPos) >= 22.5) {
                    element.css('left', '0px');
                } else {
                    element.css('left', '45px');
                }

                updateToggling(element);
            });
        },
        dealsOverview:function(){
            $(".description")
                .mouseover(function() {
                    $(this).find('.shortDesc').hide();
                    $(this).find('.longDesc').show();
                    $(this).height('auto');
                    if($(this).height() <= 86){
                        $(this).height('68px');
                    }
                })
                .mouseout(function() {
                        $(this).find('.longDesc').hide();
                        $(this).find('.shortDesc').show();

                        if(  $(window).height() > 1150){
                            $(this).height('68px');
                        }
                    }
                );
        },
        lightboxes: function() {
            lightboxOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                height: 'auto',
                modal: true,
                width: '430',
                resizable: false,
                open: function(event, ui) {
                    $("body").css({overflow: 'hidden'});
                    $("body").addClass('lightbox-currently-open');
                },
                beforeClose: function(event, ui) {
                    $("body").css({overflow: 'inherit'});
                    $("body").removeClass('lightbox-currently-open');
                }
            };
            mobileLightboxOptions = dh.standardMobileLightboxOptions;
            updatePreferencesOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                height: 'auto',
                modal: true,
                width: '610',
                resizable: false,
                open: function(event, ui) {
                    $("body").css({overflow: 'hidden'});
                    $("body").addClass('lightbox-currently-open');
                },
                beforeClose: function(event, ui) {
                    $("body").css({overflow: 'inherit'});
                    $("body").removeClass('lightbox-currently-open');
                }
            }

            lightboxOptionsRegAuto = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                modal: true,
                width: '800',
                height: 'auto',
                resizable: false,
                open: function(event, ui) {
                    $("body").css({overflow: 'hidden'});
                    $("body").addClass('lightbox-currently-open');
                },
                beforeClose: function(event, ui) {

                    var date = new Date();
                    date.setTime(date.getTime()+(7*24*60*60*1000));
                    var expires = "; expires="+date.toGMTString();
                    document.cookie = 'auto_signup_showndhSignupLightboxOpened=1; path=/'+ expires;
                    $("body").css({overflow: 'inherit'});
                    $("body").removeClass('lightbox-currently-open');
                }
            };
            
            var lightboxOptionsWhyBook = jQuery.extend({}, lightboxOptions);
            lightboxOptionsWhyBook.width = '640';
            
            // Create lightboxes

            if($(window).width() <= MobileScreenSize){
                $('div#lightboxRegister').dialog(mobileLightboxOptions);
                $('div#lightboxLogin').dialog(mobileLightboxOptions);
                $('div#lightboxLoginOrFb').dialog(mobileLightboxOptions);
                $('div#lightboxRegisterOrFb').dialog(mobileLightboxOptions);
                $('div#lightboxForgotPassword').dialog(mobileLightboxOptions);
                $('div#lightboxAccountDelete').dialog(mobileLightboxOptions);
                $('div#lightboxAccountDeleteConfirmation').dialog(mobileLightboxOptions);
                $('div#lightboxAccountSaved').dialog(mobileLightboxOptions);
                $('div#lightboxReset').dialog(mobileLightboxOptions);
                $('div#lightboxUpdatePreferences').dialog(mobileLightboxOptions);
                $('#lightboxWhyBookWithUs').dialog(mobileLightboxOptions);
                $('#lightboxChildAgeMenuWidget').dialog(mobileLightboxOptions);
                $('#lightboxChildAgeMenuSearch').dialog(mobileLightboxOptions);
                $('#lightboxRegisterAuto').dialog(mobileLightboxOptions);
                $('#lightboxExplore').dialog(mobileLightboxOptions);
            } else {
                $('div#lightboxRegister').dialog(lightboxOptions);
                $('div#lightboxLogin').dialog(lightboxOptions);
                $('div#lightboxLoginOrFb').dialog(lightboxOptions);
                $('div#lightboxRegisterOrFb').dialog(lightboxOptions);
                $('div#lightboxForgotPassword').dialog(lightboxOptions);
                $('div#lightboxAccountDelete').dialog(lightboxOptions);
                $('div#lightboxAccountDeleteConfirmation').dialog(lightboxOptions);
                $('div#lightboxAccountSaved').dialog(lightboxOptions);
                $('div#lightboxReset').dialog(lightboxOptions);
                $('div#lightboxUpdatePreferences').dialog(updatePreferencesOptions);
                $('#lightboxWhyBookWithUs').dialog(lightboxOptionsWhyBook);
                $('#lightboxChildAgeMenuWidget').dialog(mobileLightboxOptions);
                $('#lightboxChildAgeMenuSearch').dialog(mobileLightboxOptions);
                $('#lightboxRegisterAuto').dialog(lightboxOptionsRegAuto);
                $('#lightboxExplore').dialog(lightboxOptions);
            };

            $(window).resize(function() {
                if($(window).width() <= MobileScreenSize){
                    $('div#lightboxRegister').dialog(mobileLightboxOptions);
                    $('div#lightboxLogin').dialog(mobileLightboxOptions);
                    $('div#lightboxLoginOrFb').dialog(mobileLightboxOptions);
                    $('div#lightboxRegisterOrFb').dialog(mobileLightboxOptions);
                    $('div#lightboxForgotPassword').dialog(mobileLightboxOptions);
                    $('div#lightboxAccountDelete').dialog(mobileLightboxOptions);
                    $('div#lightboxAccountDeleteConfirmation').dialog(mobileLightboxOptions);
                    $('div#lightboxAccountSaved').dialog(mobileLightboxOptions);
                    $('div#lightboxReset').dialog(mobileLightboxOptions);
                    $('div#lightboxUpdatePreferences').dialog(mobileLightboxOptions);
                    $('#lightboxWhyBookWithUs').dialog(mobileLightboxOptions);
                    $('#lightboxChildAgeMenuWidget').dialog(mobileLightboxOptions);
                    $('#lightboxChildAgeMenuSearch').dialog(mobileLightboxOptions);
                    $('#lightboxRegisterAuto').dialog(mobileLightboxOptions);
                    $('#lightboxExplore').dialog(mobileLightboxOptions);
                } else {
                    $('div#lightboxRegister').dialog(lightboxOptions);
                    $('div#lightboxLogin').dialog(lightboxOptions);
                    $('div#lightboxLoginOrFb').dialog(lightboxOptions);
                    $('div#lightboxRegisterOrFb').dialog(lightboxOptions);
                    $('div#lightboxForgotPassword').dialog(lightboxOptions);
                    $('div#lightboxAccountDelete').dialog(lightboxOptions);
                    $('div#lightboxAccountDeleteConfirmation').dialog(lightboxOptions);
                    $('div#lightboxAccountSaved').dialog(lightboxOptions);
                    $('div#lightboxReset').dialog(lightboxOptions);
                    $('div#lightboxUpdatePreferences').dialog(updatePreferencesOptions);
                    $('#lightboxWhyBookWithUs').dialog(lightboxOptionsWhyBook);
                    $('#lightboxChildAgeMenuWidget').dialog(mobileLightboxOptions);
                    $('#lightboxChildAgeMenuSearch').dialog(mobileLightboxOptions);
                    $('#lightboxRegisterAuto').dialog(lightboxOptionsRegAuto);
                    $('#lightboxExplore').dialog(lightboxOptions);
                }
            });

            //Pass target URL
            // REGISTER
            $('#lightboxRegisterForm form').bind("keyup keypress", function(e) {
              var code = e.keyCode || e.which;
              if (code  == 13) {
                e.preventDefault();
                $('#lightboxRegisterForm form a.submit').click();
              }
            });
            $('#lightboxRegisterForm form').bind("keyup keypress", function(e) {
              var code = e.keyCode || e.which;
              if (code  == 13) {
                e.preventDefault();
                $('#lightboxRegisterForm form a.submit').click();
              }
            });
            $('div#lightboxRegister').on('click', 'a.submit', function() {
               // var min_price_range = 0;
                var valid = true;
                var $emailAddress = $('input#registerEmailAddress');
                //var $emailAddressConfirm = $('input#registerEmailAddressConfirm');
                var emailAddress = $emailAddress.val();
                var signupSource = $('input#signup_source').val();
                var travelAgent = $('input#travelAgent').val();
                var agreeWithTerms = $('input#agreeWithTerms').val();

                // if (emailAddress != $('input#registerEmailAddressConfirm').val() ) {
                //     valid = false;

                //     $emailAddressConfirm.parent().addClass('error');
                //     $emailAddressConfirm.parent().find('.errorMessage').text('Error: does not match');
                //     $emailAddressConfirm.parent().find('div.valid').hide();
                // } else {
                //     $emailAddressConfirm.parent().removeClass('error');
                //     $emailAddressConfirm.parent().find('div.valid').show();
                //     $emailAddressConfirm.parent().find('.errorMessage').text('');
                // }

                if (agreeWithTerms == 0) {
                    valid = false;
                    $('input#agreeWithTerms').parent().addClass('error');
                    $('input#agreeWithTerms').parent().find('.errorMessage').text('Error: agree with legal terms');
                    $('input#agreeWithTerms').parent().find('.checkbox').focus();
                    //$('input#agreeWithTerms').parent().find('div.valid').hide();
                } else {
                    $('input#agreeWithTerms').parent().removeClass('error');
                    //$('input#agreeWithTerms').parent().find('div.valid').show();
                    $('input#agreeWithTerms').parent().find('.errorMessage').text('');
                }
                var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (emailPattern.test(emailAddress) == false) {
                    valid = false;

                    $emailAddress.parent().addClass('error');
                    $emailAddress.parent().find('.errorMessage').text('Error: enter a valid e-mail address');
                    $emailAddress.parent().find('div.valid').hide();
                    $emailAddress.focus();
                } else {
                    $emailAddress.parent().removeClass('error');
                    //$emailAddress.parent().find('div.valid').show();
                    $emailAddress.parent().find('.errorMessage').text('');
                }

                if (valid) {
                    $.ajax({
                        type: "POST",
                        url: "/user/ajax/register",
                        data: {emailAddress: emailAddress, signupSource: signupSource, travelAgent: travelAgent}
                    }).done(function(msg) {
                        s.events = 'even1, event4';
                        s.prop4 = 'signup';
                        if (typeof msg.success != 'undefined') {
                            $emailAddress.parent().find('div.valid').show().delay(1000).queue(function(){
                                if (ScreenSizeIsMobile) {
                                    window.location.href = '/mycommunity/thankyou';
                                } else {
                                    $('div#lightboxRegisterForm').hide();
                                    $('div#lightboxRegisterSuccess').show();
                                }
                            });
                            
                        } else {
                            $('input#registerEmailAddress').parent().addClass('error');
                            $('input#registerEmailAddress').parent().find('.errorMessage').text("Error: The email address is in use");
                            $('input#registerEmailAddress').parent().find('div.valid').hide();
                        }
                    });
                }
            });
            $('#lightboxLoginForm form').bind("keyup keypress", function(e) {
              var code = e.keyCode || e.which;
              if (code  == 13) {
                e.preventDefault();
                $('#lightboxLoginForm form a.submit').click();
              }
            });
            //LOGIN
            $('div#lightboxLogin').on('click', 'a.submit', function() {
                valid = true;
                var $emailInputLogin = $('input#loginEmailAddress');
                var $passwordInputLogin = $('input#loginPassword');
                var emailLogin = $emailInputLogin.val();
                var passwordLogin = $passwordInputLogin.val();
                //validation
                var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (emailPattern.test(emailLogin) === false) {
                    valid = false;

                    $emailInputLogin.parent().addClass('error');
                    $emailInputLogin.parent().find('.errorMessage').text('Error: enter valid e-mail address');
                    $emailInputLogin.parent().find('div.valid').hide();
                } else {
                    $emailInputLogin.parent().removeClass('error');
                   // $emailInputLogin.parent().find('div.valid').show();
                    $emailInputLogin.parent().find('.errorMessage').text('');
                }

                if (passwordLogin == '') {
                    valid = false;

                    $passwordInputLogin.parent().addClass('error');
                    $passwordInputLogin.parent().find('.errorMessage').text('Error: enter password');
                    $passwordInputLogin.parent().find('div.valid').hide();
                } else {
                    $passwordInputLogin.parent().removeClass('error');
                    //$passwordInputLogin.parent().find('div.valid').show();
                    $passwordInputLogin.parent().find('.errorMessage').text('');
                }

                if (valid) {
                    $.ajax({
                        type: "POST",
                        url: "/mycommunity/login",
                        data: {'email': emailLogin, 'password': passwordLogin,'fromBookingFlow': $('input#fromBookingFlow').val()}
                    }).done(function(msg) {
                        s.event = 'event12';
                        data = JSON.parse(msg);

                        if (data.success == 'true') {
                            $emailInputLogin.parent().find('div.valid').show();
                            $passwordInputLogin.parent().find('div.valid').show().delay(1000).queue(function(){
                            
                            if ($('input#fromBookingFlow').val() == 1) {
                                $('#bookingFirstName').val(data.first);
                                $('#bookingLastName').val(data.last);
                                $('#bookingCountry').val(data.country);
                                countryname= $('#bookingCountry option[value="'+data.country+'"]').text();

                                $('.bookingCountry_selectbox .text').text(countryname);
                                $('#bookingAddress1').val(data.address_line1);
                                $('#bookingAddress2').val(data.address_line2);
                                $('#bookingCity').val(data.city);
                                $('#bookingState').val(data.state);

                                statename = $('#bookingState option[value="'+data.state+'"]').text();
                                $('.bookingState_selectbox .text').text(statename);

                                $('#bookingPostcode').val(data.postalcode);

                                $('div#lightboxLogin').dialog('close');
                            } else {
                                $('div#lightboxLogin').dialog('close');

                                if ((typeof(thisIsStep3) != 'undefined' && thisIsStep3)
                                    || (typeof(isStory) != 'undefined' && isStory)
                                    || (typeof(thisIsShoppingCartCheckout) != 'undefined' && thisIsShoppingCartCheckout)
                                    || (typeof(isCampaign) != 'undefined' && isCampaign)
                                    || (typeof(reloadAfterLogin) != 'undefined' && reloadAfterLogin)) {
                                    window.location.reload();
                                } else {
                                    window.location = "/mycommunity/overview";
                                }
                            }
                            });
                        } else {
                            //wrong parametes pass
                            $emailInputLogin.val('');
                            $passwordInputLogin.val('');
                            $emailInputLogin.parent().addClass('error');
                            $passwordInputLogin.parent().addClass('error');
                            $emailInputLogin.parent().find('.errorMessage').html('Email address and password do not match our records.<br /><br />If you had an account with us before, please<br /> reset it by clicking "Forgot Password"');
                            $passwordInputLogin.parent().find('div.valid').hide();
                            $emailInputLogin.parent().find('div.valid').hide();
                        }
                    });
                }
            });

            /* It seems to be duplicate code
            $('#lightboxForgotPasswordForm form').bind("keyup keypress", function(e) {
              var code = e.keyCode || e.which;
              if (code  == 13) {
                e.preventDefault();
                $('#lightboxForgotPasswordForm form a.submit').click();
              }
            }); */
            
            //Forgot Password
            $('#lightboxForgotPasswordForm form').bind("keypress", function(e) {
              var code = e.keyCode || e.which;
              if (code  == 13) {
                e.preventDefault();
                $('#lightboxForgotPasswordForm form a.submit').click();
              }
            });
            $('div#lightboxForgotPassword').on('click', 'a.submit', function() {
                valid = true;
                var $emailInputForgot = $('input#forgottenpasswordEmailAddress');
                var emailForgot = $emailInputForgot.val();
                var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (emailPattern.test(emailForgot) === false) {
                    valid = false;

                    $emailInputForgot.parent().addClass('error');
                    $emailInputForgot.parent().find('.errorMessage').text('Error: enter valid e-mail address');
                    $emailInputForgot.parent().find('div.valid').hide();
                } else {
                    $emailInputForgot.parent().removeClass('error');
                    //$emailInputForgot.parent().find('div.valid').show();
                    $emailInputForgot.parent().find('.errorMessage').text('');
                }

                if (valid) {
                    $.ajax({
                        type: "POST",
                        url: "/mycommunity/forgottenpassword",
                        data: {email: emailForgot}
                    }).done(function(msg) {
                        if (typeof msg.success != 'undefined') {
                            $emailInputForgot.parent().find('div.valid').show().delay(1000).queue(function(){
                                $('div#lightboxForgotPasswordForm').hide();
                                $('div#lightboxRegisterSuccess').show();
                            });
                                
                        } else {
                            $emailInputForgot.val('');
                            $emailInputForgot.parent().addClass('error');
                            $emailInputForgot.parent().find('.errorMessage').text('No account found under the email address you entered.');
                            $emailInputForgot.parent().find('div.valid').hide();
                        }
                    });
                }
            });

            $('#resendAgain').on('click',  function() {
                var $emailInputForgot = $('input#forgottenpasswordEmailAddress');
                var emailForgot = $emailInputForgot.val();
                $.ajax({
                    type: "POST",
                    url: "/mycommunity/forgottenpassword",
                    data: {email: emailForgot}
                }).done(function(msg) {
                    if (typeof msg.success != 'undefined') {
                        $('div#lightboxForgotPasswordForm').hide();
                        $('div#lightboxRegisterSuccess').show();
                    } else {
                        $emailInputForgot.val('');
                        $emailInputForgot.parent().addClass('error');
                        $emailInputForgot.parent().find('.errorMessage').text('No account found under the email address you entered.');
                        $emailInputForgot.parent().find('div.valid').hide();
                    }
                });
            });

            //Reset email from preference lightbox
            $("#lightboxUpdatePreferencesForm").on('click', "a#lnkReset", function(){
                var emailForgot = $(this).data("email");
                $.ajax({
                    type: "POST",
                    url: "/mycommunity/forgottenpassword",
                    data: {email: emailForgot}
                }).done(function(msg) {
                    if (typeof msg.success != 'undefined') {
                        $('div#lightboxUpdatePreferencesForm').hide();
                        $('.txtUpdatePreference').hide();
                        $('div#lightboxResetMessage').show();
                    }
                });
            });


            //RESET FIRST TIME
            $('div#lightboxReset').on('click', 'a.submit', function() {
                valid = true;

                var $passwordResetInput = $('input#passwordReset');
                var $retypePasswordInput = $('input#retypePassword');
                var passwordReset = $passwordResetInput.val();
                var retypePassword = $retypePasswordInput.val();

                if (passwordReset == '' ) {
                    valid = false;
                    $passwordResetInput.parent().addClass('error');
                    $passwordResetInput.parent().find('.errorMessage').text('Error: enter a password');
                    $passwordResetInput.parent().find('div.valid').hide();
                } else {
                    $passwordResetInput.parent().removeClass('error');
                    $passwordResetInput.parent().find('.errorMessage').text('');
                    //$passwordResetInput.parent().find('div.valid').show();
                }

                if (passwordReset != retypePassword || retypePassword == '') {
                    valid = false;
                    $retypePasswordInput.parent().addClass('error');
                    $retypePasswordInput.parent().find('.errorMessage').text("Error: passwords don't match");
                    $retypePasswordInput.parent().find('div.valid').hide();
                } else {
                    $retypePasswordInput.parent().removeClass('error');
                    $retypePasswordInput.parent().find('.errorMessage').text('');
                    //$retypePasswordInput.parent().find('div.valid').show();
                }

                if (valid) {
                    $.ajax({
                        type: "POST",
                        url: "/mycommunity/firstreset",
                        data: {'password': passwordReset,'retypePassword': retypePassword ,'id_instance':$('#id_customer').val()}
                    }).done(function(msg) {
                        if (typeof msg.success != 'undefined') {
                            $passwordResetInput.parent().find('div.valid').show();
                            $retypePasswordInput.parent().find('div.valid').show().delay(1000).queue(function(){
                                $('div#lightboxResetForm').hide();
                                $('div#lightboxResetSuccess').show();
                                s.prop4 = 'confirmed';
                            });
                        } else {
                            $retypePasswordInput.parent().addClass('error');
                            $retypePasswordInput.parent().find('.errorMessage').text("Password can't be reset. Probably the link has expired");
                            $retypePasswordInput.parent().find('div.valid').hide();
                        }
                    });
                }
            });

            //RESET PAGE
            //$('#reset #myaccountResetWrapper .errors').parent().find('input').css('box-shadow','0 1px 2px -1px rgba(0, 0, 0, 0.5), 0 0 0 3px #a96c16 inset');

            //UPDATE PREFERENCES (AUTOLOGIN)
            $('div#lightboxUpdatePreferences').on('click', 'a.submit', function() {
                var data = {
                    'hdnchkNewsletter': $('#hdnchkNewsletter').val(),
                    'hdnchkResetPassword': $('#hdnchkResetPassword').val(),
                    'hdnEmail': $('#hdnEmail').val(),
                    'newsletters': []
                };

                var inputs = $('.newsletter-input');
                for (var i=0; i<inputs.length; i++) {
                    var input = $(inputs[i]);
                    if (input.val() == '0') {
                        continue;
                    }

                    data.newsletters.push(input.data('newsletter'));
                }

                $.ajax({
                    type: "POST",
                    url: "/mycommunity/update-preferences",
                    data: data
                }).done(function(msg) {
                    if ( msg ) {
                        $('div#lightboxUpdatePreferencesForm').hide();
                        $('div#lightboxUpdatePreferencesSuccess').show();
                        s.prop4 = 'confirmed';
                    } else {
                        $('div#lightboxUpdatePreferencesSuccess').html("<p id='lightboxRegisterThanks'>Your email preferences were successfully saved – it may take up to 24hours to take effect. Check back to your preferences any time!</p>");
                        $('div#lightboxUpdatePreferencesForm').hide();
                        $('div#lightboxUpdatePreferencesSuccess').show();
                    }
                });


            });

            $('div.lightbox').on('click', 'a.close, p.cancel a, a.gobtn', function() {
                $(this).closest('div.lightbox').dialog('close');
            });

            $('div.lightbox .lightboxCheckboxWrapper').on('click', 'label', function(ev) {
                ev.stopPropagation();
                var $checkbox = $(this).find('.checkbox');
                var $input = $(this).parent().find('input');

                if ($checkbox.hasClass('checked')) {
                    $input.val(0);
                } else {
                    $input.val(1);
                }

                $checkbox.toggleClass('checked');
            });

            //call Lightboxes
            $('.callUpdatePreferences').on('click', function() {
                var openTheLightbox = function() {
                    $('div#lightboxLogin').dialog('open');
                    $("div#lightboxLogin #loginEmailAddress").focus();
                }
                
                openTheLightbox();
            });
            
            $('.callRegister, .callRegister2, #trigger-register').on('click', function(e) {
                e.preventDefault();
                var signup;
                if($(this).data('signup') != undefined && $(this).data('signup') != '') {
                    signup = $(this).data('signup');
                }
                var openTheLightbox = function() {
                    $('div#lightboxLoginOrFb').dialog('close');
                    $('div#lightboxLogin').dialog('close');
                    $('div#lightboxRegisterOrFb').dialog('open');
                    //$('div#lightboxRegister').dialog('open');
                    // $('div#lightboxRegister #registerEmailAddress').focus();
                    document.cookie = 'dhSignupLightboxOpened=1; path=/';
                    $('#signup_source').val(signup);
                    $('#registerEmailAddress').val($('#joinBox').val());
                    if ($('#joinBox').val() == undefined && $('.emailInput #email').val() != '') {
                        $('#registerEmailAddress').val($('.emailInput #email').val());
                    }
                }
                
                openTheLightbox();
            });
            
            $('#registerEmailButton').click(function() {
                $('div#lightboxRegister').dialog('open');
                $('div#lightboxRegisterOrFb').dialog('close');
            });
            
            $('.callForgotPassword').on('click', function() {
                var openTheLightbox = function() {
                    $('div#lightboxLogin').dialog('close');
                    $('div#lightboxRegister').dialog('close');
                    $('div#lightboxLoginOrFb').dialog('close');
                    $('div#lightboxRegisterOrFb').dialog('close');
                    $('div#lightboxForgotPassword').dialog('open');
                    $('div#lightboxForgotPassword #forgottenpasswordEmailAddress').focus();
                }

                openTheLightbox();
            });

            $('#callLogin, .callLogin, #trigger-login').on('click', function(e) {
                e.preventDefault();
                
                var openTheLightbox = function() {
                    $('div#lightboxRegister').dialog('close');
                    $('div#lightboxForgotPassword').dialog('close');
                    $('div#lightboxRegisterOrFb').dialog('close');
                    $('div#lightboxLoginOrFb').dialog('open');
                    //$("div#lightboxLogin #loginEmailAddress").focus();
                    
                    try {
                        $("#lightboxRegisterAuto").unbind("dialogclose");
                        $('div#lightboxRegisterAuto').dialog('close');
                    } catch(e) { }
                }

                openTheLightbox();
            });
            
            $('#loginEmailButton').click(function() {
                $('div#lightboxLoginOrFb').dialog('close');
                $('div#lightboxLogin').dialog('open');
                $("div#lightboxLogin #loginEmailAddress").focus();
            });
            
            lightboxOptionsSignupWelcome = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                modal: true,
                width: '800',
                height: 'auto',
                resizable: false,
                open: function(event, ui) {
                    $("body").css({overflow: 'hidden'});
                    $("body").addClass('lightbox-currently-open');
                },
                beforeClose: function(event, ui) {
                    $("body").css({overflow: 'inherit'});
                    $("body").removeClass('lightbox-currently-open');
                }
            };
            
            $('#lightboxSignupWelcome').dialog(lightboxOptionsSignupWelcome);
            
        },


        mainBanner: function() {
            if (dh.el.homepageBanner.length) {
                dh.el.homepageBanner.flexslider({
                    animation: 'fade',
                    directionNav: false,
                    keyboard: false,
                    slideshowSpeed:5000,
                    //slideshow: false,
                });

                $('div.homepageBannerCopyWrapper').on('click', function() {
                    window.location = $(this).find('a.homepageBannerURL').attr('href');
                });

            }
        },

        topDestinationsTabs: function() {
            $('.topDestination-0').addClass('selected');
            if (dh.el.topDestinations.length) {
                var tabs = dh.el.topDestinations.find('ul#topDestinationsList li');

                tabs.click(function() {
                    var $this = $(this);
                    var topDestinationIndex = $this.data('topdestinationindex');
                    var subHeading = $('div.sectionSubHeading', dh.el.topDestinations);

                    tabs.removeClass('selected');
                    $this.addClass('selected');

                    $('.topDestination', dh.el.topDestinations).removeClass('selected');
                    $('.topDestination-' + topDestinationIndex, dh.el.topDestinations).addClass('selected');
                    title = $this.find('a').text();
                    if(title == 'Stockholm'){
                        title = '<span style="letter-spacing:1px">St</span>ockholm';
                    }
                    subHeading.html(title);
                });
            }
        },
        pdpOverview: function() {
            $('#lightboxRegisterAuto').bind('dialogclose');
            $('#lightboxRegisterAuto a.close').click(function () {
                $('div#lightboxRegisterAuto').dialog('close');
            });

            $('.callAutoSignup').on("click", function () {
                $('#lightboxRegisterAuto').dialog('open');
            });
        },
        // PDP Layout Functions
        pdpBook: function() {
            var monthExist =[];
            var monthLoading = {};

            // ROOM AMOUNT SELECTION
            
            $('span#pdpBookRooms div.roomsSelect a' ).click(function() {
                var rooms = $(this).text();

                $('span#amountRooms').text(rooms);
                if (rooms == "1") {
                    $('span#numRooms').text('room');
                }
                else {
                    $('span#numRooms').text('rooms');
                }
                $('div.roomsSelect').hide();
            });

            $('span#pdpBookRooms').click(function(ev) {
                ev.stopPropagation();

                $('div.roomsSelect', $(this)).show();
            });

            $('span#pdpBookRooms div.roomsSelect').click(function(ev) {
                ev.stopPropagation();
            });

            dh.el.body.click(function(ev) {
                $('div.roomsSelect', $(this)).hide();
            });

            $('span#amountAdults').text(2);
            // Adults AMOUNT SELECTION
            $('span#pdpBookAdults div.adultsSelect a' ).click(function() {
                var adults = $(this).text();

                $('span#amountAdults').text(adults);
                if (adults == "1") {
                    $('span#numAdults').text('adult');
                }
                else {
                    $('span#numAdults').text('adults');
                }
                $('div.adultsSelect').hide();

                bookDealhref = $('#bookNowDeal').attr('href');
                if(bookDealhref !== undefined){
                    oldNumberOfAdults = bookDealhref.substr(bookDealhref.indexOf("nA=") + 3,1);
                    bookDealhref = bookDealhref.replace('nA='+oldNumberOfAdults,'nA='+adults);
                    $('#bookNowDeal').attr('href', bookDealhref);
                }

                $('.roomInfo .bookNow').each(function(i, el) {
                    oldNumberOfAdults =$(this).attr('href').substr($(this).attr('href').indexOf("nA=") + 3,1);
                    bookNowHref = $(this).attr('href').replace('nA='+oldNumberOfAdults,'nA='+adults);
                    $(this).attr('href', bookNowHref);
                    s.eVar51 = bookNowHref;
                });

                $('#num_adults').val(adults);
            });

            $('span#pdpBookAdults').click(function(ev) {
                ev.stopPropagation();

                $('div.adultsSelect', $(this)).show();
            });

            $('span#pdpBookAdults div.adultsSelect').click(function(ev) {
                ev.stopPropagation();
            });

            dh.el.body.click(function(ev) {
                $('div.adultsSelect', $(this)).hide();
            });

            var d = new Date();
            var d2 = new Date();
            d2.setMonth(d2.getMonth() + 1);

            monthExist.push(d.getMonth()+1 + '_' + d.getFullYear());
            monthExist.push(d.getMonth()+2 + '_' + d.getFullYear());
            monthExist.push(d.getMonth()+3 + '_' + d.getFullYear());

            // Keep track of which 2 months are visible
            var currentMonth1 = d.getFullYear() + '_' + d.getMonth();
            var currentMonth2 = d2.getFullYear() + '_' + d2.getMonth();

            // Timer to prevent too many ajax calls
            var updateCalendarAjaxTimer = false;
            var updateCalendarAjaxTimer1 = false;

            var updateCalendarAjax = function(date, whichMonth, currentMonth, index) {
                monthLoading[whichMonth] = true;

                if (currentMonth == currentMonth1) {
                    $("#loadingimg1").show();
                }

                if (currentMonth == currentMonth2) {
                    $("#loadingimg2").show();
                }

                dealcode = ( $('#deal_code').val() != '') ? $('#deal_code').val() : 0 ;
                currency = $('#selected_currency_code').val() != '' ? $('#selected_currency_code').val() : 'USD';
                $.ajax({
                    type: "POST",
                    url: "/futurerate/" + propid + "/" + date +'/'+dealcode + '/' + currency,
                    //data: ,
                    cache: true,
                    dataType: 'json',
                    success: function(response){
                        monthLoading[whichMonth] = false;
                        calendarLowestRates = $.extend(calendarLowestRates, response);
                        dh.fillCalendar(calendarLowestRates);

                        if (currentMonth == currentMonth1) {
                            $("#loadingimg1").hide();
                        }

                        if (currentMonth == currentMonth2) {
                            $("#loadingimg2").hide();
                        }

                        monthExist.push(whichMonth);
                    }
                });
            };

            // CALENDAR
            $('div#bookCalendar').DatePicker({
                flat: true,
                prev: '',
                next: '',
                date: typeof global_defCalendarDates !== 'undefined' ? global_defCalendarDates : new Date(),
                format: 'b d, Y',
                starts: 1,
                calendars: 2,
                mode: 'range',
                view: 'days',
                autoHide: false,
                onMonthChange: function(firstDate) {
                    $("div#bookCalendar #loadingimg1").hide();
                    $("div#bookCalendar #loadingimg2").hide();

                    var secondDate = new Date(firstDate.valueOf());
                    secondDate.setMonth(secondDate.getMonth() + 1);

                    currentMonth1 = firstDate.getFullYear() + '_' + firstDate.getMonth();
                    currentMonth2 = secondDate.getFullYear() + '_' + secondDate.getMonth();

                    var thisCurrentMonth1 = currentMonth1;
                    var thisCurrentMonth2 = currentMonth2;

                    var month = firstDate.getMonth() + 2;
                    var month1 = firstDate.getMonth() + 1;
                    var month2 = month + '_' + firstDate.getFullYear();
                    var year  = firstDate.getFullYear();

                    if (month == 13) {
                        month = 1;
                        year  = year + 1;
                    }

                    if (month < 10) {
                        month = '0' + month;
                    }

                    if (month1 < 10) {
                        month1 = '0' + month1;
                    }

                    var date1 = year + '-' + month1 + '-01';
                    var date = year + '-' + month + '-01';

                    month1 = firstDate.getMonth() + 1 + '_' + firstDate.getFullYear();

                    if (
                        typeof(monthLoading[month1]) != 'undefined'
                        && monthLoading[month1]
                    ) {
                        $("#loadingimg1").show();
                    } else if ( $.inArray( month1,monthExist) < 0 ) {
                        if (updateCalendarAjaxTimer1) {
                            clearTimeout(updateCalendarAjaxTimer1);
                        }

                        updateCalendarAjaxTimer1 = setTimeout(function() {
                            updateCalendarAjaxTimer1 = false;
                            updateCalendarAjax(date1, month1, thisCurrentMonth1, '1');
                        }, 500);
                    }

                    if (
                        typeof(monthLoading[month2]) != 'undefined'
                        && monthLoading[month2]
                    ) {
                        $("div#bookCalendar #loadingimg2").show();
                    } else if ( $.inArray( month2,monthExist) < 0 ) {
                        if (updateCalendarAjaxTimer) {
                            clearTimeout(updateCalendarAjaxTimer);
                        }

                        updateCalendarAjaxTimer = setTimeout(function() {
                            updateCalendarAjaxTimer = false;
                            updateCalendarAjax(date, month2, thisCurrentMonth2, '2');
                        }, 500);
                    }
                },
                onFill: function() {
                    //it wouldn't work if month's name will be translated in diff lang in the future
                    dh.fillCalendar(calendarLowestRates);
                },
                onChange: function(formatted, datesArr) {
                    if (typeof dh.state.ajaxPdpRooms !== 'undefined') {
                        dh.state.ajaxPdpRooms.abort();
                    }

                    var monthsArr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                    var startDate =  datesArr[0].getFullYear() + '-' + (datesArr[0].getMonth() + 1) + '-' + datesArr[0].getDate();
                    var endDate =  datesArr[1].getFullYear() + '-' + (datesArr[1].getMonth() + 1) + '-' + datesArr[1].getDate();

                    var startDateDeeplink = '&aY='+ datesArr[0].getFullYear() + '&aM=' + (datesArr[0].getMonth() + 1) + '&aD=' + datesArr[0].getDate();
                    var endDateDeeplink = '&dY='+ datesArr[1].getFullYear() + '&dM=' + (datesArr[1].getMonth() + 1) + '&dD=' + datesArr[1].getDate();

                    if ((datesArr[0].getMonth() + 1) == (datesArr[1].getMonth() + 1) && datesArr[0].getDate() == datesArr[1].getDate()) {

                        endDate = '';
                        $('div#bookCalendar li#searchDateCheckOut').find('> a').text('');
                        $('div#bookCalendar li#searchDateCheckOut').find('> input').val('');
                    }

                    if (startDate.replace(/\s+/ig, '').length > 0) {

                        $('div#bookCalendar li#searchDateCheckIn').find('> a').text(
                            monthsArr[datesArr[0].getMonth()] + ' ' + datesArr[0].getDate() + ', ' + datesArr[0].getFullYear()
                        );

                        $('div#bookCalendar li#searchDateCheckIn').find('> input').val(datesArr[0]);
                    }

                    if (endDate.replace(/\s+/ig, '').length > 0) {

                        $('div#bookCalendar li#searchDateCheckOut').find('> a').text(
                            monthsArr[datesArr[1].getMonth()] + ' ' + datesArr[1].getDate() + ', ' + datesArr[1].getFullYear()
                        );

                        $('div#bookCalendar li#searchDateCheckOut').find('> input').val(datesArr[1]);
                    }

                    $('#pdpBook #bookNowDeal').attr('href', $('#pdpBook #bookNowDeal').attr('href') + startDateDeeplink + endDateDeeplink);
                    if (typeof global_numAdults !== 'undefined' && typeof global_numChildren !== 'undefined') {

                        noAjaxCall = true;
                        $('#campaign section#calendar .bookNow')
                            .attr('href', '/reservation?isSearched=1&hotel=' + propid + '&nA=' + global_numAdults + '&nC=' + global_numChildren + startDateDeeplink + endDateDeeplink);
                           s.eVar51 = '/reservation?isSearched=1&hotel=' + propid + '&nA=' + global_numAdults + '&nC=' + global_numChildren + startDateDeeplink + endDateDeeplink;
                    }

                    if (typeof(noAjaxCall) == 'undefined' || (typeof(noAjaxCall) !== 'undefined' && noAjaxCall == false)) {

                        $('#pbpBookRooms > ul')
                            .html('<li id="bookNowLoader" style="width: 100%; margin: 0;"><img src="/assets/images/loading.gif" style="width: 48px; height: 48px; display: block; margin: 0 auto;" /></li>');

                        var winHeight = $(window).innerHeight();
                        var scrolledHeight = $(window).scrollTop();
                        var loaderCalc = typeof $('#bookNowLoader') !== 'undefined' ? $('#bookNowLoader').offset().top + $('#bookNowLoader').innerHeight() : null;
                        if (loaderCalc != null && loaderCalc > winHeight && (loaderCalc - scrolledHeight) > winHeight) {
                            $('html, body').animate({ scrollTop: loaderCalc - winHeight }, 'fast');
                        }

                        jQuery('#pbpBookRooms > h3').html(
                            dh.utilities.getMonthName((datesArr[0].getMonth() + 1) < 10 ? '0' + (datesArr[0].getMonth() + 1) : datesArr[0].getMonth() + 1) + ' ' + datesArr[0].getDate() + ', ' + datesArr[0].getFullYear()
                            + ' &ndash; ' +
                            dh.utilities.getMonthName((datesArr[1].getMonth() + 1) < 10 ? '0' + (datesArr[1].getMonth() + 1) : datesArr[1].getMonth() + 1) + ' ' + datesArr[1].getDate() + ', ' + datesArr[1].getFullYear()
                        );

                        adults = $('#amountAdults').text();

                        var jsonParams = {"count": $('#amountRooms').text(), "propid": propid, "start": startDate, "end": endDate,"adults":adults};
                        if (typeof(rateCode) !== 'undefined' && rateCode != null) {

                            jsonParams = {"count": $('#amountRooms').text(), "propid": propid, "start": startDate, "end": endDate,"adults":adults,"ratecode": rateCode};
                        }

                        dh.state.ajaxPdpRooms = $.ajax({
                            type: "POST",
                            url: '/get-rooms',
                            cache: true,
                            data: jsonParams,
                            success: function(response){
                                $('#pbpBookRooms > ul')
                                    .html(response);

                                $('.roomInfo .bookNow').each(function(i, el) {
                                    $(this).attr('href',$(this).attr('href')+ '&nA=' + adults + '&numberOfRooms=' +  $('#amountRooms').text());
                                    s.eVar11 =  $(this).attr('href')+ '&nA=' + adults + '&numberOfRooms=' +  $('#amountRooms').text();
                                });
                                new Imager('sabre-rooms');
                                if (response.trim() == '<li class="bookNow" style="width: 100%; margin: 0; font-style: italic; font-size: 24px; font-family: Plantin,Arial,serif; text-align: center; color: #A96C16;">The hotel is currently reporting no availability for the selected date(s).<br />However, this may be due to restrictions that our Design Hotels reservations team can override.<br />Please <a href="mailto:res@designhotels.com" style="text-decoration: underline;">send us an email</a> for help with your booking or <a href="https://www.designhotels.com/toll-free-numbers" style="text-decoration: underline;">call us toll free</a>.<br /></li>') {
                                    numRates = 0;
                                } else {
                                    numRates = 1;
                                }

                                availCheck(datesArr[0], datesArr[1], adults, numRates);
                                dh.clickableAreas();

                                window.setTimeout(function() {

                                    if (typeof $('.bookNow') !== 'undefined' && $('.bookNow').length > 0) {

                                        var bookNowLoc = $('.bookNow').first().offset().top + $('.bookNow').first().innerHeight();
                                        var scrolledHeight = $(window).scrollTop();
                                        var winHeight = $(window).innerHeight();
                                        if (typeof bookNowLoc !== 'undefined' && bookNowLoc > winHeight && (bookNowLoc - scrolledHeight) > winHeight) {

                                            $('html, body').animate({ scrollTop: bookNowLoc - winHeight}, 'fast');
                                        }
                                    }
                                }, 200);
                            }
                        });

                    }
                }
            });

            $('#bookCalendar').DatePickerShow();
            $('div#bookCalendar div.datepicker').show();

            $('p#pdpBookCalendarClear').mousedown(function(ev) {
                ev.stopPropagation();

                $('div#bookCalendar').DatePickerClear();
            });

            $('a#pdpBookPrevMonth').mousedown(function(ev) {
                ev.stopPropagation();
                $("#loadingimg").hide();
                $('div#bookCalendar th.datepickerGoPrev').eq(0).trigger('click');
            });

            $('a#pdpBookNextMonth').mousedown(function(ev) {
                ev.stopPropagation();

                $('div#bookCalendar th.datepickerGoNext').eq(0).trigger('click');
            });

            //if ($('#selectedStartDate').val() != '' && $('#selectedEndDate').val() != '' ) {
            //    $('div#bookCalendar').DatePickerSetDate(new Array($('#selectedStartDate').val(), $('#selectedEndDate').val()));
           // }
        },
        pdpRooms:function(){

            $('#lightboxRegisterAuto').bind('dialogclose');
            $('#lightboxRegisterAuto a.close').click(function(){
                $('div#lightboxRegisterAuto').dialog('close');
            });

            var errorlightboxOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                height: 'auto',
                modal: true,
                width: '430',
                resizable: false
            };

            $('#roomError').dialog(errorlightboxOptions);

            $('a.selectRoom').click(function(ev) {
                ev.preventDefault();
                if($(this).hasClass("callAutoSignup")) {
                    $('#lightboxRegisterAuto').dialog('open');

                } else {
                    var ratePlan = $(this).data('rateplan');
                    var roomId = $(this).parent().find('span.roomId').html();
                    var roomSelect = $('#bookingNumRoom').val();
                    var roomName = $(this).parent().find('h4').html();

                    $('input#bookingRatePlan').val(ratePlan);
                    $('input#bookingRoom').val(roomId);
                    $('input#bookingNumRoom').val(roomSelect);
                    $('input#bookingRoomName').val(roomName);

                    $('form#bookingStep2').submit();
                }
            });
        },
        roomsWidget: function(){
                        $('a.readmore').click(function(ev) {
                $(this).parent().find('.roomDescFull').slideDown();
                $(this).parent().find('.roomDescShort').css('margin-bottom','0');
                $(this).hide();
            });
            $('a.readless').click(function(ev) {
                $(this).parent().parent().find('.roomDescFull').slideUp(400,function(){
                    $(this).parent().parent().find('a.readmore').show();
                    $(this).parent().parent().find('.roomDescShort').css('margin-bottom','10px');
                });
            });
            
            $('div.roomInfoToggle').click(function(ev) {
                $(this).parent().parent().find('.roomDropdown').slideToggle();
                $(this).children().toggleClass( "icon-arrow-small-down" );
                $(this).children().toggleClass( "icon-arrow-small-up" );
            });

        },
        fillCalendar: function(rates) {

            $('div#bookCalendar .datepickerNotInMonth a').remove();
            $('div#bookCalendar tbody.datepickerDays td, div#bookingCalendar tbody.datepickerDays td').each(function(i, el) {
                var $el = $(el);


                if ($el.hasClass('datepickerDisabled') == false) {
                    calDay = $el.find('span.day').text().replace('-','');

                  if ( ! dh.utilities.isEmpty(calDay)) {
                        // can optimize this since month/year won't change as often
                        calMonth = dh.utilities.getMonthObject($el.closest('.datepickerViewDays').find('.datepickerMonthName').text());
                        calYear = $el.closest('.datepickerViewDays').find('.datepickerMonthYear').text();
                        
                        // account for previous and next month's dates in current month
                        if ($el.hasClass('datepickerNotInMonth') == true) {
                            if (calDay.length > 1) {
                                if(parseInt(calDay)<=15){
                                    calKey= calYear + "-" + (parseInt(calMonth)+1).toString() + "-" + calDay;
                                }else{
                                    calKey= calYear + "-" + (parseInt(calMonth)-1).toString() + "-" + calDay;
                                }
                            } else {
                                if(parseInt(calDay)<=15){
                                    calKey= calYear + "-" + (parseInt(calMonth)+1).toString() + "-0" + calDay;
                                }else{
                                   calKey= calYear + "-" + (parseInt(calMonth)-1).toString() + "-0" + calDay;
                                }
                            }
                        }else{
                            if (calDay.length > 1) {
                            calKey= calYear + "-" + calMonth + "-" + calDay;
                            } else {
                                calKey= calYear + "-" + calMonth + "-0" + calDay;
                            }
                        }
                    }

                    // if this date has a rate
                    if ( ! dh.utilities.isEmpty(rates[calKey]) && ! dh.utilities.isEmpty(rates[calKey]['ratePerNight'])) {
                        $el.removeClass('noRate');
                        if ($el.find('span.rate').length == 0) {
                            $el.find('span.dash').parent().find('br').first().remove();
                            $el.find('span.dash').remove();


                            if (typeof minstay != 'undefined'){
                                if (minstay.minstay == 0) {
                                    minstay.minstay = 1;
                                }
                                title = 'Please select a minimum of '+ minstay.minstay +' nights to book this rate';

                                if ($el.hasClass('datepickerNotInMonth') == false) {
                                    //$el.find('a').attr('title', title);
                                    $('a', $el).append("<div class='tooltip'>"+title+"<div class='arrow'></div></div>");

                                    $('a', $el).mouseover(function(){
                                        $(this).find('.tooltip').show();
                                    }).mouseout(function(){
                                        $(this).find('.tooltip').hide();
                                    });

                                }
                            }

                            $('a', $el).append('<br/><span class="rate">' + rates[calKey]['currency'] + ' ' + parseInt(rates[calKey]['ratePerNight']) + '</span>');
                            $el.addClass('rate');
                        }
                    } else {
                        if ( $el.find('span.dash').length == 0) {

                            $('a', $el).append('<br/><span class="dash">&mdash;</span>');
                            $el.addClass('noRate');
                        }
                    }
                } else {
                    $('a', $el).append('<br/><span class="dash">&mdash;</span>');
                }


            });
        },

        pdpNeighbourhood: function($el) {
            var neighboughoodMapEl = document.getElementById($el);

            if (neighboughoodMapEl) {
                // var offset = {lat: -0.0045, lng: 0.003}
                var offset = {lat: -0.0028, lng: 0.0025}
                var mapCenter = new google.maps.LatLng(propertyInfo.latitude + offset.lng, propertyInfo.longitude + offset.lat);

                var myOptions = {
                    backgroundColor    : '#ececec',
                    center             : mapCenter,
                    disableDefaultUI   : true,
                    disableDoubleClickZoom: false,
                    draggable          : true,
                    keyboardShortcuts  : false,
                    mapTypeControl     : false,
                    mapTypeId          : google.maps.MapTypeId.ROADMAP,
                    scrollwheel        : false,
                    zoomControl        : true,
                    streetViewControl  : false,
                    zoom               : 13,
                    panControl         : false
                };

                var neighbourhoodMap = new google.maps.Map(document.getElementById('neighbourhoodMap'), myOptions);
                dh.mapStyle(neighbourhoodMap);
                var lnglat = new google.maps.LatLng(propertyInfo.latitude, propertyInfo.longitude);
                var marker = new google.maps.Marker({
                    position: lnglat,
                    map: neighbourhoodMap,
                    icon: new google.maps.MarkerImage("assets/images/sprites.png", new google.maps.Size(60, 36), new google.maps.Point(0, 86))
                });

                var boxText = document.createElement("div");
                boxText.style.cssText = " margin-top: 8px; background: #fff; padding: 5px;";
                content = '<span class="arrow"></span>'+
                          '<p class="name">' + propertyInfo.languages[lang].name + '</p>' +
                          '<p class="address">' + propertyInfo.languages[lang].address1 + '<br/>' + propertyInfo.languages[lang].address2 + '</p>';
                if ($(window).width() <= MobileScreenSize) {
                    content += '<p class="directions"><a href="http://maps.google.com/maps?q='+ propertyInfo.languages[lang].name+'@'+propertyInfo.latitude+','+propertyInfo.longitude+'&saddr=Current%20Location&daddr='+propertyInfo.languages[lang].address1+','+propertyInfo.languages[lang].address2+'">View Directions</a></p>';
                } else {
                    content += '<p class="directions"><a href="http://maps.google.com/maps?q='+propertyInfo.languages[lang].name+'@'+propertyInfo.latitude+','+propertyInfo.longitude+'">View Directions</a></p>';

                }

                infobox = new InfoBox({
                    latlng: marker.getPosition(),
                    map: neighbourhoodMap,
                    boxClass: 'infobox',
                    content:content,
                    closeBoxURL: 'assets/images/mapClose.png',
                    closeBoxMargin: "-15px",
                    alignBottom: true,
                    disableAutoPan: true
                });

                infobox.open(neighbourhoodMap, marker);

                google.maps.event.addListener(infobox, 'domready', function() {

                    $infobox = $('div#neighbourhoodMap div.infobox');
                    $infobox.css('margin-left', $infobox.width() * -1 - 4);
                });

                google.maps.event.addListener(marker, 'click', function() {

                    infobox.open(neighbourhoodMap, this);
                });

                neighbourhoodMap.setCenter(mapCenter);
            }
        },
        resizeVideo: function(container, video){

            var width = parseInt(container.css('width'), 10);
            var height = (width * (9/16));

            video.css('width', width + 'px');
            video.css('height', height + 'px');
            video.css('marginTop','-3.278%'); //~732px wide, the video border is about 24px thick (24/732)

            container.css('height', (height * 0.88) + 'px'); //0.88 was the magic number that you needed to shrink the height of the outer container with.

        },
        // Contact Layout Functions
        contactForm: function() {
            $("a#collapseContactForm").hide();
            $('a#expandContactForm').click(function() {
                $('li.li-contact-form').show();
                $("a#collapseContactForm").show();
                $(this).hide();
            });
            $('a#collapseContactForm').click(function() {
                $('li.li-contact-form').hide();
                $('a#expandContactForm').show();
                $(this).hide();
            });

            $('select#contactTitle').tzSelect({
                selectboxClass: 'contactTitle_selectbox',
                dropdownClass: 'contactTitle_dropdown'
            });

            $('label.radio').click(function(ev) {
                ev.preventDefault();
                
                var $input = $('input', $(this));
                
                $('label.radio').prop('checked', false).find('span.icon').removeClass('checked');

                $input.prop('checked', true).find('span.icon').addClass('checked');

                $('span.icon', $(this)).addClass('checked');
                $('.toHide').hide();
                $(".contact"+$input.val()).show();
            });

            $("form#contactForm input.submit").on("click", function(){
                var valid = true;
                var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                
                $('#contactForm *').filter(':input').not("#contact"+$('[name=contactBy]').not(':checked').val()).each(function(){
                    if($(this).val()=='' || 
                            ($(this).attr("id")=="contactEmailaddress" && emailPattern.test($(this).val()) === false)
                            && $(this).attr("id")!="contactComments"){
                        valid = false;
                        $(this).closest("li").addClass('error');
                        $(this).closest("li").find('.errorMessage').show();
                    } else {
                        $(this).closest("li").removeClass('error');
                        $(this).closest("li").find('.errorMessage').hide();
                    }
                });
                if(valid) {
                    $("form#contactForm").submit();
                }
            });

            var errorlightboxOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                height: 'auto',
                modal: true,
                width: '430',
                resizable: false
            };

            var errorlightboxOptionsMobile = dh.standardMobileLightboxOptions;

            $(window).on('to-desktop', function() {
                $('#message').dialog(errorlightboxOptions);
            });

            $(window).on('to-mobile', function() {
                $('#message').dialog(errorlightboxOptionsMobile);
            });
        },

        // Secondary Layout Functions
        discoverUsSlider: function() {
            if ($('div#discoverUsSlider').length) {
                $('div#discoverUsSlider').flexslider({
                    animation: 'fade',
                    controlNav: false,
                    keyboard: false,
                    prevText: '',
                    nextText: ''
                });
            }

        },

        videoLightbox: function() {
            var video_width=$(window).width();
            var iframe = $('<iframe id="thevideoiframeforvimeo" frameborder="0" width="900" height="506" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');

             $('#videoLightbox').append(iframe);

            var videoLightBoxOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                modal: true,
                resizable: true,
                width: 'auto',
                height: 'auto',
                close: function () {
                    iframe.attr("src", "");
                }
            };

            $(' #videoLightbox').dialog(videoLightBoxOptions);

            $(".imageWrapper, #videoWrap, #travel-start-video").on("click", function (e) {
                var isTravelproSignup = ($(this).attr('id') == 'travel-start-video');
                
                if (!$(this).find('.videoid').length) {
                    return;
                }

                if (ScreenSizeIsMobile){
                    window.location.href='https://player.vimeo.com/video/' + $(this).find('.videoid').val();
                }
                if ($(this).find('.playerBtn').length > 0  && $(this).find('.videoid').length > 0) {
                    e.preventDefault();
                    e.stopPropagation();

                    if (isTravelproSignup) {
                        var src = 'https://player.vimeo.com/video/' + $(this).find('.videoid').val() + '?autoplay=1';
                    } else {
                        var src = 'https://player.vimeo.com/video/' + $(this).find('.videoid').val() + '?autoplay=0';
                    }

                    iframe.attr({
                        width: 900,
                        height: 506,
                        src: src
                    });

                    $('#videoLightbox').dialog("open");
                    //$('.lightboxDialog').style('height','100px','important');

                   // iframe.api('play');
               }

            });

        },
        
        // Cancellation Functions
        confirmCancelLightbox: function() {
            var confirmCancelLightboxOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                height: 'auto',
                modal: true,
                width: '430',
                resizable: false
            };
            var confirmCancelMobileLightboxOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                height: 'auto',
                modal: true,
                width: '100%',
                resizable: false
            };

            $(window).on('to-desktop', function() {
                $('#confirm-cancel-lightbox').dialog(confirmCancelLightboxOptions);
                $('div.lightboxDialog').on('click', 'a.close', function() {
                    $('#confirm-cancel-lightbox').dialog('close');
                });
            });

            $(window).on('to-mobile', function() {
                $('#confirm-cancel-lightbox').dialog(confirmCancelMobileLightboxOptions);
                $('div.lightboxDialog').on('click', 'a.close', function() {
                    $('#confirm-cancel-lightbox').dialog('close');
                });
            });
            
            $('#cancel-booking-button').click(function() {
                $('#confirm-cancel-lightbox').dialog("open");
            });

        },
        

        //Search Functions
        setMarker: function(property) {
            markerObjs = [];
            markersGroupped= [];
            infobox = new InfoBox({
                map: map,
                boxClass: 'infobox',
                boxStyle: {
                    marginLeft: "-351px",
                    marginBottom: "22px",
                },
                content: '',
                closeBoxURL: 'assets/images/mapClose.png',
                closeBoxMargin: "0px",
                alignBottom: true,
                disableAutoPan: true
            });
            mcOptions =  [{
                                    textColor: 'white',
                                    height: 55,
                                    backgroundPosition:"-42px -70px",
                                    url: "/assets/images/map_numbers.png",
                                    width: 55,
                                    textSize:13,
                                    fontWeight:'normal'
                                    },
                                    {
                                    height: 60,
                                    textColor: 'white',
                                    backgroundPosition:"-101px -65px",
                                    url: "/assets/images/map_numbers.png",
                                    width: 62,
                                    textSize:13,
                                    fontWeight:'normal'
                                    },
                                    {
                                    height: 65,
                                    textColor: 'white',
                                    backgroundPosition:"-164px -62px",
                                    url: "/assets/images/map_numbers.png",
                                    width: 73,
                                    textSize:13,
                                    fontWeight:'normal'
                                    }];



            markerCluster.setStyles(mcOptions);

            $.each(property, function(i, p) {

                if (p.lat != null && p.lng != null) {


                    var thisLatLong = new google.maps.LatLng(parseFloat(p.lat).toFixed(6), parseFloat(p.lng).toFixed(6));
                    var marker = new google.maps.Marker({
                        'position': thisLatLong,
                        map: map,
                        icon: new google.maps.MarkerImage("assets/images/sprites.png", new google.maps.Size(24, 24), new google.maps.Point(453, 21)),
                        zIndex: 5
                    });



                    marker.setMap(map);
                    markerObjs.push({marker:marker, city:p.city, country:p.country, rate:p.rate, latlng: thisLatLong, spg_hotel:p.spg_hotel});
                    markerCluster.addMarker(marker, true);

                    dh.map.bounds.extend(thisLatLong);

                    google.maps.event.addListener(marker, 'click', function() {
                        // handle gtm push
                        window.dataLayer = window.dataLayer || [];
                        dataLayer.push({
                            'event':'hotelresortpage',
                            'typeclickmacro':"Maps",
                            'typeclickmirco':p.country
                        });

                        var tthis = this;

                        var timeoutTime = (ScreenSizeIsMobile ? 300 : 50);

                        // Timeout is for android phone automatically triggering the link
                        // when you open the infobox sometimes.
                        setTimeout(function() {
                            if (infobox) {
                                infobox.close();
                            }

                            var lineHeight = '18';
                            if (ScreenSizeIsMobile) {
                                lineHeight = '16';
                            }

                            var content = '<div class="mapHotels">';
                            if (p.image != 'noimage') {
                                content += '<img src="' + p.image + '">';
                            }
                            content +=      '<div class="hotelInfo">' +
                                                    '<div class="hotelName" style="line-height:' + lineHeight + 'px !important">' +
                                                        p.name + '<br /><span class="location">' + p.city + '<br />' + p.country + '</span>' +
                                                    '</div>' +
                                                    '<div class="line"></div>';
                            if(p.rate !== 'N/A' ) {
                                content +=          '<div class="price" style="line-height:18px !important">from <span> ' + p.currency + ' ' + p.rate + '</span> /night</div>';
                            }
                            content +=         '</div>' +
                                                    '<div class="downArrow"></div>' +
                                '</div>';
                            if (p.prop_url != '') {
                                content += '<a style="position:absolute;width:346px;height:100%;top:0;left:0;" href="'+p.prop_url+'">&nbsp;</a>';
                            }
                            content += '</div>';

                            map.setCenter(marker.getPosition());
                            if (ScreenSizeIsMobile) {
                                map.panBy(-80, -80);
                            } else {
                                map.panBy(-80, 0);
                            }

                            infobox.setContent(content);
                            infobox.setPosition(tthis.getPosition());
                            infobox.open(map, tthis);

                            //Omniture tracking
                            s.prop23 = p.name;
                            s.t();
                        }, timeoutTime);
                    });
                }

           });

          map.fitBounds(dh.map.bounds);
           var listener = google.maps.event.addListener(map, "idle", function() {

               if (map.getZoom() < 2) {

                   map.setZoom(2);
                   map.setCenter(dh.map.bounds.getCenter());
                   google.maps.event.removeListener(listener);
               }
           });


        },

        mapStyle: function(map) {
            var styles = [
                    {
                            "featureType":"administrative",
                            "elementType":"labels.text.fill",
                            "stylers":[
                                    {
                                        "color":"#444444"
                                    }
                            ]
                    },
                    {
                            "featureType":"administrative.province",
                            "elementType":"labels.text.fill",
                            "stylers":[
                                    {
                                        "color":"#8d8d8d"
                                    }
                            ]
                    },
                    {
                            "featureType":"administrative.locality",
                            "elementType":"labels.text.fill",
                            "stylers":[
                                    {
                                        "color":"#777777"
                                    }
                            ]
                    },
                    {
                            "featureType":"administrative.locality",
                            "elementType":"labels.text.stroke",
                            "stylers":[
                                    {
                                        "color":"#dadad1"
                                    },
                                    {
                                        "visibility":"off"
                                    }
                            ]
                    },
                    {
                            "featureType":"administrative.neighborhood",
                            "elementType":"labels.text.fill",
                            "stylers":[
                                    {
                                        "lightness":"23"
                                    }
                            ]
                    },
                    {
                            "featureType":"administrative.neighborhood",
                            "elementType":"labels.text.stroke",
                            "stylers":[
                                    {
                                        "visibility":"off"
                                    }
                            ]
                    },
                    {
                            "featureType":"landscape",
                            "elementType":"labels",
                            "stylers":[
                                    {
                                        "visibility": "off"
                                    }
                            ]
                    },
                    {
                            "featureType":"landscape",
                            "elementType":"geometry.fill",
                            "stylers":[
                                    {
                                        "color":"#dadad1"
                                    }
                            ]
                    },
                    {
                            "featureType":"poi",
                            "elementType":"all",
                            "stylers":[
                                    {
                                        "visibility":"off"
                                    }
                            ]
                    },
                    {
                            "featureType":"poi",
                            "elementType":"labels.text.fill",
                            "stylers":[
                                    {
                                        "lightness":"0"
                                    }
                            ]
                    },
                    {
                            "featureType":"road",
                            "elementType":"all",
                            "stylers":[
                                    {
                                        "saturation":-100
                                    },
                                    {
                                        "lightness":45
                                    },
                                    {
                                        "visibility":"on"
                                    }
                            ]
                    },
                    {
                            "featureType":"road",
                            "elementType":"labels.icon",
                            "stylers":[
                                    {
                                        "visibility":"on"
                                    }
                            ]
                    },
                    {
                            "featureType":"road.highway",
                            "elementType":"all",
                            "stylers":[
                                    {
                                        "visibility":"simplified"
                                    }
                            ]
                    },
                    {
                            "featureType":"road.highway",
                            "elementType":"geometry.fill",
                            "stylers":[
                                    {
                                        "lightness":"71"
                                    }
                            ]
                    },
                    {
                            "featureType":"road.highway",
                            "elementType":"labels.icon",
                            "stylers":[
                                    {
                                        "visibility":"off"
                                    }
                            ]
                    },
                    {
                            "featureType":"road.arterial",
                            "elementType":"labels.icon",
                            "stylers":[
                                    {
                                        "visibility":"off"
                                    }
                            ]
                    },
                    {
                            "featureType":"transit",
                            "elementType":"all",
                            "stylers":[
                                    {
                                        "visibility":"off"
                                    }
                            ]
                    },
                    {
                            "featureType":"water",
                            "elementType":"all",
                            "stylers":[
                                    {
                                        "color":"#46bcec"
                                    },
                                    {
                                        "visibility":"on"
                                    }
                            ]
                    },
                    {
                            "featureType":"water",
                            "elementType":"geometry.fill",
                            "stylers":[
                                    {
                                        "color":"#fbfbfa"
                                    }
                            ]
                    },
                    {
                            "featureType":"water",
                            "elementType":"labels.text.fill",
                            "stylers":[
                                    {
                                        "color":"#b3b3b3"
                                    }
                            ]
                    },
                    {
                            "featureType":"water",
                            "elementType":"labels.text.stroke",
                            "stylers":[
                                    {
                                        "visibility":"off"
                                    }
                            ]
                    }
            ];
            
            var mapType = new google.maps.StyledMapType(styles, { name:"Grayscale" });
            map.mapTypes.set('gray', mapType);
            map.setMapTypeId('gray');
        },

        clear: function(map) {
            $('div.sidebarHelper').find('li').removeClass('selected');

            sendData = {
                'map': map,
                'properties': properties
            };

            if (map != 'map') {
                $('#slider-range').slider('values', 0, min_price_range);
                $('#slider-range').slider('values', 1, max_price_range);
                $('#minAmount').html($('#slider-range').slider('values', 0));
                $('#maxAmount').html($('#slider-range').slider('values', 1));

                $('#countryOption').parent().find('li').removeClass('selected');
                $('#cityOption').parent().find('li').show();              
                $('#cityOption').parent().find('li').removeClass('selected');
                $('#neighborhoodOption').parent().find('li').removeClass('selected');

                $('div#hotelsList div.memberHotels').show();
                $('.filter-category .filter-option-active').removeClass('filter-option-active');
                $('#SPGToggle').removeClass('filter-option-active');
            } else {
                $('#mapslider-range').slider('values', 0, min_map_price_range);
                $('#mapslider-range').slider('values', 1, max_map_price_range);
                $('#mapminAmount').html($('#mapslider-range').slider('values', 0));
                $('#mapmaxAmount').html($('#mapslider-range').slider('values', 1));
                $("#cbx_spg_hotel").prop("checked", false);
                
                $.each(markerObjs, function(i, e) {
                    e.marker.setVisible(true);
                });
            }
                $('#mapCityOption').customScrollbar({fixedThumbHeight:30});
        },

        searching: function() {
            if ( ! noproperties) {
                //$('#mapWrapper').addClass('hidden');
                $('#mapView, #viewMap, #sidebarMapCopy').click(function() {
                    if (ScreenSizeIsMobile) {
                        return;
                    }

                    $('#mapView div').attr('class', 'active');
                    $('#listView div').attr('class', 'notActive');
                    $('#mapWrapper').removeClass('hidden');
                    $('div#main').css('margin-bottom', '0');
                    $('#mapWrapper').height('641px');
                    $('#hotelsWrapper').hide();
                    //$('#mapWrapper ul').mCustomScrollbar('update');
                    $('#mapWrapper ul').customScrollbar({fixedThumbHeight:30});
                    //$('#mapCityOption').mCustomScrollbar('update');
                });

                if (withDate) {
                    // If user searches destination by date, update slider range lowest rates after all ajax has completed
                    $(document).one("ajaxStop", function () {
                        if (availPropertyCount == ratesSearched) {
                            if (dh.el.body.attr('id') != 'city') {
                                var hotelLowestRates = $('#hotelsWithAvailability .memberHotels').map(function() {
                                    if ($(this).attr('data-rate') != "N/A") {
                                        return $(this).attr('data-rate');
                                    }
                                }).get();
                                if (hotelLowestRates.length > 0) {
                                    var lowestRateRange = Number(hotelLowestRates.sort(function(a, b) {return a-b}).shift());
                                    var highestRateRange = Number(hotelLowestRates.sort(function(a, b) {return -b-a}).shift());
                                    min_price_range = lowestRateRange;
                                    max_price_range = highestRateRange;
                                }
                            }
                            dh.gMapInit();
                        }
                    });
                } else {
                    dh.gMapInit();
                }
            } else {
                dh.bookingCalendar();
            }
        },

        propertyFancyBox: function() {
            $(".property-fancy-box")
                .fancybox({
                    padding : 0,
                    autoResize  : true,
                    nextClick   :true ,
                    openEffect  : 'elastic',
                    closeEffect : 'elastic',
                    width       : '95%',
                    height      : '95%',
                    helpers : {
                    title : {
                        type : 'inside'
                    }
                }
            });
        },

        gMapInit: function() {

            var resizeFilterList = function() {
                    $(".filterList").each(function() {
                            var overviewHeight = $(this).find(".overview").height();
                            var maxListHeight = parseInt($(this).css("max-height"));
                            
                            $(this).height(overviewHeight<maxListHeight?overviewHeight:maxListHeight);
                            if(overviewHeight<maxListHeight) {
                                    $(this).find(".scroll-bar").hide();
                                    $(this).find(".overview").css('position', 'static');
                            } else {
                                    $(this).find(".scroll-bar").height(maxListHeight);
                                    $(this).find(".overview").css('position', 'absolute');
                            }
                            $(this).find(".viewport").height(overviewHeight<maxListHeight?overviewHeight:maxListHeight);
                    });
                    if($("#mapSPGToggle").hasClass("filter-option-active")) {
                        $("#mapSPGToggle").removeClass("filter-option-active");
                        $("#mapSPGToggle").click();
                    }
            }
            
            var filterMap = function() {

                var max = $('#mapmaxAmount').length > 0 ? $('#mapmaxAmount').attr('data-max') : $('#maxAmount').attr('data-max');
                var min = $('#mapminAmount').length > 0 ? $('#mapminAmount').attr('data-min') : $('#minAmount').attr('data-min');
                var country = $('#mapCountryOption li.selected').data('value');
                var city = $('#mapCityOption li.selected').data('value');
                var spgHotel = $('#mapSPGToggle').hasClass("filter-option-active");
                
                $.each(markerObjs, function(i, e) {
                    var spg = true;
                    if(e.spg_hotel == 0) {
                        spg = false;
                    }
                    var show = e.rate == 'N/A' ? true :
                                      (min == 0 || isNaN(min)) && (max == 0 || isNaN(max))
                                                         ? true
                                                         : parseFloat(e.rate) >= parseFloat(min)
                                                           && parseFloat(e.rate) <= parseFloat(max) ? true
                                                                                            : false;

                    show = show != false && country != null && country != e.country ? false : show;
                    show = show != false && city != null && city != e.city ? false : show;
                                        show = show ? ((spgHotel && spg) || !spgHotel ? show : spgHotel && spg != "0") : show;
                    e.marker.setVisible(show !== false);

                });
                markerCluster.repaint();
            }

            var filterList = function() {
                var max = $('#maxAmount').attr('data-max');
                var min = $('#minAmount').attr('data-min');
                var country = $('#countryOption li.selected').data('value');
                var city = $('#cityOption li.selected').data('value');
                var neighborhood = $('#neighborhoodOption li.selected').data('value');
                var hotelList = $('div#hotelsList div.memberHotels');
                var spgHotel = $('#SPGToggle').hasClass("filter-option-active");

                if(country !== null) {
                    $('#cityOption li').show();
                    //hide cities not in selected country
                    $('#cityOption li').each(function(){
                        if($(this).data("country") != country) {
                            $(this).hide();
                        }
                    });
                }

                hotelList.each(function(i, e) {
                    var $hotel = $(this);
                    var show = ($hotel.data('rate') >= min && $hotel.data('rate') <= max) || $hotel.data('rate')=='N/A';

                    //not available member hotels (exclude price check)
                    if(!show && typeof(withDate)!=="undefined" && withDate == 1) {
                        if($hotel.parent().prop('id') == 'hotelsNoAvailability') {
                            show = true;
                        }
                    }

                    var options = [];
                    try {
                        options = ('' + $hotel.data('filteroptions')).split(',');
                    } catch(e) {}

                    if (show != false && (spgHotel && !$hotel.data('spghotel'))) {
                        show = false;
                    }
                    
                    if (show != false && country != null && country != $hotel.data('country')) {
                        show = false;
                        s.prop27='country';
                    }
                    
                    if (show != false && city != null && city != $hotel.data('city')) {
                        show = false;
                        s.prop27='city';
                    }
                    
                    if (show != false && neighborhood != null && neighborhood != $hotel.data('neighborhood')) {
                        show = false;
                    }
                    
                    if (show) {
                        $('.filter-category .filter-option-active').each(function() {
                            var activeFilter = '' + $(this).data('value');
                            if ($.inArray(activeFilter, options) == -1) {
                                show = false;
                            }
                        });
                    }
                    
                    if (show !== false) {
                        $hotel.show();
                    } else {
                        $hotel.hide();
                    }
                });

            }

            $('#slider-range').slider({
                range: true,
                min: parseInt(min_price_range),
                max: parseInt(max_price_range),
                values: [min_price_range, max_price_range],
                slide: function(event, ui) {
                    $('#minAmount').html( ui.values[0]);
                    $('#maxAmount').html( ui.values[1]);
                    $('#minAmount').attr('data-min', ui.values[0]);
                    $('#maxAmount').attr('data-max', ui.values[1]);
                },
                change: function(event, ui) {
                    filterList();
                    s.prop27='price';
                }
            });

            $('#minAmount').html( $('#slider-range').slider('values', 0));
            $('#maxAmount').html( $('#slider-range').slider('values', 1));
            $('#minAmount').attr('data-min', $('#slider-range').slider('values', 0));
            $('#maxAmount').attr('data-max', $('#slider-range').slider('values', 1));
            $('#countryOption, #cityOption, #neighborhoodOption').on('click', 'li', function() {
                if ($(this).hasClass('selected')) {
                    $(this).parent().find('li').removeClass('selected');
                } else {
                    $(this).parent().find('li').removeClass('selected');
                    $(this).addClass('selected');
                }

                filterList();
            });
            $(document).bind('campaignLoad', filterList);
            $(document).bind('searchLoad', filterList);
            
            $('select#sortings_dd').tzSelect({
                selectboxClass: 'sortSearch_selectbox',
                dropdownClass: 'sortSearch_dropdown',
                onChange: function() {
                    dh.sortListings();
                }
            });
            
            $('.filter-category .filter-option').click(function() {
                $(this).toggleClass('filter-option-active');
                filterList();
            });

            $('#SPGToggle').click(function() {
                $(this).toggleClass('filter-option-active');
                filterList();
            });
            
            // $(".checkbox").click(function(){
                // $(this).toggleClass("checked");
            // });

            $(".toggle").click(function(){
                $(this).toggleClass("closeTag");
            });

            initMap();

            $('#mapslider-range').slider({
                range: true,
                min: parseInt(min_map_price_range),
                max: parseInt(max_map_price_range),
                values: [min_map_price_range, max_map_price_range],
                slide: function( event, ui ) {
                    $('#mapminAmount').html( ui.values[0]);
                    $('#mapmaxAmount').html( ui.values[1]);
                    $('#mapminAmount').attr('data-min', ui.values[0]);
                    $('#mapmaxAmount').attr('data-max', ui.values[1]);
                },
                change: function(event, ui) {
                    filterMap();
                }
            });

            $('#mapminAmount').html($('#mapslider-range').slider('values', 0));
            $('#mapmaxAmount').html($('#mapslider-range').slider('values', 1));
            $('#mapminAmount').attr('data-min', $('#mapslider-range').slider('values', 0));
            $('#mapmaxAmount').attr('data-max', $('#mapslider-range').slider('values', 1));
            $('#mapWrapper #clearAll > a').on('click', function() {
                $('#mapCountryOption, #mapCityOption').find('li').removeClass('selected');
                $('#mapCityOption').find('li').css('display', 'block');
            });
            
            $('#hotelsAndResorts #mapWrapper #clearAll > a, #campaign #mapWrapper #clearAll > a, #countryPage #mapWrapper #clearAll > a').on('click', function() {
                                if (infobox) {
                                    infobox.close();
                                }
                map.setZoom(2);
                map.setCenter(dh.map.bounds.getCenter());
                //$('#mapCityOption').mCustomScrollbar('update');
                $('#mapCityOption').customScrollbar({fixedThumbHeight:30});
                                resizeFilterList();
            });
            $('#mapCountryOption, #mapCityOption').on('click', 'li', function() {

                if ($(this).hasClass('selected')) {

                    $('#mapCountryOption, #mapCityOption').find('li').not(this).removeClass('selected');

                } else {

                    $('#mapCountryOption, #mapCityOption').find('li').removeClass('selected');
                    $(this).addClass('selected');
                }

                filterMap();

                if ($('#mapCountryOption').length > 0) {

                    $('div.mCustomScrollBox').css('min-height', '65px');
                    var cityList = [];
                    var bounds = new google.maps.LatLngBounds();
                    var isCountry = $(this).parents('#mapCountryOption').length > 0;
                    for(var i = 0; i < markerObjs.length; i++) {

                        var cmprObj = isCountry ? markerObjs[i].country : markerObjs[i].city;
                        if (cmprObj == $(this).data('value')) {

                            bounds.extend(markerObjs[i].latlng);
                            cityList.push(markerObjs[i].city);
                        }
                    }

                    if (isCountry) {

                        $('#mapCityOption').find('li').css('display', 'none');
                        for (var i = 0; i < cityList.length; i++) {

                            $('ul#mapCityOption li[data-value="' + cityList[i] + '"]').css('display', 'block');
                        }

                        //$('#mapCityOption').mCustomScrollbar('update');
                        $('#mapCityOption').customScrollbar({fixedThumbHeight:30});
                        resizeFilterList();
                    }

                    var zoomChangeBoundsListener = google.maps.event.addListener(map, 'bounds_changed', function(event) {

                       if (map.getZoom() > 14 && map.initialZoom == true) {

                           map.setZoom(14);
                           map.setCenter(bounds.getCenter());
                       }

                       google.maps.event.removeListener(zoomChangeBoundsListener);

                    });

                    map.initialZoom = true;
                    map.fitBounds(bounds);
                }
            });

            $("#mapSPGToggle").on("click", function() {
                $(this).toggleClass('filter-option-active');
                var checked = $(this).hasClass("filter-option-active");
                var country = $('#mapCountryOption li.selected').data('value');
                var city = $('#mapCityOption li.selected').data('value');

                filterMap();

                var bounds = new google.maps.LatLngBounds();

                var isCountry = $(this).parents('#mapCountryOption').length > 0;
    
                for(var i = 0; i < markerObjs.length; i++) {
                    if (!checked
                       || (checked && markerObjs[i].spg_hotel !="0")
                       || (country && markerObjs[i].country == country)
                       || (city && markerObjs[i].city == city)
                       ) {
                        bounds.extend(markerObjs[i].latlng);
                    }
                }
                
                var zoomChangeBoundsListener = google.maps.event.addListener(map, 'bounds_changed', function(event) {
                    if (map.getZoom() > 14 && map.initialZoom == true) {
                        map.setZoom(14);
                        map.setCenter(bounds.getCenter());
                    } else if(map.getZoom() < 2) {
                        map.setZoom(2);
                        map.setCenter(dh.map.bounds.getCenter());
                    }
                    google.maps.event.removeListener(zoomChangeBoundsListener);
                });
                map.initialZoom = true;
                map.fitBounds(bounds);
            });

            $("#mapView, #viewMap").click(function(){
                if (ScreenSizeIsMobile) {
                    return;
                }

                $("#mapView div").addClass('active').removeClass('notActive');
                $("#listView div").addClass('notActive').removeClass('active');

                $("#mapWrapper").removeClass('hidden');
                $('div#main').css('margin-bottom', '0');

                $("#hotelsWrapper, #sort").hide('slow');

                //$("#mapWrapper ul").mCustomScrollbar("update");
                $('#mapWrapper ul').customScrollbar({fixedThumbHeight:30});

                // Reset map slider ranges
                $('#mapslider-range').slider('values', 0, min_map_price_range);
                $('#mapslider-range').slider('values', 1, max_map_price_range);
                $('#mapminAmount').html($('#mapslider-range').slider('values', 0));
                $('#mapmaxAmount').html($('#mapslider-range').slider('values', 1));

                initMap();
                resizeFilterList();
            });

            $('#listView').click(function() {
                if (ScreenSizeIsMobile) {
                    return;
                }

                $("#mapView div").addClass('notActive').removeClass('active');
                $("#listView div").addClass('active').removeClass('notActive');
                if ($('#listView div').hasClass('active') && infobox) {
                    infobox.close();
                }

                $("#mapWrapper").addClass('hidden');
                $('div#main').css('margin-bottom', '40px');

                $("#hotelsWrapper, #sort, #pdpDealimage, #pdpDealinfo").show('slow');

                //$("#hotelsWrapper ul").mCustomScrollbar("update");
                $('#hotelsWrapper .sidebarHelper ul').customScrollbar({fixedThumbHeight:30});
                                resizeFilterList();
            });

            $(".toggle").click(function(){
                $(this).toggleClass("closeTag");
            });

            var hideShowAboutSpg = function(info) {
                if(info.hasClass("open")) {
                    info.removeClass("open");
                    info.parents(".spg").find('.box-tail').hide();
                    info.parents(".spg").find('.about-spg').hide();
                } else {
                    info.addClass("open");
                    info.parents(".spg").find('.box-tail').show();
                    info.parents(".spg").find('.about-spg').show();
                }
            };

            $(".spg").on("mouseover", "a.info", function(e){
                hideShowAboutSpg($(this));
            });

            $(".spg").on("click", "a.info", function(e){
                e.preventDefault();
                hideShowAboutSpg($(this));
            });

            $(document).mouseup(function (e) {
                var container = $(".spg a.info");
                var parent = $(".about-spg");
                if (!parent.is(e.target) && container.hasClass("open")) // if the target of the click isn't the info?...
                {
                    container.removeClass("open");
                    container.parents(".spg").find('.box-tail').hide();
                    container.parents(".spg").find('.about-spg').hide();
                }
            });

            $("#priceToggle").click(function(){
                $("#priceOption").toggle("slow");
            });

            $("#countryToggle").click(function(){
                $("#countryOption").toggle("slow");
            });

            $("#styleToggle").click(function(){
                $("#styleOption").toggle("slow");
            });

            $("#cityToggle").click(function(){
                $("#cityOption").toggle("slow");
            });
            
            $("#neighborhoodToggle").click(function(){
                $("#neighborhoodOption").toggle("slow");
            });
            
            $('.filter-category-toggle').click(function() {
                $(this).parent().find('.filterList').toggle('slow');
            });

            $("#mapCountryToggle").click(function(){
                $("#mapCountryOption").slideToggle("fast", function() {
                    $(this).parent().find('.toggle').text($(this).is(':visible') ? '-' : '+');
                });
            });

            $("#mapCityToggle").click(function(){
                $("#mapCityOption").slideToggle("fast", function() {
                    $(this).parent().find('.toggle').text($(this).is(':visible') ? '-' : '+');
                });
            });

            $("#mapStyleToggle").click(function(){
                $("#mapStyleOption").slideToggle("fast", function() {
                    $(this).parent().find('.toggle').text($(this).is(':visible') ? '-' : '+');
                });
            });

            $("#mapPriceToggle").click(function(){
                $("#mapPriceOption").slideToggle("fast", function() {
                    $(this).parent().find('.toggle').text($(this).is(':visible') ? '-' : '+');
                });
            });
            $(".sidebarDiv > ul").customScrollbar({fixedThumbHeight:30});
            resizeFilterList();
        },

        sortListings: function() {

            var sortSelectedValue = $('select#sortings_dd').val();

            var hotelList ;
            if( $('div#hotelsWithAvailability div.memberHotels').length > 0){
                hasAvailability = true;
                hotelList =  $('div#hotelsWithAvailability div.memberHotels');

            } else {
                hasAvailability = false;
                hotelList =  $('div#hotelsNoAvailability div.memberHotels');
            }
            hotelList.detach().sort(function(a, b) {
                switch (sortSelectedValue) {
                case 'rate asc':
                    value1 = $(a).data('rate') == 'N/A' ? 10000 : $(a).data('rate');
                    value2 = $(b).data('rate') == 'N/A' ? 10000 : $(b).data('rate');

                    var compare = parseFloat(value1) - parseFloat(value2);

                    if (compare == 0) {
                        value1 = $(a).data('name');
                        value2 = $(b).data('name');

                        return value1.localeCompare(value2, 'en-US');
                    }

                    return compare;

                case 'rate desc':
                    value1 = $(a).data('maxrate')== 'N/A' ? 0 : $(a).data('maxrate');
                    value2 = $(b).data('maxrate')== 'N/A' ? 0 : $(b).data('maxrate');

                    var compare = parseFloat(value2) - parseFloat(value1)

                    if (compare == 0) {
                        value1 = $(a).data('name');
                        value2 = $(b).data('name');

                        return value1.localeCompare(value2, 'en-US');
                    }

                    return compare;

                case 'name asc':
                    value1 = $(a).data('name');
                    value2 = $(b).data('name');

                    return value1.localeCompare(value2, 'en-US');
                case 'name desc':
                    value1 = $(a).data('name');
                    value2 = $(b).data('name');

                    return value1.localeCompare(value2, 'en-US') * -1;
                case 'mostPopular':
                    value1 = $(a).data('order') == 'N/A' ? 10000 : $(a).data('order');
                    value2 = $(b).data('order') == 'N/A' ? 10000 : $(b).data('order');

                    var compare = parseFloat(value1) - parseFloat(value2);
                    //If equal, arrange by with name
                    if (compare == 0) {
                        value1 = $(a).data('name');
                        value2 = $(b).data('name');

                        return value1.localeCompare(value2, 'en-US');
                    }
                    return compare;
                }
            });
                if (hasAvailability){
                    $('div#hotelsWithAvailability').append(hotelList);
                } else {
                    $('div#hotelsNoAvailability').append(hotelList);
                }

        },

        bookingCalendar: function() {
            var monthExist =[];
            var monthLoading = {};

            var d = new Date();
            var d2 = new Date();
            d2.setMonth(d2.getMonth() + 1);

            monthExist.push(d.getMonth()+1 + '_' + d.getFullYear());
            monthExist.push(d.getMonth()+2 + '_' + d.getFullYear());
            monthExist.push(d.getMonth()+3 + '_' + d.getFullYear());

            // Keep track of which 2 months are visible
            var currentMonth1 = d.getFullYear() + '_' + d.getMonth();
            var currentMonth2 = d2.getFullYear() + '_' + d2.getMonth();

            // Timer to prevent too many ajax calls
            var updateCalendarAjaxTimer = false;
            var updateCalendarAjaxTimer1 = false;

            var updateCalendarAjax = function(date, whichMonth, currentMonth, index) {
                monthLoading[whichMonth] = true;

                if (currentMonth == currentMonth1) {
                    $("#loadingimg1").show();
                }

                if (currentMonth == currentMonth2) {
                    $("#loadingimg2").show();
                }

                var currency = $('#selectedcurrency').val();

                $.ajax({
                    type: "POST",
                    url: "/futurerate/" + propid + "/" + date +"/0" + '/' + currency,
                    //data: ,
                    cache: true,
                    dataType: 'json',
                    success: function(response) {
                        monthLoading[whichMonth] = false;

                        calendarLowestRates = $.extend(calendarLowestRates, response);
                        dh.fillCalendar(calendarLowestRates);

                        if (currentMonth == currentMonth1) {
                            $("#loadingimg1").hide();
                        }

                        if (currentMonth == currentMonth2) {
                            $("#loadingimg2").hide();
                        }

                        monthExist.push(whichMonth);
                    }
                });
            };

            // CALENDAR
            $('div#bookingCalendar').DatePicker({
                flat: true,
                prev: '',
                next: '',
                date: new Date(),
                format: 'Y-m-d',
                starts: 1,
                calendars: 2,
                mode: 'range',
                view: 'days',
                autoHide: false,
                onMonthChange: function(firstDate) {
                    $("#loadingimg1").hide();
                    $("#loadingimg2").hide();

                    var secondDate = new Date(firstDate.valueOf());
                    secondDate.setMonth(secondDate.getMonth() + 1);

                    currentMonth1 = firstDate.getFullYear() + '_' + firstDate.getMonth();
                    currentMonth2 = secondDate.getFullYear() + '_' + secondDate.getMonth();

                    var month = firstDate.getMonth() + 2;
                    month1 = firstDate.getMonth() + 1;
                    month2 = month + '_' + firstDate.getFullYear();
                    var year  = firstDate.getFullYear();

                    if (month == 13) {
                        month = 1;
                        year  = year + 1;
                    }

                    if (month < 10) {
                        month = '0' + month;
                    }

                    if (month1 < 10) {
                        month1 = '0' + month1;
                    }

                    var date1 = year + '-' + month1 + '-01';
                    var date = year + '-' + month + '-01';

                    month1 = firstDate.getMonth() + 1 + '_' + firstDate.getFullYear();

                    if (
                        typeof(monthLoading[month1]) != 'undefined'
                        && monthLoading[month1]
                    ) {
                        $("#loadingimg1").show();
                    } else if ( $.inArray( month1,monthExist) < 0 ) {
                        if (updateCalendarAjaxTimer1) {
                            clearTimeout(updateCalendarAjaxTimer1);
                        }

                        updateCalendarAjaxTimer1 = setTimeout(function() {
                            updateCalendarAjaxTimer1 = false;
                            updateCalendarAjax(date1, month1, currentMonth1, '1');
                        }, 500);
                    }

                    if (
                        typeof(monthLoading[month2]) != 'undefined'
                        && monthLoading[month2]
                    ) {
                        $("#loadingimg2").show();
                    } else if ( $.inArray( month2,monthExist) < 0 ) {
                        if (updateCalendarAjaxTimer) {
                            clearTimeout(updateCalendarAjaxTimer);
                        }

                        updateCalendarAjaxTimer = setTimeout(function() {
                            updateCalendarAjaxTimer = false;
                            updateCalendarAjax(date, month2, currentMonth2, '2');
                        }, 500);
                    }
                },
                onFill: function() {

                    //it wouldn't work if month's name will be translated in diff lang in the future
                    dh.fillCalendar(calendarLowestRates);
                },
                onChange: function(formatted, dates) {
                    $('input#bookingStart').val(formatted[0]);
                    $('input#bookingEnd').val(formatted[1]);

                    dh.bookingPopulateSummary(formatted[0], formatted[1]);
                }
            });

            $('#bookingCalendar').DatePickerShow();
            $('div#bookingCalendar div.datepicker').show();

            $('p#bookingCalendarClear').mousedown(function(ev) {
                ev.stopPropagation();

                $('div#bookingCalendar').DatePickerClear();
            });

            $('a#bookingPrevMonth').mousedown(function(ev) {
                ev.stopPropagation();
                $('#loadingimg').hide();
                $('div#bookingCalendar th.datepickerGoPrev').eq(0).trigger('click');
            });

            $('a#bookingNextMonth').mousedown(function(ev) {
                ev.stopPropagation();

                $('div#bookingCalendar th.datepickerGoNext').eq(0).trigger('click');
            });
        },

        bookingPopulateSummary: function(start, end) {

            var startArr = start.split('-');
            var endArr = end.split('-');

            var start = new Date(startArr[0], parseInt(startArr[1]) - 1, parseInt(startArr[2]));
            var end = new Date(endArr[0], parseInt(endArr[1]) - 1, parseInt(endArr[2]));

            var days = Math.round((end - start) / (1000 * 60 * 60 * 24) + 1);
            var nights = days - 1;

            $('span#bookingSummaryDays').html(days);
            $('span#bookingSummaryNights').html(nights);

            if(nights > 1){
                $('span#bookingSummaryTotalNights').html(nights+"<span> nights</span>");
            }
            else {
                $('span#bookingSummaryTotalNights').html(nights+"<span> night</span>");
            }

            var mStart = moment(start);
            var mEnd = moment(end);

            $('span#bookingSummaryDateRange').html(
                mStart.format('MMM D') + ' &ndash; ' + mEnd.format('MMM D, YYYY')
            );
            $(".bookingSummaryAdults").css("padding-bottom", "0px");
            $('li.bookingSummaryDate').show();
        },

        bookingStep1: function() {
            var errorlightboxOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                height: 'auto',
                modal: true,
                width: '430',
                resizable: false
                // open: function(event, ui) {
                //     $("body").css({ overflow: 'hidden' })
                // },
                // beforeClose: function(event, ui) {
                //     $("body").css({ overflow: 'inherit' })
                // }

            };

            var errorlightboxOptionsMobile = dh.standardMobileLightboxOptions;

            $(window).on('to-desktop', function() {
                $('#sabreError').dialog(errorlightboxOptions);
            });

            $(window).on('to-mobile', function() {
                $('#sabreError').dialog(errorlightboxOptionsMobile);
            });

             var $bookingSummaryInfo = $('ul#bookingSummaryInfo');

            if ($bookingSummaryInfo.data('start') != '' && $bookingSummaryInfo.data('end') != '' && $bookingSummaryInfo.data('start') != null && $bookingSummaryInfo.data('end') != null) {
                $('div#bookingCalendar').DatePickerSetDate(new Array($bookingSummaryInfo.data('start'), $bookingSummaryInfo.data('end')));
                $('#Calendar .calendar-holder .inner').DatePickerSetDate(new Array($bookingSummaryInfo.data('start'), $bookingSummaryInfo.data('end')));
                $('#checkIn span').text($bookingSummaryInfo.data('formatstart'));
                $('#checkOut span').text($bookingSummaryInfo.data('formatend'));
            }

            $('li.bookingSummaryDate').hide();

            $('#bookingStep1 a#bookingContinueStep1,#bookingStep1 a#bookingContinueStep1Mobile').click(function(ev) {
                ev.preventDefault();

                $('form#bookingStep1').submit();
            });


            $('select#bookingAdults').tzSelect({
                selectboxClass: 'bookingAdults_selectbox',
                dropdownClass: 'bookingAdults_dropdown',
                onChange: function() {
                    var selectedValue = $('select#bookingAdults').val();

                    $('span#bookingSummaryAdults').html(selectedValue);
                }
            });

            $('select#bookingChildren').tzSelect({
                selectboxClass: 'bookingChildren_selectbox',
                dropdownClass: 'bookingChildren_dropdown',
                onChange: function() {
                    var selectedValue = $('select#bookingChildren').val();

                    $('span#bookingSummaryChildren').html(selectedValue);
                }
            });
        },

        bookingStep2: function() {
           var $bookingSummaryInfo = $('ul#bookingSummaryInfo');
            if ($bookingSummaryInfo.data('start') != '' && $bookingSummaryInfo.data('end') != '' && $bookingSummaryInfo.data('start') != null && $bookingSummaryInfo.data('end') != null) {
                dh.bookingPopulateSummary($bookingSummaryInfo.data('start'), $bookingSummaryInfo.data('end'));
            }

            //Cancelation Policy Lightbox
            cplightboxOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                height: 'auto',
                modal: true,
                width: '430',
                resizable: false
                //open: function(event, ui) {
                    //$("body").css({ overflow: 'hidden' })
                // },
                // beforeClose: function(event, ui) {
                //    // $("body").css({ overflow: 'inherit' })
                // }
            };

            var errorlightboxOptionsMobile = dh.standardMobileLightboxOptions;

            $(window).on('to-desktop', function() {
                $('#cancelPolicy').dialog(cplightboxOptions);
            });

            $(window).on('to-mobile', function() {
                $('#cancelPolicy').dialog(errorlightboxOptionsMobile);
            });

            $( ".bookingRoomCancelPolicy a" ).on('click', function() {
                cancelPolicy = $(this).parent().parent().find('.cancelPolicyInput').val();
                hotelPolicy = $(this).parent().parent().find('.hotelPolicyInput').val();
                $('#cancelPolicyText').text(cancelPolicy);
                $('#hotelPolicyText').text(hotelPolicy);

                $('#cancelPolicy').dialog('open');
            });
            $( ".showMoreRooms" ).on('click', function() {

                $(this).parent().parent().find('.hide').toggle("slow");
                $(this).find('.moreText').toggle();
                $(this).find('.lessText').toggle();
                $(this).parent().find('.rooms-show').toggle();
                $(this).parent().find('.rooms-show-full').toggle();


            });
            var errorlightboxOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                height: 'auto',
                modal: true,
                width: '430',
                resizable: false
            };

            $('#roomError').dialog(errorlightboxOptions);

            $('input.bookingRoomValue').spinsterPro({
                    'max'     : $("input.bookingRoomMax").val(),
                    'min'     : 1,
                    'start'   : 'value',
                    'onChange': function(value) {
                        var label = (parseInt(value) == 1) ? ' Adult' : ' Adults';
                        $('span#searchGuestsTotalAdults').text(value + label);
                    }
                });

            $('a.readmore').click(function(ev) {
                 $(this).parent().hide();
                 $(this).parent().parent().find('.fullDesc').show();
            });

            $('a.readless').click(function(ev) {
                $(this).parent().hide();
                $(this).parent().parent().find('.partDesc').show();
            });

            $('a.selectRoom').click(function(ev) {
                ev.preventDefault();

                var ratePlan = $(this).data('rateplan');
                var roomId = $(this).parent().find('span.roomId').html();
                var roomSelect = $(this).parent().find('input.bookingRoomValue').val();
                var roomName = $(this).parent().find('h4').html();

                $('input#bookingRatePlan').val(ratePlan);
                $('input#bookingRoom').val(roomId);
                $('input#bookingNumRoom').val(roomSelect);
                $('input#bookingRoomName').val(roomName);

                $('form#bookingStep2').submit();
            });

            $('h3.bookingRateName').click(function() {
                var $item = $(this).parent();
                var open = $item.hasClass('open');

                $('.bookingRoomRate').removeClass('open');

                if (open == false) {
                    $item.addClass('open');

                    if (ScreenSizeIsMobile) {
                        var scrollTarget = $item.offset().top - 76;
                        $(window).scrollTop(scrollTarget);
                    }
                }
            });
        },

        bookingStep3: function() {
        
        
            var valid = true;

            var validateName = function() {
                var $bookingFirstName = $('input#bookingFirstName');
                var $bookingLastName = $('input#bookingLastName');

                if ($bookingFirstName.val() == '' || $bookingLastName.val() == '') {
                    valid = false;

                    if ($bookingFirstName.val() == '') {
                        $bookingFirstName.parent().addClass('error');
                    } else {
                        $bookingFirstName.parent().removeClass('error');
                    }

                    if ($bookingLastName.val() == '') {
                        $bookingLastName.parent().addClass('error');
                    } else {
                        $bookingLastName.parent().removeClass('error');
                    }

                    $bookingFirstName.parent().find('.errorMessage').text('Error: enter your name');
                    $bookingLastName.parent().find('div.valid').hide();
                } else {
                    $bookingFirstName.parent().removeClass('error');
                    $bookingLastName.parent().removeClass('error');

                    $bookingFirstName.parent().find('.errorMessage').text('');
                    $bookingLastName.parent().find('div.valid').show();
                }
            }

            var validateEmail = function() {
                var $input = $('input#bookingEmail');
                var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (emailPattern.test($input.val())) {
                    $("form#newsletterSignup").submit();

                    $input.parent().removeClass('error');
                    // $input.parent().find('div.valid').show();
                    $input.parent().find('.errorMessage').text('');

                    // $.ajax({
                    //     type: "POST",
                    //     url: "/reservation/checkEmail",
                    //     data: { emailAddress: $input.val(), 'propertyId' : $('input#propertyId').val()}
                    // }).done(function(msg) {
                    //     data = $.parseJSON(msg);
                    //     if (data.success == 'true') {
                    //         $('div#lightboxLogin').dialog('open');

                    //         $('#fromBookingFlow').val(1);

                    //     } else {
                    //         // console.log('not registered');
                    //     }
                    // });
                } else {
                    valid = false;

                    $input.parent().addClass('error');
                    $input.parent().find('.errorMessage').text('Error: enter your Email address');
                    $input.parent().find('div.valid').hide();
                }
            }

            var validatePhone = function() {
                var $input = $('input#bookingPhone');
                var phonePattern = /^[\s()+-.]*([0-9][\s()+-.]*){6,20}$/;

                if (phonePattern.test($input.val())) {

                    $input.parent().removeClass('error');
                    // check if email is also valid
                    $inputEmail = $('input#bookingEmail');
                    if (!$inputEmail.parent().hasClass('error')) {
                        // show only if email is also valid
                        $input.parent().find('div.valid').show();
                    } else {    // otherwise hide
                        $input.parent().find('div.valid').hide();
                    }
                    $input.parent().find('.errorMessage').text('');

                } else {
                    valid = false;

                    $input.parent().addClass('error');
                    $input.parent().find('.errorMessage').text('Error: enter your phone number');
                    $input.parent().find('div.valid').hide();
                }
            }

            var validateCountry = function() {
                var $input = $('input#bookingCountry');

                if ($input.val() == '') {
                    valid = false;

                    $input.parent().addClass('error');
                    $input.parent().find('.errorMessage').text('Error: choose your country');
                } else {
                    $input.parent().removeClass('error');
                    $input.parent().find('.errorMessage').text('');
                }
            }

            var validateCity = function() {
                var $input = $('input#bookingCity');

                if ($input.val() == '') {
                    valid = false;

                    $input.parent().addClass('error');
                    $input.parent().find('.errorMessage').text('Error: enter your city');
                    $input.parent().find('div.valid').hide();
                } else {
                    $input.parent().removeClass('error');
                    $input.parent().find('div.valid').show();
                    $input.parent().find('.errorMessage').text('');
                }
            }

            var validateAddress = function() {
                var $input = $('input#bookingAddress1');

                if ($input.val() == '') {
                    valid = false;

                    $input.parent().addClass('error');
                    $('.bookingAddress2Wrapper .errorMessage').text('Error: enter your address');
                    $input.parent().find('div.valid').hide();
                } else {
                    $input.parent().removeClass('error');
                    $input.parent().find('div.valid').show();
                    $('.bookingAddress2Wrapper .errorMessage').text('');
                }
            }

            var validateCCName = function() {
                var $bookingCCName = $('input#bookingCCName');
                var $bookingCCNameWrapper = $bookingCCName.parent();

                if ($bookingCCName.val() == '') {
                    valid = false;

                    $bookingCCName.parent().addClass('error');

                    $bookingCCNameWrapper.find('.errorMessage').text('Error: enter your name');
                    $bookingCCNameWrapper.find('div.valid').hide();
                } else {
                    $bookingCCNameWrapper.removeClass('error');

                    $bookingCCNameWrapper.find('.errorMessage').text('');
                    $bookingCCNameWrapper.find('div.valid').show();
                }
            }

            var validateCCNumber = function() {
                var $bookingCCNumber = $('input#bookingCCNumber');
                var $bookingCCNumberWrapper = $bookingCCNumber.parent();
                var $bookingCCCVS = $('input#bookingCCCVS');
                var $bookingCCCVSWrapper = $bookingCCCVS.parent();
                var $bookingCCType = $('input#bookingCCType');
                $(".bookingCCTypes").children().hide();
                
                if(/^5[1-5][0-9]{14}$/.test($bookingCCNumber.val())==true ){
                    //Mastercard regexp
                    $('.ccMC').show();
                    $bookingCCNumber.parent().removeClass('error');
                    $bookingCCNumberWrapper.find('.errorMessage').text('');
                    $bookingCCType.val("MC");
                    ccvalid = true;
                }else if(/^4[0-9]{12}(?:[0-9]{3})?$/.test($bookingCCNumber.val())==true){
                    //Visa regexp
                    $('.ccVisa').show();
                    $bookingCCNumber.parent().removeClass('error');
                    $bookingCCNumberWrapper.find('.errorMessage').text('');
                    $bookingCCType.val("VI");
                    ccvalid = true;
                }else if(/^3[47][0-9]{13}$/.test($bookingCCNumber.val())==true){
                    //American Express regexp
                    $('.ccAA').show();
                    $bookingCCNumber.parent().removeClass('error');
                    $bookingCCNumberWrapper.find('.errorMessage').text('');
                    $bookingCCType.val("AX");
                    ccvalid = true;
                }else{
                    $('.ccNone').show();
                    $bookingCCNumber.parent().addClass('error');
                    ccvalid = false;
                    $bookingCCNumberWrapper.find('.errorMessage').text('Credit Card is invalid.');
                    $bookingCCCVSWrapper.find('div.valid').hide();
                }

                if ($bookingCCCVS.val() == '' ) {
                    $bookingCCCVS.parent().addClass('error');
                    cvsvalid = false;

                    $bookingCCCVSWrapper.find('.errorMessage').text('CVS code is invalid.');
                    $bookingCCCVSWrapper.find('div.valid').hide();
                } else {
                    $bookingCCCVS.parent().removeClass('error');
                    $bookingCCCVSWrapper.find('.errorMessage').text('');
                    cvsvalid = true;
                }

                if (  cvsvalid ==true &&  ccvalid == true) {
                    //$bookingCCNumberWrapper.find('.errorMessage').text(errorMsg);
                    // $bookingCCCVSWrapper.find('div.valid').hide();

                    $bookingCCNumberWrapper.removeClass('error');
                    $bookingCCCVSWrapper.removeClass('error');

                    $bookingCCNumberWrapper.find('.errorMessage').text('');
                    $bookingCCCVSWrapper.find('.errorMessage').text('');
                    $bookingCCCVSWrapper.find('div.valid').show();
               }
            }

            var $bookingAgreeTC = $('input#bookingAgreeTC');

            var errorlightboxOptions = {
                autoOpen: false,
                dialogClass: 'lightboxDialog',
                height: 'auto',
                modal: true,
                width: '430',
                resizable: false
            };

            var errorlightboxOptionsMobile = dh.standardMobileLightboxOptions;

            $(window).on('to-desktop', function() {
                $('#bookError').dialog(errorlightboxOptions);
                $('#warning').dialog(errorlightboxOptions);
                $('.cancelPolicy').dialog(errorlightboxOptions);
                $('.cvsHelp').dialog(errorlightboxOptions);
		$('#verifyReservation').dialog(errorlightboxOptions);
            });

            $(window).on('to-mobile', function() {
                $('#bookError').dialog(errorlightboxOptionsMobile);
                $('#warning').dialog(errorlightboxOptionsMobile);
                $('.cancelPolicy').dialog(errorlightboxOptionsMobile);
                $('.cvsHelp').dialog(errorlightboxOptionsMobile);
		$('#verifyReservation').dialog(errorlightboxOptions);
            });

	    $('#resConfirmButton').click(function() {
	    	// submit the form
		$('form#bookingStep3').submit();
	    });

	    $('#resCancelButton').click(function() {
	    	// close the dialog box 
		$('#verifyReservation').dialog('close');
	    });

            $('#helpGuarantee').click(function(){
                $('.cancelPolicy').dialog('open');
            });

            $('#helpCVS').click(function(){
                $('.cvsHelp').dialog('open');
            });

            $('input#bookingCCName').blur(function() {
                validateCCName();
            });

            $('input#bookingCCNumber').blur(function() {
                validateCCNumber();
            });

            $('input#bookingCCCVS').blur(function() {
                validateCCNumber();
            });

            $('input#bookingLastName').blur(function() {
                validateName();
            });

            $('input#bookingEmail').blur(function() {
                validateEmail();
            });

            $('input#bookingPhone').blur(function() {
                validatePhone();
            });

            /*$('input#bookingCountry').blur(function() {
                validateCountry();
            });*/
            
            $('input#bookingCity').blur(function() {
                validateCity();
            });

            $('input#bookingAddress1').blur(function() {
                validateAddress();
            });
            $('span#bookingAgreeTCLabel, li.bookingAgreeTCWrapper span.checkbox').click(function(ev) {
                var $checkbox = $(this).parent().find('.checkbox');

                if ($checkbox.hasClass('checked')) {
                    $checkbox.removeClass('checked');
                    $bookingAgreeTC.prop('checked', false);
                } else {
                    $checkbox.addClass('checked');
                    $bookingAgreeTC.prop('checked', true);
                }
            });


            var isGuestActive = $("#guestTPbookingDetails").css("display");

            if ( isGuestActive = "block" ) {

                var valid = true;

                var validateGuestName = function() {
                    var $bookingFirstName = $('input#bookingTPguestFirstName');
                    var $bookingLastName = $('input#bookingTPguestLastName');

                    if ($bookingFirstName.val() == '' || $bookingLastName.val() == '') {
                        valid = false;

                        if ($bookingFirstName.val() == '') {
                            $bookingFirstName.parent().addClass('error');
                        } else {
                            $bookingFirstName.parent().removeClass('error');
                        }

                        if ($bookingLastName.val() == '') {
                            $bookingLastName.parent().addClass('error');
                        } else {
                            $bookingLastName.parent().removeClass('error');
                        }

                        $bookingFirstName.parent().find('.errorMessage').text('Error: enter the guest\'s name');
                        $bookingLastName.parent().find('div.valid').hide();
                    } else {
                        $bookingFirstName.parent().removeClass('error');
                        $bookingLastName.parent().removeClass('error');

                        $bookingFirstName.parent().find('.errorMessage').text('');
                        $bookingLastName.parent().find('div.valid').show();
                    }
                }

                $('input#bookingTPguestLastName').blur(function() {
                    validateGuestName();
                });

                var validateGuestAddress = function() {
                    var $input = $('input#bookingTPguestAddress1');

                    if ($input.val() == '') {
                        valid = false;

                        $input.parent().addClass('error');
                        $('.bookingTPguestAddress2Wrapper .errorMessage').text('Error: enter the guest\'s address');
                        $input.parent().find('div.valid').hide();
                    } else {
                        $input.parent().removeClass('error');
                        $input.parent().find('div.valid').show();
                        $('.bookingTPguestAddress2Wrapper .errorMessage').text('');
                    }
                }

                $('input#bookingTPguestAddress1').blur(function() {
                    validateGuestAddress();
                });

                var validateGuestCountry = function() {
                    var $input = $('input#bookingTPguestCountry');

                    if ($input.val() == '') {
                        valid = false;

                        $input.parent().addClass('error');
                        $input.parent().find('.errorMessage').text('Error: choose the guest\'s country');
                    } else {
                        $input.parent().removeClass('error');
                        $input.parent().find('.errorMessage').text('');
                    }
                }

                var validateGuestCity = function() {
                    var $input = $('input#bookingTPguestCity');

                    if ($input.val() == '') {
                        valid = false;

                        $input.parent().addClass('error');
                        $input.parent().find('.errorMessage').text('Error: enter the guest\'s city');
                        $input.parent().find('div.valid').hide();
                    } else {
                        $input.parent().removeClass('error');
                        $input.parent().find('div.valid').show();
                        $input.parent().find('.errorMessage').text('');
                    }
                }

                $('input#bookingTPguestCity').blur(function() {
                    validateGuestCity();
                });
            }

	    var getFormDataAndVerifyReservation = function () {
		// collect the form data AFTER validation only
		var bookingSummaryData = {};
		bookingSummaryData.email = $('input#bookingEmail').val();
		bookingSummaryData.adults = window.bookingSummary.adults;
		bookingSummaryData.children = window.bookingSummary.children;
		bookingSummaryData.start_date = window.bookingSummary.start;
		bookingSummaryData.end_date = window.bookingSummary.end;
		bookingSummaryData.room_name = window.bookingSummary.room_name;
		bookingSummaryData.propId = window.bookingSummary.propID;
		
		// send data to controller to verify 
		$.ajax({
		    type: "GET",
		    url: "/reservation",
		    data: {
			    verifyReservation : 1,
			    email             : bookingSummaryData.email,
			    adults            : bookingSummaryData.adults,
			    children          : bookingSummaryData.children,
			    start_date        : bookingSummaryData.start_date,
			    end_date          : bookingSummaryData.end_date,
			    room_name         : bookingSummaryData.room_name,
			    propId            : bookingSummaryData.propId
			  }
		}).done(function(data) {
		    var dataJson = jQuery.parseJSON(data);
		    // reservation was found
		    var found = "FOUND";
		    // reservation was not found
		    var notFound = "NOT FOUND";
		    if (dataJson == notFound) { // proceed as normal
		        // submit the form
			$('form#bookingStep3').submit();
		    } else if (dataJson == found) {
			// show pop up for user to confirm booking
			$('#verifyReservation').dialog('open');
		    } else {
		        // error happened while verifying res. dont interrupt the booking
		        // submit the form
			$('form#bookingStep3').submit();
		    }
		});
	    }

            $('form#bookingStep3 a#confirmBooking').click(function(ev) {
                ev.preventDefault();
                valid = true;

                if($('#chkBookForClient').is(":checked")) {
                    validateName();
                    validateEmail();
                    validatePhone();
                    validateCountry();
                    validateCity();
                    validateAddress();
                    validateCCName();
                    validateCCNumber();
                    validateGuestName();
                    validateGuestAddress();
                    validateGuestCity();

                    if (valid && $bookingAgreeTC.prop('checked')) {
			// two step verification
			getFormDataAndVerifyReservation();
                    } else {
                        $('#bookError').dialog('open');
                        $('#bookError').find('div').html('Please agree with Terms and Conditions in order to proceed or fill in all fields.');
                    }

                } else {
                    validateName();
                    validateEmail();
                    validatePhone();
                    validateCountry();
                    validateCity();
                    validateAddress();
                    validateCCName();
                    validateCCNumber();

                    if (valid && $bookingAgreeTC.prop('checked')) {
			// two step verification
			getFormDataAndVerifyReservation();
                    } else {
                        $('#bookError').dialog('open');
                        $('#bookError').find('div').html('Please agree with Terms and Conditions in order to proceed or fill in all fields.');
                    }
                }

            });

//            var displayState = function(country) {
//                if (country == 'US') {
//                    $('li.bookingStateWrapper').show();
//                    $('div#bookingStateWrapper').show();
//                    $('div#bookingProvinceWrapper').hide();
//
//                    $("select#bookingProvince").val([]);
//                } else if (country == 'CA') {
//                    $('li.bookingStateWrapper').show();
//                    $('div#bookingProvinceWrapper').show();
//                    $('div#bookingStateWrapper').hide();
//
//                    $("select#bookingState").val([]);
//                } else {
//                    $('li.bookingStateWrapper').hide();
//                    $('div#bookingProvinceWrapper').hide();
//                    $('div#bookingStateWrapper').hide();
//
//                    $("select#bookingState").val([]);
//                    $("select#bookingProvince").val([]);
//                }
//            }

//            displayState($('select#bookingCountry').val());
//
//            $('select#bookingCountry').tzSelect({
//                selectboxClass: 'bookingCountry_selectbox',
//                dropdownClass: 'bookingCountry_dropdown',
//                onChange: function() {
//                    var selectedValue = $('select#bookingCountry').val();
//                    displayState(selectedValue);
//                }
//            });
            
//            $('select#bookingTitle').tzSelect({
//                selectboxClass: 'bookingTitle_selectbox',
//                dropdownClass: 'bookingTitle_dropdown',
//            });
            
//            $('select#bookingState').tzSelect({
//                selectboxClass: 'bookingCountry_selectbox',
//                dropdownClass: 'bookingCountry_dropdown',
//                onChange: function() {
//                    var selectedValue = $('select#bookingState').val();
//                }
//            });
//
//            $('select#bookingProvince').tzSelect({
//                selectboxClass: 'bookingCountry_selectbox',
//                dropdownClass: 'bookingCountry_dropdown',
//                onChange: function() {
//                    var selectedValue = $('select#bookingProvince').val();
//                }
//            });

            // TP booking for a Guest
            //
//            var displayTPguestState = function(country) {
//                if (country == 'US') {
//                    $('li.bookingTPguestStateWrapper').show();
//                    $('div#bookingTPguestStateWrapper').show();
//                    $('div#bookingTPguestProvinceWrapper').hide();
//
//                    $("select#bookingTPguestProvince").val([]);
//                } else if (country == 'CA') {
//                    $('li.bookingTPguestStateWrapper').show();
//                    $('div#bookingTPguestProvinceWrapper').show();
//                    $('div#bookingTPguestStateWrapper').hide();
//
//                    $("select#bookingTPguestState").val([]);
//                } else {
//                    $('li.bookingTPguestStateWrapper').hide();
//                    $('div#bookingTPguestProvinceWrapper').hide();
//                    $('div#bookingTPguestStateWrapper').hide();
//
//                    $("select#bookingTPguestState").val([]);
//                    $("select#bookingTPguestProvince").val([]);
//                }
//            }

            //displayTPguestState($('select#bookingTPguestCountry').val());

//            $('select#bookingTPguestCountry').tzSelect({
//                selectboxClass: 'bookingCountry_selectbox',
//                dropdownClass: 'bookingCountry_dropdown',
//                onChange: function() {
//                    var selectedValue = $('select#bookingTPguestCountry').val();
//                    displayTPguestState(selectedValue);
//                }
//            });

//            $('select#bookingTPguestState').tzSelect({
//                selectboxClass: 'bookingCountry_selectbox',
//                dropdownClass: 'bookingCountry_dropdown',
//                onChange: function() {
//                    var selectedValue = $('select#bookingTPguestState').val();
//                }
//            });

//            $('select#bookingTPguestProvince').tzSelect({
//                selectboxClass: 'bookingCountry_selectbox',
//                dropdownClass: 'bookingCountry_dropdown',
//                onChange: function() {
//                    var selectedValue = $('select#bookingTPguestProvince').val();
//                }
//            });

            // end TP booking for Guest

            $('select#bookingCCType').tzSelect({
                selectboxClass: 'bookingCCType_selectbox',
                dropdownClass: 'bookingCCType_dropdown',
                onChange: function() {
                    var selectedValue = $('select#bookingCCType').val();
                }
            });

//            $('select#bookingCCMonth').tzSelect({
//                selectboxClass: 'bookingCCMonth_selectbox',
//                dropdownClass: 'bookingCCMonth_dropdown'
//            });
//
//            $('select#bookingCCYear').tzSelect({
//                selectboxClass: 'bookingCCYear_selectbox',
//                dropdownClass: 'bookingCCYear_dropdown'
//            });
//
//            $('select#bookingTPType').tzSelect({
//                selectboxClass: 'bookingTPType_selectbox',
//                dropdownClass: 'bookingTPType_dropdown'
//            });

            $('#chkBookForClient').change(function() {
                if($(this).is(":checked")) {
                    $("#guestTPbookingDetails").slideDown();
                } else {
                    $("#guestTPbookingDetails").slideUp();
                }
            });
            
            
            /*$( "#bookingCountry" ).autocomplete({
              source: availableTags,
              autoFocus: true,
              change: function (event, ui) {
                    if(!ui.item){
                        $("li.bookingCountryWrapper .errorMessage").text('Please select a valid country');
                        $("#bookingCountry").val("");
                    }else{
                       $("li.bookingCountryWrapper .errorMessage").text('');
                    }
                },
                select: function (event, ui) {
                    $("li.bookingCountryWrapper .errorMessage").text('');
                }
            });*/
            
            /*$.ui.autocomplete.filter = function (array, term) {
                var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
                return $.grep(array, function (value) {
                    return matcher.test(value.label || value.value || value);
                });
            };*/
        },

        bookingStep4: function() {
            var valid = true;


            // $('form#bookingStep4 a#confirmBooking').click(function(ev) {
            //     // ev.preventDefault();

            //     // valid = true;

            //     // validateCCName();
            //     // validateCCNumber();

            //     // if (valid && $bookingAgreeTC.prop('checked')) {
            //     //     $('form#bookingStep4').submit();
            //     // } else {
            //     //     $('#bookError').dialog('open');
            //     //     $('#bookError').find('div').html('Please agree with Terms and Conditions in order to proceed or fill in all fields.');
            //     }
            // });


        },

        bookingStep5: function() {
            var $bookingThankYouInfo = $('div#bookingThankYouInfo');

            if ($bookingThankYouInfo.data('start') != '' && $bookingThankYouInfo.data('start') != null && $bookingThankYouInfo.data('end') != '' && $bookingThankYouInfo.data('end') != null) {
                dh.bookingPopulateSummary($bookingThankYouInfo.data('start'), $bookingThankYouInfo.data('end'));
            }
        },

        pdpImageSocialButtons: function() {
            // For desktop use window.open for a popup window, for mobile use
            // default link action.
            $('#pdpImage .caption .social-share .button').click(function(e) {
                if (ScreenSizeIsMobile) {
                    return;
                }
                
                e.preventDefault();
                e.stopPropagation();
                
                var href = $(this).attr('href');
                window.open(href, 'social', 'width=500,height=300,menubar=no,location=no,status=no,titlebar=no,toolbar=no');
            });
        },
        pdpSearchWidget: function() {
            $searchInput = $('li#searchInputWrap input');
            $searchIcon = $('li#searchIcon');
            $pdpSearchButton = $('div#availability-widget .yellow-button, div#availability-widget-search .yellow-button');

            $searchInput.focus(function() {
                $searchInput.parent().removeClass('error');
                //blink = setInterval(dh.blink, 1000);
                $searchIcon.removeClass('error');
                $('li.blink').css('background-color','#c3c3c3');

            });

            var searchFunction = function(ev) {
                ev.preventDefault();
                
                    var hasDates = false;
                    var dateRange = '';
                    
                    var strDateStart;
                    var strDateEnd;
                    
                    if (ScreenSizeIsMobile) {
                        strDateStart = $('#availability-widget .control-mobile-in > input').val();
                        strDateEnd = $('#availability-widget .control-mobile-out > input').val();
                        
                        if (typeof strDateStart === 'undefined'){
                            strDateStart = $('#availability-widget-search .control-mobile-in > input').val();
                        }
                        
                        if (typeof strDateEnd === 'undefined'){
                            strDateEnd = $('#availability-widget-search .control-mobile-out > input').val();
                        }
                    } else {
                        strDateStart = $('#availability-widget .control-check-in > input').val();
                        strDateEnd = $('#availability-widget .control-check-out > input').val();
                        
                        if (typeof strDateStart === 'undefined'){
                            strDateStart = $('#availability-widget-search .control-check-in > input').val();
                        }
                        
                        if (typeof strDateEnd === 'undefined'){
                            strDateEnd = $('#availability-widget-search .control-check-out > input').val();
                        }
                    }
                    
                    var adult = $('input#guestsPickerAdultsValueWidget');
                    var children = $('input#guestsPickerChildrenValueWidget');
					// promo code entered by the user in the availablility widget
					var promo_code = $('input#txtPromoCodeInput').val();
					var hasPromo;
                    var child_age_url = '';
                    // if there are more than 0 children then get the age for
                    // each of them
                    if ($(children).val() > 0) {
                        var num_children = $(children).val();
                        var children_obj = {};
                        for (var count = 0; count < num_children; count++) {
                            // get the age of this child
                            // where the ages are store in the DOM depends on the screen width
                            if ($(window).width() < 769) {    // mobile
                                // search in the lightbox for avail widget
                                var age_value = $('#lightboxChildAgeMenuWidget span#ageForChildMobile' + count);
                            } else {  // desktop
                                // search in the avail widget
                                var age_value = $('#availability-widget li#ageForChild' + count)
                                        .find('span.spinnerValue');
                            }
                            var age = parseInt(age_value.text());
                            children_obj[count] = age;
                        }
                        child_age_url = '&ages=' + encodeURIComponent(JSON.stringify(children_obj));
                    }

                    if (strDateStart != null && strDateStart.replace(/\s+/i, '').length > 0 && strDateEnd != null && strDateEnd.replace(/\s+/i, '').length > 0) {
                        dateRange = '&start=' + strDateStart + '&end=' + strDateEnd;
                        hasDates = true;
                    }

                    // we will add the promo param in the url ONLY if something
                    // was entered, otherwise do the same seach as before
                    if (typeof promo_code === 'undefined' || promo_code == "") {
                        hasPromo = false; 
                    } else {
                        hasPromo = true; 
                    }

                    var parts = window.location.pathname.split("/");
                    var location = 'http://' + window.location.hostname + window.location.pathname +'?search=1&q=' + encodeURIComponent($searchInput.val()) + '&adults=' + $(adult).val() + '&children=' + $(children).val() + child_age_url + (hasPromo? '&promo=' + promo_code: '');

                    // if search made from NOT room and NOT deals pages
                    if ((parts[parts.length-1] != "rooms" && parts[parts.length-1] != "deals")&&(parts[parts.length-2]!="deals")){
                        location = 'http://' + window.location.hostname + window.location.pathname +'/rooms?search=1&q=' + encodeURIComponent($searchInput.val()) + '&adults=' + $(adult).val() + '&children=' + $(children).val()  + child_age_url + (hasPromo? '&promo=' + promo_code: '');
                    }
                    window.location = hasDates ? location + dateRange : location;

                if (typeof dh.state.ajaxSearchBar !== 'undefined') {

                    dh.state.ajaxSearchBar.abort();
                }
            };

            $pdpSearchButton.click(searchFunction);
            $('#SearchForm').submit(searchFunction);
        },
        // Utility Functions
        utilities: {
            isEmpty: function(obj) {
                var blEmpty = true;

                if (typeof obj !== 'undefined' && obj != null) {
                    if (typeof obj === 'string') {
                        blEmpty = obj.replace(/\s+/ig, '').length == 0;
                    } else if (Object.prototype.toString.call(obj) === '[object Array]') {
                        blEmpty = obj.length == 0;
                    } else {
                        blEmpty = false;
                    }
                }

                return blEmpty;
            },

            _months: {
                "January" : "01",
                "February" : "02",
                "March" : "03",
                "April" : "04",
                "May" : "05",
                "June" : "06",
                "July" : "07",
                "August" : "08",
                "September" : "09",
                "October" : "10",
                "November" : "11",
                "December" : "12"
            },

            getMonthObject: function(month) {
                return dh.utilities._months[month];
            },
            _months_number: {
                "01" : "January",
                "02" : "February",
                "03" : "March",
                "04" : "April",
                "05" : "May",
                "06" : "June",
                "07" : "July",
                "08" : "August",
                "09" : "September",
                "10" : "October",
                "11" : "November",
                "12" : "December"
            },
            getMonthName: function(month) {
                return dh.utilities._months_number[month];
            }
        },
        
        pdpSeparatorLogic: function() {
            if (!$('#pdpTabs').length) {
                return;
            }
            
            var tabs = $('#pdpTabs ul.slides li:not(.tabDivider)');
            
            $(window).on('to-mobile', function() {
                tabs.css('padding-left', '');
            });
            
            $(window).on('to-desktop', function() {
                setTimeout(updateSeparatorVisibility, 10);
            });
            
            var updateSeparatorVisibility = function() {
                if (ScreenSizeIsMobile) {
                    return;
                }
                
                var lastSelectedOrHover = false;
                
                for (var i=0; i<tabs.length; i++) {
                    var tab = $(tabs[i]);
                    var separator = tab.find('.tabDivider');
                    
                    var selectedOrHover = (tab.hasClass('selected') || tab.hasClass('hover'));
                    
                    if (separator.length) {
                        if (selectedOrHover || lastSelectedOrHover) {
                            separator.hide();
                            tab.css('padding-left', '1px');
                        } else {
                            separator.show();
                            tab.css('padding-left', '0px');
                        }
                    }
                    
                    lastSelectedOrHover = selectedOrHover;
                }
            }
            
            tabs.mouseover(function() {
                $(this).addClass('hover');
                
                setTimeout(updateSeparatorVisibility, 10);
            });
            
            tabs.mouseout(function() {
                $(this).removeClass('hover');
                
                setTimeout(updateSeparatorVisibility, 10);
            });
            
            updateSeparatorVisibility();
        },

        adjustBlogSize: function() {
            var remote = "https://blog.designhotels.me";
            // var remote = "http://dh.dev.livecms.biz";
            // var remote = "http://designhotels.kumar.dev.webcanada.com";

            var windowResized = function() {
                var handleSizingResponse = function(e) {
                    if(e.origin == remote) {
                        var action = e.data.split(':')[0];
                        if(action == 'sizing') {                 
                            // remote safari consistantly returns 150 less than desired
                            var adj = (isSafari || isMobile) ? 150 : 0;
                            adj += tabBreak ? 100 : 0;
                            var size = (e.data.split(':')[1]).split(',');
                            var adj_size = (parseInt(size[0]) + adj).toString();
                            $("#blogWrapper").css("height", adj_size + "px" ); 
                            // after resizing scroll to the top
                            window.scrollTo(0, 0);                         
                        }
                    }
                };
                window.addEventListener('message', handleSizingResponse, false);
                var blog = document.getElementById('iblog');
                blog.onload = function() {
                    window.addEventListener('message', handleSizingResponse, false);
                    blog.contentWindow.postMessage('sizing?', remote);
                };
            };

            windowResized();

            //check for safari 
            var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
            var isMobile = (window.innerWidth <= 768) ? true : false;
            var tabBreak = (window.innerWidth <= 768 && window.innerWidth >= 690) ? true : false;
            
            /*since moible browsers fire resize event repeatedly, 
            iframe src keeps resetting to original src.
            So we need to check if the window is 'actually' resized
            and only then reset the iframe */

            var cachedWidth = $(window).width();
            $(window).resize(function(){
                console.log("event fired");
                var newWidth = $(window).width();
                if(newWidth !== cachedWidth){
                    //resize here
                    cachedWidth = newWidth;

                    var blog = document.getElementById('iblog');
                    var blogPage = blog.getAttribute('data-ref');
                    blog.src = remote + '/' + blogPage;
                }
            });
        },

        appendBlogUrl: function() {
            var remote = "https://blog.designhotels.me";
            // var remote = "http://dh.dev.livecms.biz";
            // var remote = "http://designhotels.kumar.dev.webcanada.com";

            // Save current url to data attribute for src when resizing page
            var blog = document.getElementById('iblog');
            blog.setAttribute('data-ref', blog.src.replace(remote + '/', ''));

            var listenMessage = function(mssg) {
                if (mssg.origin == remote) {
                    var action = mssg.data.split(':')[0];
                    // console.log("action is " + action);
                    // filter for proper action
                    if (action == 'updateUrl') {
                        // string without 'updateUrl:'
                        var href = mssg.data.split(/:(.+)/)[1];
                        // string after domain of the href
                        var params = href.split("/").slice(3).join("/");
                        // console.log("the href is "+ href);
                        // console.log("the params are "+ params);
                        // append params to the browser url.
                        // window.history.replaceState(null, null, window.location.href+'/'+params);

                        // Save current url to data attribute for src when resizing page
                        blog.setAttribute('data-ref', params);

                        window.history.pushState({}, null , '/mycommunity/blog/' + params);
                    }
                }

            };
            if (window.addEventListener) {
                window.addEventListener("message", listenMessage, false);
            } else {
                window.attachEvent("onmessage", listenMessage);
            }
        },

        /**
         * this function initializes the filter bar for OE overivew page
         */
        initOEFilter: function() {
            // console.log(window.stories);
            // console.log(themes_order);
            var stories_list = window.stories;

            // set up sticky filter bar
                // first get the position of the bar -mobile only
            // 52 is the height of the bar
            // subtracting  52 to begin sticky as soon as bar touches the header
            var mobile_header_height = 52;
            var y_coord = $('section#oe_tabs').offset().top - mobile_header_height;
            // set up scroll event
            $(window).scroll(function () {
                if ($(window).scrollTop() > y_coord) {
                    $('section#oe_tabs').addClass('filter-fixed');
                }
                if ($(window).scrollTop() < y_coord + 1) {
                    $('section#oe_tabs').removeClass('filter-fixed');
                }
            });

            // set up the click events for filter links
            $('section#oe_tabs div li a').on('click', function (event) {
                event.preventDefault();
                // get the theme txt that user selected
                var selected_theme = $(this).text();
                // remove the current li with selected class
                $('section#oe_tabs div li.selected').removeClass('selected');
                // apply selected class to this filter item
                var $parentLi = $(this).closest('li');
                $parentLi.addClass("selected");
                // update the page with proper stories by filter
                shuffleStories(selected_theme);
            });

            // update the stories container  with appropriate themed stories
            var shuffleStories = function(theme) {
                var stories = $(stories_list).children('li:not(.empty-block)');
                if (theme.toUpperCase() === "ALL" ) {
                    // show all
                    stories.each(function(index, li) {
                        $(li).show();
                    });
                } else {
                    // show stories that have this  theme
                    stories.each(function(index, li) {
                        var story_themes = $(li).attr('data-themes').split(",");
                        for (var i = 0; i < story_themes.length; i++) {
                            if (story_themes[i].toUpperCase()
                                === theme.toUpperCase()) {
                                $(li).show();
                                break;
                            } else {
                                $(li).hide();
                            }
                        }
                    });
                }
                // make the signup block occupy remaining empty story spaces/blocks when switching themes
                var totalVisibleStories = $(stories_list).children('li:visible:not(.empty-block)').length;
                // the signup block appears when the total stories are not a
                // multiple of 6. i.e. 1, 2, 4, 5, 7
                var min_multiple = 6;
                var signupBlockParts = parseInt(totalVisibleStories) % min_multiple;
                if (signupBlockParts !== 0) {
                    $('.empty-block').attr('class', $('.empty-block').get(0).className.replace(/\part-\S+/g, 'part-' + signupBlockParts));
                }
            };
        },
        // this is a fallback incase a new page doesnt have the css to 
        // fix the footer at the bottom
        fixFooterBottom: function() {
            if (dh.el.body.attr('id') != 'accountJoin') {
                $(window).load(function () {
                    function autoHeight() {
                        $('div#wrapper, div#pdpOverview-wrapper').css('min-height',
                            $(window).outerHeight(true)
                                - $('footer#mainFooter').outerHeight(true)
                        );
                    }
                    autoHeight();
                    $(window).resize(autoHeight);
                });
            }
        }

    };

    dh.init();
});

// PLUGINS
// Spinster PROFESSIONAL
(function($) {
    $.fn.spinsterPro = function(options) {
        return this.each(function(i, el) {
            var $that = $(el);
            var settings = $.extend({
                    'max'     : undefined,
                    'min'     : undefined,
                    'start'   : 0,
                    'target'  : undefined,
                    'onChange': function(value) {}
                }, options);

            if (settings.start == 'value') {
                settings.start = $that.val();
            }

            var html = '<span class="spinnerValue">' + settings.start + '</span><a class="spinnerIncrease" href="javascript:void(0)">&uarr;</a><a class="spinnerDecrease" href="javascript:void(0)">&darr;</a></div>';
            var container = (settings.target != undefined) ? settings.target : $that.parent();

            if (settings.target != undefined) {
                settings.target.html(html);
            } else {
                $that.after(html);
            }
            
            $('a.spinnerIncrease', container).click(function() {
                setValue($(this), 'increase');
            });

            $('a.spinnerDecrease', container).click(function() {
                setValue($(this));
            });

            
            function setValue($el, direction) {
                var $spinnerValue = $el.parent().find('span.spinnerValue');
                var value = parseInt($spinnerValue.text());

                if (direction == 'increase' && (settings.max == undefined || settings.max != undefined && value < settings.max)) {
                    value++;
                    console.log($el.parent().parent().attr('id'));
                    // if the increse was cliked for children
                    // add childage picker
                    if (
                        $el.parent().parent().attr('id') == "guestsPickerChildrenWidget" ||
                        $el.parent().parent().attr('id') == "guestsPickerChildren" 
                    ) {
                        addChildAgeLi($el.parent().parent());
                    }
                } else if (direction != 'increase' && (settings.min == undefined || settings.min != undefined && value > settings.min)) {
                    value--;
                    console.log($el.parent().parent().attr('id'));
                    if (
                        $el.parent().parent().attr('id') == "guestsPickerChildrenWidget" ||
                        $el.parent().parent().attr('id') == "guestsPickerChildren" 
                    ) {
                        removeChildAgeLi($el.parent().parent());
                    }
                }

                $spinnerValue.text(value);

                $that.val(value);

                $.isFunction(settings.onChange) && settings.onChange.call(this, value);
            }
            
            function setChildValue($el, direction) {
                var $spinnerValue = $el.parent().find('span.spinnerValue');
                var value = parseInt($spinnerValue.text());

                if (direction == 'increase' && (settings.max == undefined || settings.max != undefined && value < 17)) {
                    value++;
                } else if (direction != 'increase' && (settings.min == undefined || settings.min != undefined && value > settings.min)) {
                    value--;
                }

                $spinnerValue.text(value);
            }
            // add a LI with picker to the ul to the end
            function addChildAgeLi($guestList) {
                // var $guestList = $('li#guestsPickerChildrenWidget');
                var value = parseInt($guestList.find("span.spinnerValue").text());

                var child_html = '<li id="ageForChild' + value + '">' + 
                                 '    <span class="label_child"> Child\'s age </span>' +
                                 '    <span class="label_child_hint"> (at check-out date) </span>' +
                                 '    <div class="spinner">' + 
                                 '        <span class="spinnerValue">' + 0 + '</span>' + 
                                 '        <a class="spinnerIncrease" href="javascript:void(0)">&uarr;</a> ' + 
                                 '        <a class="spinnerDecrease" href="javascript:void(0)">&darr;</a> ' + 
                                 '    </div>' + 
                                 '</li>';
                // add to the ul
                $guestList.parent().append(child_html);
                // add click events to the age increase and decrease buttons
                $guestList.siblings("li#ageForChild" + value).find("a.spinnerIncrease").click(function() {
                  setChildValue($(this), 'increase');
                });

                $guestList.siblings("li#ageForChild" + value).find("a.spinnerDecrease").click(function() {
                  setChildValue($(this));
                });
            }

            // remove  a LI with picker from the end of the ul
            function removeChildAgeLi($guestList) {
                // var $guestList = $('li#guestsPickerChildrenWidget');
                var children = $guestList.siblings("li[id^='ageForChild']");
                children.last().remove();
            }
        });
    }
})(jQuery);



// Source: http://tutorialzine.com/2011/02/converting-jquery-code-plugin/
// Altered for jQuery 1.7.1 compatibility
(function($) {
    $.fn.tzSelect = function(options) {
        options = $.extend({
            render : function(option) {
                if (typeof options.renderItem == 'function') {
                    return options.renderItem.call(this, option);
                } else {
                    return $('<li>', {
                        html : option.text()
                    });
                }
            },
            'class' : ''
        }, options);

        return this.each(function() {
            var select = $(this);
            
            if(options['selectboxClass']=='bookingTitle_selectbox'){
                var selectBoxContainer = $('<div>', {
                    'class': 'tzSelect '+options['selectboxClass'],
                    html: '<div class="selectBox"><span class="text"></span><span class="icon"></span></div>'
                });
            }else{
                var selectBoxContainer = $('<div>', {
                    width: select.outerWidth(true),
                    'class': 'tzSelect '+options['selectboxClass'],
                    html: '<div class="selectBox"><span class="text"></span><span class="icon"></span></div>'
                });
            }            
            
            var dropDown = $('<ul>', {'class': 'dropDown'});
            var selectBox = selectBoxContainer.find('div.selectBox');
            var selectBoxText = selectBox.find('span.text');

            if (options.dropdownClass) {
                dropDown.addClass(options.dropdownClass);
            }

            if (options.selectboxClass) {
                selectBox.addClass(options.selectboxClass);
            }

            var first = true;

            select.find('option').each(function(i) {
                var option = $(this);

                if (first) {
                    first = false;

                    selectBoxText.html(option.text());
                }

                if (option.attr('selected') == 'selected') {
                    selectBoxText.html(option.text());
                }

                if (option.data('skip')) {
                    return true;
                }

                var li = options.render(option);

                li.click(function() {

                    selectBoxText.html(option.text());
                    dropDown.trigger('hide');

                    select.val(option.val());

                    if (typeof options.onChange == 'function') {
                        options.onChange.call(this, select);
                    }

                    return false;
                });

                dropDown.append(li);
            });

            selectBoxContainer.append(dropDown.hide());
            select.hide().after(selectBoxContainer);

            /*$("div.tzSelect ul.dropDown").mCustomScrollbar({
                horizontalScroll: false,
                scrollButtons: {
                    enable: true
                },
                theme: "light-thick"
            });*/

            dropDown.bind('show', function() {

                if (dropDown.is(':animated')) {
                    return false;
                }

                selectBox.addClass('expanded');
                $('ul.dropDown').slideUp();
                dropDown.slideDown();

            }).bind('hide', function() {
                if (dropDown.is(':animated')) {
                    return false;
                }

                selectBox.removeClass('expanded');
                dropDown.slideUp();

            }).bind('toggle', function() {
                if (selectBox.hasClass('expanded')) {
                    dropDown.trigger('hide');
                } else {
                    dropDown.trigger('show');
                }
            });

            selectBox.click(function(event) {
                dropDown.trigger('toggle');
                event.stopPropagation();

                return false;
            });

            $(document).click(function() {
                dropDown.trigger('hide');
            });

        });
    }
})(jQuery);



/*!
* FitVids 1.0
*
* Copyright 2013, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
* Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
* Released under the WTFPL license - http://sam.zoy.org/wtfpl/
*
* Date: Thu Sept 01 18:00:00 2011 -0500
*/
(function( $ ){

  "use strict";

  $.fn.fitVids = function( options ) {
    var settings = {
      customSelector: null
    };

    if(!document.getElementById('fit-vids-style')) {

      var div = document.createElement('div'),
          ref = document.getElementsByTagName('base')[0] || document.getElementsByTagName('script')[0],
          cssStyles = '&shy;<style>.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>';

      div.className = 'fit-vids-style';
      div.id = 'fit-vids-style';
      div.style.display = 'none';
      div.innerHTML = cssStyles;

      ref.parentNode.insertBefore(div,ref);

    }

    if ( options ) {
      $.extend( settings, options );
    }

    return this.each(function(){
      var selectors = [
        "iframe[src*='player.vimeo.com']",
        "iframe[src*='youtube.com']",
        "iframe[src*='youtube-nocookie.com']",
        "iframe[src*='kickstarter.com'][src*='video.html']",
        "object",
        "embed"
      ];

      if (settings.customSelector) {
        selectors.push(settings.customSelector);
      }

      var $allVideos = $(this).find(selectors.join(','));
      $allVideos = $allVideos.not("object object"); // SwfObj conflict patch

      $allVideos.each(function(){
        var $this = $(this);
        if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
        var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
            width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
            aspectRatio = height / width;
        if(!$this.attr('id')){
          var videoID = 'fitvid' + Math.floor(Math.random()*999999);
          $this.attr('id', videoID);
        }
        $this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+"%");
        $this.removeAttr('height').removeAttr('width');
      });
    });
  };
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );

//Temporary
function callLogin() {
    $('#callLogin').trigger('click');
}


// Imager.js
(function (window, document) {
    'use strict';

    var $, Imager;

    window.requestAnimationFrame =
    window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    function (callback) {
        window.setTimeout(callback, 1000 / 60);
    };

    $ = (function (dollar) {
        if (dollar) {
            return dollar;
        }

        return function (selector) {
            return Array.prototype.slice.call(document.querySelectorAll(selector));
        };
    }(window.$));

    /*
        Construct a new Imager instance, passing an optional configuration object.

        Example usage:

            {
                // Available widths for your images
                availableWidths: [Number]

                // Selector to be used to locate your div placeholders
                selector: '',

                // Class name to give your resizable images.
                className: '',

                // Regular expression to match against your image endpoint's naming conventions
                // e.g. http://yourserver.com/image/horse/400
                regex: RegExp

                // Toggle the lazy load functionality on or off
                lazyload: Boolean

                // Used alongside the lazyload feature (helps performance by setting a higher delay)
                scrollDelay: Number
            }

        @param {object} configuration settings
        @return {object} instance of Imager
     */
    window.Imager = Imager = function (opts) {
        var self = this;
            opts = opts || {};

        this.imagesOffScreen = [];
        this.viewportHeight  = document.documentElement.clientHeight;
        this.availableWidths = opts.availableWidths || {
            'landscape': [
                // ['126','73'],
                // ['168','98'],
                // ['166','97'],
                // ['240','140'],
                // ['241','141'],
                // ['251','147'],
                // ['271','159'],
                ['320','188'],
                //['748','439'],
                // ['335','197'],
                ['460','270'],
                ['480','280'],
                // ['482','283'],
                // ['518','304'],
                // ['534','314'],
                // ['542','318'],
                // ['571','335'],
                ['640','376'],
                ['920','540'],
                ['1020','600'],
                // ['1036','608'],
                // ['1068','628'],
                ['1090','640'],
                // ['1141','670'],
                ['2040','1200'],
                ['2180','1280'],
                ['5760','3382']
            ],
            'square': [
                // ['46','46'],
                // ['63','63'],
                // ['74','74'],
                // ['80','80'],
                // ['88','88'],
                // ['92','92'],
                // ['121','121'],
                // ['127','127'],
                // ['142','142'],
                // ['149','149'],
                ['152','152'],
                ['159','159'],
                ['176','176'],
                ['242','242'],
                ['284','284'],
                ['304','304'],
                ['320','320']
                // ['3600','3600']
            ],
            'block-1090x1280': [
                ['285','335'],
                ['545','640'],
                ['571','670'],
                ['1090','1280']
            ],
            'block-1090x640': [
                ['285','168'],
                ['545','320'],
                ['571','335'],
                ['1090','640']
            ],
            'block-546x640': [
                ['143','168'],
                ['273','320'],
                ['286','335'],
                ['546','640']
            ],
            'block-544x640': [
                ['142','168'],
                ['272','320'],
                ['285','335'],
                ['544','640']
            ],
            'block-2180x1280': [
                ['571','335'],
                ['1090','640'],
                ['1141','670'],
                ['2180','1280']
            ],
            'block-1280x1280': [
                ['335','335'],
                ['640','640'],
                ['670','670'],
                ['1280','1280']
            ],
            'block-450x450': [
                ['118','118'],
                ['225','225'],
                ['236','236'],
                ['450','450']
            ],
            'block-225x225': [
                ['225','225']
            ],
            'room': [
                ['300','225'],
                ['400','300'],
                ['600','450'],
                ['800','600']
            ],
            'mbo-banner': [
                ['900','500'],
            ],
            'mbo-image-1': [
                ['598','400']
            ],
            'mbo-image-2': [
                ['296','400']
            ],
            'mbo-image-group-1': [
                ['296','197']
            ],
            'mbo-image-group-2': [
                ['447','600']
            ],
            'mbo-image-group-3': [
                ['296','400']
            ],
            'campaign-banner': [
                ['735','556']
            ],
            'block-1600x640' : [
                ['1600', '640']
            ],
            'block-750x750': [
                ['750','750']
            ],
        };
        this.selector        = opts.selector || '.delayed-image-load';
        this.className       = '.' + (opts.className || 'image-replace').replace(/^\.+/, '.');
        this.regex           = opts.regex || /(^.*\.rackcdn\.com\/)(.*)(\/.*$)/i;
        this.gif             = document.createElement('img');
        this.gif.src         = 'data:image/gif;base64,R0lGODlhEAAJAIAAAP///wAAACH5BAEAAAAALAAAAAAQAAkAAAIKhI+py+0Po5yUFQA7';
        this.gif.className   = this.className.replace(/^[#.]/, '');
        this.divs            = $(this.selector);
        this.cache           = {};
        this.scrollDelay     = opts.scrollDelay || 250;
        this.lazyload        = opts.lazyload || false;
        this.changeDivsToEmptyImages();

        window.requestAnimationFrame(function() {
            self.init();
        });

        if (this.lazyload) {
            this.interval = window.setInterval(this.scrollCheck.bind(this), this.scrollDelay);
        }
    };

    Imager.prototype.scrollCheck = function() {
        if (this.scrolled) {
            if (!this.imagesOffScreen.length) {
                window.clearInterval(this.interval);
            }
            this.divs = this.imagesOffScreen.slice(0); // copy by value, don't copy by reference
            this.imagesOffScreen.length = 0;
            this.changeDivsToEmptyImages();
            this.scrolled = false;
        }
    };

    Imager.prototype.init = function() {
        var self = this;

        this.initialized = true;
        this.scrolled = false;
        this.checkImagesNeedReplacing();

        $( window ).resize(function() {
            self.checkImagesNeedReplacing();
        }, false);

        if (this.lazyload) {
            window.addEventListener('scroll', function() {
                this.scrolled = true;
            }.bind(this), false);
        }
    };

    Imager.prototype.createGif = function (element) {
        var gif = this.gif.cloneNode(false);

        gif.width = element.getAttribute('data-width');
        // if ($(window).width() <= MobileScreenSize && gif.width==320){
            // gif.width = 920;
        // }
        gif.setAttribute('data-src', element.getAttribute('data-src'));
        gif.setAttribute('data-imagetype', element.getAttribute('data-imagetype'));
        gif.setAttribute('alt', element.getAttribute('alt'));
        gif.setAttribute('title', element.getAttribute('title'));
        gif.setAttribute('id', element.getAttribute('data-id'));

        $(gif).addClass(element.getAttribute('data-class'));

        element.parentNode.replaceChild(gif, element);
    }

    Imager.prototype.changeDivsToEmptyImages = function() {
        var divs = this.divs,
            i = divs.length,
            element;

        while (i--) {
            element = divs[i];

            if (this.lazyload) {
                if (this.isThisElementOnScreen(element)) {
                    this.createGif(element);
                } else {
                    this.imagesOffScreen.push(element);
                }
            } else {
                this.createGif(element);
            }
        }

        if (this.initialized) {
            this.checkImagesNeedReplacing();
        }
    };

    Imager.prototype.isThisElementOnScreen = function (element) {
        // document.body.scrollTop was working in Chrome but didn't work on Firefox, so had to resort to window.pageYOffset
        // but can't fallback to document.body.scrollTop as that doesn't work in IE with a doctype (?) so have to use document.documentElement.scrollTop
        var offset = ('pageYOffset' in window) ? window.pageYOffset : document.documentElement.scrollTop;

        return (element.offsetTop < (this.viewportHeight + offset)) ? true : false;
    };

    Imager.prototype.checkImagesNeedReplacing = function() {
        var self = this,
            images = $(this.className),
            i = images.length,
            currentImage;

        if (!this.isResizing) {
            this.isResizing = true;

            while (i--) {
                currentImage = images[i];
                this.replaceImagesBasedOnScreenDimensions(currentImage);
            }

            this.isResizing = false;
        }
    };

    Imager.prototype.replaceImagesBasedOnScreenDimensions = function (image) {
        var src = this.determineAppropriateResolution(image),
            parent = image.parentNode,
            replacedImage;

        if (this.cache[src]) {
            replacedImage = this.cache[src].cloneNode(false);
            replacedImage.width = image.getAttribute('width');

        } else {
            replacedImage = image.cloneNode(false);
            replacedImage.src = src;
            this.cache[src] = replacedImage;
        }
        parent.replaceChild(replacedImage, image);
    };

    Imager.prototype.determineAppropriateResolution = function (image) {
        //only for mobile ste with screen size from 480 to 768. All images that have size 320x188 are downloaded with size 920x540
        if (window.document.documentElement.clientWidth <= MobileScreenSize){

            if(window.document.documentElement.clientWidth > 480 && this.availableWidths[image.getAttribute('data-imagetype')][0][0] == 320){

                this.availableWidths[image.getAttribute('data-imagetype')][0][0] = 920;
                this.availableWidths[image.getAttribute('data-imagetype')][0][1] = 540;
            }

            if(window.document.documentElement.clientWidth > 480 && this.availableWidths[image.getAttribute('data-imagetype')][0][0] == 300){

                this.availableWidths[image.getAttribute('data-imagetype')][0][0] = 600;
                this.availableWidths[image.getAttribute('data-imagetype')][0][1] = 450;
            }

        }

        var src            = image.getAttribute('data-src'),
            imagetype      = image.getAttribute('data-imagetype'),
            imagewidth     = image.clientWidth,
            selectedWidth  = this.availableWidths[imagetype][0][0],
            selectedHeight = this.availableWidths[imagetype][0][1],
            i              = this.availableWidths[imagetype].length;

        while (i--) {
            if (imagewidth <= this.availableWidths[imagetype][i][0]) {
                selectedWidth = this.availableWidths[imagetype][i][0];
                selectedHeight = this.availableWidths[imagetype][i][1];
            }
        }
        return this.changeImageSrcToUseNewImageDimensions(src, + selectedWidth + 'x' + selectedHeight );
    };

    Imager.prototype.changeImageSrcToUseNewImageDimensions = function (src, dimensions) {
        return src.replace(this.regex, "$1" + dimensions + "$3");
    };


}(window, document));


var DhBookingCalendar = function(
    version,
    parent,
    triggerCheckIn,
    triggerCheckOut,
    checkInLabel,
    checkInInput,
    checkOutLabel,
    checkOutInput
) {
    this.version = version;
    this.parent = parent;
    this.triggerCheckIn = triggerCheckIn;
    this.triggerCheckOut = triggerCheckOut;
    this.isOpen = false;
    this.checkStatus = 'in';
    this.checkInLabel = checkInLabel;
    this.checkInInput = checkInInput;
    this.checkOutLabel = checkOutLabel;
    this.checkOutInput = checkOutInput;
    
    this.checkInDate = false;
    this.checkOutDate = false;
    this.firstDateMonth = new Date();
    this.dataUnavailable = {};
	this.dataNoArrival ={};
    this.dataUnavailableLoading = {};
    
    this.debug = true;
};

DhBookingCalendar.prototype.init = function() {
    var self = this;
    var date = new Date();
    
    if (this.checkInInput.val() && this.checkOutInput.val()) {
        this.checkInDate = this.valueToDate(this.checkInInput.val());
        this.checkOutDate = this.valueToDate(this.checkOutInput.val());
    }
    
    var hidePickerCode = function() {
        self.isOpen = false;
        self.parent.find('.calendar-tail').hide();
        self.parent.find('.dividerCal').hide();
        self.parent.find('div.datePickerLegend').hide();
        self.parent.find('div.datePickerClose').hide();
        self.parent.find('div.datePickerArrow').hide();
        self.parent.find('div.datePickerClearAll').hide();
        self.parent.find('a.datePickerPrevMonth').hide();
        self.parent.find('a.datePickerNextMonth').hide();
        
        //self.checkInLabel.css('font-family', 'DINWebPro');
        //self.checkOutLabel.css('font-family', 'DINWebPro');
    }
    
    var showPickerCode = function() {
        self.isOpen = true;
        self.parent.find('.calendar-tail').show();
        self.parent.find('.dividerCal').show();
        self.parent.find('div.datePickerLegend').show();
        self.parent.find('div.datePickerClose').show();
        self.parent.find('div.datePickerArrow').show();
        self.parent.find('div.datePickerClearAll').show();
        self.parent.find('a.datePickerPrevMonth').show();
        self.parent.find('a.datePickerNextMonth').show();
    }
    
    var widgetOptions = {
        flat: true,
        prev: '',
        next: '',
        date: date,
        format: 'b d, Y',
        starts: 1,
        calendars: 2,
        mode: 'single',
        view: 'days',
        autoHide: true,
        
        // This happens before onFill
        onMonthChange: function(firstDate) {
            self.firstDateMonth = new Date(firstDate.getTime());
        },
        
        // This happens when the calendar is done updating HTML after month
        // changes (not after initially showing)
        onFill: function() {
            if (self.isOpen) {
                self.showAvailability();
            }
        },
        
        onShow: function() {
            showPickerCode();
            self.showAvailability();
        },
        
        onHide: function() {
            hidePickerCode();
        },
        
        // A date has been picked
        onChange: function(formated, date) {
            self.parent.DatePickerHide();
            hidePickerCode();
            self.datePicked(formated, date);
        }
    };
    
    this.parent.DatePicker(widgetOptions);
    
    this.triggerCheckIn.mousedown(function(ev) {
        ev.stopPropagation();
        ev.preventDefault();
        if (!ScreenSizeIsMobile/* && !self.isOpen*/) {
            self.openCheckIn();
            
            if (self.isOpen) {
                self.showAvailability();
            }
        }
    });
    
    this.triggerCheckOut.mousedown(function(ev) {
        ev.stopPropagation();
        ev.preventDefault();
        if (!ScreenSizeIsMobile/* && !self.isOpen*/) {
            if (self.checkInDate) {
                self.openCheckOut();
            } else {
                self.openCheckIn();
            }
            
            if (self.isOpen) {
                self.showAvailability();
            }
        }
    });
    
    this.parent.find('a.datePickerPrevMonth').mousedown(function(ev) {
        ev.stopPropagation();
        self.parent.find('th.datepickerGoPrev').eq(0).trigger('click');
    });

    this.parent.find('a.datePickerNextMonth').mousedown(function(ev) {
        ev.stopPropagation();
        self.parent.find('th.datepickerGoNext').eq(0).trigger('click');
    });
    
    this.parent.find('div.datePickerClearAll').mousedown(function(ev) {
        ev.stopPropagation();
        
        self.checkInLabel.text('Check In');
        self.checkOutLabel.text('Check Out');
        
        self.checkInInput.val('');
        self.checkOutInput.val('');
        
        self.parent.DatePickerClear();
        self.checkInDate = false;
        self.checkOutDate = false;
        
        self.parent.DatePickerHide();
        hidePickerCode();
    });
    // using custom calendar for pdp deals tab (page with multiple deals)
    if (
        this.version != 'searchbar' && 
        this.version != 'searchbar2' && 
        !$('body').hasClass("dealsPage")
    ) {
        this.availability();
    }
};

DhBookingCalendar.prototype.availability = function() {
    var now = new Date();
    this.getAvailabilityForMonth(now);
    now.setMonth(now.getMonth( ) + 1);
    this.getAvailabilityForMonth(now);
};

DhBookingCalendar.prototype.getAvailabilityForMonth = function(date) {
    var self = this;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var yearMonth = year + '_' + ('0' + month).slice(-2);
    
    if (
        typeof(this.dataUnavailable[yearMonth]) != 'undefined'
        || typeof(this.dataUnavailableLoading[yearMonth]) != 'undefined'
    ) {
        return;
    }
    
    this.dataUnavailableLoading[yearMonth] = true;
    
    var startDate = year + '/' + ('0' + month).slice(-2) + '/01';
    var endDate = year + '/' + ('0' + month).slice(-2) + '/' + daysInMonth(month, year);
    
    AvailabilityService.get(date, function(data) {
        //console.log(data);
        self.dataUnavailable[yearMonth] = data.unavailable;
		self.dataNoArrival[yearMonth] = data.dataNoArrival;
		//console.log(unavailable);
		//console.log(dataNoArrival);
        var currentYearMonth = self.getCurrentYearMonth();
        
        if (currentYearMonth[0] == yearMonth) {
            self.redraw(0);
        }
        
        if (currentYearMonth[1] == yearMonth) {
            self.redraw(1);
        }
    }, true);
};

DhBookingCalendar.prototype.openCheckIn = function() {
    this.checkStatus = 'in';
    this.parent.DatePickerShow();
    
    if (this.version != 'searchbar2') {
        //this.checkInLabel.css('font-family', 'DINWebProMedium');
        //this.checkOutLabel.css('font-family', 'DINWebPro');
    }
    
    if (this.version == 'widget') {
        this.parent.css('top', '0px');
    } else if (this.version == 'search') {
        this.parent.css('right', '0px');
    } else if (this.version == 'searchbar') {
        if ($(window).width() > 1264) {
            this.parent.css('right', '230px');
        } else if ($(window).width() > 1150) { 
            this.parent.css('right', '28%');
        } else {
            this.parent.css('right', '31%');
        }
    } else if (this.version == 'searchbar2') {
        this.parent.css('right', '232px');
    }
};

DhBookingCalendar.prototype.openCheckOut = function() {
    this.checkStatus = 'out';
    this.parent.DatePickerShow();
    
    if (this.version != 'searchbar2') {
        //this.checkInLabel.css('font-family', 'DINWebPro');
        //this.checkOutLabel.css('font-family', 'DINWebProMedium');
    }
    
    if (this.version == 'widget') {
        this.parent.css('top', '50px');
    } else if (this.version == 'search') {
        this.parent.css('right', '-70px');
    } else if (this.version == 'searchbar') {
        if ($(window).width() > 1264) {
            this.parent.css('right', '100px');
        } else if ($(window).width() > 1150) { 
            this.parent.css('right', '16%');
        } else {
            this.parent.css('right', '20%');
        }
    } else if (this.version == 'searchbar2') {
        this.parent.css('right', '92px');
    }
};

DhBookingCalendar.prototype.datePicked = function(formated, date) {
    date.setHours(0, 0, 0, 0);
    
    var label = formated;
    var value = this.dateToValue(date);
    
    if (this.version == 'searchbar' || this.version == 'searchbar2') {
        value = date;
    }
    
    if (this.checkStatus == 'in') {
        this.checkInDate = date;
        
        if (!this.checkOutDate || this._checkoutBeforeCheckin() || this._unavailabilityCheckInOut()) {
            this.checkOutDate = new Date(date.getTime());
            this.checkOutDate.setDate(this.checkOutDate.getDate() + 1);
        }
        
        this.checkInLabel.text(label);
        this.checkInInput.val(value);
        
        var outLabel = this.parent.DatePickerFormat(this.checkOutDate, 'b d, Y');
        var outValue = this.dateToValue(this.checkOutDate);
        
        if (this.version == 'searchbar' || this.version == 'searchbar2') {
            outValue = this.checkOutDate;
        }
        
        this.checkOutLabel.text(outLabel);
        this.checkOutInput.val(outValue);
    } else {
        this.checkOutDate = date;
        this.checkOutLabel.text(label);
        this.checkOutInput.val(value);
    }
};

DhBookingCalendar.prototype._checkoutBeforeCheckin = function() {
    if (!this.checkOutDate) {
        return false;
    }
    
    var checkIn = this.checkInDate.getFullYear() * 10000
        + (this.checkInDate.getMonth() + 1) * 100
        +  this.checkInDate.getDate();
    
    var checkOut = this.checkOutDate.getFullYear() * 10000
        + (this.checkOutDate.getMonth() + 1) * 100
        +  this.checkOutDate.getDate();
    
    return checkOut <= checkIn;
};

DhBookingCalendar.prototype._unavailabilityCheckInOut = function() {
    // using custom calendar for pdp deals tab (page with multiple deals)
    if (
          this.version == 'searchbar' || 
          this.version == 'searchbar2' || 
          $('body').hasClass("dealsPage")
    ) {
        return false;
    }
    
    var startDate = new Date(this.checkInDate.getTime());
    var endDate = new Date(this.checkOutDate.getTime());
    
    var currentDate = new Date(startDate.getTime());
    while (currentDate < endDate) {
        var year = currentDate.getFullYear();
        var month = currentDate.getMonth();
        var date = currentDate.getDate();
        
        var yearMonth = year + '_' + ('0' + (month + 1)).slice(-2);
        var unavailableDays;
        if (typeof(this.dataUnavailable[yearMonth]) != 'undefined' && this.version != 'searchbar' && !$('body').hasClass("dealsPage")) {
            unavailableDays = this.dataUnavailable[yearMonth];
        } else {
            unavailableDays = {};
        }
        
        if (typeof(unavailableDays[date]) != 'undefined') {
            return true;
        }
        
        currentDate = new Date(year, month, date + 1, 0, 0, 0, 0);
    }
    
    return false;
};

DhBookingCalendar.prototype.dateToValue = function(date) {
    return date.getFullYear()
        + '-' + twoDigits(date.getMonth() + 1)
        + '-' + twoDigits(date.getDate());
};

DhBookingCalendar.prototype.showAvailability = function() {
    // using custom calendar for pdp deals tab (page with multiple deals)
    if (
        this.version == 'searchbar' || 
        this.version == 'searchbar2' || 
        $('body').hasClass("dealsPage")
    ) {
        this.redraw(0);
        this.redraw(1);
        return;
    }
    
    var tbodies = this.parent.find('tbody.datepickerDays');
    tbodies.find('td').not('.datepickerNotInMonth').addClass('date-not-loaded');
    
    var month2 = new Date(this.firstDateMonth.getTime());
    month2.setMonth(month2.getMonth() + 1);
    
    var yearMonth = this.getCurrentYearMonth();
    
    if (typeof(this.dataUnavailableLoading[yearMonth[0]]) == 'undefined') {
        // Didn't attempt to load, load
        this.getAvailabilityForMonth(this.firstDateMonth);
    } else if (typeof(this.dataUnavailable[yearMonth[0]]) != 'undefined') {
        // Already loaded, draw
        this.redraw(0);
    }
    
    if (typeof(this.dataUnavailableLoading[yearMonth[1]]) == 'undefined') {
        // Didn't attempt to load, load
        this.getAvailabilityForMonth(month2);
    } else if (typeof(this.dataUnavailable[yearMonth[1]]) != 'undefined') {
        // Already loaded, draw
        this.redraw(1);
    }
    
    // If it's already loading do nothing
};

DhBookingCalendar.prototype.getCurrentYearMonth = function() {
    var month2 = new Date(this.firstDateMonth.getTime());
    month2.setMonth(month2.getMonth() + 1);
    
    var yearMonth1 = this.firstDateMonth.getFullYear() + '_'
        + ('0' + (this.firstDateMonth.getMonth() + 1)).slice(-2);
    
    var yearMonth2 = month2.getFullYear() + '_'
        + ('0' + (month2.getMonth() + 1)).slice(-2);
    
    return [yearMonth1, yearMonth2];
};

DhBookingCalendar.prototype.redraw = function(whichCalendar) {
    if (!this.isOpen) {
        return;
    }
    
    var yearMonth = this.getCurrentYearMonth();
    yearMonth = yearMonth[whichCalendar];
    
    var unavailableDays;
    if (typeof(this.dataUnavailable[yearMonth]) != 'undefined' && this.version != 'searchbar' && this.version != 'searchbar2' && !$('body').hasClass("dealsPage")) {
        unavailableDays = this.dataUnavailable[yearMonth];
    } else {
        // This shouldn't happen, but if it does don't break the application
        unavailableDays = {};
    }
    
	
	/* No Arrival Days */
	var noArrivalDays;
    if (this.dataNoArrival && typeof(this.dataNoArrival[yearMonth]) != 'undefined') {
        noArrivalDays = this.dataNoArrival[yearMonth];
    } else {
        noArrivalDays = {};
    }
	
	
    var tbodies = this.parent.find('tbody.datepickerDays');
    var tbody = $(tbodies[whichCalendar]);
    var cells = tbody.find('td').not('.datepickerNotInMonth');
    cells.removeClass('date-not-loaded');
    
    if (this.checkStatus != 'in') {
        var nextUnavailable = this.findNextUnavailableDate();
    }
    
    for(var i=0; i<cells.length; i++) {
        var currentDate = new Date(2000, 1, 1, 0, 0, 0, 0);
        currentDate.setFullYear(this.firstDateMonth.getFullYear());
        currentDate.setMonth(this.firstDateMonth.getMonth() + whichCalendar);
        currentDate.setDate(i + 1);
        currentDate.setHours(0, 0, 0, 0);
        
        var cell = $(cells[i]);
        
        if (this.checkStatus == 'in') {
            this._redrawCellIn(cell, currentDate, unavailableDays,noArrivalDays);
        } else {
            this._redrawCellOut(cell, currentDate, nextUnavailable,noArrivalDays);
        }
    }
};

DhBookingCalendar.prototype.findNextUnavailableDate = function() {
    if (!this.checkInDate) {
        return false;
    }
    
    var startDate = new Date(this.checkInDate.getTime());
    var endDate = new Date(this.firstDateMonth.getTime());
    endDate.setMonth(endDate.getMonth() + 1);
    endDate.setDate(32);
    
    var currentDate = new Date(startDate.getTime());
    while (currentDate < endDate) {
        var year = currentDate.getFullYear();
        var month = currentDate.getMonth();
        var date = currentDate.getDate();
        
        var yearMonth = year + '_' + ('0' + (month + 1)).slice(-2);
        var unavailableDays;
        if (typeof(this.dataUnavailable[yearMonth]) != 'undefined' && this.version != 'searchbar' && this.version != 'searchbar2' && !$('body').hasClass("dealsPage")) {
            unavailableDays = this.dataUnavailable[yearMonth];
        } else {
            unavailableDays = {};
        }
		
		/* No Arrival Days */
		var noArrivalDays;
		if (this.dataNoArrival && typeof(this.dataNoArrival[yearMonth]) != 'undefined') {
			noArrivalDays = this.dataNoArrival[yearMonth];
		} else {
			noArrivalDays = {};
		}
        
        if (typeof(unavailableDays[date]) != 'undefined') {
            return currentDate;
        }			
        
        currentDate = new Date(year, month, date + 1, 0, 0, 0, 0);
    }
    
    return false;
}

DhBookingCalendar.prototype._redrawCellIn = function(cell, date, unavailableDays,noArrivalDays) {
    var day = date.getDate();

    cell.removeClass('datepickerDisabled');
    cell.removeClass('datepickerSelected');
    cell.removeClass('date-booked');
	cell.removeClass('date-noarrival');
    cell.removeClass('date-available');
    cell.removeClass('datepickerSelectedDisabled');
    cell.removeClass('date-checkin');
    cell.removeClass('date-checkout');
    
    var now = new Date();
    now.setHours(0, 0, 0, 0);
    
    if (date.getTime() < now.getTime()) {
        cell.addClass('datepickerDisabled');
    } else {		
        if (typeof(unavailableDays[day]) != 'undefined') {		
            cell.addClass('date-booked');
        } else if (this.version != 'searchbar' && this.version != 'searchbar2' && !$('body').hasClass("dealsPage")) {           
            cell.addClass('date-available');	            
            /* No Arrival Days */
            if (typeof(noArrivalDays[day]) != 'undefined') {		            
                cell.removeClass('date-available');
                cell.addClass('date-noarrival');                       
            }                
        }		        		
    }
    
    if (this.dateEquals(date, this.checkInDate)) {
        cell.addClass('date-checkin');
    }
    
    if (this.dateEquals(date, this.checkOutDate)) {
        cell.addClass('date-checkout');
    }
    
    if (this.checkInDate && this.checkOutDate) {
        if (date.getTime() >= this.checkInDate.getTime() && date.getTime() <= this.checkOutDate.getTime()) {
            cell.addClass('datepickerSelected');
        }
    } else if (this.checkInDate) {
        if (date.getTime() == this.checkInDate.getTime()) {
            cell.addClass('datepickerSelected');
        }
    }
};

DhBookingCalendar.prototype._redrawCellOut = function(cell, date, nextUnavailable) {
    cell.removeClass('datepickerDisabled');
    cell.removeClass('datepickerSelected');
    cell.removeClass('date-booked');
    cell.removeClass('date-available');
    cell.removeClass('datepickerSelectedDisabled');
    cell.removeClass('date-checkin');
    cell.removeClass('date-checkout');
    
    if (!this.checkInDate) {
        return;
    }
    
    var checkInPlusOne = new Date(this.checkInDate.getTime());
    checkInPlusOne.setDate(checkInPlusOne.getDate() + 1);
    checkInPlusOne.setHours(0, 0, 0, 0);
    
    if (date < checkInPlusOne) {
        cell.addClass('datepickerDisabled');
    } else {
        if (nextUnavailable && date > nextUnavailable) {
            cell.addClass('date-booked');
        } else if (this.version != 'searchbar' && this.version != 'searchbar2' && !$('body').hasClass("dealsPage")) {
            cell.removeClass('date-noarrival');
            cell.addClass('date-available');
        }
    }
    
    if (this.dateEquals(date, this.checkInDate)) {
        cell.addClass('date-checkin');
    }
    
    if (this.dateEquals(date, this.checkOutDate)) {
        cell.addClass('date-checkout');
    }
    
    if (date.getTime() > this.checkInDate.getTime() && date.getTime() <= this.checkOutDate.getTime()) {
        cell.addClass('datepickerSelected');
    } else if (date.getTime() == this.checkInDate.getTime()) {
        cell.addClass('datepickerSelectedDisabled');
    }
};

DhBookingCalendar.prototype._debug = function(thing) {
    if (!this.debug) {
        return;
    }
    
    console.debug(thing);
};

DhBookingCalendar.prototype.valueToDate = function(value) {
    if (value.length > 12) {
        return new Date(value);
    }
    
    value = value.split('-');
    var year = parseInt(value[0], 10);
    var month = parseInt(value[1], 10) - 1;
    var day = parseInt(value[2], 10);
    
    return new Date(year, month, day, 0, 0, 0, 0);
};

DhBookingCalendar.prototype.dateEquals = function(date1, date2) {
    if (!date1 || !date2) {
        return false;
    }
    
    return date1.getDate() == date2.getDate()
        && date1.getMonth() == date2.getMonth()
        && date1.getFullYear() == date2.getFullYear();
};




// ! moment.js
// ! version : 2.4.0
// ! authors : Tim Wood, Iskren Chernev, Moment.js contributors
// ! license : MIT
// ! momentjs.com
(function(a){function b(a,b){return function(c){return i(a.call(this,c),b)}}function c(a,b){return function(c){return this.lang().ordinal(a.call(this,c),b)}}function d(){}function e(a){u(a),g(this,a)}function f(a){var b=o(a),c=b.year||0,d=b.month||0,e=b.week||0,f=b.day||0,g=b.hour||0,h=b.minute||0,i=b.second||0,j=b.millisecond||0;this._input=a,this._milliseconds=+j+1e3*i+6e4*h+36e5*g,this._days=+f+7*e,this._months=+d+12*c,this._data={},this._bubble()}function g(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c]);return b.hasOwnProperty("toString")&&(a.toString=b.toString),b.hasOwnProperty("valueOf")&&(a.valueOf=b.valueOf),a}function h(a){return 0>a?Math.ceil(a):Math.floor(a)}function i(a,b){for(var c=a+"";c.length<b;)c="0"+c;return c}function j(a,b,c,d){var e,f,g=b._milliseconds,h=b._days,i=b._months;g&&a._d.setTime(+a._d+g*c),(h||i)&&(e=a.minute(),f=a.hour()),h&&a.date(a.date()+h*c),i&&a.month(a.month()+i*c),g&&!d&&bb.updateOffset(a),(h||i)&&(a.minute(e),a.hour(f))}function k(a){return"[object Array]"===Object.prototype.toString.call(a)}function l(a){return"[object Date]"===Object.prototype.toString.call(a)||a instanceof Date}function m(a,b,c){var d,e=Math.min(a.length,b.length),f=Math.abs(a.length-b.length),g=0;for(d=0;e>d;d++)(c&&a[d]!==b[d]||!c&&q(a[d])!==q(b[d]))&&g++;return g+f}function n(a){if(a){var b=a.toLowerCase().replace(/(.)s$/,"$1");a=Kb[a]||Lb[b]||b}return a}function o(a){var b,c,d={};for(c in a)a.hasOwnProperty(c)&&(b=n(c),b&&(d[b]=a[c]));return d}function p(b){var c,d;if(0===b.indexOf("week"))c=7,d="day";else{if(0!==b.indexOf("month"))return;c=12,d="month"}bb[b]=function(e,f){var g,h,i=bb.fn._lang[b],j=[];if("number"==typeof e&&(f=e,e=a),h=function(a){var b=bb().utc().set(d,a);return i.call(bb.fn._lang,b,e||"")},null!=f)return h(f);for(g=0;c>g;g++)j.push(h(g));return j}}function q(a){var b=+a,c=0;return 0!==b&&isFinite(b)&&(c=b>=0?Math.floor(b):Math.ceil(b)),c}function r(a,b){return new Date(Date.UTC(a,b+1,0)).getUTCDate()}function s(a){return t(a)?366:365}function t(a){return 0===a%4&&0!==a%100||0===a%400}function u(a){var b;a._a&&-2===a._pf.overflow&&(b=a._a[gb]<0||a._a[gb]>11?gb:a._a[hb]<1||a._a[hb]>r(a._a[fb],a._a[gb])?hb:a._a[ib]<0||a._a[ib]>23?ib:a._a[jb]<0||a._a[jb]>59?jb:a._a[kb]<0||a._a[kb]>59?kb:a._a[lb]<0||a._a[lb]>999?lb:-1,a._pf._overflowDayOfYear&&(fb>b||b>hb)&&(b=hb),a._pf.overflow=b)}function v(a){a._pf={empty:!1,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:!1,invalidMonth:null,invalidFormat:!1,userInvalidated:!1,iso:!1}}function w(a){return null==a._isValid&&(a._isValid=!isNaN(a._d.getTime())&&a._pf.overflow<0&&!a._pf.empty&&!a._pf.invalidMonth&&!a._pf.nullInput&&!a._pf.invalidFormat&&!a._pf.userInvalidated,a._strict&&(a._isValid=a._isValid&&0===a._pf.charsLeftOver&&0===a._pf.unusedTokens.length)),a._isValid}function x(a){return a?a.toLowerCase().replace("_","-"):a}function y(a,b){return b.abbr=a,mb[a]||(mb[a]=new d),mb[a].set(b),mb[a]}function z(a){delete mb[a]}function A(a){var b,c,d,e,f=0,g=function(a){if(!mb[a]&&nb)try{require("./lang/"+a)}catch(b){}return mb[a]};if(!a)return bb.fn._lang;if(!k(a)){if(c=g(a))return c;a=[a]}for(;f<a.length;){for(e=x(a[f]).split("-"),b=e.length,d=x(a[f+1]),d=d?d.split("-"):null;b>0;){if(c=g(e.slice(0,b).join("-")))return c;if(d&&d.length>=b&&m(e,d,!0)>=b-1)break;b--}f++}return bb.fn._lang}function B(a){return a.match(/\[[\s\S]/)?a.replace(/^\[|\]$/g,""):a.replace(/\\/g,"")}function C(a){var b,c,d=a.match(rb);for(b=0,c=d.length;c>b;b++)d[b]=Pb[d[b]]?Pb[d[b]]:B(d[b]);return function(e){var f="";for(b=0;c>b;b++)f+=d[b]instanceof Function?d[b].call(e,a):d[b];return f}}function D(a,b){return a.isValid()?(b=E(b,a.lang()),Mb[b]||(Mb[b]=C(b)),Mb[b](a)):a.lang().invalidDate()}function E(a,b){function c(a){return b.longDateFormat(a)||a}var d=5;for(sb.lastIndex=0;d>=0&&sb.test(a);)a=a.replace(sb,c),sb.lastIndex=0,d-=1;return a}function F(a,b){var c;switch(a){case"DDDD":return vb;case"YYYY":case"GGGG":case"gggg":return wb;case"YYYYY":case"GGGGG":case"ggggg":return xb;case"S":case"SS":case"SSS":case"DDD":return ub;case"MMM":case"MMMM":case"dd":case"ddd":case"dddd":return zb;case"a":case"A":return A(b._l)._meridiemParse;case"X":return Cb;case"Z":case"ZZ":return Ab;case"T":return Bb;case"SSSS":return yb;case"MM":case"DD":case"YY":case"GG":case"gg":case"HH":case"hh":case"mm":case"ss":case"M":case"D":case"d":case"H":case"h":case"m":case"s":case"w":case"ww":case"W":case"WW":case"e":case"E":return tb;default:return c=new RegExp(N(M(a.replace("\\","")),"i"))}}function G(a){var b=(Ab.exec(a)||[])[0],c=(b+"").match(Hb)||["-",0,0],d=+(60*c[1])+q(c[2]);return"+"===c[0]?-d:d}function H(a,b,c){var d,e=c._a;switch(a){case"M":case"MM":null!=b&&(e[gb]=q(b)-1);break;case"MMM":case"MMMM":d=A(c._l).monthsParse(b),null!=d?e[gb]=d:c._pf.invalidMonth=b;break;case"D":case"DD":null!=b&&(e[hb]=q(b));break;case"DDD":case"DDDD":null!=b&&(c._dayOfYear=q(b));break;case"YY":e[fb]=q(b)+(q(b)>68?1900:2e3);break;case"YYYY":case"YYYYY":e[fb]=q(b);break;case"a":case"A":c._isPm=A(c._l).isPM(b);break;case"H":case"HH":case"h":case"hh":e[ib]=q(b);break;case"m":case"mm":e[jb]=q(b);break;case"s":case"ss":e[kb]=q(b);break;case"S":case"SS":case"SSS":case"SSSS":e[lb]=q(1e3*("0."+b));break;case"X":c._d=new Date(1e3*parseFloat(b));break;case"Z":case"ZZ":c._useUTC=!0,c._tzm=G(b);break;case"w":case"ww":case"W":case"WW":case"d":case"dd":case"ddd":case"dddd":case"e":case"E":a=a.substr(0,1);case"gg":case"gggg":case"GG":case"GGGG":case"GGGGG":a=a.substr(0,2),b&&(c._w=c._w||{},c._w[a]=b)}}function I(a){var b,c,d,e,f,g,h,i,j,k,l=[];if(!a._d){for(d=K(a),a._w&&null==a._a[hb]&&null==a._a[gb]&&(f=function(b){return b?b.length<3?parseInt(b,10)>68?"19"+b:"20"+b:b:null==a._a[fb]?bb().weekYear():a._a[fb]},g=a._w,null!=g.GG||null!=g.W||null!=g.E?h=X(f(g.GG),g.W||1,g.E,4,1):(i=A(a._l),j=null!=g.d?T(g.d,i):null!=g.e?parseInt(g.e,10)+i._week.dow:0,k=parseInt(g.w,10)||1,null!=g.d&&j<i._week.dow&&k++,h=X(f(g.gg),k,j,i._week.doy,i._week.dow)),a._a[fb]=h.year,a._dayOfYear=h.dayOfYear),a._dayOfYear&&(e=null==a._a[fb]?d[fb]:a._a[fb],a._dayOfYear>s(e)&&(a._pf._overflowDayOfYear=!0),c=S(e,0,a._dayOfYear),a._a[gb]=c.getUTCMonth(),a._a[hb]=c.getUTCDate()),b=0;3>b&&null==a._a[b];++b)a._a[b]=l[b]=d[b];for(;7>b;b++)a._a[b]=l[b]=null==a._a[b]?2===b?1:0:a._a[b];l[ib]+=q((a._tzm||0)/60),l[jb]+=q((a._tzm||0)%60),a._d=(a._useUTC?S:R).apply(null,l)}}function J(a){var b;a._d||(b=o(a._i),a._a=[b.year,b.month,b.day,b.hour,b.minute,b.second,b.millisecond],I(a))}function K(a){var b=new Date;return a._useUTC?[b.getUTCFullYear(),b.getUTCMonth(),b.getUTCDate()]:[b.getFullYear(),b.getMonth(),b.getDate()]}function L(a){a._a=[],a._pf.empty=!0;var b,c,d,e,f,g=A(a._l),h=""+a._i,i=h.length,j=0;for(d=E(a._f,g).match(rb)||[],b=0;b<d.length;b++)e=d[b],c=(F(e,a).exec(h)||[])[0],c&&(f=h.substr(0,h.indexOf(c)),f.length>0&&a._pf.unusedInput.push(f),h=h.slice(h.indexOf(c)+c.length),j+=c.length),Pb[e]?(c?a._pf.empty=!1:a._pf.unusedTokens.push(e),H(e,c,a)):a._strict&&!c&&a._pf.unusedTokens.push(e);a._pf.charsLeftOver=i-j,h.length>0&&a._pf.unusedInput.push(h),a._isPm&&a._a[ib]<12&&(a._a[ib]+=12),a._isPm===!1&&12===a._a[ib]&&(a._a[ib]=0),I(a),u(a)}function M(a){return a.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(a,b,c,d,e){return b||c||d||e})}function N(a){return a.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&")}function O(a){var b,c,d,e,f;if(0===a._f.length)return a._pf.invalidFormat=!0,a._d=new Date(0/0),void 0;for(e=0;e<a._f.length;e++)f=0,b=g({},a),v(b),b._f=a._f[e],L(b),w(b)&&(f+=b._pf.charsLeftOver,f+=10*b._pf.unusedTokens.length,b._pf.score=f,(null==d||d>f)&&(d=f,c=b));g(a,c||b)}function P(a){var b,c=a._i,d=Db.exec(c);if(d){for(a._pf.iso=!0,b=4;b>0;b--)if(d[b]){a._f=Fb[b-1]+(d[6]||" ");break}for(b=0;4>b;b++)if(Gb[b][1].exec(c)){a._f+=Gb[b][0];break}Ab.exec(c)&&(a._f+="Z"),L(a)}else a._d=new Date(c)}function Q(b){var c=b._i,d=ob.exec(c);c===a?b._d=new Date:d?b._d=new Date(+d[1]):"string"==typeof c?P(b):k(c)?(b._a=c.slice(0),I(b)):l(c)?b._d=new Date(+c):"object"==typeof c?J(b):b._d=new Date(c)}function R(a,b,c,d,e,f,g){var h=new Date(a,b,c,d,e,f,g);return 1970>a&&h.setFullYear(a),h}function S(a){var b=new Date(Date.UTC.apply(null,arguments));return 1970>a&&b.setUTCFullYear(a),b}function T(a,b){if("string"==typeof a)if(isNaN(a)){if(a=b.weekdaysParse(a),"number"!=typeof a)return null}else a=parseInt(a,10);return a}function U(a,b,c,d,e){return e.relativeTime(b||1,!!c,a,d)}function V(a,b,c){var d=eb(Math.abs(a)/1e3),e=eb(d/60),f=eb(e/60),g=eb(f/24),h=eb(g/365),i=45>d&&["s",d]||1===e&&["m"]||45>e&&["mm",e]||1===f&&["h"]||22>f&&["hh",f]||1===g&&["d"]||25>=g&&["dd",g]||45>=g&&["M"]||345>g&&["MM",eb(g/30)]||1===h&&["y"]||["yy",h];return i[2]=b,i[3]=a>0,i[4]=c,U.apply({},i)}function W(a,b,c){var d,e=c-b,f=c-a.day();return f>e&&(f-=7),e-7>f&&(f+=7),d=bb(a).add("d",f),{week:Math.ceil(d.dayOfYear()/7),year:d.year()}}function X(a,b,c,d,e){var f,g,h=new Date(Date.UTC(a,0)).getUTCDay();return c=null!=c?c:e,f=e-h+(h>d?7:0),g=7*(b-1)+(c-e)+f+1,{year:g>0?a:a-1,dayOfYear:g>0?g:s(a-1)+g}}function Y(a){var b=a._i,c=a._f;return"undefined"==typeof a._pf&&v(a),null===b?bb.invalid({nullInput:!0}):("string"==typeof b&&(a._i=b=A().preparse(b)),bb.isMoment(b)?(a=g({},b),a._d=new Date(+b._d)):c?k(c)?O(a):L(a):Q(a),new e(a))}function Z(a,b){bb.fn[a]=bb.fn[a+"s"]=function(a){var c=this._isUTC?"UTC":"";return null!=a?(this._d["set"+c+b](a),bb.updateOffset(this),this):this._d["get"+c+b]()}}function $(a){bb.duration.fn[a]=function(){return this._data[a]}}function _(a,b){bb.duration.fn["as"+a]=function(){return+this/b}}function ab(a){var b=!1,c=bb;"undefined"==typeof ender&&(this.moment=a?function(){return!b&&console&&console.warn&&(b=!0,console.warn("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.")),c.apply(null,arguments)}:bb)}for(var bb,cb,db="2.4.0",eb=Math.round,fb=0,gb=1,hb=2,ib=3,jb=4,kb=5,lb=6,mb={},nb="undefined"!=typeof module&&module.exports,ob=/^\/?Date\((\-?\d+)/i,pb=/(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,qb=/^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/,rb=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|X|zz?|ZZ?|.)/g,sb=/(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g,tb=/\d\d?/,ub=/\d{1,3}/,vb=/\d{3}/,wb=/\d{1,4}/,xb=/[+\-]?\d{1,6}/,yb=/\d+/,zb=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,Ab=/Z|[\+\-]\d\d:?\d\d/i,Bb=/T/i,Cb=/[\+\-]?\d+(\.\d{1,3})?/,Db=/^\s*\d{4}-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d:?\d\d|Z)?)?$/,Eb="YYYY-MM-DDTHH:mm:ssZ",Fb=["YYYY-MM-DD","GGGG-[W]WW","GGGG-[W]WW-E","YYYY-DDD"],Gb=[["HH:mm:ss.SSSS",/(T| )\d\d:\d\d:\d\d\.\d{1,3}/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],Hb=/([\+\-]|\d\d)/gi,Ib="Date|Hours|Minutes|Seconds|Milliseconds".split("|"),Jb={Milliseconds:1,Seconds:1e3,Minutes:6e4,Hours:36e5,Days:864e5,Months:2592e6,Years:31536e6},Kb={ms:"millisecond",s:"second",m:"minute",h:"hour",d:"day",D:"date",w:"week",W:"isoWeek",M:"month",y:"year",DDD:"dayOfYear",e:"weekday",E:"isoWeekday",gg:"weekYear",GG:"isoWeekYear"},Lb={dayofyear:"dayOfYear",isoweekday:"isoWeekday",isoweek:"isoWeek",weekyear:"weekYear",isoweekyear:"isoWeekYear"},Mb={},Nb="DDD w W M D d".split(" "),Ob="M D H h m s w W".split(" "),Pb={M:function(){return this.month()+1},MMM:function(a){return this.lang().monthsShort(this,a)},MMMM:function(a){return this.lang().months(this,a)},D:function(){return this.date()},DDD:function(){return this.dayOfYear()},d:function(){return this.day()},dd:function(a){return this.lang().weekdaysMin(this,a)},ddd:function(a){return this.lang().weekdaysShort(this,a)},dddd:function(a){return this.lang().weekdays(this,a)},w:function(){return this.week()},W:function(){return this.isoWeek()},YY:function(){return i(this.year()%100,2)},YYYY:function(){return i(this.year(),4)},YYYYY:function(){return i(this.year(),5)},gg:function(){return i(this.weekYear()%100,2)},gggg:function(){return this.weekYear()},ggggg:function(){return i(this.weekYear(),5)},GG:function(){return i(this.isoWeekYear()%100,2)},GGGG:function(){return this.isoWeekYear()},GGGGG:function(){return i(this.isoWeekYear(),5)},e:function(){return this.weekday()},E:function(){return this.isoWeekday()},a:function(){return this.lang().meridiem(this.hours(),this.minutes(),!0)},A:function(){return this.lang().meridiem(this.hours(),this.minutes(),!1)},H:function(){return this.hours()},h:function(){return this.hours()%12||12},m:function(){return this.minutes()},s:function(){return this.seconds()},S:function(){return q(this.milliseconds()/100)},SS:function(){return i(q(this.milliseconds()/10),2)},SSS:function(){return i(this.milliseconds(),3)},SSSS:function(){return i(this.milliseconds(),3)},Z:function(){var a=-this.zone(),b="+";return 0>a&&(a=-a,b="-"),b+i(q(a/60),2)+":"+i(q(a)%60,2)},ZZ:function(){var a=-this.zone(),b="+";return 0>a&&(a=-a,b="-"),b+i(q(10*a/6),4)},z:function(){return this.zoneAbbr()},zz:function(){return this.zoneName()},X:function(){return this.unix()}},Qb=["months","monthsShort","weekdays","weekdaysShort","weekdaysMin"];Nb.length;)cb=Nb.pop(),Pb[cb+"o"]=c(Pb[cb],cb);for(;Ob.length;)cb=Ob.pop(),Pb[cb+cb]=b(Pb[cb],2);for(Pb.DDDD=b(Pb.DDD,3),g(d.prototype,{set:function(a){var b,c;for(c in a)b=a[c],"function"==typeof b?this[c]=b:this["_"+c]=b},_months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),months:function(a){return this._months[a.month()]},_monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),monthsShort:function(a){return this._monthsShort[a.month()]},monthsParse:function(a){var b,c,d;for(this._monthsParse||(this._monthsParse=[]),b=0;12>b;b++)if(this._monthsParse[b]||(c=bb.utc([2e3,b]),d="^"+this.months(c,"")+"|^"+this.monthsShort(c,""),this._monthsParse[b]=new RegExp(d.replace(".",""),"i")),this._monthsParse[b].test(a))return b},_weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdays:function(a){return this._weekdays[a.day()]},_weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysShort:function(a){return this._weekdaysShort[a.day()]},_weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),weekdaysMin:function(a){return this._weekdaysMin[a.day()]},weekdaysParse:function(a){var b,c,d;for(this._weekdaysParse||(this._weekdaysParse=[]),b=0;7>b;b++)if(this._weekdaysParse[b]||(c=bb([2e3,1]).day(b),d="^"+this.weekdays(c,"")+"|^"+this.weekdaysShort(c,"")+"|^"+this.weekdaysMin(c,""),this._weekdaysParse[b]=new RegExp(d.replace(".",""),"i")),this._weekdaysParse[b].test(a))return b},_longDateFormat:{LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D YYYY",LLL:"MMMM D YYYY LT",LLLL:"dddd, MMMM D YYYY LT"},longDateFormat:function(a){var b=this._longDateFormat[a];return!b&&this._longDateFormat[a.toUpperCase()]&&(b=this._longDateFormat[a.toUpperCase()].replace(/MMMM|MM|DD|dddd/g,function(a){return a.slice(1)}),this._longDateFormat[a]=b),b},isPM:function(a){return"p"===(a+"").toLowerCase().charAt(0)},_meridiemParse:/[ap]\.?m?\.?/i,meridiem:function(a,b,c){return a>11?c?"pm":"PM":c?"am":"AM"},_calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},calendar:function(a,b){var c=this._calendar[a];return"function"==typeof c?c.apply(b):c},_relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},relativeTime:function(a,b,c,d){var e=this._relativeTime[c];return"function"==typeof e?e(a,b,c,d):e.replace(/%d/i,a)},pastFuture:function(a,b){var c=this._relativeTime[a>0?"future":"past"];return"function"==typeof c?c(b):c.replace(/%s/i,b)},ordinal:function(a){return this._ordinal.replace("%d",a)},_ordinal:"%d",preparse:function(a){return a},postformat:function(a){return a},week:function(a){return W(a,this._week.dow,this._week.doy).week},_week:{dow:0,doy:6},_invalidDate:"Invalid date",invalidDate:function(){return this._invalidDate}}),bb=function(b,c,d,e){return"boolean"==typeof d&&(e=d,d=a),Y({_i:b,_f:c,_l:d,_strict:e,_isUTC:!1})},bb.utc=function(b,c,d,e){var f;return"boolean"==typeof d&&(e=d,d=a),f=Y({_useUTC:!0,_isUTC:!0,_l:d,_i:b,_f:c,_strict:e}).utc()},bb.unix=function(a){return bb(1e3*a)},bb.duration=function(a,b){var c,d,e,g=bb.isDuration(a),h="number"==typeof a,i=g?a._input:h?{}:a,j=null;return h?b?i[b]=a:i.milliseconds=a:(j=pb.exec(a))?(c="-"===j[1]?-1:1,i={y:0,d:q(j[hb])*c,h:q(j[ib])*c,m:q(j[jb])*c,s:q(j[kb])*c,ms:q(j[lb])*c}):(j=qb.exec(a))&&(c="-"===j[1]?-1:1,e=function(a){var b=a&&parseFloat(a.replace(",","."));return(isNaN(b)?0:b)*c},i={y:e(j[2]),M:e(j[3]),d:e(j[4]),h:e(j[5]),m:e(j[6]),s:e(j[7]),w:e(j[8])}),d=new f(i),g&&a.hasOwnProperty("_lang")&&(d._lang=a._lang),d},bb.version=db,bb.defaultFormat=Eb,bb.updateOffset=function(){},bb.lang=function(a,b){var c;return a?(b?y(x(a),b):null===b?(z(a),a="en"):mb[a]||A(a),c=bb.duration.fn._lang=bb.fn._lang=A(a),c._abbr):bb.fn._lang._abbr},bb.langData=function(a){return a&&a._lang&&a._lang._abbr&&(a=a._lang._abbr),A(a)},bb.isMoment=function(a){return a instanceof e},bb.isDuration=function(a){return a instanceof f},cb=Qb.length-1;cb>=0;--cb)p(Qb[cb]);for(bb.normalizeUnits=function(a){return n(a)},bb.invalid=function(a){var b=bb.utc(0/0);return null!=a?g(b._pf,a):b._pf.userInvalidated=!0,b},bb.parseZone=function(a){return bb(a).parseZone()},g(bb.fn=e.prototype,{clone:function(){return bb(this)},valueOf:function(){return+this._d+6e4*(this._offset||0)},unix:function(){return Math.floor(+this/1e3)},toString:function(){return this.clone().lang("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")},toDate:function(){return this._offset?new Date(+this):this._d},toISOString:function(){return D(bb(this).utc(),"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]")},toArray:function(){var a=this;return[a.year(),a.month(),a.date(),a.hours(),a.minutes(),a.seconds(),a.milliseconds()]},isValid:function(){return w(this)},isDSTShifted:function(){return this._a?this.isValid()&&m(this._a,(this._isUTC?bb.utc(this._a):bb(this._a)).toArray())>0:!1},parsingFlags:function(){return g({},this._pf)},invalidAt:function(){return this._pf.overflow},utc:function(){return this.zone(0)},local:function(){return this.zone(0),this._isUTC=!1,this},format:function(a){var b=D(this,a||bb.defaultFormat);return this.lang().postformat(b)},add:function(a,b){var c;return c="string"==typeof a?bb.duration(+b,a):bb.duration(a,b),j(this,c,1),this},subtract:function(a,b){var c;return c="string"==typeof a?bb.duration(+b,a):bb.duration(a,b),j(this,c,-1),this},diff:function(a,b,c){var d,e,f=this._isUTC?bb(a).zone(this._offset||0):bb(a).local(),g=6e4*(this.zone()-f.zone());return b=n(b),"year"===b||"month"===b?(d=432e5*(this.daysInMonth()+f.daysInMonth()),e=12*(this.year()-f.year())+(this.month()-f.month()),e+=(this-bb(this).startOf("month")-(f-bb(f).startOf("month")))/d,e-=6e4*(this.zone()-bb(this).startOf("month").zone()-(f.zone()-bb(f).startOf("month").zone()))/d,"year"===b&&(e/=12)):(d=this-f,e="second"===b?d/1e3:"minute"===b?d/6e4:"hour"===b?d/36e5:"day"===b?(d-g)/864e5:"week"===b?(d-g)/6048e5:d),c?e:h(e)},from:function(a,b){return bb.duration(this.diff(a)).lang(this.lang()._abbr).humanize(!b)},fromNow:function(a){return this.from(bb(),a)},calendar:function(){var a=this.diff(bb().zone(this.zone()).startOf("day"),"days",!0),b=-6>a?"sameElse":-1>a?"lastWeek":0>a?"lastDay":1>a?"sameDay":2>a?"nextDay":7>a?"nextWeek":"sameElse";return this.format(this.lang().calendar(b,this))},isLeapYear:function(){return t(this.year())},isDST:function(){return this.zone()<this.clone().month(0).zone()||this.zone()<this.clone().month(5).zone()},day:function(a){var b=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=a?(a=T(a,this.lang()),this.add({d:a-b})):b},month:function(a){var b,c=this._isUTC?"UTC":"";return null!=a?"string"==typeof a&&(a=this.lang().monthsParse(a),"number"!=typeof a)?this:(b=this.date(),this.date(1),this._d["set"+c+"Month"](a),this.date(Math.min(b,this.daysInMonth())),bb.updateOffset(this),this):this._d["get"+c+"Month"]()},startOf:function(a){switch(a=n(a)){case"year":this.month(0);case"month":this.date(1);case"week":case"isoWeek":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===a?this.weekday(0):"isoWeek"===a&&this.isoWeekday(1),this},endOf:function(a){return a=n(a),this.startOf(a).add("isoWeek"===a?"week":a,1).subtract("ms",1)},isAfter:function(a,b){return b="undefined"!=typeof b?b:"millisecond",+this.clone().startOf(b)>+bb(a).startOf(b)},isBefore:function(a,b){return b="undefined"!=typeof b?b:"millisecond",+this.clone().startOf(b)<+bb(a).startOf(b)},isSame:function(a,b){return b="undefined"!=typeof b?b:"millisecond",+this.clone().startOf(b)===+bb(a).startOf(b)},min:function(a){return a=bb.apply(null,arguments),this>a?this:a},max:function(a){return a=bb.apply(null,arguments),a>this?this:a},zone:function(a){var b=this._offset||0;return null==a?this._isUTC?b:this._d.getTimezoneOffset():("string"==typeof a&&(a=G(a)),Math.abs(a)<16&&(a=60*a),this._offset=a,this._isUTC=!0,b!==a&&j(this,bb.duration(b-a,"m"),1,!0),this)},zoneAbbr:function(){return this._isUTC?"UTC":""},zoneName:function(){return this._isUTC?"Coordinated Universal Time":""},parseZone:function(){return"string"==typeof this._i&&this.zone(this._i),this},hasAlignedHourOffset:function(a){return a=a?bb(a).zone():0,0===(this.zone()-a)%60},daysInMonth:function(){return r(this.year(),this.month())},dayOfYear:function(a){var b=eb((bb(this).startOf("day")-bb(this).startOf("year"))/864e5)+1;return null==a?b:this.add("d",a-b)},weekYear:function(a){var b=W(this,this.lang()._week.dow,this.lang()._week.doy).year;return null==a?b:this.add("y",a-b)},isoWeekYear:function(a){var b=W(this,1,4).year;return null==a?b:this.add("y",a-b)},week:function(a){var b=this.lang().week(this);return null==a?b:this.add("d",7*(a-b))},isoWeek:function(a){var b=W(this,1,4).week;return null==a?b:this.add("d",7*(a-b))},weekday:function(a){var b=(this.day()+7-this.lang()._week.dow)%7;return null==a?b:this.add("d",a-b)},isoWeekday:function(a){return null==a?this.day()||7:this.day(this.day()%7?a:a-7)},get:function(a){return a=n(a),this[a]()},set:function(a,b){return a=n(a),"function"==typeof this[a]&&this[a](b),this},lang:function(b){return b===a?this._lang:(this._lang=A(b),this)}}),cb=0;cb<Ib.length;cb++)Z(Ib[cb].toLowerCase().replace(/s$/,""),Ib[cb]);Z("year","FullYear"),bb.fn.days=bb.fn.day,bb.fn.months=bb.fn.month,bb.fn.weeks=bb.fn.week,bb.fn.isoWeeks=bb.fn.isoWeek,bb.fn.toJSON=bb.fn.toISOString,g(bb.duration.fn=f.prototype,{_bubble:function(){var a,b,c,d,e=this._milliseconds,f=this._days,g=this._months,i=this._data;i.milliseconds=e%1e3,a=h(e/1e3),i.seconds=a%60,b=h(a/60),i.minutes=b%60,c=h(b/60),i.hours=c%24,f+=h(c/24),i.days=f%30,g+=h(f/30),i.months=g%12,d=h(g/12),i.years=d},weeks:function(){return h(this.days()/7)},valueOf:function(){return this._milliseconds+864e5*this._days+2592e6*(this._months%12)+31536e6*q(this._months/12)},humanize:function(a){var b=+this,c=V(b,!a,this.lang());return a&&(c=this.lang().pastFuture(b,c)),this.lang().postformat(c)},add:function(a,b){var c=bb.duration(a,b);return this._milliseconds+=c._milliseconds,this._days+=c._days,this._months+=c._months,this._bubble(),this},subtract:function(a,b){var c=bb.duration(a,b);return this._milliseconds-=c._milliseconds,this._days-=c._days,this._months-=c._months,this._bubble(),this},get:function(a){return a=n(a),this[a.toLowerCase()+"s"]()},as:function(a){return a=n(a),this["as"+a.charAt(0).toUpperCase()+a.slice(1)+"s"]()},lang:bb.fn.lang,toIsoString:function(){var a=Math.abs(this.years()),b=Math.abs(this.months()),c=Math.abs(this.days()),d=Math.abs(this.hours()),e=Math.abs(this.minutes()),f=Math.abs(this.seconds()+this.milliseconds()/1e3);return this.asSeconds()?(this.asSeconds()<0?"-":"")+"P"+(a?a+"Y":"")+(b?b+"M":"")+(c?c+"D":"")+(d||e||f?"T":"")+(d?d+"H":"")+(e?e+"M":"")+(f?f+"S":""):"P0D"}});for(cb in Jb)Jb.hasOwnProperty(cb)&&(_(cb,Jb[cb]),$(cb.toLowerCase()));_("Weeks",6048e5),bb.duration.fn.asMonths=function(){return(+this-31536e6*this.years())/2592e6+12*this.years()},bb.lang("en",{ordinal:function(a){var b=a%10,c=1===q(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c}}),nb?(module.exports=bb,ab(!0)):"function"==typeof define&&define.amd?define("moment",function(b,c,d){return d.config().noGlobal!==!0&&ab(d.config().noGlobal===a),bb}):ab()}).call(this);

!function(t,e){function n(t,e,n){var r=t.children(),o=!1;t.empty();for(var a=0,d=r.length;d>a;a++){var l=r.eq(a);if(t.append(l),n&&t.append(n),i(t,e)){l.remove(),o=!0;break}n&&n.detach()}return o}function r(e,n,a,d,l){var s=!1,c="a, table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, blockquote, select, optgroup, option, textarea, script, style",u="script, .dotdotdot-keep";return e.contents().detach().each(function(){var f=this,h=t(f);if("undefined"==typeof f)return!0;if(h.is(u))e.append(h);else{if(s)return!0;e.append(h),!l||h.is(d.after)||h.find(d.after).length||e[e.is(c)?"after":"append"](l),i(a,d)&&(s=3==f.nodeType?o(h,n,a,d,l):r(h,n,a,d,l),s||(h.detach(),s=!0)),s||l&&l.detach()}}),s}function o(e,n,r,o,d){var c=e[0];if(!c)return!1;var f=s(c),h=-1!==f.indexOf(" ")?" ":"　",p="letter"==o.wrap?"":h,g=f.split(p),v=-1,w=-1,b=0,y=g.length-1;for(o.fallbackToLetter&&0==b&&0==y&&(p="",g=f.split(p),y=g.length-1);y>=b&&(0!=b||0!=y);){var m=Math.floor((b+y)/2);if(m==w)break;w=m,l(c,g.slice(0,w+1).join(p)+o.ellipsis),i(r,o)?(y=w,o.fallbackToLetter&&0==b&&0==y&&(p="",g=g[0].split(p),v=-1,w=-1,b=0,y=g.length-1)):(v=w,b=w)}if(-1==v||1==g.length&&0==g[0].length){var x=e.parent();e.detach();var T=d&&d.closest(x).length?d.length:0;x.contents().length>T?c=u(x.contents().eq(-1-T),n):(c=u(x,n,!0),T||x.detach()),c&&(f=a(s(c),o),l(c,f),T&&d&&t(c).parent().append(d))}else f=a(g.slice(0,v+1).join(p),o),l(c,f);return!0}function i(t,e){return t.innerHeight()>e.maxHeight}function a(e,n){for(;t.inArray(e.slice(-1),n.lastCharacter.remove)>-1;)e=e.slice(0,-1);return t.inArray(e.slice(-1),n.lastCharacter.noEllipsis)<0&&(e+=n.ellipsis),e}function d(t){return{width:t.innerWidth(),height:t.innerHeight()}}function l(t,e){t.innerText?t.innerText=e:t.nodeValue?t.nodeValue=e:t.textContent&&(t.textContent=e)}function s(t){return t.innerText?t.innerText:t.nodeValue?t.nodeValue:t.textContent?t.textContent:""}function c(t){do t=t.previousSibling;while(t&&1!==t.nodeType&&3!==t.nodeType);return t}function u(e,n,r){var o,i=e&&e[0];if(i){if(!r){if(3===i.nodeType)return i;if(t.trim(e.text()))return u(e.contents().last(),n)}for(o=c(i);!o;){if(e=e.parent(),e.is(n)||!e.length)return!1;o=c(e[0])}if(o)return u(t(o),n)}return!1}function f(e,n){return e?"string"==typeof e?(e=t(e,n),e.length?e:!1):e.jquery?e:!1:!1}function h(t){for(var e=t.innerHeight(),n=["paddingTop","paddingBottom"],r=0,o=n.length;o>r;r++){var i=parseInt(t.css(n[r]),10);isNaN(i)&&(i=0),e-=i}return e}if(!t.fn.dotdotdot){t.fn.dotdotdot=function(e){if(0==this.length)return t.fn.dotdotdot.debug('No element found for "'+this.selector+'".'),this;if(this.length>1)return this.each(function(){t(this).dotdotdot(e)});var o=this;o.data("dotdotdot")&&o.trigger("destroy.dot"),o.data("dotdotdot-style",o.attr("style")||""),o.css("word-wrap","break-word"),"nowrap"===o.css("white-space")&&o.css("white-space","normal"),o.bind_events=function(){return o.bind("update.dot",function(e,d){e.preventDefault(),e.stopPropagation(),l.maxHeight="number"==typeof l.height?l.height:h(o),l.maxHeight+=l.tolerance,"undefined"!=typeof d&&(("string"==typeof d||"nodeType"in d&&1===d.nodeType)&&(d=t("<div />").append(d).contents()),d instanceof t&&(a=d)),g=o.wrapInner('<div class="dotdotdot" />').children(),g.contents().detach().end().append(a.clone(!0)).find("br").replaceWith("  <br />  ").end().css({height:"auto",width:"auto",border:"none",padding:0,margin:0});var c=!1,u=!1;return s.afterElement&&(c=s.afterElement.clone(!0),c.show(),s.afterElement.detach()),i(g,l)&&(u="children"==l.wrap?n(g,l,c):r(g,o,g,l,c)),g.replaceWith(g.contents()),g=null,t.isFunction(l.callback)&&l.callback.call(o[0],u,a),s.isTruncated=u,u}).bind("isTruncated.dot",function(t,e){return t.preventDefault(),t.stopPropagation(),"function"==typeof e&&e.call(o[0],s.isTruncated),s.isTruncated}).bind("originalContent.dot",function(t,e){return t.preventDefault(),t.stopPropagation(),"function"==typeof e&&e.call(o[0],a),a}).bind("destroy.dot",function(t){t.preventDefault(),t.stopPropagation(),o.unwatch().unbind_events().contents().detach().end().append(a).attr("style",o.data("dotdotdot-style")||"").data("dotdotdot",!1)}),o},o.unbind_events=function(){return o.unbind(".dot"),o},o.watch=function(){if(o.unwatch(),"window"==l.watch){var e=t(window),n=e.width(),r=e.height();e.bind("resize.dot"+s.dotId,function(){n==e.width()&&r==e.height()&&l.windowResizeFix||(n=e.width(),r=e.height(),u&&clearInterval(u),u=setTimeout(function(){o.trigger("update.dot")},100))})}else c=d(o),u=setInterval(function(){if(o.is(":visible")){var t=d(o);(c.width!=t.width||c.height!=t.height)&&(o.trigger("update.dot"),c=t)}},500);return o},o.unwatch=function(){return t(window).unbind("resize.dot"+s.dotId),u&&clearInterval(u),o};var a=o.contents(),l=t.extend(!0,{},t.fn.dotdotdot.defaults,e),s={},c={},u=null,g=null;return l.lastCharacter.remove instanceof Array||(l.lastCharacter.remove=t.fn.dotdotdot.defaultArrays.lastCharacter.remove),l.lastCharacter.noEllipsis instanceof Array||(l.lastCharacter.noEllipsis=t.fn.dotdotdot.defaultArrays.lastCharacter.noEllipsis),s.afterElement=f(l.after,o),s.isTruncated=!1,s.dotId=p++,o.data("dotdotdot",!0).bind_events().trigger("update.dot"),l.watch&&o.watch(),o},t.fn.dotdotdot.defaults={ellipsis:"... ",wrap:"word",fallbackToLetter:!0,lastCharacter:{},tolerance:0,callback:null,after:null,height:null,watch:!1,windowResizeFix:!0},t.fn.dotdotdot.defaultArrays={lastCharacter:{remove:[" ","　",",",";",".","!","?"],noEllipsis:[]}},t.fn.dotdotdot.debug=function(){};var p=1,g=t.fn.html;t.fn.html=function(n){return n!=e&&!t.isFunction(n)&&this.data("dotdotdot")?this.trigger("update",[n]):g.apply(this,arguments)};var v=t.fn.text;t.fn.text=function(n){return n!=e&&!t.isFunction(n)&&this.data("dotdotdot")?(n=t("<div />").text(n).html(),this.trigger("update",[n])):v.apply(this,arguments)}}}(jQuery);

function callLogin() {
    $('#callLogin').trigger('click');
}


function closeDialog() {
    window.location.href = document.URL.replace("&error=1","");
}

function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}


var AvailabilityService = new function() {
    var callbacks = {};
    var unavailableDays = {};
    var unavailableDaysLoading = {};
    
    var dateToKey = function(date) {
        var newDate = new Date(date.getTime());
        newDate.setDate(1);
        newDate.setHours(0, 0, 0, 0);
        return newDate.getTime();
    }
    
    var loadUnavailability = function(date) {
        var key = dateToKey(date);
        unavailableDaysLoading[key] = true;
        
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        
        var startDate = year + '/' + ('0' + month).slice(-2) + '/01';
        var endDate = year + '/' + ('0' + month).slice(-2) + '/' + daysInMonth(month, year);
        var ratePlanCode = '';
        if(typeof rateCode !='undefined') {
            ratePlanCode = rateCode;
        }
        if(typeof propid !='undefined'){
            $.ajax({
                type: "POST",
                url: "/get-availability",
                data: {propid: propid, rateCode: ratePlanCode, start: startDate, end: endDate}
            }).done(function(data) {
                var daysUnavailable = {};
				var dataNoArrival = {};
                var dataJson = jQuery.parseJSON(data);
				var closedDates = dataJson.closed;
				var onarrival = dataJson.onarrival;						
                
              if (closedDates != null ) {                  
                  if (typeof closedDates.length != 'undefined' ) {
                    for (var i=0; i<closedDates.length; i++) {

                        // since we only receive closed dates...
                        if (closedDates[i].start && closedDates[i].end) {
                            var start = parseInt(closedDates[i].start.split('/')[1], 10);
                            var end = parseInt(closedDates[i].end.split('/')[1], 10);                                                        
							for (var j=start; j<=end; j++) {
                                daysUnavailable[j] = true;
                            }
                        }
                    }
                  }
                } 
                else{
                    for (var j=0; j<=31; j++) {
                        daysUnavailable[j] = true;
                    }
                }
                
				if (onarrival != null ) {
				if (typeof onarrival.length != 'undefined') {
                    for (var i=0; i<onarrival.length; i++) {

                        // since we only receive NoArrival date...
                        if (onarrival[i].start && onarrival[i].end) {
                            var start = parseInt(onarrival[i].start.split('/')[1], 10);
                            var end = parseInt(onarrival[i].end.split('/')[1], 10);                            
                            for (var j=start; j<=end; j++) {
                                dataNoArrival[j] = true;								
                            }
                        }
                }}
                } 							
											           
                unavailableDays[key] = {
                    processed: daysUnavailable,	
					dataNoArrival:dataNoArrival,
                    raw: data
                };						
                
                if (typeof(callbacks[key]) != 'undefined') {
                    for (var i=0; i<callbacks[key].length; i++) {
                        var callback = callbacks[key][i];
                        
                        
                        if (callback.process) {																				
							//var result = {unavailable: unavailableDays[key]['processed']};	
                            var result = {unavailable: unavailableDays[key]['processed'],dataNoArrival:unavailableDays[key]['dataNoArrival']};
                            callback.callback(result);																					                           							
                        } else {
                           callback.callback(unavailableDays[key].raw);
                        }
                        
                        /*if (callback.process) {
                            callback.callback(unavailableDays[key].processed);                            
                        } else {
                            callback.callback(unavailableDays[key].raw);
                        }*/
                    }
                    
                    callbacks[key] = [];
                }
            });
        }
    }
    
    this.get = function(date, callback, process) {
        if (typeof(process) == 'undefined') {
            process = false;
        }
        
        var key = dateToKey(date);
        
        if (typeof(unavailableDays[key]) != 'undefined') {
			
            if (process) {                
                //var result = {unavailable: unavailableDays[key]['processed']};
				var result = {unavailable: unavailableDays[key]['processed'],dataNoArrival:unavailableDays[key]['dataNoArrival']};
                callback(result);
                return;
            }  
                                   
			/* if (process) {
                callback(unavailableDays[key]['processed']);                
                return;
            }  */ 
            callback(unavailableDays[key]['raw']);
            return;
        }
        
        if (typeof(callbacks[key]) == 'undefined') {
            callbacks[key] = [];
        }
        
        callbacks[key].push({
            callback: callback,
            process: process
        });
        
        if (typeof(unavailableDaysLoading[key]) == 'undefined') {
            loadUnavailability(date);
        }
    }
}();


var NewStyledCheckbox = function(element) {
    this.element = element;
}

NewStyledCheckbox.prototype.init = function() {
    var cb = this.element;
    var styled = $('<div class="new-styled-checkbox"></div>');
    var check = $('<div class="new-styled-checkbox-check"></div>');
    cb.hide();
    
    styled.append(check);
    this.element.after(styled);
    
    var updateLayout = function() {
        if (cb.is(':checked')) {
            cb.attr('checked', 'checked');
            styled.addClass('new-styled-checkbox-active');
        } else {
            cb.removeAttr('checked');
            styled.removeClass('new-styled-checkbox-active');
        }
    }
    
    styled.click(function() {
        if (cb.is(':checked')) {
            cb.prop('checked', false);
        } else {
            cb.prop('checked', true);
        }
        
        cb.trigger('change');
        updateLayout();
    });
    
    this.element.change(function() {
        updateLayout();
    });
    
    updateLayout();
    this.updateLayout = updateLayout;
}

