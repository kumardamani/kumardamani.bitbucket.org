
var hasBirthday = true;
var requiredArr = [
	'txtFirstName',
	'txtLastName',
	'hdncountry',
	'txtCity',
	'txtEmail',
    'txtAddress1'
];

$('span.toggler').draggable({
	containment: "parent",
	stop: function() {

		var xPos = $(this).css('left');
		if (parseInt(xPos) >= 22.5) {

			$(this).parent().attr('data-value', 0).data('value', 0).removeClass('on').addClass('off').find('.toggler-label').first().text('Off');
			$('input#hdn' + $(this).parent().attr('id')).val(0);
			$(this).closest('div#userType').find("span.element-label").removeClass('userTypeOn').addClass('userTypeOff');
			$(this).closest('div#userType').find("span.travel-professional").removeClass('userTypeOff').addClass('userTypeOn');
		}

		else {

			$(this).parent().attr('data-value', 1).data('value', 1).removeClass('off').addClass('on').find('.toggler-label').first().text('On');
			$('input#hdn' + $(this).parent().attr('id')).val(1);
			$(this).closest('div#userType').find("span.element-label").removeClass('userTypeOff').addClass('userTypeOn');
			$(this).closest('div#userType').find("span.travel-professional").removeClass('userTypeOn').addClass('userTypeOff');
		}

		$(this).css('left', parseInt(xPos) >= 22.5 ? '45px' : '0');
		if ($(this).parent().attr('id') == 'chkTravelProf') {

			$('div.company-info.form-cntr').css('display', parseInt(xPos) >= 22.5 ? 'none' : 'block');
			if (parseInt(xPos) >= 22.5) {

				$('div.company-info.form-cntr')
					.find('input')
					.each(function() {

						$(this).val('');
					});

				$('div.company-info.form-cntr')
					.find('.tzSelect div.selectBox span.text')
					.first()
					.html('Country');
			}
		}
	}
});

$('.take-break label input').on('click', function() {
    var breakType = $(this).attr('data-type');
    $('.break-type').val(breakType);
});

$('a#lnkDelete')
	.on('click', function() {

		$('div#lightboxAccountDelete').dialog('open');
	});

$('a#cancel')
	.on('click', function() {

		$(this).closest('div.lightbox').dialog('close');
	});

$('form#frmSettings')
	.submit(function() {

		// var isValid = validBirth();
		// if (isValid == true) {

			isValid = checkRequiredFields(false);
		// }

		return isValid;
	});

$('a#lnkDeleteAccount')
	.on('click', function() {
		s.event = 'event5';
		s.prop4 = 'cancelled';
		$('form#frmAccountDelete').submit();
	});

// $('body')
	// .on('change', '#hdndateBirthYear, #hdndateBirthMonth, #hdndateBirthDate', function() {

		// validateBirth();
	// });

function validateMiles(isOnFocus) {

	var milesObj = $('input#txtMiles');
	var value = $(milesObj).val();
	var isValid = true;
	if (value.replace(/\s+/i, '').length > 0) {

		isValid = value.length == 15 && value.replace(/[^\d]+/i, '').length == 15;
		if (isValid) {

			$('span#chkMiles').css('display', 'inline-block');
			$(milesObj).css('border', '0').css('height', '46px');
			$(milesObj).parent().find('p.error').first().remove();
			$(milesObj).parent().css('padding-bottom', '10px');
		}

		else {

			$('span#chkMiles').css('display', 'none');
			$(milesObj).css('border', '6px solid #BF883B').css('height', '34px').css('line-height', '34px');
			$(milesObj).parent().css('padding-bottom', 0);
			if ($(milesObj).parent().find('p.error').length == 0) {

				$(milesObj).parent().append('<p class="error">Error: Card Number must be exactly 15 digits</p>');
			}

			if (isOnFocus == false) {

				scrollToElement($(milesObj).parent());
			}
		}
	}

	return isValid;
}

function validateBirth() {

	var birthYear = $('#hdndateBirthYear').val();
	var birthMonth = $('#hdndateBirthMonth').val();
	var birthDate = $('#hdndateBirthDate').val();
	var birthArr = [
		'dateBirthYear',
		'dateBirthMonth',
		'dateBirthDate'
	];
	if (!isEmpty(birthYear) || !isEmpty(birthMonth) || !isEmpty(birthDate)) {

		if ((!isEmpty(birthYear) && !isEmpty(birthMonth) && !isEmpty(birthDate)) || (isEmpty(birthYear) && isEmpty(birthMonth) && isEmpty(birthDate))) {

			$('#hdndateBirthYear').parent().find('p.error').remove();
		}

		else {

			if ($('#hdndateBirthYear').parent().find('p.error').length == 0) {

				$('#hdndateBirthYear')
					.parent()
					.append('<p class="error">Please fill in your complete birthday.</p>');
			}

			scrollToElement($('div.birth-date').parent());
		}

		for (var i = 0; i < birthArr.length; i++) {

			var obj = $('#hdn' + birthArr[i]);
			if (isEmpty($(obj).val())) {

				$('.' + birthArr[i] + '_selectbox').css('border', '6px solid #BF883B').css('height', '34px').css('line-height', '34px');
				$('.' + birthArr[i] + '_selectbox span.icon').css('top', '14px');
				$('.' + birthArr[i] + '_selectbox').parent().parent().css('padding-bottom', 0);
			}

			else {

				$('.' + birthArr[i] + '_selectbox').css('border', 0).css('height', '46px').css('line-height', '46px');
				$('.' + birthArr[i] + '_selectbox span.icon').css('top', '22px');
				$('.' + birthArr[i] + '_selectbox').parent().parent().css('padding-bottom', '16px');
			}
		}
	}

	else {

		$('#hdndateBirthYear').parent().find('p.error').remove();
		for (var i = 0; i < birthArr.length; i++) {

			$('.' + birthArr[i] + '_selectbox').css('border', 0).css('height', '46px').css('line-height', '46px');
			$('.' + birthArr[i] + '_selectbox span.icon').css('top', '22px');
			$('.' + birthArr[i] + '_selectbox').parent().parent().css('padding-bottom', '16px');
		}
	}
}

function validBirth() {

	var birthYear = $('#hdndateBirthYear').val();
	var birthMonth = $('#hdndateBirthMonth').val();
	var birthDate = $('#hdndateBirthDate').val();
	var isValid = (isEmpty(birthYear) && isEmpty(birthMonth) && isEmpty(birthDate)) || (!isEmpty(birthYear) && !isEmpty(birthMonth) && !isEmpty(birthDate));
	if (isValid == false) {

		validateBirth();
	}

	return isValid;
}

function fancifySelectBoxes() {

	var dropdownArr = [
		'accountGender',
		'dateBirthYear',
		'dateBirthMonth',
		'dateBirthDate',
		'country',
		'currency',
		'countryCompany',
		'idType'
	];
	var i ;
	for (i = 0; i < dropdownArr.length; i++) {

		$('select#' + dropdownArr[i]).tzSelect({
			selectboxClass: dropdownArr[i] + '_selectbox',
			dropdownClass: dropdownArr[i] + '_dropdown',
			onChange: function() {

				var text = $(this).text();
				var id = $(this).parent().attr('class').replace(/_dropdown*/i, '').replace(/dropdown*/i, '').replace(/\s+/i, '');
				$('select#' + id + ' option').each(function() {

					if ($(this).text() == text) {

						$('input#hdn' + id).val($(this).val());
						$('#hdn' + id).trigger('change');
						return false;
					}
				});

			}
		});

		$('select#' + dropdownArr[i] + ' option').each(function() {

			var text = $('div.' + dropdownArr[i] + '_selectbox > span.text').text();
			if ($(this).text() == text) {

				$('input#hdn' + dropdownArr[i]).val($(this).val());
				return false;
			}
		});
	}
}

$('html')
	.on('click', function(e) {

		// console.log($(e.target)[0]);
		// console.log($('#txtAddress1')[0]);
		// console.log($(e.target)[0] == $('#txtAddress1')[0]);
		checkForCompleteness();
		checkRequiredFields(true);
	});

function checkForCompleteness() {

	$('span#chkName').css(
		'display',
		$('#' + requiredArr[0]).val().replace(/\s+/i, '').length > 0 && $('#' + requiredArr[1]).val().replace(/\s+/i, '').length > 0 ? 'inline-block' : 'none'
	);

	$('span#chkCountry').css(
		'display',
		$('#' + requiredArr[2]).val().replace(/\s+/i, '').length > 0 ? 'inline-block' : 'none'
	);

        $('span#chkAddress1').css(
		'display',
		$('#' + requiredArr[5]).val().replace(/\s+/i, '').length > 0 && $('#' + requiredArr[5]).val().replace(/\s+/i, '').length > 0 ? 'inline-block' : 'none'
	);

	$('span#chkCity').css(
		'display',
		$('#' + requiredArr[3]).val().replace(/\s+/i, '').length > 0 ? 'inline-block' : 'none'
	);

	$('span#chkEmail').css(
		'display',
		$('#' + requiredArr[4]).val().replace(/\s+/i, '').length > 0 ? 'inline-block' : 'none'
	);

	/*$('span#chkPass').css(
		'display',
		$('#' + requiredArr[5]).val().replace(/\s+/i, '').length > 0 ? 'inline-block' : 'none'
	);*/
}

function decorateErroredFields() {

	for (var i = 0; i < requiredArr.length; i++) {

		var parentObj = $('#' + requiredArr[i]).parent();
		var errorObj = $(parentObj).find('p.error');
		if (errorObj.length > 0) {

			var currReqObj = $('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden'
				? $('div.' + requiredArr[i].replace('hdn', '') + '_selectbox')
				: $('#' + requiredArr[i]);
			var immediateParentCntr = $(currReqObj).parent();
			var mainElementsCntr = $(currReqObj).parents('div.elements-cntr').first();

			$(currReqObj).css('border', '6px solid #BF883B').css('height', '34px');
			$(mainElementsCntr).css('padding-bottom', 0);
			if ($(immediateParentCntr).find('p.error').length == 0) {

				$(immediateParentCntr).append('<p class="error">Required</p>');
			}

			if ($('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden') {

				$(currReqObj).css('line-height', '34px');
			}
		}
	}
}

function checkRequiredFields(isOnFocus) {

	var success = true;
	var scrolled = false;
	for (var i = 0; i < requiredArr.length; i++) {
		if (typeof($('#' + requiredArr[i]).prop('tagName')) == 'undefined') {
			continue;
		}
		var extraValidation = true;
		var currReqObj = $('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden'
			? $('div.' + requiredArr[i].replace('hdn', '') + '_selectbox div')
			: $('#' + requiredArr[i]);
		var immediateParentCntr = $(currReqObj).parent();
		var mainElementsCntr = $(currReqObj).parents('div.elements-cntr').first();

		if(requiredArr[i] === "txtEmail") {
			var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			extraValidation = emailPattern.test($('#' + requiredArr[i]).val());
			if (!extraValidation) {
				$('span#chkEmail').css('display', 'none');
			}
		}
		if (requiredArr[i] == 'txtMiles') {

			var validMiles = validateMiles(isOnFocus);
			success = success ? validMiles : success;
		}

		if ($('#' + requiredArr[i]).val() == null || $('#' + requiredArr[i]).val().replace(/\s+/i, '').length == 0 || !extraValidation) {

			$(currReqObj).css('border', '6px solid #BF883B').css('height', '34px');
			$(mainElementsCntr).css('padding-bottom', 0);
			if ($(immediateParentCntr).find('p.error').length == 0) {

				$(immediateParentCntr).append('<p class="error">Required</p>');
			}

			if ($('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden') {

				$(currReqObj).css('line-height', '34px');
				$(currReqObj).find('span.icon').first().css('top', '14px');
			}

			if (scrolled == false && isOnFocus == false) {

				scrolled = true;
				scrollToElement($(currReqObj).parents('.elements-cntr').first().find('.element-label').first());
			}

			success = success ? false : success;

		}

		else {

			$(currReqObj).css('border', '0').css('height', '46px');
			$(immediateParentCntr).find('p.error').first().remove();
			$(mainElementsCntr).css('padding-bottom', '10px');
			if ($('#' + requiredArr[i]).prop('tagName').toLowerCase() == 'input' && typeof $('#' + requiredArr[i]).attr('type') !== 'undefined' && $('#' + requiredArr[i]).attr('type') == 'hidden') {

				$(currReqObj).css('line-height', '46px');
				$(currReqObj).find('span.icon').first().css('top', '22px');
			}
		}
	}

	return success;
}

function isEmpty(obj) {

    var blEmpty = true;
    if (typeof obj !== 'undefined' && obj != null) {

        if (typeof obj === 'string') {

            blEmpty = obj.replace(/\s+/ig, '').length == 0;
        }

        else if (Object.prototype.toString.call(obj) === '[object Array]') {

            blEmpty = obj.length == 0;
        }

        else {

            blEmpty = false;
        }
    }

    return blEmpty;
}

function scrollToElement(element) {

	try {
		$('html, body').animate({
			scrollTop: $(element).offset().top - 100
		}, 'fast');
	} catch (Error) { console.log(Error); }
}
