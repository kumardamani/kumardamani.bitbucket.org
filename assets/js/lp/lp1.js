$(document).ready(function(){
    lightboxOptions = {
        autoOpen: false,
        dialogClass: 'lightboxDialog',
        height: 'auto',
        modal: true,
        width: '430',
        resizable: false,
        open: function(event, ui) {
            $("body").css({overflow: 'hidden'});
        },
        beforeClose: function(event, ui) {
            $("body").css({overflow: 'inherit'});
        }
    };
    
    // Create lightboxes
    $('div#lightboxRegister').dialog(lightboxOptions);
    $('div#lightboxLogin').dialog(lightboxOptions);
    $('div#lightboxForgotPassword').dialog(lightboxOptions);
    
    // REGISTER
    $('div#lightboxRegister').on('click', 'a.submit', function() {

        var valid = true;
        var $emailAddress = $('input#registerEmailAddress');
		var $lpSource   =  $('input#lpsource');
                var $lppartner  =  $('input#lppartner');
        var emailAddress = $emailAddress.val();
		var source = $lpSource.val();
                var partner = $lppartner.val();
        var agreeWithTerms = $('input#agreeWithTerms').val();

        // if (emailAddress != $('input#registerEmailAddressConfirm').val() ) {
        //     valid = false;
            
        //     $emailAddressConfirm.parent().addClass('error');
        //     $emailAddressConfirm.parent().find('.errorMessage').text('Error: does not match');
        //     $emailAddressConfirm.parent().find('div.valid').hide();
        // } else {
        //     $emailAddressConfirm.parent().removeClass('error');
        //     $emailAddressConfirm.parent().find('div.valid').show();
        //     $emailAddressConfirm.parent().find('.errorMessage').text('');
        // }
        
        if (agreeWithTerms == 0) {
            valid = false;
            $('input#agreeWithTerms').parent().addClass('error');
            $('input#agreeWithTerms').parent().find('.errorMessage').text('Error: agree with legal terms');
            $('input#agreeWithTerms').parent().find('.checkbox').focus();
            //$('input#agreeWithTerms').parent().find('div.valid').hide();
        } else {
            $('input#agreeWithTerms').parent().removeClass('error');
            //$('input#agreeWithTerms').parent().find('div.valid').show();
            $('input#agreeWithTerms').parent().find('.errorMessage').text('');
        }
        var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if (emailPattern.test(emailAddress) == false) {
            valid = false;
            
            $emailAddress.parent().addClass('error');
            $emailAddress.parent().find('.errorMessage').text('Error: enter a valid e-mail address');
            $emailAddress.parent().find('div.valid').hide();
            $emailAddress.focus();
        } else {
            $emailAddress.parent().removeClass('error');
            //$emailAddress.parent().find('div.valid').show();
            $emailAddress.parent().find('.errorMessage').text('');
        }
        
        if (valid) {
            $.ajax({
                type: "POST",
                url: "/user/ajax/registerlp",
                data: {emailAddress: emailAddress, source: source, partner: partner}
            }).done(function(msg) {
                if (typeof msg.success != 'undefined') {
                    $emailAddress.parent().find('div.valid').show().delay(1000).queue(function(){
                        $('div#lightboxRegisterForm').hide();
                        $('div#lightboxRegisterSuccess').show();
                    });
                } else {
                    $('input#registerEmailAddress').parent().addClass('error');
                    $('input#registerEmailAddress').parent().find('.errorMessage').text("Error: The email address is in use");
                    $('input#registerEmailAddress').parent().find('div.valid').hide();
                }
            });
        }
    });

    $('.callLogin').on('click', function() {
        var openTheLightbox = function() {
            $('div#lightboxRegister').dialog('close');
            $('div#lightboxForgotPassword').dialog('close');
            $('div#lightboxLogin').dialog('open');
            $("div#lightboxLogin #loginEmailAddress").focus();

            try {
                $("#lightboxRegisterAuto").unbind("dialogclose");
                $('div#lightboxRegisterAuto').dialog('close');
            } catch(e) { }
        }

        openTheLightbox();
    });

    $('.callForgotPassword').on('click', function() {
        var openTheLightbox = function() {
            $('div#lightboxLogin').dialog('close');
            $('div#lightboxRegister').dialog('close');
            $('div#lightboxForgotPassword').dialog('open');
            $('div#lightboxForgotPassword #forgottenpasswordEmailAddress').focus();
        }

        openTheLightbox();
    });
    
    $('div.lightbox').on('click', 'a.close, p.cancel a, a.gobtn', function() {
        $(this).closest('div.lightbox').dialog('close');
    });

    $('div#join').bind("keyup keypress", function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                $('div#join').click();
            }
    });

    $('div#join').on('click', function() {
	    $.ajax({
			type: "POST",
			url: "/user/ajax/check-email",
			data: {emailAddress: $('#email').val(), source:  $('input#lpsource').val()}
        }).done(function(msg) {
			if (typeof msg.success != 'undefined') {
			    $('#emailform .error').hide();
				$('div#lightboxRegister').dialog('open');
                $('#registerEmailAddress').val($('#email').val());
			} else {
			    if (typeof msg.invalid1 != 'undefined') {
				    $('#emailform .error').text('Error: Email address is in use');
				} else if (typeof msg.invalid2 != 'undefined') {
				    $('#emailform .error').text('Error: Email address is invalid');
				}
				
				$('#emailform .error').show();
			}
        });
        
    });

	$('div.lightbox .lightboxCheckboxWrapper').on('click', 'label', function(ev) {
        ev.stopPropagation();
        var $checkbox = $(this).find('.checkbox');
        var $input = $(this).parent().find('input');
        
        if ($checkbox.hasClass('checked')) {
            $input.val(0);
        } else {
            $input.val(1);
        }
        
        $checkbox.toggleClass('checked');
	});


});