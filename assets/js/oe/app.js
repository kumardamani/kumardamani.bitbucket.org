var App = {
    isTouchDevice : 'ontouchstart' in window || navigator.msMaxTouchPoints,
    // ua: navigator.userAgent,
    // browser: /Edge\/\d+/.test(ua) ? 'ed' : /MSIE 9/.test(ua) ? 'ie9' : /MSIE 10/.test(ua) ? 'ie10' : /MSIE 11/.test(ua) ? 'ie11' : /MSIE\s\d/.test(ua) ? 'ie?' : /rv\:11/.test(ua) ? 'ie11' : /Firefox\W\d/.test(ua) ? 'ff' : /Chrome\W\d/.test(ua) ? 'gc' : /Chromium\W\d/.test(ua) ? 'oc' : /\bSafari\W\d/.test(ua) ? 'sa' : /\bOpera\W\d/.test(ua) ? 'op' : /\bOPR\W\d/i.test(ua) ? 'op' : typeof MSPointerEvent !== 'undefined' ? 'ie?' : '',
    // os: /Windows NT 10/.test(ua) ? "win10" : /Windows NT 6\.0/.test(ua) ? "winvista" : /Windows NT 6\.1/.test(ua) ? "win7" : /Windows NT 6\.\d/.test(ua) ? "win8" : /Windows NT 5\.1/.test(ua) ? "winxp" : /Windows NT [1-5]\./.test(ua) ? "winnt" : /Mac/.test(ua) ? "mac" : /Linux/.test(ua) ? "linux" : /X11/.test(ua) ? "nix" : "",
    // mobile: /IEMobile|Windows Phone|Lumia/i.test(ua) ? 'w' : /iPhone|iP[oa]d/.test(ua) ? 'i' : /Android/.test(ua) ? 'a' : /BlackBerry|PlayBook|BB10/.test(ua) ? 'b' : /Mobile Safari/.test(ua) ? 's' : /webOS|Mobile|Tablet|Opera Mini|\bCrMo\/|Opera Mobi/i.test(ua) ? 1 : 0,
    // tablet: /Tablet|iPad/i.test(ua),
    // touch: 'ontouchstart' in document.documentElement,
	init : function() {
        // TODO Refactor 
        $('body').on('click','#at3lb, #at3winheaderclose',function(ev) {
            ev.preventDefault();
            $('#at3lb').hide();
            $('#at3win').hide();
        });

        //video.setup();
        home.init();
        story.init();
        ddlLists.init();
        stickyNav.init();
        teasers.init();
        videoPoster.init();
        addThis.init();
        pinterestHover.init();
	}
};

var videoPoster = {
    init: function() {
        $('video').each(function() {
            if (App.isTouchDevice != true) {
                $('video').removeAttr('poster');
                $('div').remove('div[data-pinterest-android]');
            }
        })
    }
}

var ddlLists = {
    init: function() {
        $(window).bind('resize', function() {
            if ($(window).width() <= 767) {
                $('.hotels-list > li > h3, footer > div > div:first-child > div h3').unbind('click').click(function() {
                    $(this).toggleClass('close').parent().find('ul').slideToggle('fast');
                });
            } else {
                $('.hotels-list > li > h3, footer > div > div:first-child > div h3').unbind('click');
            }
        }).resize();
    }
}

var stickyNav = {
    nav: $('body > header'),
    init: function() {
        $(window).bind('scroll', function() {
            if ($(window).scrollTop() > 0) {
                $(stickyNav.nav).addClass('is_stuck');
            } else {
                $(stickyNav.nav).removeClass('is_stuck');
            }
        });
        $(window).bind('resize', function() {
            if ($(window).width() <= 767) {
                $(stickyNav.nav).removeClass('is_stuck');
            }
        }).resize();
    }
}

var home = {

    mapElement: document.getElementById('map'),

    init: function() {
        home.video();
        home.loadMap();
        home.createEvents();
    },

    video: function() {
            
        //homepage video
        var htmlVid = document.getElementById('video-home');

        //switch video/map
        $('#intro > div:last-child > div > ul:first-child li a').click(function() {
            var $li = $(this).parent();
            if (!$li.hasClass('selected')) {
                $('#intro > section').fadeToggle('fast');
                $li.parent().find('li').toggleClass('selected');
                //show map
                if ($li.attr('id') == 'map-toggle') {
                    //pause and hide play/pause btn (show map)
                    htmlVid.play();
                    $('#video-toggle').data('status', 'pause').addClass('pause');
                    $('#video-toggle, #volume-toggle').hide();
                } else {
                    $('#video-toggle').show();
                    if (App.isTouchDevice!=true) {
                        $('#volume-toggle').show();
                    }
                }
            }
        });

        //toggle play/pause
        $('#video-toggle').click(function() {
            var $el = $(this);
            if ($el.data('status') == 'play') {
                htmlVid.pause();
                $el.data('status', 'pause');
            } else {
                htmlVid.play();
                $el.data('status', 'play');
                $('#play-again:visible, #explore-stories:visible').fadeOut('fast');
            }
            $el.toggleClass('pause');
        });

        //toggle volume on/off
        $('#volume-toggle').click(function() {
            var $el = $(this);
            if (!$el.hasClass('mute')) {
                htmlVid.muted = true;
                $el.addClass('mute');
            } else {
                htmlVid.muted = false;
                $el.removeClass('mute');
            }
        });
        if ($('#video-home').length > 0) {
            //Mute player on load
            htmlVid.muted = true;
            $('#volume-toggle').addClass('mute');
            htmlVid.onended = function() {
                $('#video-toggle').data('status', 'pause').addClass('pause');
                $('#play-again, #explore-stories').fadeIn('fast');
            };
        }

        //play again
        $('#play-again h6:last-child a').click(function() {
            htmlVid.play();
            $('#video-toggle').data('status', 'play').removeClass('pause');
            $('#play-again, #explore-stories').fadeOut('fast');
        });

        //touch devices
        if ((App.isTouchDevice==true) && ($('#video-home').length > 0)) {
            //autoplay disabled
            htmlVid.pause();
            $('#video-toggle').data('status', 'pause').addClass('pause');
            $('#volume-toggle').hide();

            //Done click on iOS
            htmlVid.addEventListener('webkitendfullscreen', function() {
                $('#video-toggle').data('status', 'pause').addClass('pause');
            }, false);
        }

    },

    loadMap: function() {
        if (home.mapElement !== null) {
            google.maps.event.addDomListener(window, 'load', home.createMap);
        }
        if($("li#map-toggle").hasClass("selected")) {
            $("li#map-toggle").removeClass("selected")
            $('#map-toggle a').click();
        }
    },
    createMap: function() {
        // Get the HTML DOM element that will contain your map 
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 3,
            minZoom: 2,
            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(46.1654637, 8.9931593), // Positioner SA
            // How you would like to style the map. 
            styles: [
                        {
                            "featureType": "administrative",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                },
                                {
                                    "color": "#b8c2be"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape.natural",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                },
                                {
                                    "color": "#939f9a"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        }
                    ],

            //disabling/enabling UI features
            disableDefaultUI: true,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            }
        };
        // Create the Google Map using our element and options defined above
        var storymap = new google.maps.Map(home.mapElement, mapOptions);
        //trigger the click to show the map pnl
        $('#map-toggle a').click(function() {
            var center = storymap.getCenter();
            google.maps.event.trigger(storymap, 'resize');
            storymap.setCenter(center);
        })

        
        var markers = [];
        $.each(storyMarkers, function(i, p) {
            if (p.lat != null && p.lng != null) {
                var thisLatLong = new google.maps.LatLng(parseFloat(p.lat).toFixed(6), parseFloat(p.lng).toFixed(6));
                var marker = new google.maps.Marker({
                        position: thisLatLong,
                        title: p.name,
                        icon: 'assets/images/oe/icons/icon-marker.png'
                });

                var boxText = document.createElement("div");
                boxText.className = "info-box";
                boxText.style.cssText = "background-image: url(" + p.image + ")";
                boxText.innerHTML = "" +
                "<a href='" + p.url + "'>" +
                "<div><p>" + p.name +
                "</p><h6>" + p.hotelName + " " + p.city + ", " + p.country +
                "</h6></div></a>";

                var boxOptions = {
                    content: boxText
                    ,pixelOffset: new google.maps.Size(-125, -90) // (1/2 -70 H marker + 1/2 H -20 info-box)
                    ,closeBoxURL: "assets/images/oe/icons/icon-close-tooltip.png"
                    ,closeBoxMargin: "0px"
                };

                var ib = new InfoBox(boxOptions);
                marker.addListener('mouseover', function() {
                    ib.open(storymap, marker);
                });

                google.maps.event.addListener(ib, 'domready', function () {
                    $(".info-box").bind("mouseleave", function () {
                        ib.close()
                    });
                });
                markers.push(marker);
            }
        });
        var markerCluster = new MarkerClusterer(storymap, markers, {
            'averageCenter':    true,
            'clusterClass':     'marker-cluster',
            'gridSize':         20
        });
        //end
    },
    createEvents: function() {
        // original experience page
        $('#explore-stories').on('click tap', function(ev){
            ev.stopPropagation();
            ev.preventDefault();
                
            if (ev.hasOwnProperty('gesture')) {
                ev.gesture.stopPropagation();
                ev.gesture.preventDefault();
            }
            
            $('html, body').animate({scrollTop: $('#intro + section').offset().top}, 750, 'easeInOutExpo');
        });
        $('#wc_godown').on('click tap', function(ev){
            ev.stopPropagation();
            ev.preventDefault();
                
            if (ev.hasOwnProperty('gesture')) {
                ev.gesture.stopPropagation();
                ev.gesture.preventDefault();
            }
            
            $('html, body').animate({scrollTop: $('#intro + section').offset().top}, 750, 'easeInOutExpo');
        });
        // story page
        
    }
}

var teasers = {
    init: function() {
        $('#stories video').each(function() {
            var $jVid = $(this);
            var storyVid = document.getElementById($jVid.attr('id'));
            
            $jVid.closest('a').on('mouseleave', function() {
                if ($(window).width() > 767){
                    if($jVid.hasClass("autoplay")) {
                        storyVid.play();
                    } else {
                        storyVid.pause();
                    }
                }
            })
            $jVid.closest('a').on('mouseenter', function() {
                if ($(window).width() > 767){
                    if($jVid.hasClass("autoplay")) {
                        storyVid.pause();
                    } else {
                        storyVid.play();
                    }
                }
            })
        });
    }
}

var story = {
    init: function() {
        story.setup();
        story.fullscreen();
    },
    setup: function () {
        $('body#story > section:not(#cover)').viewportChecker({
            classToAdd: 'fadeInUp',
            callbackFunction: function(elem, action) {
                var $el = $(elem).find('.flexslider:not(.carousel)');
                if ($el.length != 0) {
                    story.slideshow($el);
                }
            }
        });
        story.carousel();
    },

    fullscreen: function() {

        var video = new $.BigVideo({
            useFlashForFirefox:     false,
            container:              $('#video-wrapper'),
            controls:               $('#video-wrapper').data('controls'),
            doLoop:                 $('#video-wrapper').data('loop'),
            autoplay:               $('#video-wrapper').data('autoplay')
        });
           
        var $el = $('#video-wrapper');

	
	
        if ($el.length != 0) {

            //close
            $('#fullscreen-video a.close').on("click", function() {
                $('body').removeClass('overflow-hidden');
                if ($('#big-video-vid').hasClass('vjs-playing')) {
                    $('#big-video-control-play').click();
                }
                $('#fullscreen-video').fadeOut('fast', function(){
                    var htmlVid = document.getElementById('video-story');
                    htmlVid.play();
                });
            });

            //open
            $('#cover a.open').on("click", function() {

                //homepage video
                var htmlVid = document.getElementById('video-story');

                video.init();

                if ((App.isTouchDevice == true) && ($(window).width() < 768) && (browser != 'gc')) {
                    alert($el.data('video'));
                    // small, play directly
                    // video.show($el.data('video'));
					video.show("https://player.vimeo.com/external/209882815.hd.mp4?s=9102a45a8e6bc4eaec222433109a0bd0c95c57bc&profile_id=119");
                    video.getPlayer().play();
                    
                } else {
                    
                    $('body').addClass('overflow-hidden');
                    htmlVid.pause();
                    alert($el.data('video'));
                    $('#fullscreen-video').fadeIn('fast', function() {
                        if (!$('#big-video-control-play').hasClass('pause')) {

                            if (App.isTouchDevice == true) {
                                $('#big-video-vid video').attr('poster', $('#fullscreen-video #video-wrapper').data('poster'));
                            }
                            video.show($el.data('video'));
                            video.getPlayer().play();
                            
                            if (App.isTouchDevice == true) { // touch with viewport > small
                                //reset isPlaying var in bigVideo
                                // $('#big-video-control-play')[0].trigger('click');
								$('#big-video-control-play').trigger('mousedown');
                                $('#big-video-control-volume').remove();
                                $('#big-video-control-middle').css('margin-left', $('#big-video-control-play').width() + 10);

                            }

                            //trick to show video from previous hidden element
                            $(window).trigger('resize');

                        }
                    });
                }
                
            });
        }
    },

    slideshow: function ($el) {
        var size = $el.find('ul.slides > li').size();
        $el.flexslider({
            animation: (App.isTouchDevice ? "slide" : "fade"),
            slideshowSpeed: 4000,
            touch: size > 1,
            slideshow: 1,
            start: function(slider) {
                $('div.flexslider > ul.slides li').click(function (event) {
                    event.preventDefault();
                    slider.flexAnimate(slider.getTarget("next"));
                });
            }
        });
    },

    carousel: function () {
        var $el = $('.flexslider.carousel');
        if ($el.length > 0) {
            var size = $el.find('ul.slides > li').size();
            $el.flexslider({
                animation: "slide",
                itemWidth: 1,
                slideshow: 0,
                move: 1,
                touch: size > 1,
                minItems: story.getGridSize(),
                maxItems: story.getGridSize()
            });
            story.onResize();
        }
    },

    getGridSize: function() {
        //square ratio
        $('.flexslider.carousel .slides > li').each(function() {
            var $li = $(this);
            $li.height($li.width());
        })
        return (window.innerWidth < 600) ? 1 : (window.innerWidth < 900) ? 2 : 3;
    },

    onResize: function() {
        $(window).bind('resize', function() {
            var $el = $('.flexslider.carousel');
            var gridSize = story.getGridSize();
            $el.data('flexslider').vars.minItems = gridSize;
            $el.data('flexslider').vars.maxItems = gridSize;
        });
    }
}

var addThis = {
    init: function() {
        $('a.share').on('click', function(ev) {
            ev.stopPropagation();
            ev.preventDefault();
            if ($(this).hasClass('addthis_custom_active')) {
                $(this).removeClass('addthis_custom_active');
                if (!App.isTouchDevice) {
                    $('.addthis_custom').css('display', 'none');
                    $(this).one('mouseout', function() {
                        $('.addthis_custom').css('display', '');
                    });
                } else {
                    $('.addthis_custom').css('display', 'none');
                }
            } else {
                $(this).addClass('addthis_custom_active');
                $(this).next('.addthis_custom').css('display', 'block');
            }
        });
        $('body').on('click touchstart', function(ev) {
            if (!$(ev.target).parent().parent().hasClass('share-wrapper')) {
                if (!App.isTouchDevice) {
                    $('.addthis_custom').css('display', '');
                } else {
                    $('.addthis_custom').css('display', 'none');
                }
                $('a.share').removeClass('addthis_custom_active');
            }
        });
        $('#fullscreen-video .share, #fullscreen-video .share .addthis_custom').on('mouseenter', function(ev) {
            $('.info-box').css('display', 'none');
        });
        $('#fullscreen-video .addthis_custom').on('mouseleave', function(ev) {
            $('.info-box').css('display', '');
        });
        $('.play-again-share').on('click touchstart', function(ev) {
            ev.stopPropagation();
            ev.preventDefault();
            $('.addthis_play_again_custom').addClass('active');
        });
        $('.addthis_play_again_custom .close').on('click touchstart', function(ev) {
            ev.stopPropagation();
            ev.preventDefault();
            $('.addthis_play_again_custom').removeClass('active');
        });
    }
}

var pinterestHover = {
    init: function() {
        setTimeout(function() {
            var storyPage = $('body').attr('id');

            if (storyPage == 'story') {
                $('header:not(#searchEngineMobile)').addClass('pinterest-save--hover');

                var maxOffsetHeight = 144;
                var pinterestOffsetHeight = 0;
                if ($('.section-7__part-2 figure img').attr('src').trim().length > 0) {
                    var textHeight = 0;
                    $('.section-7__part-2 p').each(function() {
                        if ($(this).html().length > 0) {
                            textHeight += $(this).outerHeight();
                        }
                    });

                    if (textHeight == 0) {
                        // Add extra height offset of 60 when there's no text
                        pinterestOffsetHeight = textHeight + 60;
                    } else {
                        // Add extra height offset of 74
                        pinterestOffsetHeight = textHeight + 74;
                    }
                    if (pinterestOffsetHeight >= maxOffsetHeight) {
                        pinterestOffsetHeight = maxOffsetHeight;
                    }
                    $('.section-8__image-10 .pinterest-save').css('top', pinterestOffsetHeight + 'px');
                }
            }
        }, 500);
    }
}

var
    ua = navigator.userAgent,
    browser = /Edge\/\d+/.test(ua) ? 'ed' : /MSIE 9/.test(ua) ? 'ie9' : /MSIE 10/.test(ua) ? 'ie10' : /MSIE 11/.test(ua) ? 'ie11' : /MSIE\s\d/.test(ua) ? 'ie?' : /rv\:11/.test(ua) ? 'ie11' : /Firefox\W\d/.test(ua) ? 'ff' : /Chrome\W\d/.test(ua) ? 'gc' : /Chromium\W\d/.test(ua) ? 'oc' : /\bSafari\W\d/.test(ua) ? 'sa' : /\bOpera\W\d/.test(ua) ? 'op' : /\bOPR\W\d/i.test(ua) ? 'op' : typeof MSPointerEvent !== 'undefined' ? 'ie?' : '',
    os = /Windows NT 10/.test(ua) ? "win10" : /Windows NT 6\.0/.test(ua) ? "winvista" : /Windows NT 6\.1/.test(ua) ? "win7" : /Windows NT 6\.\d/.test(ua) ? "win8" : /Windows NT 5\.1/.test(ua) ? "winxp" : /Windows NT [1-5]\./.test(ua) ? "winnt" : /Mac/.test(ua) ? "mac" : /Linux/.test(ua) ? "linux" : /X11/.test(ua) ? "nix" : "",
    mobile = /IEMobile|Windows Phone|Lumia/i.test(ua) ? 'w' : /iPhone|iP[oa]d/.test(ua) ? 'i' : /Android/.test(ua) ? 'a' : /BlackBerry|PlayBook|BB10/.test(ua) ? 'b' : /Mobile Safari/.test(ua) ? 's' : /webOS|Mobile|Tablet|Opera Mini|\bCrMo\/|Opera Mobi/i.test(ua) ? 1 : 0,
    tablet = /Tablet|iPad/i.test(ua),
    touch = 'ontouchstart' in document.documentElement;

App.init();